﻿$(document).ready(function () {
    IssueType();
    Auto_IssueTo();
    Auto_Denomination();
    Auto_Series();
    //Auto_TicketFrom();
    //Auto_TicketTo();
    Auto_ReferanceNo();
    Auto_IssueNoforSearch();
    IssueDate();


    $('#btnIssueAdd').click(function () {
        Add_Issue_for();
    });

    $('#btnSave').click(function () {
        Save();
    });

    $('#btnRefresh').click(function () {
        Blank();
    });

    $('#btnEdit').click(function () {
        Grid();
        //$('#dialog').modal('show');
    });

    $('#txtDenomination').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtDenomination").val('');
            $("#hdnDenominationID").val('');
            $("#txtTicketSeries").val('');
            $("#hdnTicketSeries").val('');
            $("#txtTicketFrom").val('');
            $("#txtTicketTo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtDenomination").val('');
            $("#hdnDenominationID").val('');
            $("#txtTicketSeries").val('');
            $("#hdnTicketSeries").val('');
            $("#txtTicketFrom").val('');
            $("#txtTicketTo").val('');
        }
    });
    $('#txtTicketSeries').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtTicketSeries").val('');
            $("#hdnTicketSeries").val('');
            $("#txtTicketFrom").val('');
            $("#txtTicketTo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtTicketSeries").val('');
            $("#hdnTicketSeries").val('');
            $("#txtTicketFrom").val('');
            $("#txtTicketTo").val('');
        }
    });
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});

function IssueDate() {
    $('#txtIssueDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtIssueDate').val(output);
}

function IssueType()
{
    var S = "{TransType: '" + "IssueType" + "'}";

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTktIssue/IssueType',
        contentType: "application/json; charset=utf-8",
        data: S,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('#ddlIssueType').empty();
            $('#ddlIssueType').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('#ddlIssueType').append('<option value=' + item.ID + '>' + item.Description + '</option>');
                });
            }
        }
    });
}

function Auto_IssueTo() {

    $('#txtIssueTo').autocomplete({
        source: function (request, response) {

            var Desc = '';
            if ($('#ddlIssueType').val() == 'B' || $('#ddlIssueType').val() == 'C')
            {
                var a = $('#txtIssueTo').val();
                var b = a.split('-');
                Desc = $.trim(b[0]);
            }
            else {
                Desc = $('#txtIssueTo').val();
            }
            
            var S = "{TransType: '" + "IssueTo" + "', Desc:'" + Desc + "', IssueTo:'" + $('#hdnIssueTo').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "'}";
            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_IssueTo',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.Text,
                                ID: item.ID
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnIssueTo').val(i.item.ID);
        },
        minLength: 0
    }).click(function () {
        if ($('#ddlIssueType').val() == "") { $('#ddlIssueType').focus(); return false; }
        $(this).autocomplete('search', ($(this).val()));
    });
}

function Auto_Denomination() {

    $('#txtDenomination').autocomplete({
        source: function (request, response) {
            var TransDate = "";
            if ($('#txtIssueDate').val() != "") {
                var a = $('#txtIssueDate').val().split('/');
                TransDate = a[2] + '-' + a[1] + '-' + a[0];
            }
            var S = "{TransType: '" + "Denomination" + "', TransDt:'" + TransDate + "', DenominationId:'" + $('#hdnDenominationID').val() + "', " +
                    "TktSeries:'" + $('#txtTicketSeries').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "', " + 
                "IssuedToSectorIdEmpId:'" + $('#hdnIssueTo').val() + "', TktRequisitionWaybillId: '" + $('#hdnReferanceNo').val() + "', " + 
                "TktSeriesFrom: '" + $('#txtTicketFrom').val() + "', TktSeriesTo: '" + $('#txtTicketTo').val() + "', UPDN: '" + $('#ddlUpDn').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_Ticket',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.DenominationRemarks,
                                DenominationId: item.DenominationId
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnDenominationID').val(i.item.DenominationId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}
function Auto_Series() {

    $('#txtTicketSeries').autocomplete({
        source: function (request, response) {
            var TransDate = "";
            if ($('#txtIssueDate').val() != "") {
                var a = $('#txtIssueDate').val().split('/');
                TransDate = a[2] + '-' + a[1] + '-' + a[0];
            }

            var S = "{TransType: '" + "Series" + "', TransDt:'" + TransDate + "', DenominationId:'" + $('#hdnDenominationID').val() + "', " +
                "TktSeries:'" + $('#txtTicketSeries').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "', " +
                "IssuedToSectorIdEmpId:'" + $('#hdnIssueTo').val() + "', TktRequisitionWaybillId: '" + $('#hdnReferanceNo').val() + "', " +
                "TktSeriesFrom: '" + $('#txtTicketFrom').val() + "', TktSeriesTo: '" + $('#txtTicketTo').val() + "', UPDN: '" + $('#ddlUpDn').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_Ticket',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.TktSeries,
                                TktSeries: item.TktSeries
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnTicketSeries').val(i.item.TktSeries);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}
function Auto_TicketFrom() {

    $('#txtTicketFrom').autocomplete({
        source: function (request, response) {
            var TransDate = "";

            if ($('#txtIssueDate').val() != "")
            {
                var a = $('#txtIssueDate').val().split('/');
                TransDate = a[2] + '-' + a[1] + '-' + a[0];
            }

            var S = "{TransType: '" + "Ticket" + "', TransDt:'" + TransDate + "', DenominationId:'" + $('#hdnDenominationID').val() + "', " +
                "TktSeries:'" + $('#txtTicketSeries').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "', " +
                "IssuedToSectorIdEmpId:'" + $('#hdnIssueTo').val() + "', TktRequisitionWaybillId: '" + $('#hdnReferanceNo').val() + "'," +
                "TktSeriesFrom: '" + $('#txtTicketFrom').val() + "', TktSeriesTo: '" + $('#txtTicketTo').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_Ticket',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.TktSrlIdFrom,
                                TktSrlIdFrom: item.TktSrlIdFrom
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            //$('#hdnDenominationID').val(i.item.DenominationId);
        },
        minLength: 0
    }).click(function () {
        if ($('#hdnTicketSeries').val() == "") { $('#txtTicketSeries').focus(); return false; }
        $(this).autocomplete('search', ($(this).val()));
    });
}
function Auto_TicketTo() {

    $('#txtTicketTo').autocomplete({
        source: function (request, response) {
            var TransDate = "";

            if ($('#txtIssueDate').val() != "") {
                var a = $('#txtIssueDate').val().split('/');
                TransDate = a[2] + '-' + a[1] + '-' + a[0];
            }

            var S = "{TransType: '" + "Ticket" + "', TransDt:'" + TransDate + "', DenominationId:'" + $('#hdnDenominationID').val() + "', " +
                "TktSeries:'" + $('#txtTicketSeries').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "', " +
                "IssuedToSectorIdEmpId:'" + $('#hdnIssueTo').val() + "', TktRequisitionWaybillId: '" + $('#hdnReferanceNo').val() + "'," +
                "TktSeriesFrom: '" + $('#txtTicketFrom').val() + "', TktSeriesTo: '" + $('#txtTicketTo').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_Ticket',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.TktSrlIdTo,
                                TktSrlIdTo: item.TktSrlIdTo
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            //$('#hdnDenominationID').val(i.item.DenominationId);
        },
        minLength: 0
    }).click(function () {
        if ($('#hdnTicketSeries').val() == "") { $('#txtTicketSeries').focus(); return false; }
        $(this).autocomplete('search', ($(this).val()));
    });
}
function Auto_IssueNoforSearch() {

    $('#txtSearchIssueNo').autocomplete({
        source: function (request, response) {
          
            var S = "{TransType: '" + "IssueNo" + "', Desc:'" + $('#txtSearchIssueNo').val() + "', IssueTo:'" + $('#hdnIssueTo').val() + "', IssueType:'" + "" + "', SectorID:'" + $('#ddlSector').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_IssueTo',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.TktIssueNo,
                                MstTktIssueId: item.MstTktIssueId
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnIssueID").val(i.item.MstTktIssueId);
            Bind(i.item.MstTktIssueId);
          
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}
function Auto_ReferanceNo() {
    if ($('#ddlIssueType').val() != 'D') {
    
    $('#txtReferanceNo').autocomplete({
        source: function (request, response) {

            var S = "{TransType: '" + "ReferanceNo" + "', Desc:'" + $('#txtReferanceNo').val() + "', IssueTo:'" + $('#hdnIssueTo').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/PaperTktIssue/Get_IssueTo',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data;
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.Text,
                                ID: item.ID
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnReferanceNo').val(i.item.ID);
        },
        minLength: 0
    }).click(function () {
        if ($('#ddlIssueType').val() == "") { $('#ddlIssueType').focus(); return false; }
        $(this).autocomplete('search', ($(this).val()));
    });
}
}

function BlankIssueTo()
{
    if ($('#ddlIssueType').val() == 'F')
    {
        $('#txtIssueTo').prop('disabled', true); $('#txtIssueTo').val(''); $('#hdnIssueTo').val('');
        $('#txtReferanceNo').prop('disabled', true); $('#txtReferanceNo').val(''); $('#hdnReferanceNo').val('');
    }
    else
    {
        $('#txtIssueTo').val(''); $('#hdnIssueTo').val('');
        $('#txtReferanceNo').val(''); $('#hdnReferanceNo').val('');

        $('#txtIssueTo').prop('disabled', false); $('#txtReferanceNo').prop('disabled', false); 
    }
}

function Add_Issue() {
   
    var chkAccCode = $("#hdnDenominationID").val();
    var chkTicketSeries = $("#txtTicketSeries").val();

    var l1 = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;
    var l2 = $('#tbl tr.myData').find("td[chk-series='" + chkTicketSeries + "']").length;

    if (l1 > 0 && l2 >0) {
        alert('Sorry ! This Denomination & Series is Already Available.');
        $("#txtDenomination").val(''); $("#hdnDenominationID").val(''); $("#txtTicketSeries").val(''); $("#txtTicketFrom").val(''); $("#txtTicketTo").val('');
        return false;
    }

    var html = ""

    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnDenominationID").val() + "' >" + $("#hdnDenominationID").val() + "</td>"
        + "<td class='denomination'>" + $("#txtDenomination").val() + "</td>"
        + "<td class='updn' style='text-align:center'>" + $("#ddlUpDn option:selected").text() + "</td>"
        + "<td class='updnid' style='display:none;'>" + $("#ddlUpDn").val() + "</td>"
        + "<td class='TicketSeries' chk-series='" + $("#txtTicketSeries").val() + "'>" + $("#txtTicketSeries").val() + "</td>"
        + "<td class='TicketFrom'>" + $("#txtTicketFrom").val() + "</td>"
        + "<td class='TicketTo'>" + $("#txtTicketTo").val() + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_Row(this, 1);' />"
        + "<img src='/Content/Images/edit.png' title='Edit' style='width:22px;height:22px;cursor:pointer; text-align:center;' onClick='Edit_Row(this);' /></td >"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtDenomination").val(''); $("#hdnDenominationID").val(''); $("#txtTicketSeries").val(''); $("#txtTicketFrom").val(''); $("#txtTicketTo").val('');
}
function Add_Issue_for()
{
    if ($("#hdnDenominationID").val() == "") { $("#txtDenomination").focus(); return false; }
    if ($("#ddlUpDn").val() == "") { $("#ddlUpDn").focus(); return false; }
    if ($("#txtTicketSeries").val() == "") { $("#txtTicketSeries").focus(); return false; }
    if ($("#txtTicketFrom").val() == "") { $("#txtTicketFrom").focus(); return false; }
    if ($("#txtTicketTo").val() == "") { $("#txtTicketTo").focus(); return false; }

    var TransDate = "";
    if ($('#txtIssueDate').val() != "") {
        var a = $('#txtIssueDate').val().split('/');
        TransDate = a[2] + '-' + a[1] + '-' + a[0];
    }

    var S = "{TransType: '" + "AddDtls" + "', TransDt:'" + TransDate + "', DenominationId:'" + $('#hdnDenominationID').val() + "', " +
        "TktSeries:'" + $('#txtTicketSeries').val() + "', IssueType:'" + $('#ddlIssueType').val() + "', SectorID:'" + $('#ddlSector').val() + "', " +
        "IssuedToSectorIdEmpId:'" + $('#hdnIssueTo').val() + "', TktRequisitionWaybillId: '" + $('#hdnReferanceNo').val() + "'," +
        "TktSeriesFrom: '" + $('#txtTicketFrom').val() + "', TktSeriesTo: '" + $('#txtTicketTo').val() + "', UPDN: '" + $('#ddlUpDn').val() + "'} ";
    //alert(S);
    //return false;
    $.ajax({
        url: '/Accounts_Form/PaperTktIssue/Get_Ticket',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: S,
        dataType: 'json',
        success: function (data) {
            var a = data.Data; //alert(JSON.stringify(a));
            var TranStatus = a[0].TranStatus; //alert(TranStatus);
            var Mesg = a[0].Mess; //alert(Mesg);

            if (TranStatus == 1)
            {
                Add_Issue();
            }
            else
            {
                alert(Mesg); return false;
            }
        }
    });
}

function Delete_Row(ID, CON) {
    if(CON == 1)
        $(ID).closest('tr').remove();
    else
    {
        var IssueID = $(ID).closest('tr').find('.tktissueid').text();
        Delete(ID, IssueID, CON);
    }
}

function Edit_Row(ID)
{
    var Denomination = $(ID).closest('tr').find('.denomination').text();
    var DenominationID = $(ID).closest('tr').find('.clsAccCode').text();
    var UPDN = $(ID).closest('tr').find('.updnid').text();
    var TicketSeries = $(ID).closest('tr').find('.TicketSeries').text();
    var txtTicketFrom = $(ID).closest('tr').find('.TicketFrom').text();
    var TicketTo = $(ID).closest('tr').find('.TicketTo').text();

    $("#txtDenomination").val(Denomination); $("#hdnDenominationID").val(DenominationID); $("#txtTicketSeries").val(TicketSeries);
    $("#txtTicketFrom").val(txtTicketFrom); $("#txtTicketTo").val(TicketTo); $("#ddlUpDn").val(UPDN);
    $(ID).closest('tr').remove(); 
}

function Bind(IssueId) {
    
    if (IssueId != '') {
        var S = "{TransType: '" + "IssueByID" + "', IssueID:'" + IssueId + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";
        //alert(S);
        //return false;
        $.ajax({
            url: '/Accounts_Form/PaperTktIssue/Get_IssueDetail',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: S,
            dataType: 'json',
            success: function (data) {
                var a = data.Data;
                var t1 = a["Table"]; //alert(JSON.stringify(t1)); alert(t1[0].TktRequisitionIdDt);
                var t2 = a["Table1"];

                $("#txtIssueDate").val(t1[0].TktIssueDt); $("#ddlIssueType").val(t1[0].IssueType);
                $("#txtIssueTo").val(t1[0].IssueTo); $("#hdnIssueTo").val(t1[0].IssuedToSectorIdEmpId);
                $("#txtReferanceNo").val(t1[0].ReferanceNo); $("#hdnReferanceNo").val(t1[0].TktRequisitionWaybillId);
                $("#txtRemarks").val(t1[0].Remarks); 

                var $this = $('#tbl .test_0');
                $parentTR = $this.closest('tr');
                $('#tbl tbody tr.myData').remove();
                if (t2.length > 0) {
                    var html = "";
                    for (var i = 0; i < t2.length; i++) {
                        html += "<tr class='myData'>"
                            + "<td class='tktissueid' style='display:none;'>" + t2[i].DatTktIssueId + "</td>"
                            + "<td style='display:none;' class='clsAccCode' chk-data='" + t2[i].DenominationId + "' >" + t2[i].DenominationId + "</td>"
                            + "<td class='denomination'>" + t2[i].Denomination + "</td>"
                            + "<td class='updn' style='text-align:center' >" + t2[i].UPDN + "</td>"
                            + "<td class='updnid' style='display:none;'>" + t2[i].UPDNID + "</td>"
                            + "<td class='TicketSeries'>" + t2[i].TktSeries + "</td>"
                            + "<td class='TicketFrom'>" + t2[i].TktSrlIdFrom + "</td>"
                            + "<td class='TicketTo'>" + t2[i].TktSrlIdTo + "</td>"
                            + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_Row(this, 2);' /></td > "
                        html + "</tr>";
                    }
                    $parentTR.after(html);
                }

                $('#dialog').modal('hide');
            }
        });
    }
}

function Grid() {

    var S = "{TransType: '" + "AllData" + "', Desc:'" + "" + "', IssueTo:'" + "" + "', IssueType:'" + "" + "', SectorID:'" + $('#ddlSector').val() + "'}";
    //alert(S);
    //return false;
    $.ajax({
        url: '/Accounts_Form/PaperTktIssue/Get_IssueTo',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: S,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;

            var columnData = [
                { "mDataProp": "TktIssueNo" },
                { "mDataProp": "TktIssueDt" },
                { "mDataProp": "IssueType" },
                { "mDataProp": "IssueTo" },
                { "mDataProp": "ReferanceNo" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = '<div><select onchange="Execute(this, ' + row.MstTktIssueId + ')">';
                        if (row.Edit == 0)
                        {
                            str += "<option value=''>Select</option>";
                            str += "<option value='1' style='display:none;'>Edit</option>";
                            str += "<option value='2' style='display:none;'>Delete</option>";
                            str += "<option value='3' >Print</option>";
                            str += "</select></div>";
                        }
                        else {
                            str += "<option value=''>Select</option>";
                            str += "<option value='1'>Edit</option>";
                            str += "<option value='2'>Delete</option>";
                            str += "<option value='3'>Print</option>";
                            str += "</select></div>";
                        }

                        return str;
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": 3,
                        "className": "text-center",
                        "width": "10%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });
            $('#dialog').modal('show');
        }
    });
    
}

function Execute(ID, IssueId) {
     //return false;
    var rID = $(ID).val();
    if (rID != "") {
        if (rID == 1) {
            Edit(IssueId);
        }
        if (rID == 2) {
            Delete(ID, IssueId, 3);
        }
        if (rID == 3) {
            Print(IssueId);
        }
    }
}

function Delete(ID, IssueIds, CON) {

   
    var res = confirm("Are you sure want to Delete this Issue ??");

    if (res == true) {

        var S = "{TransType: '" + "Cancel" + "', IssueID:'" + IssueIds + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "', CON: '" + CON + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/PaperTktIssue/Get_IssueDetail',
            contentType: "application/json; charset=utf-8",
            data: S,
            dataType: "json",
            success: function (data, status) {
                var a = data.Data;
                var t = a["Table"];
                var msgcnt = t[0].MSGCOUNT;
                var msg = t[0].MSG;

                if (msgcnt == '1') {
                    swal({
                        title: "Success",
                        text: msg,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                if (CON == 2)
                                    $(ID).closest('tr').remove();
                                else
                                    Grid();
                            }
                        });
                }
                else {
                    swal({
                        title: "Error",
                        text: msg,
                        type: "error",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    });
                }
            }
        });
    }

}

function Edit(IssueId) {
    $("#hdnIssueID").val(IssueId);
    Bind(IssueId)
}

function Blank() {
    location.reload();
}

function Save() {

    if ($("#txtIssueDate").val() == "") { $("#txtIssueDate").focus(); return false; }
    if ($("#ddlIssueType").val() == "") { $("#ddlIssueType").focus(); return false; }
    

    var IssueType = $("#ddlIssueType").val();
    if (IssueType == 'D')
    {
        if ($("#hdnIssueTo").val() == "") { $("#txtIssueTo").focus(); return false; }
        if ($("#txtReferanceNo").val() == "") { $("#txtReferanceNo").focus(); return false;}
    }
    else if (IssueType == 'F')
    {
        if ($("#txtRemarks").val() == "") { $("#txtRemarks").focus(); return false; }
    }
    else if (IssueType == 'E') {
        if ($("#hdnIssueTo").val() == "") { $("#txtIssueTo").focus(); return false; }
    }
    else
    {
        if ($("#hdnIssueTo").val() == "") { $("#txtIssueTo").focus(); return false; }
        if ($("#hdnReferanceNo").val() == "") { $("#txtReferanceNo").focus(); return false; }
    }
   

    var ArrList = [];
    var grdLen = $('#tbl tbody tr.myData').length;
    if (grdLen > 0) {
        $('#tbl tbody tr.myData').each(function () {

            var DenominationID = $(this).find('.clsAccCode').text();
            var UpDn = $(this).find('.updnid').text();
            var TicketSeries = $(this).find('.TicketSeries').text();
            var TicketFrom = $(this).find('.TicketFrom').text();
            var TicketTo = $(this).find('.TicketTo').text();

            ArrList.push({
                'TicketSeries': TicketSeries, 'DenominationID': DenominationID, 'UPDN': UpDn, 'TicketFrom': TicketFrom, 'TicketTo': TicketTo
            });
        });
    }
    else {
        alert('Please add Denomination details.'); return false;
    }

    var E = "{IssueID: '" + $("#hdnIssueID").val() + "', IssueDate: '" + $("#txtIssueDate").val() + "',   Remarks: '" + $("#txtRemarks").val() + "'," +
        "IssueType: '" + $("#ddlIssueType").val() + "', IssueTo: '" + $("#hdnIssueTo").val() + "', ReferanceNo: '" + (IssueType == 'D' ? $("#txtReferanceNo").val() : $("#hdnReferanceNo").val()) + "'," +
        "SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "', IssueDetail: " + JSON.stringify(ArrList) + "} ";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/PaperTktIssue/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t != "") {

                swal({
                    title: "Success",
                    text: 'Issue' + ($("#hdnIssueID").val() == "" ?  ' Generated ' : ' Updated ') + 'Successfully !!\nIssue No. ' + t,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}

function PendingReq()
{
    var S = "{TransType: '" + "PendingReq" + "', Desc:'" + "" + "', IssueTo:'" + "" + "', IssueType:'" + "" + "', SectorID:'" + $('#ddlSector').val() + "'}";
    //alert(S);
    //return false;
    $.ajax({
        url: '/Accounts_Form/PaperTktIssue/Get_IssueTo',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: S,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;

            var columnData = [
                { "mDataProp": "SectorName" },
                { "mDataProp": "TktRequisitionIdDt" },
                { "mDataProp": "TktRequisitionNo" },
                { "mDataProp": "TktRequisitionRemarks" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = '<div><select onchange="PendingRequisition(this, \'' + row.TktRequisitionId + '\')">';
                        str += "<option value=''>Select</option>";
                        str += "<option value='1'>View</option>";
                        str += "<option value='2'>Issue</option>";
                        str += "</select></div>";
                        return str;
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#pendingreq').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [1,2,4],
                        "className": "text-center",
                        "width": "15%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });
        }
    });
}

function PendingRequisition(ID, RequisitionId)
{
    
    var rID = $(ID).val(); 
    if (rID != "") {
        //VIEW
        if (rID == 1) {
            if (RequisitionId != '') {
                var S = "{TransType: '" + "RequisitionByID" + "', Requisition:'" + RequisitionId + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";

                $.ajax({
                    url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: S,
                    dataType: 'json',
                    success: function (data) {
                        var a = data.Data;
                        var t1 = a["Table"]; //alert(JSON.stringify(t1)); alert(t1[0].TktRequisitionIdDt);
                        var t2 = a["Table1"];

                        //$("#txtRequisitionDate").val(t1[0].TktRequisitionIdDt); $("#txtRemarks").val(t1[0].TktRequisitionRemarks);

                        if (t2.length > 0) {
                            var html = "";
                            for (var i = 0; i < t2.length; i++) {
                                html += "<tr class='myData'>"
                                    + "<td>" + t2[i].DenominationRemarks + "</td>"
                                    + "<td style='text-align:center'>" + t2[i].UPDN + "</td>"
                                    + "<td class='denomination' style='text-align:right'>" + t2[i].TktRequisitionQty + "</td>"
                                html + "</tr>";
                            }
                            $('#tbl tbody tr.myData').empty();
                            $('#tbl tbody').append(html);
                            $('#dialog_req').modal('show');
                        }
                    }
                });
            }
        }
        //ISSUE
        else if (rID == 2) {
            if (RequisitionId != '') {
                var S = "{TransType: '" + "RequisitionByID" + "', Requisition:'" + RequisitionId + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";

                $.ajax({
                    url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: S,
                    dataType: 'json',
                    success: function (data) {
                        var a = data.Data;
                        var t1 = a["Table"]; //alert(JSON.stringify(t1)); alert(t1[0].TktRequisitionIdDt);
                        var t2 = a["Table1"];

                        //$("#txtIssueDate").val(t1[0].TktRequisitionIdDt);
                        $("#ddlIssueType").val('A'); 
                        $("#txtIssueTo").val(t1[0].SectorName); $("#hdnIssueTo").val(t1[0].SectorID);
                        $("#txtReferanceNo").val(t1[0].TktRequisitionNo); $("#hdnReferanceNo").val(t1[0].TktRequisitionId);
                        $("#txtRemarks").val(t1[0].TktRequisitionRemarks);

                        var html = ""
                        var $this = $('#tbl .test_0');
                        $parentTR = $this.closest('tr');

                        if (t2.length > 0) {
                            for (var i = 0; i < t2.length; i++) {
                                html += "<tr class='myData' >"
                                    + "<td style='display:none;' class='clsAccCode' chk-data='" + t2[i].DenominationId + "' >" + t2[i].DenominationId + "</td>"
                                    + "<td class='denomination'>" + t2[i].DenominationRemarks + "</td>"
                                    + "<td class='updn' style='text-align:center'>" + t2[i].UPDN + "</td>"
                                    + "<td class='updnid' style='display:none;'>" + t2[i].UPDNID + "</td>"
                                    + "<td class='TicketSeries'></td>"
                                    + "<td class='TicketFrom'></td>"
                                    + "<td class='TicketTo'></td>"
                                    + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_Row(this);' />"
                                    + "<img src='/Content/Images/edit.png' title='Edit' style='width:22px;height:22px;cursor:pointer; text-align:center;' onClick='Edit_Row(this);' /></td > "
                                html + "</tr>";
                            }
                            $('#tbl tbody tr.myData').empty();
                             $parentTR.after(html);
                        }
                    }
                });
            }

            $('#tab-2').removeClass('active');
            $('#tab-1').addClass('active');

            $('.tab2').removeClass('active');
            $('.tab1').addClass('active');
        }
    }
}

function Print(IssueId)
{
    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "Ticket_Issue.rpt",
        FileName: "Tickt_Issue",
        Database: ''
    });

    detail.push({
        TransType: "Print",
        TransFrom:'',
        IssueID: IssueId,
        IssueDate: '',
        DenominationID: 0,
        Desc: '',
        IssueType: '',
        IssueTo: 0,
        ReferanceNo: 0,
        TicketSeries: '',
        TicketFrom: 0,
        TicketTo: 0,
        Remarks:'',
        SectorId: $('#ddlSector').val(),
        SectorTo:0,
        FinYear: '',
        UPDN: '',
        UserID: 0,
        CON	:1
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

