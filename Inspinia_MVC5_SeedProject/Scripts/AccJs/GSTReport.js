﻿

$(document).ready(function () {

    FromDate();
    ToDate();

});

function FromDate() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function ToDate() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

function Excel()
{
    var f = $('#txtFromDate').val();
    var t = $('#txtToDate').val();
    var r = $('#ddlReportName').val();

    if (f == "") { $('#txtFromDate').focus(); return false; }
    if (t == "") { $('#txtToDate').focus(); return false; }
    if (r == "") { $('#ddlReportName').focus(); return false; }

    var master = []; var final = {};

    var secid = $("#ddlSector").val().toString();
    master.push({
        SectorID: secid,
        FromDate: f,
        ToDate: t,
        FileName: $("#ddlReportName option:selected").text(),
        TransType: r
    });
    final = {
        Master: master
    }
    //alert(JSON.stringify(final)); return false;
    location.href = "/Accounts_Form/GSTReport/Excel?ReportName=" + JSON.stringify(final);
}