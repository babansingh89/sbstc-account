﻿var ArrayOpenListID = []; var ArrayOpenAccDesc = []; var isGLSL = "L";
var hide_Columns = [];
var sum_Columns = [];
var Grid_Level = 0;
var unPaidVch = ""; var isunpaidvch = 0;

$(document).ready(function () {

    //Sub Ledger hide and show
    $('#radioGL').change(function () {
        isGLSL = "L";
    });
    $('#radioSL').change(function () {
        isGLSL = "S";
    });
    $('#radioUV').click(function () {

        var isChecked = $('#radioUV').prop('checked');
        if (isChecked == true && isunpaidvch == 0) {
            unPaidVch = "I"; isunpaidvch = 1;
        }
        else {
            unPaidVch = ""; isunpaidvch = 0; $('#radioUV').prop('checked', false);
        }
    });

    //Date Checking
    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();
    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //======================================== ACCOUNT DESCRIPTION AUTO-COMPLETE =============================================
    $("#txtLedger").autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $('#txtLedger').val() + "', MND:'" + isGLSL + "'}"; //alert(S);
               
            $.ajax({
                type: "POST",
                url: '/Accounts_Form/AccountMaster/Account_Description',
                data: S,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                                   
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnLedger").val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
        if (iKeyCode == 46) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
    });

    $('#btnSearch').click(function (evt) {
        Load_Detail();
    });

    //$('#datatable').DataTable();
});

//Date Checking
function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            var a = $('#txtAccFinYear').val();
            if (a != "") {
                var res = FinYear_Validation(a);
                if (res == false) {
                    alert('Sorry ! Please Enter Valid Account Financial Year.'); $('#txtFromDate').val(''); return false;
                }
                else
                    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
            }
        }
    });
}
function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Set_DefaultDate() {
    //Set From Date
    var a = $('#txtAccFinYear').val();
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);




}
function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}


//Show Button Click
function MyForm_Validate() {
    $('#frmTrailBalance').validate({
        rules: {
            txtToDate: {
                required: true
            },
            txtFromDate: {
                required: true
            }
        }
    });
}
function Between_Dates() {
    if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }
    if (isGLSL == 'S') { if ($("#hdnSearchSubLedger").val() == "") { $("#txtSearchSubLedger").focus(); return false; } }

    var fdate = $('#txtFromDate').val();
    var tdate = $("#txtToDate").val();

    var fSplitDate = fdate.split('/');
    var tSplitDate = tdate.split('/');

    var a = $('#txtAccFinYear').val();
    if (a != '') {
        var fy = a.split('-');
        a1 = fy[0];
        a2 = fy[1];

        var startDate = "01/04/" + a1;
        var endDate = "31/03/" + a2;

        var newStartDate = new Date(a1, 03, 01);
        var newendDate = new Date(a2, 02, 31);

        var givenFromDate = new Date(fSplitDate[2], parseInt(fSplitDate[1]) - 1, fSplitDate[0]);
        var givenToDate = new Date(tSplitDate[2], parseInt(tSplitDate[1]) - 1, tSplitDate[0]);

        if ((givenFromDate >= newStartDate && givenFromDate <= newendDate)) { }
        else
        {
            alert('Sorry ! Given From Date is not within Account Financial Year.'); $('#txtFromDate').focus(); return false;
        }

        if ((givenToDate >= newStartDate && givenToDate <= newendDate)) { }
        else {
            alert('Sorry ! Given To Date is not within Account Financial Year.'); $('#txtToDate').focus(); return false;
        }

        Populate_Grid(fdate, tdate);
    }
}

function Load_Detail()
{
    var frm = ''; var to = '';
    if ($('#txtFromDate').val() != '')
    {
        var f = $('#txtFromDate').val().split('/');
        frm = f[2] + '-' + f[1] + '-' + f[0];
    }
    if ($('#txtToDate').val() != '') {
        var t = $('#txtToDate').val().split('/');
        to = t[2] + '-' + t[1] + '-' + t[0];
    }


    var E = "{From: '" + frm + "', To: '" + to + "', AmountFrom: '" + $('#txtAmountFrom').val() + "', AmountTo: '" + $('#txtAmountTo').val() + "'," + 
            "AccountCode: '" + $('#hdnLedger').val() + "', DRCR: '" + $('#ddlDrCr').val() + "',  VchType: '" + $('#ddlVchType').val() + "',  AdjType: '" + $('#ddlAdjType').val() + "'," +
        "ChequeNo: '" + $('#txtChequeNo').val() + "', Narration: '" + $('#txtNarration').val() + "', SectorID: '" + $('#ddlSector').val() + "', IncludeUnpaidTransac: '" + isGLSL + "'}";

    //alert(E);
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/AllSearch/Load_ItemDetail',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data; //alert(JSON.stringify(t));

          
 $('#datatable tbody tr').remove();
            if (data.Status == 200 && t.length > 0) {

               

                var columnData = [{ "mDataProp": "VoucherNumber" },
                           { "mDataProp": "VchDate" },
                           { "mDataProp": "Narration" },
                           { "mDataProp": "VchType" },
                           { "mDataProp": "VchNo" },
                           { "mDataProp": "Debit" },
                           { "mDataProp": "Credit" },
                           {
                               "mDataProp": "Edit",
                               "render": function (data, type, row) {
                                   return '<a href="javascript:void(0)" title="Edit" style="text-align:center" class="btn btn-azure btn-xs" onclick="Load_ItemDetail(\'' + row.VoucherNumber + '\')" ><span class="label label-info pull-right">Edit</span>  </a>';
                                  
                               }
                           }];

                var columnDataHide = [0];

                var oTable = $('#datatable').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'MyExcel' },
                        { extend: 'pdf', title: 'MyPDF', width: '100%' },
                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            }
                        }
                    ],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                         {
                             "targets": columnDataHide,
                             "visible": false,
                             "searchable": false
                         },
                        {
                            "targets": 7,
                            "className": "text-center",
                            "width": "10%"
                        }],
                    'iDisplayLength': 10,
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        var netTotaldr = api
                            .column(5)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        var netTotalcr = api
                            .column(6)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        $(api.column(4).footer()).html('Total');
                        $(api.column(5).footer()).html((netTotaldr).toFixed(2));
                        $(api.column(6).footer()).html((netTotalcr).toFixed(2));
                    },
                    destroy: true,
                });
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });
}

function Load_ItemDetail(VoucherNumber)
{
    window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank');
}

function Excel()
{
    var table = $('#datatable').DataTable();
    var tblCount = table.column(0).data().length;

    if (tblCount > 0) {
        var res = confirm('Are You Sure Want to download Excel Report ??');
        if (res == true) {

            var frm = ''; var to = '';
            if ($('#txtFromDate').val() != '') {
                var f = $('#txtFromDate').val().split('/');
                frm = f[2] + '-' + f[1] + '-' + f[0];
            }
            if ($('#txtToDate').val() != '') {
                var t = $('#txtToDate').val().split('/');
                to = t[2] + '-' + t[1] + '-' + t[0];
            }

            var master = []; var final = {};

            master.push({
                From: frm,
                To: to,
                AmountFrom: $('#txtAmountFrom').val(),
                AmountTo: $('#txtAmountTo').val(),
                AccountCode: $('#hdnLedger').val(),
                DRCR: $('#ddlDrCr').val(),
                VchType: $('#ddlVchType').val(),
                AdjType: $('#ddlAdjType').val(),
                ChequeNo: $('#txtChequeNo').val(),
                Narration: $('#txtNarration').val(),
                SectorID: $('#ddlSector').val(),
                IncludeUnpaidTransac: unPaidVch
            });
            final = {
                Master: master
            }

            location.href = "/Accounts_Form/AllSearch/Excel?ReportName=" + JSON.stringify(final);
        }
    }
    else {
        alert('Sorry ! There is no any Data Available to download.'); return false;
    }

}


















