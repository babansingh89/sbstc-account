﻿
$(document).ready(function () {
 
    SetFrom_Date();
    SetTo_Date();

});
   
function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

function BindGrid(AccountCode) {

    if ($("#txtFromDate").val() == "") {
        $("#txtFromDate").focus(); return false;
    }

    if ($("#txtToDate").val() == "") {
        $("#txtToDate").focus(); return false;
    }
    if ($('#lstSector').val() == null || $('#lstSector').val() == "") {
        $(".select2-search__field").focus(); return false;
    }
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();

    if (fdate != "") {
        var rDate = fdate.split('/');
        fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
    }
    if (tdate != "") {
        var trDate = tdate.split('/');
        tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
    }

    var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#lstSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/TDSPayable/TDSDetail',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('#datatable tbody tr').remove();
            if (data.Status == 200 && t.length > 0) {

                var columnData = [{ "mDataProp": "VoucherNumber" },
                { "mDataProp": "VoucherDate" },
                { "mDataProp": "VoucherNo" },
                { "mDataProp": "PartyName" },
                { "mDataProp": "PanNo" },
                { "mDataProp": "GstNo" },
                { "mDataProp": "TdsCode" },
                { "mDataProp": "BasicAmount" },
                { "mDataProp": "TdsRt" },
                { "mDataProp": "TdsAmount" },
                { "mDataProp": "CgstRt" }, //10
                { "mDataProp": "CgstAmount" },
                { "mDataProp": "SgstRt" },
                { "mDataProp": "SgstAmount" },
                { "mDataProp": "IgstRt" },
                { "mDataProp": "IgstAmount" },
                { "mDataProp": "AutoTrans" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        return '<a href="javascript:void(0)" title="Edit" style="text-align:center" class="btn btn-azure btn-xs" onclick="Open(\'' + row.VoucherNumber + '\')" ><span class="label label-info pull-right">Edit</span>  </a>';
                    }
                }];

                var columnDataHide = [0, 6, 8, 10, 12, 14, 16];

                var oTable = $('#datatable').DataTable({
                    buttons: [],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": [7,9,11,13,15],
                            "className": "text-right"
                        }
                    ],
                    'iDisplayLength': 10,
                    destroy: true,
                });
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }

    });
    
}
function Open(VoucherNo)
{
    window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNo, '_blank');
}

Print_Voucher = function () {

    if ($("#txtFromDate").val() == "") {
         $("#txtFromDate").focus(); return false;
    }

    if ($("#txtToDate").val() == "") {
       $("#txtToDate").focus(); return false;
    }

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: $("#txtVoucherNo").val()
    });

    detail.push({
        VoucherNumber: $("#hdnVoucherNo").val(),
        sectorid: $("#lstSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function Excel() {

   

    if ($("#txtFromDate").val() == "") {
        $("#txtFromDate").focus(); return false;
    }

    if ($("#txtToDate").val() == "") {
        $("#txtToDate").focus(); return false;
    }
    if ($('#lstSector').val() == null || $('#lstSector').val() == "") {
        $(".select2-search__field").focus(); return false;
    }

    var table = $('#datatable').DataTable();
    var tblCount = table.column(0).data().length;

    if (tblCount > 0) {
        var res = confirm('Are You Sure Want to download Excel Report ??');
        if (res == true) {

            var fDate = ""; var tDate = "";
            if ($("#txtFromDate").val() != "") {
                var a = ($("#txtFromDate").val()).split('/');
                fDate = a[2] + '-' + a[1] + '-' + a[0];
            }
            if ($("#txtToDate").val() != "") {
                var b = ($("#txtToDate").val()).split('/');
                tDate = b[2] + '-' + b[1] + '-' + b[0];
            }

            var master = []; var final = {};

            master.push({
                FromDate: fDate,
                ToDate: tDate,
                SectorID: $.trim($("#lstSector").val())
            });
            final = {
                Master: master
            }
            //alert(JSON.stringify(final)); return false;
            location.href = "/Accounts_Form/TDSPayable/Excel?ReportName=" + JSON.stringify(final);
        }
    }
    else {
        alert('Sorry ! First Show the TDS Detail.'); return false;
    }

}











