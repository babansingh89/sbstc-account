﻿

$(document).ready(function () {

    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();

    Autocomplete_FCNo();
    AutoComplete_Ledger();
    Bind_Tbl();

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#txtEfftGL').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtEfftGL").val('');
            $("#hdnEffectAdd").val('');
            $("#hdnEffectAmount").val('');
            $("#hdnEffectTempBudgetID").val('');
            $("#txtEfftAmount").val('');
            $('#lblFCTotalAmt').text('0.00');
            $(".chkReq").prop('checked', false); $("#txtAmount").val('');
        }
        if (iKeyCode == 46) {
            $("#txtEfftGL").val('');
            $("#hdnEffectAdd").val('');
            $("#hdnEffectAmount").val('');
            $("#hdnEffectTempBudgetID").val('');
            $("#txtEfftAmount").val('');
            $('#lblFCTotalAmt').text('0.00');
            $(".chkReq").prop('checked', false); $("#txtAmount").val('');
        }
    });
    $('#btnSave').click(function () {
        CheckClosingStatus();
    });
});

function AutoComplete_Ledger() {
    $("#txtAccDesc").autocomplete({
        source: function (request, response) {

            var URL = ""; var V = "";

                URL = "/Accounts_Form/LedgerMaster/Get_Ledger_Details";
                V = "{Desc:'" + $("#txtAccDesc").val() + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "',  SectorID:'" + $("#ddlSector").val() + "'}";


            $.ajax({
                type: "POST",
                url: URL,
                data: V,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode,
                                GroupLedger: item.GroupLedger
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        minLength: 0,
        select: function (e, i) {

            //$("#hdnSearchSubLedger").val(i.item.AccountCode);
            //$("#hdnGroupLedger").val(i.item.GroupLedger);

            //var fDate = ""; var tDate = "";
            //if ($("#txtFromDate").val() != "") {
            //    var a = ($("#txtFromDate").val()).split('/');
            //    fDate = a[2] + '-' + a[1] + '-' + a[0];
            //}
            //if ($("#txtToDate").val() != "") {
            //    var b = ($("#txtToDate").val()).split('/');
            //    tDate = b[2] + '-' + b[1] + '-' + b[0];
            //}

            //E = "{AccountCode:'" + ($('#hdnSubLedger').val() + i.item.AccountCode) + "', Desc:'" + $("#txtSearchSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "'," +
            //    " SectorID:'" + $("#ddlSector").val() + "', FromDate:'" + fDate + "', ToDate:'" + tDate + "', " +
            //    " LegerType: '" + i.item.GroupLedger + "', unPaidVch:'" + unPaidVch + "', ReportType:'" + "L" + "'}";

            ////alert(E);
            //$.ajax({
            //    type: "POST",
            //    url: "/Accounts_Form/LedgerMaster/Show_GeneralLedger",
            //    data: E,
            //    contentType: "application/json; charset=utf-8",
            //    success: function (D) {
            //        var t = D.Data;
            //        if (t.length > 0 && D.Status == 200) {

            //            Ledger.ShowDetail(t);
            //        }
            //    }
            //});

        },
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

//    FC NO. AUTOCOMPLETE
function Autocomplete_FCNo() {
    $('#txtEfftGL').autocomplete({
        source: function (request, response) {

            var S = "{TransType:'" + "FC" + "', TabID:'" + "" + "', Desc:'" + $('#txtEfftGL').val() + "', MemoDate:'" + $('#txtMemoDate').val() + "', SectorID:'" + $('#ddlSector').val() + "', BudgetID:'" + "" + "'}"; //$('#hdnEffectBudgetID').val()
            $.ajax({
                url: '/Accounts_Form/Update_Voucher_FC/Auto_FC',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BudgetDesc,
                                ReferanceNo: item.ReferanceNo,
                                BudgetDesc: item.BudgetDesc,
                                ConcurranceAmount: item.ConcurranceAmount,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnEffectAdd').val(i.item.ReferanceNo);
            $('#hdnEffectAmount').val(i.item.ConcurranceAmount);
            $('#lblFCTotalAmt').text(i.item.ConcurranceAmount);
            //$('#txtEfftAmount').val(i.item.ConcurranceAmount);
            $('#hdnEffectTempBudgetID').val(i.item.BudgetID);

            CheckFCLimit(2);
        },
        minLength: 0
    }).click(function () {
        if ($('#txtMemoDate').val() == "") {
            alert('Enter Memo Date first.'); $('#txtMemoDate').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });
}
function CheckFCLimit(con) {

    var fcno = $("#hdnEffectAdd").val();
    if (fcno != "") {
        var E = "{MemoNo: '" + fcno + "', TransType: '" + "FCLimit" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/Update_Voucher_FC/FCDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data; 

                var UsedFCAmount = data[0].UsedFCAmount; //alert(TotalFCAmount);
                $("#lblUsedFCTotalAmt").text(UsedFCAmount);

                $("#lblRestFCTotalAmt").text(parseFloat($("#lblFCTotalAmt").text()) - parseFloat($("#lblUsedFCTotalAmt").text()));
            }
        });
    }

}

function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function SetTo_Date() {
    $('#txtToDate, #txtMemoDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Set_DefaultDate() {
    //Set From Date
    var a = localStorage.getItem("FinYear");
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }


    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output); $('#txtMemoDate').val(output); 
}
function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}

function Show()
{
    if ($('#ddlVchType').val() == '')
    {
        $('#ddlVchType').focus(); return false;
    }
    if ($('#txtFromDate').val() == '') {
        $('#txtFromDate').focus(); return false;
    }
    if ($('#txtToDate').val() == '') {
        $('#txtToDate').focus(); return false;
    }
    var FromDate = '', ToDate = '';
    if ($.trim($("#txtFromDate").val()) != "") {
        var refDate = ($("#txtFromDate").val()).split('/');
        FromDate = refDate[2] + "-" + refDate[1] + "-" + refDate[0];
    }
    if ($.trim($("#txtToDate").val()) != "") {
        var tDate = ($("#txtToDate").val()).split('/');
        ToDate = tDate[2] + "-" + tDate[1] + "-" + tDate[0];
    }

    var E = "{VchTypeID: '" + $('#ddlVchType').val() + "', FromDate: '" + FromDate + "', ToDate: '" + ToDate + "', TransType: '" + "Fetch" + "', MemoNo: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "'}";

    $.ajax({
        url: '/Accounts_Form/Update_Voucher_FC/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 
            Bind_Table(t);
        }
    });
}
function Bind_Table(detail)
{
    var table = $("#myTable");
    table.find('tbody').empty();
    table.find('tfoot').empty();

    if (detail.length > 0)
    {
        $('.clsSave').css('display', '');
        $('.clsGrid').css('display', '');

        var Total = 0;
        for (var i = 0; i < detail.length; i++) {
            Total = parseFloat(Total) + parseFloat(detail[i].Amount);
            table.find('tbody').append("<tr class='allData'><td style='display:none;' class='VoucherNumber'>" +
                detail[i].VoucherNumber + "</td><td style='text-align:left; width:30%' class='RefVoucherSlNo'>" +
                detail[i].RefVoucherSlNo + "</td><td style='text-align:right; width:15%' class='VchDate'>" +
                detail[i].VchDate + "</td><td style='text-align:left; width:100%'>" +
                detail[i].Narration + "</td><td style='text-align:right; width:20%;' class='clsvchAmt'>" +
                detail[i].Amount + "</td><td style='width:10%'>" +
                "<img src= '/Content/Images/Edit.png' title= 'Edit' style= 'width:23px;height:25px;cursor:pointer; text-align:center;' onClick= 'Edit(this);' /></td><td style='width:10%;'>" +
                "<img src= '/Content/Images/clean.png' title= 'Clear' style= 'width:22px;height:22px;cursor:pointer; text-align:center;' onClick= 'Clear(this);' /></td><td style='width:10%'>" +
                "<img src= '/Content/Images/Delete.png' title= 'Delete' style= 'width:22px;height:20px;cursor:pointer; text-align:center;' onClick= 'Delete(this);' /></td><td style='width:10%'>" +
                "<input type='checkbox' class='chkReq' id='chkReq' name='chkReq' onclick='Calculate(this)'></td><tr>");
        }
        table.find('tfoot').append("<tr><td colspan='3' style='font-weight:bold;'>" +
            "Total" + "</td><td colspan='4' style='text-align:left; font-weight:bold;' class='clsVchTotal'>" +
            0.00 + "</td><tr>");
    }
    else {
        $('.clsSave').css('display', 'none');
    }
}
function Bind_Tbl()
{
    var E = "{VchTypeID: '" + $('#ddlVchType').val() + "', FromDate: '" + "" + "', ToDate: '" + "" + "', TransType: '" + "FetchEdit" + "', MemoNo: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "'}";

    $.ajax({
        url: '/Accounts_Form/Update_Voucher_FC/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var detail = data.Data;

            var table = $("#tbl");
            table.find('tbody').empty();
            table.find('tfoot').empty();

            if (detail.length > 0) {

                for (var i = 0; i < detail.length; i++) {
                    table.find('tbody').append("<tr class='allData'><td style='display:none;' class='TabId'>" +
                        detail[i].TabId + "</td><td style='text-align:left; width:35%' class='MemoNo'>" +
                        detail[i].MemoNo + "</td><td style='text-align:right; width:15%'>" +
                        detail[i].MemoDt + "</td><td style='text-align:right; width:20%'>" +
                        detail[i].GrossAmt + "</td><td style='text-align:left; width:100%;' class='clsvchAmt'>" +
                        detail[i].Remarks + "</td><td style='width:10%'>" +
                        "<img src= '/Content/Images/Delete.png' title= 'Delete' style= 'width:23px;height:25px;cursor:pointer; text-align:center;' onClick= 'DeleteMemo(this);' /></td><td style='width:10%'>" +
                        "<img src= '/Content/Images/printer.png' title= 'Print' style= 'width:23px;height:25px;cursor:pointer; text-align:center;' onClick= 'Print(this);' /></td><tr>");
                }
            }
        }
    });
}

function Calculate(ID)
{
    if ($("#hdnEffectAdd").val() == "")
    {
        alert('Sorry ! Please Select FC No first.'); $("#txtEfftGL").focus();
        $(ID).prop('checked', false);
        return false;
    }

    var selctAmt = 0;
    $("#myTable").find(".chkReq").each(function (idx, aid) {
        if ($(this).is(":checked")) {
            Amt = $(this).closest('tr').find('.clsvchAmt').text();
            selctAmt = parseFloat(selctAmt) + parseFloat(Amt);
        }
        else
        {
            $(".chkallReq").prop('checked', false);
        }
    });

    var actualfcAmt = $('#lblFCTotalAmt').text(); 
    var usedfcAmt = $('#lblUsedFCTotalAmt').text(); 
    var restfcAmt = $('#lblRestFCTotalAmt').text(); 
    
    if (parseFloat(selctAmt) > parseFloat(restfcAmt)) {
        alert('Sorry ! Selected FC Amount is greater than Actual FC Amount ' + actualfcAmt);
        $(ID).prop('checked', false);
        return false;
    }

    $('.clsVchTotal').text(selctAmt);
    $('#txtAmount').val(selctAmt);
}
function Clear(ID)
{
    swal({
        title: "Warning",
        text: "Are Your Sure want to Clear this record ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                $(ID).closest('tr').remove();
                Calculate(ID);

            }
        });

}
function Delete(ID) {
    swal({
        title: "Warning",
        text: "Are Your Sure want to Delete this record ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {

                var VoucherNo = $(ID).closest('tr').find('.VoucherNumber').text();

                var E = "{VchTypeID: '" + "" + "', FromDate: '" + "" + "', ToDate: '" + "" + "', TransType: '" + "Delete" + "', MemoNo: '" + VoucherNo + "', SectorID: '" + $('#ddlSector').val() + "'}";
               
                $.ajax({
                    url: '/Accounts_Form/Update_Voucher_FC/Load',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: E,
                    dataType: 'json',
                    success: function (data) {
                        var t = data.Data;

                        swal("Deleted !", "Your Voucher Number has been deleted.", "success");
                        $(ID).closest('tr').remove();
                        Calculate(ID);
                    }
                });
            }
        });

}
function DeleteMemo(ID)
{
    swal({
        title: "Warning",
        text: "Are Your Sure want to Delete this record ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {

                var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
                var TabId = $(ID).closest('tr').find('.TabId').text();

                var E = "{VchTypeID: '" + TabId + "', FromDate: '" + "" + "', ToDate: '" + "" + "', TransType: '" + "Edit" + "', MemoNo: '" + MemoNo + "', SectorID: '" + $('#ddlSector').val() + "'}";

                $.ajax({
                    url: '/Accounts_Form/Update_Voucher_FC/Load',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: E,
                    dataType: 'json',
                    success: function (data) {
                        var t = data.Data;

                        swal("Deleted !", "Your Memo No. has been deleted.", "success");
                        Bind_Tbl();
                    }
                });
            }

        });
}
function Edit(ID)
{
    var VoucherNumber = $(ID).closest('tr').find('.VoucherNumber').text();
    window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank');
}
function Print(ID)
{
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    var BillStatus = 1;

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "BillExtract.rpt",
        FileName: MemoNo
    });

    detail.push({
        MemoNo: MemoNo,
        BillStatus: BillStatus,
        ReportType: 1,
        SectorID: $('#ddlSector').val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function SelectAll(ID)
{
    $(".chkReq").prop('checked', $(ID).prop('checked'));
    Calculate(ID);
}

function Save()
{
    if ($("#txtMemoDate").val() == "")
    {
        $("#txtMemoDate").focus(); return false;
    }
    if ($("#hdnEffectAdd").val() == "") {
        $("#txtEfftGL").focus(); return false;
    }
    if ($("#txtAmount").val() == "" || $("#txtAmount").val() <= 0) {
        $("#txtAmount").focus(); return false;
    }

    var isChecked = $('.chkReq').is(':checked');
    if (isChecked == false)
    {
        alert('Sorry ! Please Select Voucher Number for FC Update.'); return false;
    }

    var VchTotal = $('.clsVchTotal').text(); 
    var enterTotal = $('#txtAmount').val(); 
    if (parseFloat(enterTotal) > parseFloat(VchTotal))
    {
        alert('Sorry ! Enter FC Amount is greater than selected Voucher Amount. ' + VchTotal); return false;
    }

    var ArrList = []; var selctAmt = 0;
    $("#myTable").find(".chkReq").each(function (idx, aid) {
        if ($(this).is(":checked")) {
            Amt = $(this).closest('tr').find('.clsvchAmt').text();
            vchNo = $(this).closest('tr').find('.VoucherNumber').text();
            vchDate = $(this).closest('tr').find('.VchDate').text();
            selctAmt = parseFloat(selctAmt) + parseFloat(Amt);

            ArrList.push({
                'VoucherNo': vchNo, 'VchDate': vchDate, 'Amount': Amt, 'RefType': "V"
            });
        }
    });

    var actualfcAmt = $('#lblFCTotalAmt').text();
    if (parseFloat(selctAmt) > parseFloat(actualfcAmt)) {
        alert('Sorry ! FC Amount is greater than ' + actualfcAmt);
        $(ID).prop('checked', false);
        return false;
    }

    var Master = JSON.stringify(ArrList);
    var E = "{MemoNo: '" + $('#txtMemoNo').val() + "', MemoDate: '" + $('#txtMemoDate').val() + "', BudgetID: '" + $('#hdnEffectTempBudgetID').val() + "', FCNo: '" + $('#hdnEffectAdd').val() + "',"+
        "SectorID: '" + $('#ddlSector').val() + "', Remarks: '" + $('#txtRemarks').val() + "', Amount: '" + $('#txtAmount').val() + "'," +
        "param: " + Master + ", FinYear: '" + localStorage.getItem("FinYear") + "'}";

    //alert(E);
    //return false;
    $.ajax({
        url: '/Accounts_Form/Update_Voucher_FC/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
               
                swal({
                    title: "Success",
                    text: 'FC Updated Successfully !!\nMemoNo is ' + t,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}
function CheckClosingStatus() {
    var a = $.trim($("#txtMemoDate").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 93 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Save();
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}