﻿

$(document).ready(function () {
  
    Budget();
    BudgetAutoComplete();
    AccountAutoComplete();


    //Click Save Events
    $('#btnSave').click(Save);

    $('#btnRefresh').click(Refresh); 

    $('#txtParentFundCode').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtParentFundCode").val('');
            $("#hdnParentFundCode").val('');
        }
        if (iKeyCode == 46) {
            $("#txtParentFundCode").val('');
            $("#hdnParentFundCode").val('');
        }
    });
    $('#txtSearch').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtSearch").val('');
            $("#hdnSearch").val('');
        }
        if (iKeyCode == 46) {
            $("#txtSearch").val('');
            $("#hdnSearch").val('');
        }
    });

    
   
});


function Refresh()
{
    $('#txtTabId').val(''); $('#hdnTabId').val(''); $('#hdnDetailID').val('');
    $('#txtAccountCode').val(''); $('#hdnAccountCode').val('');
    $('#txtTabId').prop('disabled', false);

}

//SAVE
function Save() {

    if ($('#hdnTabId').val() == "")
    {
        toastr.error('Please, Select Parent Budget Name !!', 'Warning')
        $("#txtTabId").focus(); return false;
    }
    if ($('#hdnAccountCode').val() == "") {
        toastr.error('Please, Select Account Description !!', 'Warning')
        $("#txtAccountCode").focus(); return false;
    }


    var E = "{DetailID: '" + $('#hdnDetailID').val() + "', TabId: '" + $('#hdnTabId').val() + "', AccountCode: '" + $('#hdnAccountCode').val() + "', ExpStatutory: '" + $('#ddlExpStatutory').val() + "'}"; //alert(E); return false;
    $.ajax({
        url: '/Accounts_Form/AccountBudgetMapping/InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 
            var result = t[0].Result;
            alert(result);

            $('#txtTabId').val(''); $('#hdnTabId').val(''); $('#hdnDetailID').val('');
            $('#txtAccountCode').val(''); $('#hdnAccountCode').val('');

        }
    });
}

//AUTOCOMPLETE
function BudgetAutoComplete() {

    $('#txtTabId').autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $.trim($('#txtTabId').val()) + "'}";

            $.ajax({
                url: '/Accounts_Form/AccountBudgetMapping/BudgetAutoComplete',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {

                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BDesc,
                                TabId: item.TabId
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnTabId').val(i.item.TabId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

//SEARCH AUTOCOMPLETE
function AccountAutoComplete() {
    $('#txtAccountCode').autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $.trim($('#txtAccountCode').val()) + "', LedgerSubLedger:'" + $.trim($('#ddlExpStatutory').val()) + "'}";

            $.ajax({
                url: '/Accounts_Form/AccountBudgetMapping/AccountAutoComplete',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {

                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnAccountCode').val(i.item.AccountCode);
            //Populate(i.item.FundID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}




//TREEVIEW
function Budget() {
    var E = "{BType: '" + $('#ddlDashBudgetType').val() + "', BCat: '" + $('#ddlDashCategory').val() + "'}"; //alert(E); return false;
    $.ajax({
        url: '/Accounts_Form/AccountBudgetMapping/Budget',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var table = $('#tbl_Budget');
            var thead = table.find('thead');
            var tbody = table.find('tbody');

            tbody.find('.myData').empty();
            var html = "";
            if (t.length > 0)
            {
                for (var i = 0; i < t.length; i++)
                {
                    html += "<tr class='myData'>"
                        + "<td style='display:none;' class='clsTabId'>" + t[i].TabId + "</td>"
                        + "<td style='cursor:pointer' class='clsTabDesc' onClick='GetDetail(this);'>" + t[i].BDesc + "</td>"
                    html + "</tr>";
                }
                $('#tbl_Budget tbody').append(html);

                    $("[id*=tbl_Budget] tr").not(':first').hover(
                    function () { $(this).css("background", "#D4E6F1"); },
                    function () { $(this).css("background", ""); });
            }
        }
    });
}
function GetDetail(ID)
{
    var TabId = $(ID).closest('tr').find('.clsTabId').text(); 
    var TabDesc = $(ID).closest('tr').find('.clsTabDesc').text(); 

    if (TabId != "")
    {
        $('#hdnTabId').val(TabId); $('#txtTabId').val(TabDesc);
        $('#txtTabId').prop('disabled', true);

        $("#tbl_Budget tbody tr.myData").find('td').removeClass('rowColor');
        $(ID).addClass('rowColor');

        $(ID).closest('tr').find('.selected').css('background-color', "#D4E6F1");

        Populate(TabId);
    }
}
//POPULATE
function Populate(TabId) {
    if (TabId != "") {
        var E = "{TabId: '" + TabId + "'}";
        $.ajax({
            url: '/Accounts_Form/AccountBudgetMapping/BudgetMapping',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                var table = $('#tbl_Account');
                var thead = table.find('thead');
                var tbody = table.find('tbody');

                tbody.find('.myData').empty();
                var html = "";
                if (t.length > 0) {
                    var k = 0;
                    for (var i = 0; i < t.length; i++) {
                        k = k + 1;
                        html += "<tr class='myData'>"
                            + "<td style='display:none;' class='clsDetailID'>" + t[i].DetailID + "</td>"
                            + "<td style='display:none;' class='clsTabId'>" + t[i].TabId + "</td>"
                            + "<td style='display:none;' class='clsBDesc'>" + t[i].BDesc + "</td>"
                            + "<td style='display:none;' class='clsAccountCode'>" + t[i].AccountCode + "</td>"
                            + "<td >" + k + "." + "</td>"
                            + "<td style='cursor:pointer' class='clsAccCodeDesc' onClick='UpdateDetail(this);'>" + t[i].AccountDescription + "</td>"
                            + "<td  style='text-align:center;' class='clsExpType'>" + t[i].ExpType + "</td>"
                            + "<td style='text-align:center; width:10%'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; ' onclick='Delete_Row(this)' /></td>"
                        html + "</tr>";
                    }
                    $('#tbl_Account tbody').append(html);

                    $("[id*=tbl_Account] tr").not(':first').hover(
                        function () { $(this).css("background", "#D4E6F1"); },
                        function () { $(this).css("background", ""); });
                }

            }
        });
    }
}
function Delete_Row(ID) {

    var detailID = $(ID).closest('tr').find('.clsDetailID').text(); 

    swal({
        title: "Warning",
        text: "Are you sure want to delete ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{detailID: '" + detailID + "'}";
                $.ajax({
                    url: '/Accounts_Form/AccountMapping/DeleteMapping',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: E,
                    dataType: 'json',
                    success: function (data) {
                        var t = data.Data; 
                        if (t == "success") {
                            GetDetail(ID);
                        }
                    }
                });
            }

        });
}

function UpdateDetail(ID)
{
    var DetailID = $(ID).closest('tr').find('.clsDetailID').text();

    var TabId = $(ID).closest('tr').find('.clsTabId').text();
    var TabDesc = $(ID).closest('tr').find('.clsBDesc').text();

    var AccCode = $(ID).closest('tr').find('.clsAccountCode').text();
    var AccCodeDesc = $(ID).closest('tr').find('.clsAccCodeDesc').text();

    var ExpType = $(ID).closest('tr').find('.clsExpType').text();

    $('#hdnDetailID').val(DetailID);
    $('#txtTabId').val(TabDesc); $('#hdnTabId').val(TabId);
    $('#txtAccountCode').val(AccCodeDesc); $('#hdnAccountCode').val(AccCode); $('#ddlExpStatutory').val(ExpType);

    $('#txtTabId').prop('disabled', false);

}


function SearchTable() {
    var forSearchprefix = $("#input-search").val().trim().toUpperCase();
    var tablerow = $('#tbl_Budget').find('.myData');
    $.each(tablerow, function (index, value) {
        var clsTabDesc = $(this).find('.clsTabDesc').text().toUpperCase();

        if (clsTabDesc.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}



