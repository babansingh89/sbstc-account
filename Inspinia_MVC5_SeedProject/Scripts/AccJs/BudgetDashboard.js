﻿var ArrayOpenList = [];

$(document).ready(function () {

    Populate();

    $("#txtDashReceiptType").autocomplete({

        source: function (request, response) {

            var budgetTypeID = $('#ddlDashBudgetType').val();
            var catID = $('#ddlDashCategory').val();
            var subcatID = $('#ddlDashSubCategory').val();
            var SectorID = $('#ddlSector').val();
            if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

            var E = "{TrasactionType: '" + "Particulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtDashReceiptType').val()) + "'}";// alert(E);

            $.ajax({
                type: "POST",
                url: '/Accounts_Form/BudgetMaster/Get_Particulars',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {

                            AutoComplete.push({
                                label: item.BudgetDescription,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                        //PopulateGrid(serverResponse.Data);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#txtDashReceiptType').val(i.item.label);
            Populate();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
        });
    $('#txtDashReceiptType').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtDashReceiptType").val('');
            Populate();
        }
        if (iKeyCode == 46) {
            $("#txtDashReceiptType").val('');
            Populate();
        }
    });

    var sectorID = localStorage.getItem("SectorID");
    var FinYearstorage = localStorage.getItem("FinYear");
    FinYear(sectorID, function (finyears) {
        $('#ddlBFinYear').find('option:contains("' + FinYearstorage + '")').prop('selected', true);
    });

});
function FinYear(sectorId, callback) {
    var S = "{SectorID:'" + sectorId + "'}";
    $.ajax({
        //url: '/Accounts_Form/AccountMaster/FinYear',
        url: '/Accounts_Form/BudgetMaster/FinYear',
        type: 'POST',
        data: S,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = response.Data;

            if (data.length > 0) {
                var ctlr_ddlGlobalFinYear = $('#ddlBFinYear');
                var ctlr_ddlDashGlobalFinYear = $('#ddlDFinYear');
                //var ctlr_ddlCGlobalFinYear = $('#ddlCFinYear');
                var ctlr_ddlFGlobalFinYear = $('#ddlFFinYear');
                var ctlr_ddlPGlobalFinYear = $('#ddlPFinYear');

                ctlr_ddlGlobalFinYear.empty();
                ctlr_ddlDashGlobalFinYear.empty();
                //ctlr_ddlCGlobalFinYear.empty();
                ctlr_ddlFGlobalFinYear.empty();
                ctlr_ddlPGlobalFinYear.empty();

                $(data).each(function (index, item) {
                    ctlr_ddlGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                    ctlr_ddlDashGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                    //ctlr_ddlCGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                    ctlr_ddlFGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                    ctlr_ddlPGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                });
                ctlr_ddlGlobalFinYear.append('<option value=' + 'O' + '>2020-2021</option>');
                //ctlr_ddlDashGlobalFinYear.append('<option value=' + 'O' + '>2020-2021</option>');
                //ctlr_ddlCGlobalFinYear.append('<option value=' + 'O' + '>2020-2021</option>');
                ctlr_ddlFGlobalFinYear.append('<option value=' + 'O' + '>2020-2021</option>');
                ctlr_ddlPGlobalFinYear.append('<option value=' + 'O' + '>2020-2021</option>');
                callback(data);
            }
        }
    });
}
function Get_DashCondition() {
    //$('#ddlDashCategory').val('');
    //var budgetTypeID = $('#ddlDashBudgetType').val(); 
    //if (budgetTypeID == 'R') {
    //    $('.clsDashCatSubCat').css('display', '');
    //    $('#ddlDashCategory').val('');
    //    $('#ddlDashSubCategory').val('');
    //    $('#txtDashReceiptType').val('');
    //}
    //else {
    //    $('.clsDashCatSubCat').css('display', 'none');
    //   // $('#ddlDashCategory').val('E');
    //    $('#ddlDashSubCategory').val('');
    //    $('#txtDashReceiptType').val('');
    //}
    Populate();
}
function Populate() {

    var budgetTypeID = $('#ddlDashBudgetType').val();
    var catID = $('#ddlDashCategory').val();
    var subcatID = $('#ddlDashSubCategory').val();
    var SectorID = $('#ddlSector').val();
    if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

    var FinYear = ($("#ddlDFinYear option:selected").text() == "" || $("#ddlDFinYear option:selected").text() == "FinYear") ? localStorage.getItem("FinYear") : $("#ddlDFinYear option:selected").text();

    var E = "{TrasactionType: '" + "Dashboard" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtDashReceiptType').val()) + "' , FinYear: '" + $.trim(FinYear) + "'}"; 

    if (budgetTypeID != "") {
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_Dashboard',
            data: E,
            async:false,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var t = data.Data; 
                $('#page-wrapper').toggleClass('sk-loading');
                BindGrids(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }
}
function BindGrids(detail) {

    var table = $("#myDashboard");
    table.find('thead').empty();
    table.find('tbody').empty();

    var html = ""; var headers = [];

    var color1 = "", color2 = "", color3 = "", color4 = "", color5 = ""; color11 = "", color22 = "", color33 = "", color44 = "", color55 = "";
    var color6 = "", color7 = "", color8 = "", color9 = "", color10 = ""; color66 = "", color77 = "", color88 = "", color99 = "", color1010 = "";
   
    if (detail.length > 0) {

        html += "<tr>";
        var tt = detail[0]; 
        var k = 0; var s = 0; var G1 = 0, G2=0, backColor=""; 
        for (var j = 0; j < (Object.keys(tt)).length; j++) {

            var Columns = Object.keys(tt)[j]; 

            headers.push(Object.keys(tt)[j]);

            if (j == 0 || j == 1 || j == 2 || Columns == "G1" || Columns == "G2")
                html += "<th style='display:none;'>" + Columns + "</th>";
            else {
                
                html += "<th>" + Columns + "</th>";
            }
       
            if (j == (Object.keys(tt)).length - 1)
            {
                html += "<th>" + "" + "</th>";
            }
           
        }
        html += "</tr>";
        table.find('thead').append(html);
            
        $.each(detail, function (index, row) {
                     
          
            var bodyrow = "<tr style='cursor:pointer;'>";
            $.each(headers, function (i, v) {

                if (v == "G1") {
                    G1 = 1;
                    var a = row[v];

                    if (a <= 10)
                    { color1 = "progress-bar-success"; color2 = ""; color3 = ""; color4 = ""; color5 = ""; color11 = ""; color22 = ""; color33 = ""; color44 = ""; color55 = "";}
                    if (a > 10 && a <= 20)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = ""; color4 = ""; color5 = ""; color11 = ""; color22 = ""; color33 = ""; color44 = ""; color55 = ""; }
                    if (a > 20 && a <= 30)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = ""; color5 = ""; color11 = ""; color22 = ""; color33 = ""; color44 = ""; color55 = ""; }
                    if (a > 30 && a <= 40)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = ""; color11 = ""; color22 = ""; color33 = ""; color44 = ""; color55 = ""; }
                    if (a > 40 && a <= 50)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = ""; color22 = ""; color33 = ""; color44 = ""; color55 = ""; }
                    if (a > 50 && a <= 60)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = ""; color33 = ""; color44 = ""; color55 = ""; }
                    if (a > 60 && a <= 70)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = "progress-bar-warning"; color33 = "progress-bar-warning"; color44 = "progress-bar-warning"; color55 = "progress-bar-warning"; }
                    if (a > 70 && a <= 80)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = "progress-bar-warning"; color33 = "progress-bar-warning"; color44 = "progress-bar-warning"; color55 = "progress-bar-warning"; }
                    if (a > 80 && a <= 90)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = "progress-bar-success"; color33 = "progress-bar-danger"; color44 = "progress-bar-danger"; color55 = "progress-bar-danger"; }
                    if (a > 90 && a < 100)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = "progress-bar-success"; color33 = "progress-bar-danger"; color44 = "progress-bar-danger"; color55 = "progress-bar-danger"; }
                    if (a == 100)
                    { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success"; color11 = "progress-bar-success"; color22 = "progress-bar-success"; color33 = "progress-bar-success"; color44 = "progress-bar-success"; color55 = "progress-bar-success"; }
                    if (a > 100)
                    { color1 = "progress-bar-danger"; color2 = "progress-bar-danger"; color3 = "progress-bar-danger"; color4 = "progress-bar-danger"; color5 = "progress-bar-danger"; color11 = "progress-bar-danger"; color22 = "progress-bar-danger"; color33 = "progress-bar-danger"; color44 = "progress-bar-danger"; color55 = "progress-bar-danger"; }


                    //if (a >= 20 && a <= 40)
                    //{ color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = ""; color4 = ""; color5 = ""; }

                    //if (a >= 40 && a <= 60) {
                    //    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = ""; color5 = "";
                    //}
                    //if (a >= 60 && a <= 80) {
                    //    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-warning"; color5 = "progress-bar-warning";
                    //}
                    //if (a >= 80 && a < 100) {
                    //    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-danger";
                    //}
                    //if (a == 100) {
                    //    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success";
                    //}
                    //if (a > 100) {
                    //    color1 = "progress-bar-danger"; color2 = "progress-bar-danger"; color3 = "progress-bar-danger"; color4 = "progress-bar-danger"; color5 = "progress-bar-danger";
                    //}
                }
                if (v == "G2") {
                    G2 = 1;
                    var b = row[v];

                    if (b <= 10)
                    { color6 = "progress-bar-success"; color7 = ""; color8 = ""; color9 = ""; color10 = ""; color66 = ""; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 10 && b <= 20)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = ""; color9 = ""; color10 = ""; color66 = ""; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 20 && b <= 30)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = ""; color10 = ""; color66 = ""; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 30 && b <= 40)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = ""; color66 = ""; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 40 && b <= 50)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = ""; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 50 && b <= 60)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = ""; color88 = ""; color99 = ""; color1010 = ""; }
                    if (b > 60 && b <= 70)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = "progress-bar-warning"; color88 = "progress-bar-warning"; color99 = "progress-bar-warning"; color1010 = "progress-bar-warning"; }
                    if (b > 70 && b <= 80)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = "progress-bar-warning"; color88 = "progress-bar-warning"; color99 = "progress-bar-warning"; color1010 = "progress-bar-warning"; }
                    if (b > 80 && b <= 90)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = "progress-bar-success"; color88 = "progress-bar-danger"; color99 = "progress-bar-danger"; color1010 = "progress-bar-danger"; }
                    if (b > 90 && b < 100)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = "progress-bar-success"; color88 = "progress-bar-danger"; color99 = "progress-bar-danger"; color1010 = "progress-bar-danger"; }
                    if (b == 100)
                    { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success"; color66 = "progress-bar-success"; color77 = "progress-bar-success"; color88 = "progress-bar-success"; color99 = "progress-bar-success"; color1010 = "progress-bar-success"; }
                    if (b > 100)
                    { color6 = "progress-bar-danger"; color7 = "progress-bar-danger"; color8 = "progress-bar-danger"; color9 = "progress-bar-danger"; color10 = "progress-bar-danger"; color66 = "progress-bar-danger"; color77 = "progress-bar-danger"; color88 = "progress-bar-danger"; color99 = "progress-bar-danger"; color1010 = "progress-bar-danger"; }
                    //if (b <= 20)
                    //{ color6 = "progress-bar-success"; color7 = ""; color8 = ""; color9 = ""; color10 = ""; }

                    //if (b >= 20 && b <= 40)
                    //{ color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = ""; color9 = ""; color10 = ""; }

                    //if (b >= 40 && b <= 60) {
                    //    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = ""; color10 = "";
                    //}
                    //if (b >= 60 && b <= 80) {
                    //    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-warning"; color10 = "progress-bar-warning";
                    //}
                    //if (b >= 80 && b < 100) {
                    //    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-danger";
                    //}
                    //if (b == 100) {
                    //    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success";
                    //}
                    //if (b > 100) {
                    //    color6 = "progress-bar-danger"; color7 = "progress-bar-danger"; color8 = "progress-bar-danger"; color9 = "progress-bar-danger"; color10 = "progress-bar-danger";
                    //}
                }
             
                if (i == 0 || i == 1 || i == 2 || v == "G1" || v == "G2")
                    bodyrow += "<td style='display:none;'>" + row[v] + "</td>";
                else if (v == "Actual Expense")
                    bodyrow += "<td style='color:blue; text-align:right;' onclick='Execute(this)'>" + row[v] + "</td>";
                else if (v == "FC Given")
                    bodyrow += "<td style='color:blue; text-align:right;' onclick='Execute(this)'>" + row[v] + "</td>";
                else if (v == "Actual Income")
                    bodyrow += "<td style='color:blue; text-align:right;' onclick='Execute(this)'>" + row[v] + "</td>";
                else {
                    if ($.isNumeric(row[v])) 
                    {
                        bodyrow += "<td style='text-align:right;' onclick='Execute(this)'>" + row[v] + "</td>";
                    }
                    else
                    {
                        bodyrow += "<td>" + row[v] + "</td>";
                    }
                }

                if (i + 1 == headers.length)
                {
                    if (G1 == 1 && G2 == 1) {
                        bodyrow += "<td style='width:12%'><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                            "<div class='progress-bar " + color1 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color2 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color3 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color4 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color5 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color11 + "' role= 'progressbar' style= 'width:10%;' ></div >" +
                            "<div class='progress-bar " + color22 + "' role= 'progressbar' style= 'width:10%;' ></div >" +
                            "<div class='progress-bar " + color33 + "' role= 'progressbar' style= 'width:10%;' ></div >" +
                            "<div class='progress-bar " + color44 + "' role= 'progressbar' style= 'width:10%;' ></div >" +
                            "<div class='progress-bar " + color55 + "' role= 'progressbar' style= 'width:10%;' ></div ></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>FC</div></div > " +

                            "<div><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                            "<div class='progress-bar " + color6 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color7 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color8 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color9 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color10 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color66 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color77 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color88 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color99 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color1010 + "' role='progressbar' style='width:10%;'></div></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>Exp</div></div > " +
                            "</td >";
                    }
                    else
                    {
                        bodyrow += "<td style='width:12%'><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                            "<div class='progress-bar " + color1 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color2 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color3 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color4 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color5 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color11 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color22 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color33 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color44 + "' role='progressbar' style='width:10%;'></div>" +
                            "<div class='progress-bar " + color55 + "' role='progressbar' style='width:10%;'></div></div > <div style='float:right;  font-size:10px; width:20px'>FC</div></div ></td > ";
                    }
                }

            });
            bodyrow += "</tr>";
            table.find('tbody').append(bodyrow);
        });
        
    }
}

function Execute(ID)
{
    var E = '', URL = '', BudgetID = '', con=0;
    var column_num = parseInt($(ID).index()) + 1;
    var row_num = parseInt($(ID).parent().index()) + 1;

    var th = $('#myDashboard th').eq($(ID).index());
    var MainHeaderText = $(th).text(); 

    $("#myDashboard thead tr th").each(function () {
        var columnText = $(this).text();
        if (columnText == 'AutoTrans') {
            var AutoTrans = $(ID).closest('tr').find('.AutoTrans').text(); 
            var MemoNo = $(ID).closest('tr').find('.MemoNo').text(); 
            E = "{TrasactionType: '" + "MemoWithFC" + "', BudgetType: '" + AutoTrans + "', CategoryID: '" + "B" + "', BudgetID: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + MemoNo + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";// alert(E);
            URL = '/Accounts_Form/BudgetMaster/Get_MemoFC';
            con = 1;
        }
        if (columnText == 'VoucherNumber') {
            var VoucherNumber = $(ID).closest('tr').find('.VoucherNumber').text();
            window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank');
            con = 0;
        }
    });
    
    if (MainHeaderText == 'Actual Expense') {
        BudgetID = $('#myDashboard tr:eq(' + row_num + ') td:eq(' + 2 + ')').text();
        E = "{TrasactionType: '" + "MemoWithFC" + "', BudgetType: '" + "" + "', CategoryID: '" + "A" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + "" + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";// alert(E);
        URL = '/Accounts_Form/BudgetMaster/Get_MemoFC';
        con = 1;
    }
    if (MainHeaderText == 'FC Given') {
        BudgetID = $('#myDashboard tr:eq(' + row_num + ') td:eq(' + 2 + ')').text(); 
        $("#hdnDashConBudgetID").val(BudgetID),
        Bind_FCGivenDetail(BudgetID);
        return false;
    }
    if (MainHeaderText == 'Actual Income') {
        BudgetID = $('#myDashboard tr:eq(' + row_num + ') td:eq(' + 2 + ')').text(); 
        var FinYear = ($("#ddlDFinYear option:selected").text() == "" || $("#ddlDFinYear option:selected").text() == "FinYear") ? localStorage.getItem("FinYear") : $("#ddlDFinYear option:selected").text();
        E = "{TrasactionType: '" + "Receipt" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + FinYear + "'}";// alert(E);
        URL = '/Accounts_Form/BudgetMaster/Get_BudgetDetails';
        con = 1;
   }

    if (con == 1) {
        $.ajax({
            type: "POST",
            url: URL,
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                con = 0;
                var t = data.Data; 
                ArrayOpenList.push($('#myDash').html());
                Bind_ActualExpense(t);
            }
        });
    }
}
function Bind_ActualExpense(data)
{
    var table = $("#myDashboard");
    table.find('thead').empty();
    table.find('tbody').empty();
    table.find('tfoot').empty();

    var html = ""; var TotalMemoAmt = 0; var TotalVchAmt = 0; var headers = [];
  
    if (data.length > 0)
    {
        html += "<tr>";
        var tt = data[0]; 

         //HEADER BINDING ------------------------------------------------------------------------------------
        for (var j = 0; j < (Object.keys(tt)).length; j++) {

            var Columns = Object.keys(tt)[j];

            headers.push(Object.keys(tt)[j]);
            if (Columns == "AutoTrans")
                html += "<th style='display:none;' >" + Columns + "</th>";
            else if (Columns == "VoucherNumber")
                html += "<th style='display:none;' >" + Columns + "</th>";
            else 
                html += "<th>" + Columns + "</th>";
        }
        html += "</tr>";
        table.find('thead').append(html);

        //DATA BINDING ------------------------------------------------------------------------------------
        $.each(data, function (index, row) {
            var bodyrow = "<tr style='cursor:pointer;' onclick='Execute(this)'>";
            $.each(headers, function (i, v) {

                if (v == "AutoTrans")
                    bodyrow += "<td style='display:none;' class='AutoTrans'>" + row[v] + "</td>";
                else if (v == "MemoNo")
                    bodyrow += "<td class='MemoNo'>" + row[v] + "</td>";
                else if (v == "VoucherNumber")
                    bodyrow += "<td style='display:none;' class='VoucherNumber'>" + row[v] + "</td>";
                else if (v == "Amount")
                {
                    TotalMemoAmt = parseFloat(TotalMemoAmt) + parseFloat(row[v]);
                    bodyrow += "<td style='text-align:right;' >" + row[v] + "</td>";
                }
                else if (v == "VoucherAmount") {
                    TotalVchAmt = parseFloat(TotalVchAmt) + parseFloat(row[v]);
                    bodyrow += "<td style='text-align:right;' >" + row[v] + "</td>";
                }
                else {
                    if ($.isNumeric(row[v])) {
                        bodyrow += "<td style='text-align:right;' >" + row[v] + "</td>";
                    }
                    else {
                        bodyrow += "<td>" + row[v] + "</td>";
                    }
                }

            });
            bodyrow += "</tr>";
            table.find('tbody').append(bodyrow);
           
        });
        if (parseFloat(TotalMemoAmt) > 0)
            table.find('tfoot').append("<tr style='background-color:#DFDDD9'><td colspan='4' style='font-weight:bold; text-align:right;'>Total <span>(&#x20b9;)</span></td><td style='text-align:right;'><span class='fTotal' >" + TotalMemoAmt.toFixed(2) + "</span></td></tr>");
        if (parseFloat(TotalVchAmt) > 0)
            table.find('tfoot').append("<tr style='background-color:#DFDDD9'><td colspan='3' style='font-weight:bold; text-align:right;'>Total <span>(&#x20b9;)</span></td><td style='text-align:right;'><span class='fTotal' >" + TotalVchAmt.toFixed(2) + "</span></td></tr>");
    }
    else
    {
        table.find('tbody').append("<tr style='background-color:#DFDDD9'><td  style='font-weight:bold; text-align:center;'>No Data found !!</td></tr>");
    }
}
function Back() {
   
    if (ArrayOpenList.length > 0) {
        var t = ArrayOpenList.pop(); 
        if (t != "") {
            $('#myDash').html(t);
        }
    }
    $("#txtDashConEffectiveDate").val(''); $("#txtDashConReferanceNo").val(''); $("#txtDashConBudgetAmt").val('');
    $("#txtDashConRemarks").val(''); $("#hdnDashConBudgetID").val(''); $("#hdnDashConBudgetCode").val('');
    $("#hdnDashConBudgetAmount").val(''); $("#hdnDashConAmount").val(''); $("#hdnDashConBalAmount").val('');
    $("#hdnDashConSlNo").val(''); $("#hdnDashFinancialLimit").val(''); $(".msgDashLimitExceeded").text(''); 
    $('.clsCon').css('display', 'none');
}

function PopUP_DashConBudgetDetail(ID) {

    $('#txtConReferanceNo').val(''); $('#txtConBudgetAmt').val(''); $('#txtConRemarks').val('');

    var BudgetID = $(ID).closest('tr').find('.BudgetID').text();
    var BudgetCode = $(ID).closest('tr').find('.clsBudgetCode').val();
    var BudgetAmount = $(ID).closest('tr').find('.clsBudgetAmt').text();
    var ConAmount = $(ID).closest('tr').find('.clsConAmount').text();
    var BalAmount = $(ID).closest('tr').find('.clsBalAmt').text();

    $('#hdnConBudgetID').val(BudgetID);
    $('#hdnConBudgetCode').val(BudgetCode);
    $('#hdnConBudgetAmount').val(BudgetAmount);
    $('#hdnConAmount').val(ConAmount);
    $('#hdnConBalAmount').val(BalAmount);

    Bind_ConBudgetDetail(BudgetID);
    $("#dialog-ConBudgetdetail").modal("show");
}
function Bind_FCGivenDetail(BudgetID) {
    if (BudgetID != "") {
        var E = "{TrasactionType: '" + "ConSelect" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + $("#ddlDFinYear option:selected").text() + "'}"; 
      
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; 
                ArrayOpenList.push($('#myDash').html());
                Show_FCGivenDetail(t);
            }
        });
    }

}
function Show_FCGivenDetail(data) {
    var html = "", TotalAmt = 0;

    var table = $("#myDashboard");
    table.find('thead').empty();
    table.find('tbody').empty();
    table.find('tfoot').empty();

   
    if (data.length > 0) {
        table.find('thead').append("<tr><th>Date</th><th>Concurrance No.</th><th>Amount</th><th>Remarks</th><th></th></tr>");
        for (var i = 0; i < data.length; i++) {
            TotalAmt = parseFloat(TotalAmt) + parseFloat(data[i].ConcurranceAmount);
            html += "<tr class='myData' >"
                + "<td class='conSlNo' style='display:none;'>" + data[i].ConcurranceSlNo + "</td>"
                + "<td class='effDate' style='text-align:center;'>" + data[i].ConcurranceDate + "</td>"
                + "<td class='refNo' style='text-align:center;'>" + data[i].ReferanceNo + "</td>"
                + "<td  class='bugtAmt' style='text-align:right;'>" + data[i].ConcurranceAmount + "</td>"
                + "<td class='remarks'>" + (data[i].Remarks == null ? "" : data[i].Remarks) + "</td>"
                + "<td style='text-align:center'>"
                + "<img src= '/Content/Images/Edit.png' title= 'Edit' style= 'width:23px;height:21px;cursor:pointer; text-align:center;' onClick= 'EditDashConBudgetDetail(this);' />&nbsp;&nbsp;"
                + "<img src= '/Content/Images/printer.png' title='Print' style='width:18px;height:19px;cursor:pointer; text-align:center;' onClick='PrintDashConBudgetDetail(this);' />&nbsp;&nbsp;"
                + "<img src= '/Content/Images/Transfer.png' title='Transfer' style='width:18px;height:18px;cursor:pointer; text-align:center;' onClick='TransferDashConBudgetDetail(this);' /><br/>"
                + "<select selected value='' style='margin-top:6px; width:100%; display:none;' class='clsSector' onchange='BindDashBudget(this)'><option>Select</option></select>"
                + "<select selected value='' style='margin-top:6px; width:100%; display:none;' class='clsBudget' onchange='ConfirmDash_Transfer(this)'><option>Select</option></select> </td > "

            html + "</tr>";
        }
        table.find('tbody').append(html);

        if ( parseFloat(TotalAmt) > 0)
        table.find('tfoot').append("<tr style='background-color:#DFDDD9'><td colspan='2' style='font-weight:bold; text-align:right;'>Total <span>(&#x20b9;)</span></td><td style='text-align:right;'><span class='fTotal' >" + TotalAmt.toFixed(2) + "</span></td><<td colspan='2'></td></tr>");

        BindSector();
    }
    else {
        table.find('tbody').append("<tr style='background-color:#DFDDD9'><td  style='font-weight:bold; text-align:center;'>No Data found !!</td></tr>");
    }
}
function DashCheckLimit() {

    var limitAmt = $("#hdnDashFinancialLimit").val();
    var budgtAmt = $("#txtDashConBudgetAmt").val();

    if (parseFloat(limitAmt) > 0) {
        $("#txtDashConBudgetAmt").prop('disabled', false);
        if (parseFloat(budgtAmt) > parseFloat(limitAmt)) {
            $('.msgDashLimitExceeded').css('display', ""); $("#txtDashConBudgetAmt").val(limitAmt);
        }

        else {
            $('.msgDashLimitExceeded').css('display', "none");
        }
    }
    else {
        $("#txtDashConBudgetAmt").prop('disabled', true);
    }
}
function Add_DashSaveConDetail() {
  
    var TotalAmount = 0;
    var limitAmt = $("#hdnDashFinancialLimit").val();
    var bugtAmt = $('#txtDashConBudgetAmt').val();
    var effDate = $('#txtDashConEffectiveDate').val();
    var refNo = $('#txtDashConReferanceNo').val();
    var remarks = $('#txtDashConRemarks').val();
    var ConSlNo = $('#hdnDashConSlNo').val();

  
    if (parseFloat(limitAmt) <= 0) {
        $("#txtDasConBudgetAmt").prop('disabled', true); return false;
    }
  
    if (bugtAmt >= 0) {
        //if (bugtAmt == "" || bugtAmt <= 0) {
        //    $("#txtConBudgetAmt").attr("placeholder", "Enter amount"); $("#txtConBudgetAmt").addClass("Red"); $('#txtConBudgetAmt').focus();
        //    $('.msgLimitExceeded').css('display', "none"); return false;
        //}
        if (effDate == "") {
            $("#txtDasConEffectiveDate").attr("placeholder", "Enter date"); $("#txtDasConEffectiveDate").addClass("Red"); $('#txtDasConEffectiveDate').focus(); return false;
        }

        var grdLen = $('#myDashboard tbody tr.myData').length;
        if (grdLen > 0) {
            $('#myDashboard tbody tr.myData').each(function () {

                var buAmt = $(this).find('.bugtAmt').text();
                TotalAmount = parseFloat(TotalAmount) + parseFloat(buAmt);

            });
        }
        else
            TotalAmount = parseFloat(bugtAmt);


        var BudgetAmt = $('#hdnDashConBudgetAmount').val(); //alert(TotalAmount); alert(BudgetAmt);
        if ((parseFloat(TotalAmount) + parseFloat(bugtAmt)) > parseFloat(BudgetAmt)) {
            $('.dvDashErrorMsg').css('display', '');
            $('#dasherrorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
        }
        else {
            $('.dvDashErrorMsg').css('display', 'none');
            $('#dasherrorMsg').text('');
        }

        var E = "{BudgetID: '" + $('#hdnDashConBudgetID').val() + "', SectorID: '" + $('#ddlSector').val() + "', ConSlNo: '" + ConSlNo + "',   bugtAmt: '" + bugtAmt + "', effDate: '" + effDate + "', refNo: '" + refNo + "', remarks: '" + remarks + "'}";
        //alert(E);
        //return false;

        $.ajax({
            url: '/Accounts_Form/BudgetMaster/InsertUpdate_Concurrance',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data;

                $("#hdnDashConSlNo").val(''); $("#txtDashConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtDashConReferanceNo").val(''); $("#txtDashConRemarks").val(''); $("#txtDashConBudgetAmt").focus();
                Show_FCGivenDetail(t);
                $('.clsCon').css('display', 'none');
            }
        });
    }
}
function EditDashConBudgetDetail(ID) {

    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var bugtAmt = $(ID).closest('tr').find('.bugtAmt').text();
    var effDate = $(ID).closest('tr').find('.effDate').text();
    var refNo = $(ID).closest('tr').find('.refNo').text();
    var remarks = $(ID).closest('tr').find('.remarks').text();

    $("#hdnDashConSlNo").val(conSlNo); $("#txtDashConBudgetAmt").val(bugtAmt); $("#txtDashConEffectiveDate").val(effDate); $("#txtDashConReferanceNo").val(refNo); $("#txtDashConRemarks").val(remarks);
    $('.clsCon').css('display', '');
    $(ID).closest('tr').remove();
}
function PrintDashConBudgetDetail(ID) {

    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var refNo = $(ID).closest('tr').find('.refNo').text();

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "ConcurranceReport.rpt",
        FileName: refNo
    });

    detail.push({
        BudgetID: $("#hdnDashConBudgetID").val(),
        SectorID: $("#ddlSector").val(),
        ConcurranceSlNo: conSlNo
    });

    final = {
        Master: master,
        Detail: detail
    }
    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function BindDashBudget(ID) {
    var secid = $(ID).val();
    if (secid != "") {
        var E = "{TrasactionType: '" + "TransBudgetHead" + "', BudgetID: '" + "" + "', SectorID: '" + secid + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('.clsBudget').empty();
                $('.clsBudget').append('<option value="" selected>Select</option');
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('.clsBudget').append('<option value=' + item.BudgetID + '>' + item.BudgetDescription + '</option>');
                    });
                    $('.clsBudget').val($('#hdnDashConBudgetID').val());
                }
            }
        });
    }
}
function TransferDashConBudgetDetail(ID) {

    $('#myDashboard tbody tr.myData').find('.clsSector').css('display', 'none');
    $('#myDashboard tbody tr.myData').find('.clsBudget').css('display', 'none');
    $('.clsBudget').empty();
    $('.clsBudget').append('<option value="" selected>Select</option');

    $(ID).closest('tr').find('.clsSector').css('display', '');
    $(ID).closest('tr').find('.clsBudget').css('display', '');

}
function ConfirmDash_Transfer(ID) {

    var refNo = $(ID).closest('tr').find('.refNo').text();

    swal({
        title: "Warning",
        text: "Are you sure want to Transfer FCNo. \n" + refNo,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                TransferDash_FC(ID);
            }

        });
}
function TransferDash_FC(ID) {

    var BudgetID = $(ID).closest('tr').find('.clsBudget').val();
    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var refNo = $(ID).closest('tr').find('.refNo').text();
    var secID = $(ID).closest('tr').find('.clsSector').val();


    if (BudgetID == "") { $(ID).closest('tr').find('.clsBudget').focus(); return false; }

    var E = "{BudgetID: '" + $('#hdnDashConBudgetID').val() + "', TransBudgetID: '" + BudgetID + "', SectorID: '" + secID + "', CurrSectorID: '" + $('#ddlSector').val() + "', conSlNo: '" + conSlNo + "', refNo: '" + refNo + "'}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BudgetMaster/Transfer_Concurrance',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var msg = t[0]["MSG"]; //alert(msg);
            var msgCode = t[0]["ResultCount"]; alert(msgCode);
            alert('ababan');
            if (msgCode == "1")
            {
                alert("FC No. transfer successfully.");
                $("#hdnDashConSlNo").val(''); $("#txtDashConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtDashConReferanceNo").val(''); $("#txtDashConRemarks").val(''); $("#txtDashConBudgetAmt").focus();
                Bind_FCGivenDetail(BudgetID);
            }
            else
            {
                alert(msg);
                $("#hdnDashConSlNo").val(''); $("#txtDashConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtDashConReferanceNo").val(''); $("#txtDashConRemarks").val(''); $("#txtDashConBudgetAmt").focus();
                Bind_FCGivenDetail(BudgetID);
            }
        }
    });

}

function BindGrid(detail)
{
  
    var table = $("#myDashboard");
    table.find('thead').empty();
    table.find('tbody').empty();

    if (detail.length > 0)
    {
        var Hhtml = ""; var Ihtml = "", ITotal = 0, ETotal = 0;

        Hhtml += "<tr><td></td>" +
                 "<td colspan='7' style='text-align:center;'>CORPORATE</td>" +
                 "</tr > ";
        Hhtml += "<tr><td colspan='1'>Particulars</td>" +
            "<td style='text-align:center'>Income/Expense</td>" +
            "<td style='text-align:center'>Budget Amt</td>" +
            "<td style='text-align:center'>FC Amt</td>" +
            "<td style='text-align:center'>Balance after FC</td>" +
            "<td style='text-align:center'>Actual Expense</td>" +
            "<td style='text-align:center'>Balance after actual Expense</td>" +
            "<td style='text-align:center'></td>" +
            "</tr > ";

        table.find('thead').append(Hhtml);

        for (var i = 0; i < detail.length; i++) {

            var bAmt = parseFloat(detail[i].BudgetAmount);
            var fcAmt = parseFloat(detail[i].ConcurranceAmount);
            var baeAmt = parseFloat(detail[i].AfterActualExp);
            var a = (fcAmt * 100) / bAmt; //alert(a);
            var b = (baeAmt * 100) / bAmt; //alert(b);
            var color1 = "", color2 = "", color3 = "", color4 = "", color5 = "";
            var color6 = "", color7 = "", color8 = "", color9 = "", color10 = "";

            if (parseFloat(fcAmt) > 0) {

                if (a <= 20 && a <= 20)
                { color1 = "progress-bar-success"; color2 = ""; color3 = ""; color4 = ""; color5 = ""; }

                if (a > 20 && a <= 40)
                { color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = ""; color4 = ""; color5 = ""; }

                if (a > 40 && a <= 60) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = ""; color5 = "";
                }
                if (a > 60 && a <= 80) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-warning"; color5 = "progress-bar-warning";
                }
                if (a > 80 && a < 100) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-danger";
                }
                if (a == 100) {
                    color1 = "progress-bar-success"; color2 = "progress-bar-success"; color3 = "progress-bar-success"; color4 = "progress-bar-success"; color5 = "progress-bar-success";
                }
                if (a > 100) {
                    color1 = "progress-bar-danger"; color2 = "progress-bar-danger"; color3 = "progress-bar-danger"; color4 = "progress-bar-danger"; color5 = "progress-bar-danger";
                }
            }
            else
            {
                color1 = ""; color2 = ""; color3 = ""; color4 = ""; color5 = "";
            }
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if (parseFloat(baeAmt) > 0) {

                if (b > 20 && b <= 40)
                { color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = ""; color9 = ""; color10 = ""; }

                if (b > 40 && b <= 60) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = ""; color10 = "";
                }
                if (b > 60 && b <= 80) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-warning"; color10 = "progress-bar-warning";
                }
                if (b > 80 && b < 100) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-danger";
                }
                if (b == 100) {
                    color6 = "progress-bar-success"; color7 = "progress-bar-success"; color8 = "progress-bar-success"; color9 = "progress-bar-success"; color10 = "progress-bar-success";
                }
                if (b > 100) {
                    color6 = "progress-bar-danger"; color7 = "progress-bar-danger"; color8 = "progress-bar-danger"; color9 = "progress-bar-danger"; color10 = "progress-bar-danger";
                }
            }
            else {
                color6 = ""; color7 = ""; color8 = ""; color9 = ""; color10 = "";
            }


            var BudgetID = detail[i].BudgetID; var row_color = ""; var display = "";
            if (BudgetID == 0)
            {
                row_color = '#C8CDCC';

                if ($('#ddlDashBudgetType').val() == "L")
                display = "none";
            }
            
          

            Ihtml += "<tr class='myData_I' style='background-color:" + row_color + '; display:' + display + "'>"
                    + "<td style='text-align:right; display:none;' class='BudgetID' >" + detail[i].BudgetID + "</td>"
                    + "<td style='text-align:left; '>" + detail[i].BudgetDescription + "</td>"
                    + "<td style='text-align:center; '>" + detail[i].IE + "</td>"
                    + "<td style='text-align:right;' class='clsBudgetAmt' >" + (parseFloat(detail[i].BudgetAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' >" + (parseFloat(detail[i].ConcurranceAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].BalanceAmount)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].ActualExpense)).toFixed(2) + "</td>"
                    + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].AfterActualExp)).toFixed(2) + "</td>"

                    + "<td style='width:12%'><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                    "<div class='progress-bar " + color1 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color2 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color3 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color4 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color5 + "' role='progressbar' style='width:20%;'></div></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>FC</div></div>" +

                    "<div><div ><div class='progress' style='height:10px !important; margin-bottom: 2px; float:left; width:80%'>" +
                    "<div class='progress-bar " + color6 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color7 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color8 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color9 + "' role='progressbar' style='width:20%;'></div>" +
                    "<div class='progress-bar " + color10 + "' role='progressbar' style='width:20%;'></div></div > <div style='float:right; margin-top:-2px; font-size:10px; width:20px'>Exp</div></div>" +
                    "</td > "

                Ihtml + "</tr>";
            

        }
       table.find('tbody').append(Ihtml);
    }
}