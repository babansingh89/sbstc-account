﻿$(document).ready(function () {
    Auto_Denomination();
    BindGrid();
    TransDate();

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});

function TransDate() {
    $('#txtTransDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtTransDate').val(output);
}

function Auto_Denomination() {
    var TransType = 'Denomination';
    var Denomination = $.trim($('#txtDenomination').val());
    
    $('#txtDenomination').autocomplete({
        source: function (request, response) {
            var S = "{TransType: '" + TransType + "', Denomination:'" + Denomination + "'}";
            $.ajax({
                url: '/Accounts_Form/PaperTkt/Get_Denomination',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.DenominationRemarks,
                                DenominationId: item.DenominationId
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnDenominationID').val(i.item.DenominationId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function Final_Save() {
    var TransType = 'Insert';
    var TicketID = $.trim($('#hdnPaperTktID').val());
    var Denomination = $.trim($('#txtDenomination').val());
    var DenominationID = $.trim($('#hdnDenominationID').val());
    var Series = $.trim($('#txtSeries').val());
    var TicketFrom = $.trim($('#txtTktFrom').val());
    var TicketTo = $.trim($('#txtTktTo').val());
    var TranType = $('#ddlTransType').val();
    var TransDate = $('#txtTransDate').val();
    var Remarks = $.trim($('#txtRemarks').val());
    var UpDn = $.trim($('#ddlUpDn').val());
   
    if (TranType == "") { $('#ddlTransType').focus(); return false; }
    if (TransDate == "") { $("#txtTransDate").attr("placeholder", "Trans date required"); $("#txtTransDate").addClass("Red"); $('#txtTransDate').focus(); return false; }
    if (Denomination == "" || DenominationID == "") { $("#txtDenomination").attr("placeholder", "Select Denomination"); $("#txtDenomination").addClass("Red"); $('#txtDenomination').focus(); return false; }
    if (Series == "") { $("#txtSeries").attr("placeholder", "Series required"); $("#txtSeries").addClass("Red"); $('#txtSeries').focus(); return false; }
    if (TicketFrom == "") { $("#txtTktFrom").attr("placeholder", "Tiket From required"); $("#txtTktFrom").addClass("Red"); $('#txtTktFrom').focus(); return false; }
    if (TicketTo == "") { $("#txtTktTo").attr("placeholder", "Ticket To required"); $("#txtTktTo").addClass("Red"); $('#txtTktTo').focus(); return false; }
    if (UpDn == "") { $("#ddlUpDn").focus(); return false; }
    
    //if (Remarks == "") { $("#txtRemarks").attr("placeholder", "Remarks required"); $("#txtRemarks").addClass("Red"); $('#txtRemarks').focus(); return false; }


  
    var E = "{TicketID:'" + TicketID + "', DenominationID:'" + DenominationID + "',  Series:'" + Series +
        "',  TicketFrom:'" + TicketFrom + "', " + "TicketTo: '" + TicketTo + "', Remarks:'" + Remarks +
        "', TransType:'" + TranType + "', TransDate:'" + TransDate + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() +
        "', SectorID:'" + $("#ddlSector").val() + "', UPDN:'" + UpDn + "'}";
    //alert(E);
    //return false;
    $.ajax({
        url: '/Accounts_Form/PaperTkt/Final_Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var a = data.Data;
            var t = a["Table"];
                swal({
                    title: t[0]["MSGCOUNT"] == '1' ? "Success" : "Warning",
                    text: t[0]["MSG"],
                    type: t[0]["MSGCOUNT"] == '1' ? "success" : "warning",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            if (t[0]["MSGCOUNT"] == '1')
                                window.location.href = "";

                        }
                    });
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function BindGrid()
{
    var E = "{TransType: '" + "AllData" + "', TicketID: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTkt/Get_AllData',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {

            var t = data.Data; //alert(JSON.stringify(t));

            var columnData = [
                { "mDataProp": "DenominationRemarks" },
                { "mDataProp": "UPDN" },
                { "mDataProp": "TktSeries" },
                { "mDataProp": "TktSrlNoFrom" },
                { "mDataProp": "TktSrlNoTo" },
                { "mDataProp": "TransType" },
            {
                "mDataProp": "Edit",
                "render": function (data, type, row) {
                    var str = '<div><select onchange="Execute(this, \'' + row.MstTktId + '\')">';
                    if (row.Edit == 1) {
                        str += "<option value='' >Select</option>";
                        str += "<option value='1'>View</option>";
                        str += "<option value='2' style='display:none'>Edit</option>";
                        str += "<option value='3' style='display:none'>Delete</option>";
                    }
                    else {
                        str += "<option value='' >Select</option>";
                        str += "<option value='1'>View</option>";
                        str += "<option value='2' >Edit</option>";
                        str += "<option value='3'>Delete</option>";
                    }
                    str += "</select></div>";
                    return str;
                }
            }];

            var columnDataHide = [];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [1,5,6],
                        "className": "text-center",
                        "width": "10%"
                    },
                    {
                        "targets": [2,3,4],
                        "className": "text-right",
                        "width": "10%"
                    }
                   ],
                'iDisplayLength': 10,
                destroy: true
            });

        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });


}

function Execute(ID, TktId) {
    var tktID = $(ID).val();
    if (tktID != "")
    {
        //VIEW 
        if (tktID == 1) {
            Edit(TktId);
            $('#btnSave').css('display', 'none');
        }
        //EDIT
        if (tktID == 2) {
            Edit(TktId);
            $('#btnSave').css('display', '');
        }
        //DELETE
        if (tktID == 3) {
            Delete(TktId);
            $('#btnSave').css('display', '');
        }
    }
}
function Edit(TktId)
{
    var E = "{TransType: '" + "AllDData" + "', TicketID: '" + TktId + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTkt/Get_AllData',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data; //alert(JSON.stringify(t));

            if (t.length > 0)
            {
                $('#hdnPaperTktID').val(TktId); 
                $('#ddlTransType').val(t[0].TransType); $('#txtTransDate').val(t[0].TransDate);
                $('#txtDenomination').val(t[0].DenominationRemarks); $('#hdnDenominationID').val(t[0].DenominationId);
                $('#txtSeries').val(t[0].TktSeries); $('#txtTktFrom').val(t[0].TktSrlNoFrom); $('#txtTktTo').val(t[0].TktSrlNoTo);
                $('#txtRemarks').val(t[0].TktRemarks); $('#ddlUpDn').val(t[0].UPDN);
            }
        }
    });
}
function View(TktId)
{
    var E = "{TransType: '" + "AllDData" + "', TicketID: '" + TktId + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTkt/Get_AllData',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data; //alert(JSON.stringify(t));

            if (t.length > 0) {
                var columnData = [
                    { "mDataProp": "TktSrlId" },
                    { "mDataProp": "TktNo" },
                    { "mDataProp": "DenominationAmt" },
                    {
                        "mDataProp": "Edit",
                        "render": function (data, type, row) {
                            var str = '<div><select onchange="DeleteDetail(\'' + row.DatTktId + '\', \'' + TktId + '\')">';
                            str += "<option value=''>Select</option>";
                            str += "<option value='1'>Cancel</option>";
                            str += "</select></div>";
                            return str;
                        }
                    }];

                var columnDataHide = [];

                var oTable = $('#detail').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'ExampleFile' },
                        { extend: 'pdf', title: 'ExampleFile' },
                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": [0,1,2,3],
                            "className": "text-center",
                            "width": "10%"
                        }
                    ],
                    'iDisplayLength': 10,
                    destroy: true
                });

              
            }
        }
    });
}
function Delete(TktId)
{
    swal({
        title: "Warning",
        text: "Are you sure want to Cancel this ticket ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{TransType: '" + "MDelete" + "', TicketID: '" + TktId + "', SectorID: '" + $('#ddlSector').val() + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/PaperTkt/Get_AllData',
                    contentType: "application/json; charset=utf-8",
                    data: E,
                    dataType: "json",
                    success: function (data, status) {
                        var t = data.Data;
                        var msgcnt = t[0]["MSGCOUNT"];
                        var msg = t[0]["MSG"];

                        if (msgcnt == '1') {
                            swal({
                                title: "Success",
                                text: msg,
                                type: "success",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "";
                                    }
                                });
                        }
                        else {
                            swal({
                                title: "Error",
                                text: msg,
                                type: "error",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            });
                        }
                    }
                });
            }

        });
}
function DeleteDetail(DTktId, TktId) {
    swal({
        title: "Warning",
        text: "Are you sure want to Delete this ticket ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{TransType: '" + "DDelete" + "', TicketID: '" + DTktId + "', SectorID: '" + $('#ddlSector').val() + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/PaperTkt/Get_AllData',
                    contentType: "application/json; charset=utf-8",
                    data: E,
                    dataType: "json",
                    success: function (data, status) {
                        var t = data.Data;
                        var msgcnt = t[0]["MSGCOUNT"];
                        var msg = t[0]["MSG"];

                        if (msgcnt == '1') {
                            swal({
                                title: "Success",
                                text: msg,
                                type: "success",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        //window.location.href = "";
                                        View(TktId);
                                    }
                                });
                        }
                        else {
                            swal({
                                title: "Error",
                                text: msg,
                                type: "error",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            });
                        }
                    }
                });
            }

        });
}

