﻿$(function () {
    var frm = $('#frmAccountMapping');
    $("#frmAccountMapping").validate();
    
    
    //acMap.validation();
    acMap.getAccountMapType();
    acMap.getVoucherType();

    $(document).on('change', '#ddlMapType', function () {
        acMap.getComponentTypeById();
    });

    $(document).on('keydown', '.searchAchead', function (e) {
        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id');
        acMap.auto(t);
    });

    $(document).on('keydown', '.searchAchead', function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode == 46) {
            var ths = $(this);
            ths.removeClass('selected');
            ths.addClass('selected');
            var t = ths.attr('id');

            $('#' + t).val('');
            $('#' + t).closest('tr').find('.hdn').val('');
        }
    });

    $('#btnsubmit').click(function () {
        acMap.update();
    });

    $('form#frmAccountMapping').on('submit', function (event) {
        //Add validation rule for dynamically generated name fields
        $('.searchAchead').each(function () {
            $(this.id).rules("add",
                {
                    required: true,
                    messages: {
                        required: "Name is required",
                    }
                });
        });
      
    });

    $("#frmAccountMapping").validate();


});


var acMap = {
    options: [],
    getAccountMapType: function () {
        $.ajax({
            url: '/Accounts_Form/AccountMapping/GetAccountMapType',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                acMap.popAccountMapType(response.Data);
            },
            error: function () { }
        });
    },
    popAccountMapType: function (data) {
        var ctlr_ddlMapType = $('#ddlMapType');
        ctlr_ddlMapType.empty();
        ctlr_ddlMapType.append('<option value=0>Select</option');
        $(data).each(function (index, item) {
            ctlr_ddlMapType.append('<option value=' + item.MId + '>' + item.Mdesc + '</option>');
        })
    },
    getVoucherType: function () {
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/GetAllVoucherType',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                acMap.options.push('<option value="0">-- Select --</option>');
                $(response.Data).each(function (i, element) {
                    acMap.options.push('<option value=' + element.VoucherTypeID + '>' + element.VoucherDescription + '</option>');
                });
            },
            error: function () { }
        });
    },
    getComponentTypeById: function () {

        var mapTypeId = $('#ddlMapType').val();
        var mapObj = { "MapTypeId": mapTypeId }
        $.ajax({
            url: '/Accounts_Form/AccountMapping/GetComponentByTypeId',
            type: 'POST',
            data: JSON.stringify(mapObj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                acMap.popComponentType(data.Data);
            },
            error: function () { }
        });
    },

    popComponentType: function (data) {
        $('#tblAccountMapping tbody').empty();
        var head=data[0]

        $.each(data, function (index, item) {
            $('#tblAccountMapping tbody').append("<tr>"
                + "<td style='display:none;'>" + item.MCompId + "</td>"
                + "<td>" + item.MCompDesc + "</td>"
                + "<td><select id='ddlvoucher_" + index +"' class='form-control cls_ddlvoucher'></select></td>"
                + "<td><input  type='text' class='form-control searchAchead' id=txt_" + index + " name=txt_" + index + " value='" + item.AccountDescription + "' /><input type=hidden class=hdn ID=hdn_" + index + " value=" + item.AccountCode + " /><label class=error ></label></td>"
                + "<td><select id='op_" + index + "' class='cls_ddldrcr form-control' >"
                + "<option value='0'>Dr/Cr</option>"
                + "<option value='D'>DR</option>"
                + "<option value='C'>CR</option>"
                + "</select></td>"
                + "<td style='display:none;'>" + item.MappingID + "</td>"
                + "</tr>");
            $('#op_' + index).val(item.DRCR);

            $('#ddlvoucher_' + index).html(acMap.options.join(''));
            $('#ddlvoucher_' + index).val(item.VoucherTypeID);
        });
    },
    auto: function (ref) {
        $('#' + ref).autocomplete({
           
            source: function (request, response) {
                var V = "{ AccountDescription :'" + $('#' + ref).val() + "', SectorID: '" + $('#ddlSector').val() + "'}"; 
                $.ajax({
                    url: '/Accounts_Form/AccountMapping/GetAuto',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: V,
                    dataType: 'json',
                    success: function (serverResponse) {
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {
                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            response(SubledgerAutoComplete);
                        }
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                //$(this).val(i.item.label);
                var id = i.item.AccountCode;
                $('#' + ref).closest('tr').find('.hdn').val(id);
                //alert($('#' + ref.id).closest('tr').find('.hdn').val());
            },
            minLength:0
        }).click(function () {
            $(ref).autocomplete('search', ($(ref).val()))
        });
    },
    subledgerAuto: function () {
        $('#txtAutoSub').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/GetSubledgerAuto',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'AccountDescription':'" + $('#txtAutoSub').val() + "'}",
                    dataType: 'json',
                    success: function (serverResponse) {
                        //console.log(serverResponse.Data);
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            //console.log(SubledgerAutoComplete);
                            response(SubledgerAutoComplete);
                        }
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                $('#hdnSubLedgerSearchId').val(i.item.AccountCode);
                app.subledgerAutopop($('#hdnSubLedgerSearchId').val());
                if ($('#hdnSubLedgerSearchId').val() != '') {
                    $('#btnsubsubmit').text('Update');
                    //alert($('#hdnAccountSearchID').val());
                    //$('#btnsubmit').attr('value', 'Update');
                }
                else {
                    $('#btnsubsubmit').text('Submit');
                }
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()))
        });
    },

    update: function () {
        var arr = [];
        var data = $('#tblAccountMapping tr').not(":first")
        $(data).each(function (i, element) {
            var obj = {};
            obj.MappingID = $(element).children('td').eq(5).html();
            obj.Mid = $('#ddlMapType').val();
            obj.MCompId = $(element).children('td').eq(0).html();
            obj.VoucherTypeID = $(element).find('td').find('.cls_ddlvoucher').val();
            obj.acCode = $(element).find('td').find('.hdn').val();
            obj.drcr = $(element).find('td').find('.cls_ddldrcr').val();
            arr.push(obj);
        });

        $.ajax({
            url: '/Accounts_Form/AccountMapping/AccountMapUpdate',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(arr),
            dataType: 'json',
            success: function (data) {

                swal({
                    title: "Success",
                    text: data.Message,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        //app.populateAccount();
                    }
                });
            },
            error: function () { }
        });
    },
    validation: function () {
      
        $('.searchAchead').each(function () {
            $(this).rules("add",
                {
                    required: true,
                    messages: {
                        required: "Name is required",
                    }
                });
        });

    }
}



