﻿var ArrayOpenListID = []; var isGLSL = "";



$(document).ready(function(){

    var frm_TrailBalance = $("#frmTrailBalance");
    MyForm_Validate();
   
    //Sub Ledger hide and show
    $('#radioGL').change(function () {
        isGLSL = "1";
        $("#pnl_SubLedger").css("display", "none");
    });
    $('#radioSL').change(function () {
        isGLSL = "2";
        $("#pnl_SubLedger").css("display", "block");
    });

    //Date Checking
    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();
    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
    Populate_Grid($('#txtFromDate').val(), $('#txtToDate').val());

    //Show Button Click
    $("#btnShow").click(function () {

        if (!frm_TrailBalance.valid()) {
            return false;
        }

        Between_Dates();
    });

});

//Date Checking
 function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            var a = $('#txtAccFinYear').val();
            if (a != "") {
                var res = FinYear_Validation(a);
                if (res == false) {
                    alert('Sorry ! Please Enter Valid Account Financial Year.'); $('#txtFromDate').val(''); return false;
                }
                else
                    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
            }
        }
    });
}
 function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
 function Set_DefaultDate() {
    //Set From Date
    var a = $('#txtAccFinYear').val();
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);
}
 function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}


//Show Button Click
 function MyForm_Validate() {
    $('#frmTrailBalance').validate({
        rules: {
            txtToDate: {
                required: true
            },
            txtFromDate: {
                required: true
            }
        }
    });
}
 function Between_Dates()
 {
     var fdate = $('#txtFromDate').val();
     var tdate = $("#txtToDate").val();

     var fSplitDate = fdate.split('/');
     var tSplitDate = tdate.split('/');

     var a = $('#txtAccFinYear').val();
     if (a != '') {
         var fy = a.split('-');
         a1 = fy[0];
         a2 = fy[1];

         var startDate = "01/04/" + a1;
         var endDate = "31/03/" + a2;

         var newStartDate = new Date(a1, 03, 01);
         var newendDate = new Date(a2, 02, 31);

         var givenFromDate = new Date(fSplitDate[2], parseInt(fSplitDate[1]) - 1, fSplitDate[0]); 
         var givenToDate = new Date(tSplitDate[2], parseInt(tSplitDate[1]) - 1, tSplitDate[0]);

         if ((givenFromDate >= newStartDate && givenFromDate <= newendDate)) {}
         else
         {
             alert('Sorry ! Given From Date is not within Account Financial Year.'); $('#txtFromDate').focus(); return false;
         }

         if ((givenToDate >= newStartDate && givenToDate <= newendDate)){}
         else {
             alert('Sorry ! Given To Date is not within Account Financial Year.'); $('#txtToDate').focus(); return false;
         }

         Populate_Grid(fdate, tdate);
     }
 }
 function Populate_Grid(fdate, tdate)
 {
    
     if (fdate != "")
     {
         var rDate = fdate.split('/');
         fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
     }
     if (tdate != "") {
         var trDate = tdate.split('/');
         tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
     }
     var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#ddlSector').val() + "'}"; 
     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Details",
         data: E,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var t = D.Data; 
             if(t.length > 0 && D.Status == 200)
             {
                 tbl_0(t);
             }
         }
     });
 }

 function tbl_0(data)   //============================================================================================ 1
 {
     var totalOpenDR = 0; var totalOpenCR = 0; var totalTransDR = 0; var totalTransCR = 0; var totalClosingDR = 0; var totalClosingCR = 0;
     
     $("#tbl_0 tr.allData").remove();
     $("#tbl_0 tr.footer").remove();
     for (var i = 0; i < data.length; i++) {

         var isSubLedger = data[i].SubLedger;

         totalOpenDR = parseFloat(totalOpenDR) + parseFloat(data[i].OpeningBalDR);
         totalOpenCR = parseFloat(totalOpenCR) + parseFloat(data[i].OpeningBalCR);
         totalTransDR = parseFloat(totalTransDR) + parseFloat(data[i].TransactionBalDR);
         totalTransCR = parseFloat(totalTransCR) + parseFloat(data[i].TransactionBalCR);
         totalClosingDR = parseFloat(totalClosingDR) + parseFloat(data[i].ClosingBalDR);
         totalClosingCR = parseFloat(totalClosingCR) + parseFloat(data[i].ClosingBalCR);

         if (isSubLedger == 'Y') {
             $("#tbl_0").append("<tr class='allData' style='cursor:pointer;  background-color:lightgray;' onclick='Populate_0(this)'><td style='display:none; ' class='AccountCode'>" +
                     data[i].AccountCode + "</td><td style='text-align:left; width:30%' class='AccountDescription'>" +
                     data[i].AccountDescription + "</td><td style='text-align:right;'>" +
                     data[i].OpeningBalDR + "</td><td style='text-align:right; '>" +
                     data[i].OpeningBalCR + "</td><td style='text-align:right;  '>" +
                     data[i].TransactionBalDR + "</td><td style='text-align:right;  '>" +
                     data[i].TransactionBalCR + "</td><td style='text-align:right; '>" +
                     data[i].ClosingBalDR + "</td><td style='text-align:right;  '>" +
                     data[i].ClosingBalCR + "</td><tr>");
         }
         else
         {
             $("#tbl_0").append("<tr class='allData' style='cursor:pointer' onclick='Populate_0(this)'><td style='display:none;' class='AccountCode'>" +
                    data[i].AccountCode + "</td><td style='text-align:left; width:30%' class='AccountDescription'>" +
                    data[i].AccountDescription + "</td><td style='text-align:right; '>" +
                    data[i].OpeningBalDR + "</td><td style='text-align:right; '>" +
                    data[i].OpeningBalCR + "</td><td style='text-align:right; '>" +
                    data[i].TransactionBalDR + "</td><td style='text-align:right; '>" +
                    data[i].TransactionBalCR + "</td><td style='text-align:right; '>" +
                    data[i].ClosingBalDR + "</td><td style='text-align:right; '>" +
                    data[i].ClosingBalCR + "</td><tr>");
         }

     }
     var count = $("#tbl_0 tr.allData").length;
     if (count > 0) {
         $("#tbl_0").append("<tr class='footer' style='background-color:aquamarine; font-weight:bold'><td style='display:none; '>" +
                     "" + "</td><td style='width:30%'>" +
                     "Total" + "</td><td style='text-align:right; '>" +
                     totalOpenDR + "</td><td style='text-align:right; '>" +
                     totalOpenCR + "</td><td style='text-align:right'>" +
                     totalTransDR + "</td><td style='text-align:right'>" +
                     totalTransCR + "</td><td style='text-align:right'>" +
                     totalClosingDR + "</td><td style='text-align:right'>" +
                     totalClosingCR + "</td><tr>");
     }


     Highlight_Row_tbl_0();
 } //============================================================================================ 1
 function Highlight_Row_tbl_0()
 {
     $("[id*=tbl_0] tr.allData").not(':first').hover(
  function () {
      $(this).css("background", "#D4E6F1");
  },
  function () {
      $(this).css("background", "");
  }
);
     $(function () {
         $("[id*=tbl_0] td").bind("click", function () {
             var row = $(this).parent();
             $("[id*=tbl_0] tr").each(function () {
                 if ($(this)[0] != row[0]) {
                     $("td", this).removeClass("hover");
                 }
             });
             $("td", row).each(function () {
                 if (!$(this).hasClass("hover")) {
                     $(this).addClass("hover");
                 }
                 else {
                     $(this).removeClass("hover");
                 }
             });
         });
     });
 }
 function Populate_0(ID)
 {
     var currentRow = $(ID).closest("tr");
     var AccountCode = currentRow.find(".AccountCode").html();
     var AccountDesc = currentRow.find(".AccountDescription").html();

     ArrayOpenListID.push(1);
     $("#tbl_0").hide();
     $("#tbl_1").show();

     Populate_Exp_Main_1(AccountCode, AccountDesc);
     
 }


 function Populate_Exp_Main_1(AccountCode, AccountDesc) {   //============================================================================================ 2
     var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;
     var Z = "{AccountCode:'" + AccountCode + "'}";
    
     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_1",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var t = D.Data;

             $("#tbl_1 tr.clstbl_1").remove();
             $("#tbl_1 tr.footer_1").remove();

             if (t.length > 0 && D.Status == 200) {

                 $("#lblHeader_1").html(AccountDesc);
                 for (var i = 0; i < t.length; i++) {

                     totalDebit = parseFloat(totalDebit) + parseFloat(t[i].Debit);
                     totalCredit = parseFloat(totalCredit) + parseFloat(t[i].Credit);
                     totalCloBal = parseFloat(totalCloBal) + parseFloat(t[i].ClosingBalance);

                     $("#tbl_1").append("<tr class='clstbl_1' style='cursor:pointer' onclick='Populate_1(this)'><td style='display:none;' class='AccountCode'>" +
                                        t[i].AccountCode + "</td> <td align='left' class='cls_AccountDescription_1'>" +
                                        t[i].AccountDescription + "</td> <td align='right' >" +
                                        t[i].Debit + "</td> <td align='right' >" +
                                        t[i].Credit + "</td><td align='right'>" +
                                        t[i].ClosingBalance + "</td></tr>");
                 }
                 var count = $("#tbl_1 tr.clstbl_1").length;
                 if (count > 0) {
                     $("#tbl_1").append("<tr class='footer_1' style='background-color:aquamarine; font-weight:bold'><td style='display:none; '>" +
                                 "" + "</td><td>" +
                                 "Total" + "</td><td style='text-align:right;'>" +
                                 totalDebit + "</td><td style='text-align:right'>" +
                                 totalCredit + "</td><td style='text-align:right'>" +
                                 totalCloBal + "</td><tr>");
                 }

                 Highlight_Row_tbl_1();
             }
             else {
                 $("#lblHeader_1").html(AccountDesc);

                 $("#tbl_1").append("<tr class='footer_1'><td colspan='7' style='text-align:center; color:red;'>" +
                                       'No Data Found !!' + "</td></tr>");
             }

         }
     });
    
 } //============================================================================================ 2
 function Highlight_Row_tbl_1()
 {
     $("[id*=tbl_1] tr.clstbl_1").not(':first').hover(
        function () {
        $(this).css("background", "#D4E6F1");
     },
    function () {
        $(this).css("background", "");
    });

     $(function () {
         $("[id*=tbl_1] td").bind("click", function () {
             var row = $(this).parent();
             $("[id*=tbl_1] tr").each(function () {
                 if ($(this)[0] != row[0]) {
                     $("td", this).removeClass("hover");
                 }
             });
             $("td", row).each(function () {
                 if (!$(this).hasClass("hover")) {
                     $(this).addClass("hover");
                 }
                 else {
                     $(this).removeClass("hover");
                 }
             });
         });
     });
 }
 function Populate_1(ID) {
     var currentRow = $(ID).closest("tr");
     var AccountCode = currentRow.find(".AccountCode").html();
     var AccountDesc = currentRow.find(".AccountDescription").html();

     ArrayOpenListID.push(2);
     $("#tbl_0").hide();
     $("#tbl_1").hide();
     $("#tbl_2").show();

     Populate_Exp_Main_2(AccountCode, AccountDesc);

 }

 function Populate_Exp_Main_2(AccountCode, AccountDesc) //============================================================================================ 3
 {
     var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;
     var Z = "{AccountCode:'" + AccountCode + "'}";

     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_2",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var t = D.Data;
             if (t.length > 0 && D.Status == 200) {


                 $("#tbl_2 tr.clstbl_2").remove();
                 $("#tbl_2 tr.footer_2").remove();

                 $("#lblHeader_2").html(AccountDesc);
                 for (var i = 0; i < t.length; i++) {

                     totalDebit = parseFloat(totalDebit) + parseFloat(t[i].Debit);
                     totalCredit = parseFloat(totalCredit) + parseFloat(t[i].Credit);
                     totalCloBal = parseFloat(totalCloBal) + parseFloat(t[i].ClosingBalance);

                     $("#tbl_2").append("<tr class='clstbl_2' style='cursor:pointer' onclick='Populate_2(this)'><td style='display:none;' class='AccountCode'>" +
                                        t[i].AccountCode + "</td> <td align='left' class='AccountDescription'>" +
                                        t[i].AccountDescription + "</td> <td align='right' >" +
                                        t[i].Debit + "</td> <td align='right' >" +
                                        t[i].Credit + "</td><td align='right'>" +
                                        t[i].ClosingBalance + "</td></tr>");
                 }
                 var count = $("#tbl_2 tr.clstbl_2").length;
                 if (count > 0) {
                     $("#tbl_2").append("<tr class='footer_2' style='background-color:aquamarine; font-weight:bold'><td style='display:none; '>" +
                                 "" + "</td><td>" +
                                 "Total" + "</td><td style='text-align:right;'>" +
                                 totalDebit + "</td><td style='text-align:right'>" +
                                 totalCredit + "</td><td style='text-align:right'>" +
                                 totalCloBal + "</td><tr>");
                 }

                 Highlight_Row_tbl_2();
             }
         }
     });
 } //============================================================================================ 3
 function Highlight_Row_tbl_2() {
     $("[id*=tbl_2] tr.clstbl_2").not(':first').hover(
        function () {
            $(this).css("background", "#D4E6F1");
        },
    function () {
        $(this).css("background", "");
    });

     $(function () {
         $("[id*=tbl_2] td").bind("click", function () {
             var row = $(this).parent();
             $("[id*=tbl_2] tr").each(function () {
                 if ($(this)[0] != row[0]) {
                     $("td", this).removeClass("hover");
                 }
             });
             $("td", row).each(function () {
                 if (!$(this).hasClass("hover")) {
                     $(this).addClass("hover");
                 }
                 else {
                     $(this).removeClass("hover");
                 }
             });
         });
     });
 }
 function Populate_2(ID) {
     var currentRow = $(ID).closest("tr");
     var AccountCode = currentRow.find(".AccountCode").html();
     var AccountDesc = currentRow.find(".AccountDescription").html();

     ArrayOpenListID.push(3);
     $("#tbl_0").hide();
     $("#tbl_1").hide();
     $("#tbl_2").hide();
     $("#tbl_3").show();

     Populate_Exp_Main_3(AccountCode, AccountDesc);

 }
 function Populate_Exp_Main_3(AccountCode, AccountDescription)
 {
     var VoucherStartDate = ""; var VoucherEndDate = "";

     var FinYear = $('#txtFromDate').val();
     var t = FinYear.split('/');
     var Year = t[2]; 
    

     var FinYears = $('#txtToDate').val();
     var ts = FinYears.split('/');
     var Years = ts[2]; 

     var x = (Year % 100 === 0) ? (Year % 400 === 0) : (Year % 4 === 0); 
     var y = (Years % 100 === 0) ? (Years % 400 === 0) : (Years % 4 === 0); 

     if (AccountDescription == "JANUARY") {
         VoucherStartDate = Years + '-' + '01-' + '01'; VoucherEndDate = Years + '-' + '01-' + '31';
     }
     if (AccountDescription == "FEBRUARY") {
         if (y == false) {
             VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '28';
         }
         else {
             VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '29';
         }

     }
     if (AccountDescription == "MARCH") {
         VoucherStartDate = Years + '-' + '03-' + '01'; VoucherEndDate = Years + '-' + '03-' + '31';
     }
     if (AccountDescription == "APRIL") {
         VoucherStartDate = Year + '-' + '04-' + '01'; VoucherEndDate = Year + '-' + '04-' + '30';
     }
     if (AccountDescription == "MAY") {
         VoucherStartDate = Year + '-' + '05-' + '01'; VoucherEndDate = Year + '-' + '05-' + '31';
     }
     if (AccountDescription == "JUNE") {
         VoucherStartDate = Year + '-' + '06-' + '01'; VoucherEndDate = Year + '-' + '06-' + '30';
     }
     if (AccountDescription == "JULY") {
         VoucherStartDate = Year + '-' + '07-' + '01'; VoucherEndDate = Year + '-' + '07-' + '31';
     }
     if (AccountDescription == "AUGUST") {
         VoucherStartDate = Year + '-' + '08-' + '01'; VoucherEndDate = Year + '-' + '08-' + '31';
     }
     if (AccountDescription == "SEPTEMBER") {
         VoucherStartDate = Year + '-' + '09-' + '01'; VoucherEndDate = Year + '-' + '09-' + '30';
     }
     if (AccountDescription == "OCTOBER") {
         VoucherStartDate = Year + '-' + '10-' + '01'; VoucherEndDate = Year + '-' + '10-' + '31';
     }
     if (AccountDescription == "NOVEMBER") {
         VoucherStartDate = Year + '-' + '11-' + '01'; VoucherEndDate = Year + '-' + '11-' + '30';
     }
     if (AccountDescription == "DECEMBER") {
         VoucherStartDate = Year + '-' + '12-' + '01'; VoucherEndDate = Year + '-' + '12-' + '31';
     }

     var Z = "{AccountCode:'" + AccountCode + "', VoucherStartDate:'" + VoucherStartDate + "', VoucherEndDate:'" + VoucherEndDate + "'}";

     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_3",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var t = D.Data;

             $("#tbl_3 tr.clstbl_3").remove();
             $("#tbl_3 tr.footer_3").remove();
             if (t.length > 0 && D.Status == 200) {

                 
                 var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;

                 var lblHeader = 'From : ' + VoucherStartDate + '  To : ' + VoucherEndDate; 
                 $("#lblHeader_3").html(lblHeader);
                 for (var i = 0; i < t.length; i++) {

                     totalDebit = parseFloat(totalDebit) + parseFloat(t[i].Debit);
                     totalCredit = parseFloat(totalCredit) + parseFloat(t[i].Credit);
                     totalCloBal = parseFloat(totalCloBal) + parseFloat(t[i].ClosingBalance);

                     $("#tbl_3").append("<tr class='clstbl_3'><td style='display:none;'>" +
                                        t[i].AccountCode + "</td> <td style='text-align:center;'>" +
                                        t[i].VoucherNumber + "</td> <td style='text-align:right;'>" +
                                        t[i].VoucherDate + "</td> <td >" +
                                        t[i].Narration + "</td> <td style='text-align:center;'>" +
                                        t[i].Debit + "</td> <td style='text-align:right;'>" +
                                        t[i].Credit + "</td> <td style='text-align:right;'>" +
                                        t[i].ClosingBalance + "</td></tr>");
                 }
                 var count = $("#tbl_3 tr.clstbl_3").length;
                 if (count > 0) {
                     $("#tbl_3").append("<tr class='footer_3'><td style='display:none;'>" +
                                       '' + "</td> <td align='right' style='font-weight:bold;' >" +
                                       '' + "</td> <td align='center' style='height:13px;padding: 5px; font-weight:bold;'>" +
                                       '' + "</td> <td align='center' style='height:13px;padding: 5px; font-weight:bold;'>" +
                                       'Total' + "</td> <td align='center' style='height:13px;padding: 5px; font-weight:bold;'>" +
                                       totalDebit + "</td><td align='center' style='font-weight:bold;'>" +
                                       totalCredit + "</td><td align='center' style='font-weight:bold;'>" +
                                       totalCloBal + "</td></tr>");
                 }
             }
             else
             {
                 var lblHeader = 'From : ' + VoucherStartDate + '  To : ' + VoucherEndDate;
                 $("#lblHeader_3").html(lblHeader);

                 $("#tbl_3").append("<tr class='footer_3'><td colspan='7' style='text-align:center; color:red;'>" +
                                       'No Data Found !!' + "</td></tr>");
             }
         }
     });
 }

 function SearchTable_0() {
     var forSearchprefix = $("#txtSearch_0").val().trim().toUpperCase();
     var tablerow = $('#tbl_0').find('.allData');
     $.each(tablerow, function (index, value) {
         var AccountDescription = $(this).find('.AccountDescription').text().toUpperCase();
         if (AccountDescription.indexOf(forSearchprefix) > -1 ) {
             $(this).show();
         } else {
             $(this).hide();
         }
     });
 }
 function SearchTable_1() {
     var forSearchprefix = $("#txtSearch_1").val().trim().toUpperCase();
     var tablerow = $('#tbl_1').find('.clstbl_1');
     $.each(tablerow, function (index, value) {
         var AccountDescription = $(this).find('.cls_AccountDescription_1').text().toUpperCase();
         if (AccountDescription.indexOf(forSearchprefix) > -1) {
             $(this).show();
         } else {
             $(this).hide();
         }
     });
 }

 //**********************************************************************************************************************************
 //                                                                 OTHER
 //**********************************************************************************************************************************
 $(document).keyup(function (e) {
     if (e.keyCode === 27) Back();
 });
 function Back() {
    
     var t = ArrayOpenListID.pop();

         var ttShow = '#tbl_' + (parseInt(t) - 1);
         var ttHide = '#tbl_' + (parseInt(t));
         $(ttShow).show();
         $(ttHide).hide();
     
 }

    
   
    
    
    




