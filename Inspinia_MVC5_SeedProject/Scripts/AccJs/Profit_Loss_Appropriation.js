﻿var ArrayOpenListID = []; var ArrayOpenAccDesc = []; var MaintainAccCode = "", EII="";
var hide_Columns = [];
var sum_Columns = [];
var Grid_Level = 0;
var ArrayHeaders = [];

$(document).ready(function () {

    AccountRule();

    //Date Checking
    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();
    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
    //Populate_Grid($('#txtFromDate').val(), $('#txtToDate').val());

    //Show Button Click
    $("#btnShow").click(function () {

        if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
        if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }

        if ($('#lstSector').val() == null || $('#lstSector').val() == "") {
            $(".select2-search__field").focus(); return false;
        }

        Populate_Grid($('#txtFromDate').val(), $('#txtToDate').val());
    });

    $("#lblFrom").click(function () {
        $(this).replaceWith("<input type='text' class='form-control' style='width:50px' />");
    });

    $("#btnBack").click(function () {
        Back();
    });

    //Print
    $('#btnPrint').click(function (evt) { Print(); });

    $('.mywrapper').on('click', 'tbody >tr', function () {
        var tableName = $(this).closest('table').attr('id');
        var AccCode = $(this).closest('tr').find('.AccountCode').text();
        var AccountDes = $(this).closest('tr').find('.AccountDescription').text();
        var EI = $(this).closest('tr').find('.EI').text();
        var GroupLedger = $(this).closest('tr').find('.GroupLedger').text();
        var ClosingBalance = $(this).closest('tr').find('.ClosingBalance').text();
        var VoucherNumber = $(this).closest('tr').find('.VoucherNumber').text();
        var MONTH_SRL = $(this).closest('tr').find('.MONTH_SRL').text(); //alert(MONTH_SRL);

        if (MONTH_SRL != "" && MONTH_SRL > 0) {
            Drill_Down_Voucher(AccCode, AccountDes, EI, GroupLedger); return false;
        }


        if (VoucherNumber != "") {
            window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank'); return false;
        }

        if (GroupLedger != 'L') {
            if (EI == "")
                EI = EII;
            else
                EII = EI;

            if (ClosingBalance == "") {
                Drill_Down(AccCode, AccountDes, EI, GroupLedger); return false;
            }
        }
        else {
            EII = EI;
            Drill_Down_Month(AccCode, AccountDes, EI, GroupLedger); return false;
        }

    });

    //Date Setting between FinYear
    var a = localStorage.getItem("FinYear"); 
    if (a != null || a != "") {
        var fy = a.split('-');
        $("#txtFromDate").datepicker("option", "minDate", "01/04/" + fy[0]);
        $("#txtFromDate").datepicker("option", "maxDate", "31/03/" + fy[1]);

        $("#txtToDate").datepicker("option", "minDate", "01/04/" + fy[0]);
        $("#txtToDate").datepicker("option", "maxDate", "31/03/" + fy[1]);
    }
});

//Date Checking
function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Set_DefaultDate() {
    //Set From Date
    var a = localStorage.getItem("FinYear");
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);
}
function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}


//Show Button Click
function Between_Dates() {
    if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }

    var fdate = $('#txtFromDate').val();
    var tdate = $("#txtToDate").val();

    var fSplitDate = fdate.split('/');
    var tSplitDate = tdate.split('/');

    var a = $('#txtAccFinYear').val();
    if (a != '') {
        var fy = a.split('-');
        a1 = fy[0];
        a2 = fy[1];

        var startDate = "01/04/" + a1;
        var endDate = "31/03/" + a2;

        var newStartDate = new Date(a1, 03, 01);
        var newendDate = new Date(a2, 02, 31);

        var givenFromDate = new Date(fSplitDate[2], parseInt(fSplitDate[1]) - 1, fSplitDate[0]);
        var givenToDate = new Date(tSplitDate[2], parseInt(tSplitDate[1]) - 1, tSplitDate[0]);

        if ((givenFromDate >= newStartDate && givenFromDate <= newendDate)) { }
        else {
            alert('Sorry ! Given From Date is not within Account Financial Year.'); $('#txtFromDate').focus(); return false;
        }

        if ((givenToDate >= newStartDate && givenToDate <= newendDate)) { }
        else {
            alert('Sorry ! Given To Date is not within Account Financial Year.'); $('#txtToDate').focus(); return false;
        }

        Populate_Grid(fdate, tdate);
    }
}

//on page load
function Populate_Grid(fdate, tdate) {

    if (fdate != "") {
        var rDate = fdate.split('/');
        fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
    }
    if (tdate != "") {
        var trDate = tdate.split('/');
        tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
    }
    var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#lstSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/Profit_Loss_Appropriation/Show_Details",
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (D.Status == 200) {

                var t1 = t.Table;
                var t2 = t.Table1;

                Populate_Exp1(t1, 1); Populate_Exp1(t2, 2); 
            }
        }
    });
}

function Populate_Exp1(data, rank) {
    if (data.length > 0) {

        hide_Columns = [0, 4, 5];
        if (MaintainAccCode == 'N')
            hide_Columns.push(0);
        //else
        //    hide_Columns.pop(1);

        var table = $("#tbl_" + rank);
        table.find('thead').empty();
        table.find('tbody').empty();
        table.find('tfoot').empty();

            Populate_Headers(data, rank, hide_Columns);

        var total = 0;

        for (var i = 0; i < data.length; i++) {
            if (data[i].Amount != null) {
                total = total + data[i].Amount;
            }
            var color = "";
            if (i % 2 == 0) color = "#FDF8EC"; else color = "white";

            table.find('tbody').append("<tr class='allData' style='cursor:pointer;   background-color:" + color + "' ><td style='text-align:center; width:30%; display: " + (MaintainAccCode == "N" ? "none" : "normal") + "'>" +
                (data[i].OldAccountHead == null ? "" : data[i].OldAccountHead) + "</td><td style='text-align:center; width:30%;'>" +
                (data[i].AccountDescription == null ? "" : data[i].AccountDescription) + "</td><td style='text-align:right;' >" +
                (data[i].Amount == null ? "&nbsp;" : parseFloat(data[i].Amount).toFixed(2)) + "</td><td style='display:none;' class='EI'>" +
                (data[i].EI == null ? "&nbsp;" : data[i].EI) + "</td><td style='display:none;' class='AccountCode'>" +
                (data[i].AccountCode == null ? "&nbsp;" : data[i].AccountCode) + "</td><td style='display:none;' class='AccountDescription'>" +
                (data[i].AccountDescription == null ? "&nbsp;" : data[i].AccountDescription) + "</td><td style='display:none;' class='GroupLedger'>" +
                (data[i].GroupLedger == null ? "&nbsp;" : data[i].GroupLedger) + "</td><tr>");

        }
        if (rank == 1) {
            $("#lblExpTotal").text(total.toFixed(2)); 
        }
        if (rank == 2) {
            $("#lblIncomeTotal").text(total.toFixed(2));
        }
    }
}

function Drill_Down(AccCode, AccountDes, EI, GroupLedger) {
    var fdate = ""; var tdate = "";
    if ($("#txtFromDate").val() == "") { alert('From Date should be not Blank !!'); $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { alert('To Date should be not Blank !!'); $("#txtToDate").focus(); return false; }

    // $(".loading-overlay").show();
    if ($("#txtFromDate").val() != "") {
        var rDate = $("#txtFromDate").val().split('/');
        fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
    }
    if ($("#txtToDate").val() != "") {
        var trDate = $("#txtToDate").val().split('/');
        tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
    }
    var E = "{AccCode: '" + AccCode + "', FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#lstSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/Profit_Loss/Show_Group",
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            //  $(".loading-overlay").hide();
            var t = D.Data;
            if (D.Status == 200) {
                var t1 = t.Table;
                Populate_Group(t1, AccountDes, EI, GroupLedger);
            }
        }
    });
}
function Populate_Group(data, AccountDes, EI, GroupLedger) {
    if (data.length > 0) {


        ArrayHeaders.push($('.HeaderTitle').html());
        ArrayOpenListID.push($('.mywrapper').html());


        $('.mywrapper').find('.main_1').hide();
        $("#lblHeaderTitle").text("Group Details From ");

        hide_Columns = [0, 3, 4, 7, 8, 9];

        var tt = "3";
        if (MaintainAccCode == 'N') {
            hide_Columns.push(1);
            tt = 2;
        }

        //else
        //    hide_Columns.pop(1);

        var table = $("#tbl_5");
        table.find('thead').empty();
        table.find('tbody').empty();
        table.find('tfoot').empty();

        table.find('thead').append("<tr style='background-color:#DEFAF5'><td>" + AccountDes + "</td><td colspan='" + tt + "' style='text-align:center'>" + (EI == "E" ? "Expenditure" : "Income") + "</td></tr>");
        table.find('thead').append("<tr style='background-color:#DEFAF5'><td></td><td colspan='" + tt + "' style='text-align:center'>Closing Balance</td></tr>");



        var rank = 5;
        Populate_Headers(data, rank, hide_Columns);

        var totalCredit = 0; var totalDebit = 0;
        for (var i = 0; i < data.length; i++) {

            totalDebit = totalDebit + data[i].Debit;
            totalCredit = totalCredit + data[i].Credit;

            var color = "";
            if (i % 2 == 0) color = "#FDF8EC"; else color = "white";
            //if (data[i].MenuOrder == -1 || data[i].MenuOrder == 99) color = "#E3DFDA"; else color = "white";

            table.find('tbody').append("<tr class='allData' style='cursor:pointer;  font-size:12px;  background-color:" + color + "' ><td class='AccountCode' style='display:none;'>" +
                data[i].AccountCode + "</td><td style='text-align:center; width:30%; display: " + (MaintainAccCode == "N" ? "none" : "normal") + "'>" +
                data[i].AccountHead + "</td><td class='AccountDescription'>" +
                data[i].AccountDescription + "</td><td style='text-align:right; display:none' >" +
                data[i].AccountLevel + "</td><td style='text-align:right; display:none;' class='GroupLedger'>" +
                data[i].GroupLedger + "</td><td style='text-align:right;' >" +
                data[i].Debit.toFixed(2) + "</td><td style='text-align:right;' >" +
                data[i].Credit.toFixed(2) + "</td><td style='text-align:right; display:none;' >" +
                data[i].Ei + "</td><td style='text-align:right; display:none' >" +
                data[i].PLDirectIndirect + "</td><td style='text-align:right;  display:none' >" +
                data[i].int + "</td><tr>");
        }
        var count = table.find('tbody tr.allData').length;
        if (count > 0) {
            table.find('tfoot').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:#F5F5F6' ><td class='AccountCode' style='display:none;'>" +
                "" + "</td><td style='text-align:left; width:30%; display: " + (MaintainAccCode == "N" ? "none" : "normal") + "'>" +
                "" + "</td><td class='AccountDescription' style='text-align:right'>" +
                "Total" + "</td><td style='text-align:right; display:none' >" +
                "" + "</td><td style='text-align:right; display:none;' class='GroupLedger'>" +
                "" + "</td><td style='text-align:right;' >" +
                totalDebit.toFixed(2) + "</td><td style='text-align:right;' >" +
                totalCredit.toFixed(2) + "</td><td style='text-align:right; display:none;' >" +
                "" + "</td><td style='text-align:right; display:none' >" +
                "" + "</td><td style='text-align:right;  display:none' >" +
                "" + "</td><tr>");
        }
    }
}
function Drill_Down_Month(AccCode, AccountDes, EI, GroupLedger) {
    if (AccCode != "") {


        var dataPoints1 = []; var dataPoints2 = [];

        ArrayHeaders.push($('.HeaderTitle').html());
        ArrayOpenListID.push($('.mywrapper').html());
        $('.mywrapper').find('.main_1').hide();

        $("#lblHeaderTitle").text("Month Details From  ");

        //hide_Columns = [0, 4, 5];
        hide_Columns = [0, 1, 7];

        var table = $("#tbl_5");
        table.find('thead').empty();
        table.find('tbody').empty();
        table.find('tfoot').empty();

        table.find('thead').append("<tr style='background-color:#DEFAF5'><td>" + AccountDes + "</td><td colspan='4' style='text-align:center'>" + (EII == "E" ? "Expenditure" : "Income") + "</td></tr>");
        table.find('thead').append("<tr style='background-color:#DEFAF5'><td></td><td colspan='4' style='text-align:center'>Closing Balance</td></tr>");



        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        var SDate = ""; var EDate = "";
        if ($.trim($("#txtFromDate").val()) != "") {
            var refDate = ($("#txtFromDate").val()).split('/');
            SDate = refDate[2] + "-" + refDate[1] + "-" + refDate[0];
        }
        if ($.trim($("#txtToDate").val()) != "") {
            var refDates = ($("#txtToDate").val()).split('/');
            EDate = refDates[2] + "-" + refDates[1] + "-" + refDates[0];
        }

        var Z = "{AccountCode:'" + AccCode + "', StartDate:'" + SDate + "', EndDate:'" + EDate + "', SectorID:'" + $("#lstSector").val() + "', IS_GL:'" + "" + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TrailBalance/Show_Exp_Main_2",
            data: Z,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = D.Data;
                if (data.length > 0 && D.Status == 200) {

                    //Populate Header
                    var rank = 5;
                    Populate_Headers(data, rank, hide_Columns);
                    var totalDebit = 0; var totalCredit = 0;
                    for (var i = 0; i < data.length; i++) {



                        totalDebit = parseFloat(totalDebit) + parseFloat(data[i].TR_DR);
                        totalCredit = parseFloat(totalCredit) + parseFloat(data[i].TR_CR);
                        //totalCloBal = parseFloat(totalCloBal) + parseFloat(data[i].ClosingBalance);

                        var color = "";
                        if (i % 2 == 0) color = "#FDF8EC"; else color = "white";

                        table.find('tbody').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:" + color + "' ><td style='display:none; ' class='AccountCode'>" +
                            data[i].AccountCode + "</td><td style='text-align:left; display:none; width:30%' class='MONTH_SRL' >" +
                            data[i].MONTH_SRL + "</td><td style='text-align:center;' class='AccountDescription'>" +
                            data[i].MONTH_NAME + "</td><td style='text-align:right;' >" +
                            (data[i].OP_DRCR).toFixed(2) + "</td><td style='text-align:right; ' >" +
                            (data[i].TR_DR).toFixed(2) + "</td><td style='text-align:right; class='MonthClosBal' ' >" +
                            (data[i].TR_CR).toFixed(2) + "</td><td style='text-align:right; class='MonthClosBal' ' >" +
                            (data[i].CL_DRCR).toFixed(2) + "</td><td style='text-align:right; display:none' class='MonthClosBal' >" +
                            data[i].YearMonth + "</td><tr>");

                        //table.find('tbody').append("<tr class='allData' style='cursor:pointer;  font-size:12px;  background-color:" + color + "'  ><td style='display:none; ' class='AccountCode' >" +
                        //                 data[i].AccountCode + "</td><td style='text-align:left; width:30%' class='AccountDescription'>" +
                        //                 data[i].AccountDescription + "</td><td style='text-align:right;' >" +
                        //                 data[i].Debit.toFixed(2) + "</td><td style='text-align:right; ' >" +
                        //                 data[i].Credit.toFixed(2) + "</td><td style='text-align:right; display:none;' class='ClosingBalance'>" +
                        //                 data[i].ClosingBalance + "</td><tr>");
                    }
                }
                // var count = table.find('tbody tr.allData').length;
                //if (count > 0) {
                // table.find('tfoot').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:#E3DFDA;' ><td //style='display:none; ' class='AccountCode'>" +
                // "" + "</td><td style='text-align:right; width:30%'>" +
                // "" + "</td><td style='text-align:right; width:30%'>" +
                //  "" + "</td><td style='text-align:right; width:30%'>" +
                //"Total" + "</td><td style='text-align:right;' >" +
                //totalDebit.toFixed(2) + "</td><td style='text-align:right; ' >" +
                //totalCredit.toFixed(2) + "</td><td style='text-align:right; display:none;' >" +
                //"" + "</td><tr>");
                //}

                //if (dataPoints1.length > 0) {
                //    ShowChart("Month", "Amount", dataPoints1, dataPoints2);
                //}
            }
        });
    }
}
function Drill_Down_Voucher(AccCode, AccountDescription, EI, GroupLedger) {

    if (AccCode != "") {


        ArrayHeaders.push($('.HeaderTitle').html());
        ArrayOpenListID.push($('.mywrapper').html());
        $('.mywrapper').find('.main_1').hide();
        //$('.mywrapper').find('.main_2').hide();

        $("#lblHeaderTitle").text("Voucher Details From  ");
        hide_Columns = [0, 1, 7];

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        var VoucherStartDate = ""; var VoucherEndDate = "";

        var FinYear = $('#txtFromDate').val();
        var t = FinYear.split('/');
        var Year = t[2];


        var FinYears = $('#txtToDate').val();
        var ts = FinYears.split('/');
        var Years = ts[2];

        var x = (Year % 100 === 0) ? (Year % 400 === 0) : (Year % 4 === 0);
        var y = (Years % 100 === 0) ? (Years % 400 === 0) : (Years % 4 === 0);

        if (AccountDescription == "JANUARY") {
            VoucherStartDate = Years + '-' + '01-' + '01'; VoucherEndDate = Years + '-' + '01-' + '31';
        }
        if (AccountDescription == "FEBRUARY") {
            if (y == false) {
                VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '28';
            }
            else {
                VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '29';
            }

        }
        if (AccountDescription == "MARCH") {
            VoucherStartDate = Years + '-' + '03-' + '01'; VoucherEndDate = Years + '-' + '03-' + '31';
        }
        if (AccountDescription == "APRIL") {

            VoucherStartDate = Year + '-' + '04-' + '01'; VoucherEndDate = Year + '-' + '04-' + '30';
        }
        if (AccountDescription == "MAY") {
            VoucherStartDate = Year + '-' + '05-' + '01'; VoucherEndDate = Year + '-' + '05-' + '31';
        }
        if (AccountDescription == "JUNE") {
            VoucherStartDate = Year + '-' + '06-' + '01'; VoucherEndDate = Year + '-' + '06-' + '30';
        }
        if (AccountDescription == "JULY") {
            VoucherStartDate = Year + '-' + '07-' + '01'; VoucherEndDate = Year + '-' + '07-' + '31';
        }
        if (AccountDescription == "AUGUST") {
            VoucherStartDate = Year + '-' + '08-' + '01'; VoucherEndDate = Year + '-' + '08-' + '31';
        }
        if (AccountDescription == "SEPTEMBER") {
            VoucherStartDate = Year + '-' + '09-' + '01'; VoucherEndDate = Year + '-' + '09-' + '30';
        }
        if (AccountDescription == "OCTOBER") {
            VoucherStartDate = Year + '-' + '10-' + '01'; VoucherEndDate = Year + '-' + '10-' + '31';
        }
        if (AccountDescription == "NOVEMBER") {
            VoucherStartDate = Year + '-' + '11-' + '01'; VoucherEndDate = Year + '-' + '11-' + '30';
        }
        if (AccountDescription == "DECEMBER") {
            VoucherStartDate = Year + '-' + '12-' + '01'; VoucherEndDate = Year + '-' + '12-' + '31';
        }

        var table = $("#tbl_5");
        table.find('thead').empty();
        table.find('tbody').empty();
        table.find('tfoot').empty();

        var lblHeader = 'From : ' + VoucherStartDate + '  To : ' + VoucherEndDate;
        table.find('thead').append("<tr style='background-color:#DEFAF5'><td colspan='3'>" + lblHeader + "</td><td colspan='2' style='text-align:center'>" + (EII == "E" ? "Expenditure" : "Income") + "</td></tr>");
        table.find('thead').append("<tr style='background-color:#DEFAF5'><td colspan='3'></td><td colspan='2' style='text-align:center'>Closing Balance</td></tr>");


        var Z = "{AccountCode:'" + AccCode + "', VoucherStartDate:'" + VoucherStartDate + "', VoucherEndDate:'" + VoucherEndDate + "', IS_GL:'" + "" + "',  SectorID:'" + $('#lstSector').val() + "'}";

        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TrailBalance/Show_Exp_Main_3",
            data: Z,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = D.Data;

                if (data.length > 0 && D.Status == 200) {

                    var rank = 5;
                    Populate_Headers(data, rank, hide_Columns);
                    var totalDebit = 0; var totalCredit = 0;
                    for (var i = 0; i < data.length; i++) {

                        totalDebit = parseFloat(totalDebit) + parseFloat(data[i].Debit);
                        totalCredit = parseFloat(totalCredit) + parseFloat(data[i].Credit);
                        //totalCloBal = parseFloat(totalCloBal) + parseFloat(data[i].ClosingBalance);

                        var color = "";
                        if (i % 2 == 0) color = "#FDF8EC"; else color = "white";

                        table.find('tbody').append("<tr class='allData' style='cursor:pointer;  font-size:12px;  background-color:" + color + "'  ><td style='display:none; ' class='AccountCode'>" +
                            data[i].AccountCode + "</td><td style='text-align:left; display:none;' class='VoucherNumber'>" +
                            data[i].VoucherNumber + "</td> <td style='text-align:center; width:20%' class='VoucherSlNo'>" +
                            data[i].RefVoucherSlNo + "</td> <td style='text-align:center; width:10%' >" +
                            data[i].VoucherDate + "</td> <td style='text-align:left; width:50%'' >" +
                            data[i].Narration + "</td> <td style='text-align:right;' >" +
                            data[i].Debit.toFixed(2) + "</td> <td style='text-align:right;' >" +
                            data[i].Credit.toFixed(2) + "</td> <td style='text-align:right; display:none;' >" +
                            data[i].ClosingBalance + "</td></tr>");
                    }
                    var count = table.find("tbody tr.allData").length;
                    if (count > 0) {
                        table.find('tbody').append("<tr class='footers' style='background-color:#F5F5F6; font-weight:bold'><td style='display:none; '>" +
                            "" + "</td><td style='width:30%; display:none;'>" +
                            "" + "</td><td style='width:20%'>" +
                            "" + "</td><td style='width:10%'>" +
                            "" + "</td><td style='width:50%'>" +
                            "Total" + "</td><td style='text-align:right;'>" +
                            totalDebit.toFixed(2) + "</td><td style='text-align:right;'>" +
                            totalCredit.toFixed(2) + "</td><td style='text-align:right; display:none;'>" +
                            "" + "</td><tr>");
                    }
                }
            }
        });
    }
}

function Populate_Headers(data, rank, hide_Columns) {

    var html = "";
    html += "<tr>";

    var table = $("#tbl_" + rank);

    //populate header
    for (var j = 0; j < (Object.keys(data[0])).length; j++) {
        var Columns = Object.keys(data[0])[j];

        var res = Check_Array(j, hide_Columns);

        var desc = "";
        if (Object.keys(data[0])[j] == "AccountHead")
            desc = "Account Head";
        else if (Object.keys(data[0])[j] == "AccountDescription")
            desc = "Account Description";
        else if (Object.keys(data[0])[j] == "MONTH_NAME")
            desc = "Months";
        else if (Object.keys(data[0])[j] == "OP_DRCR")
            desc = "Opening Balance";
        else if (Object.keys(data[0])[j] == "TR_DR")
            desc = "Debit";
        else if (Object.keys(data[0])[j] == "TR_CR")
            desc = "Credit";
        else if (Object.keys(data[0])[j] == "CL_DRCR")
            desc = "Closing Balance";
        else if (Object.keys(data[0])[j] == "RefVoucherSlNo")
            desc = "Voucher No.";
        else if (Object.keys(data[0])[j] == "VoucherDate")
            desc = "Voucher Date";
        else
            desc = Object.keys(data[0])[j];



        if (res == true) {
            html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "' style='display:none'>" + desc + "</th>";
        }
        else {
            html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "'>" + desc + "</th>";
        }
    }
    html += "</tr>";
    table.find('thead').append(html);
}
function Check_Array(value, hide_list) {

    if (jQuery.inArray(value, hide_list) != '-1') {
        return true;
    } else {
        return false;
    }
}
function AccountRule() {
    var V = "{SectorID : '" + $("#ddlSector").val() + "'}";
    $.ajax({
        url: '/Accounts_Form/AccountMaster/AccountRule',
        data: V,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (D) {
            var t = D.Data;
            if (t.length > 0 && D.Status == 200) {

                MaintainAccCode = t[0].MaintainAccCode;
            }
        },
        error: function () { }
    });
}
function ShowChart(Title_X, Titel_Y, dataPoints1, dataPoints2) {

    var chart = new CanvasJS.Chart("chartContainer", {

        axisX: {
            title: Title_X
        },
        axisY: {
            title: Titel_Y
        },
        theme: "light2",
        data: [{
            type: "column",
            name: "Debit",
            showInLegend: true,
            dataPoints: dataPoints1
        },
        {
            type: "column",
            name: "Credit",
            showInLegend: true,
            dataPoints: dataPoints2
        }
        ]
    });
    chart.render();
}
function Print() {
    if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }
    if ($('#lstSector').val() == null || $('#lstSector').val() == "") {
        $(".select2-search__field").focus(); return false;
    }
    var ischkonly = $('#chkOnly').prop('checked');

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "ProfitLossAppropriation_Main.rpt",
        FileName: "Profit_Loss_Appropriation",
        Database: ''
    });

    var sec = [];
    sec = $("#lstSector").val().toString();

    // return false;
    detail.push({
        p_FROM_DATE: $("#txtFromDate").val(),
        p_TO_DATE: $("#txtToDate").val(),
        p_SECTORIDS: sec,
        p_PL: "Y"
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function Back() {
    // alert(ArrayHeaders);
    if (ArrayHeaders.length > 0) {
        var s = ArrayHeaders.pop();
        if (s != "") {
            $('.HeaderTitle').html(s);
        }
    }

    if (ArrayOpenListID.length > 0) {
        var t = ArrayOpenListID.pop();
        if (t != "") {
            $('.mywrapper').html(t);
        }
    }

}












