﻿var fileName = "", Count = 0;

$(document).ready(function () {
    setDate();
    $('#dialog').draggable(); 

    //BANK AUTOCOMPLETE
    $("#txtBankName").autocomplete({
        source: function (request, response) {

            var E = "{TransType: '" + "Bank" + "', SectorID: '" + $("#ddlSector").val() + "', Desc: '" + $("#txtBankName").val() + "', AccountCode: '" + "" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/BankStatementExcelUpload/Load",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {
            $("#hdnAccCode").val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
        });
    $("#txtSBankName").autocomplete({
        source: function (request, response) {

            var E = "{TransType: '" + "SearchBank" + "', SectorID: '" + $("#ddlSector").val() + "', Desc: '" + $("#txtSBankName").val() + "', AccountCode: '" + "" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/BankStatementExcelUpload/Load",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {
            $("#hdnSAccCode").val(i.item.AccountCode);
            //PostingDate(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
        });

    $(document).on('click', '.clsLedger', function (e) {
        var ths = $(this);
        var t = ths.attr('id'); 

        $('#' + t).autocomplete({

            source: function (request, response) {

                var E = "{TransType: '" + "Ledger" + "', SectorID: '" + $("#ddlSector").val() + "', Desc: '" + $('#' + t).val() + "', AccountCode: '" + "" + "'}";
                $.ajax({
                    url: '/Accounts_Form/BankStatementExcelUpload/Load',
                    type: 'POST',
                    data: E,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
                                    SubLedger: item.SubLedger
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                var id = i.item.AccountCode;
                var isSubLedgr = i.item.SubLedger; 
                $('#' + t).closest('tr').find('.clsAccCodeLedger').val(id);

                if (isSubLedgr == "Y")
                {
                    $('#' + t).attr('sub-ledger', isSubLedgr);
                    $('#' + t).closest('tr').find('.clssubLedger').css('display', '');
                }
                else
                {
                    $('#' + t).attr('sub-ledger', "N");
                    $('#' + t).closest('tr').find('.clssubLedger').css('display', 'none');
                }
            },
            minLength: 0
        }).click(function () {
            $(t).autocomplete('search', ($(t).val()))
            });

        //$('#' + t).keydown(function (evt) {
        //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        //    if (iKeyCode == 8) {
        //        $('#' + t).val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeLedger').val('');
        //        $('#' + t).attr('sub-ledger', "N");
        //        $('#' + t).closest('tr').find('.clssubLedger').val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
        //    }
        //    if (iKeyCode == 46) {
        //        $('#' + t).val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeLedger').val('');
        //        $('#' + t).attr('sub-ledger', "N");
        //        $('#' + t).closest('tr').find('.clssubLedger').val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
        //    }
        //});

    });
    $(document).on('keydown', '.clsLedger', function (evt) {
        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id'); 

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#' + t).val('');
            $('#' + t).closest('tr').find('.clsAccCodeLedger').val('');
            $('#' + t).attr('sub-ledger', "N");
            $('#' + t).closest('tr').find('.clssubLedger').val('');
            $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
            return false;
        }
        if (iKeyCode == 46) {
            $('#' + t).val('');
            $('#' + t).closest('tr').find('.clsAccCodeLedger').val('');
            $('#' + t).attr('sub-ledger', "N");
            $('#' + t).closest('tr').find('.clssubLedger').val('');
            $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
            return false;
        }
    });

    $(document).on('click', '.clssubLedger', function (e) {
        var ths = $(this);
        var t = ths.attr('id'); 
        var AccCode = ths.closest('tr').find('.clsAccCodeLedger').val();
        if (AccCode == "" || AccCode == null)
        {
            alert('Please, Confirm Ledger first.'); return false;
        }

        $('#' + t).autocomplete({

            source: function (request, response) {

                var E = "{TransType: '" + "SubLedger" + "', SectorID: '" + $("#ddlSector").val() + "', Desc: '" + $('#' + t).val() + "', AccountCode: '" + AccCode + "'}";
                $.ajax({
                    url: '/Accounts_Form/BankStatementExcelUpload/Load',
                    type: 'POST',
                    data: E,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                var id = i.item.AccountCode;
                $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val(id);
            },
            minLength: 0
        }).click(function () {
            $(t).autocomplete('search', ($(t).val()))
            });

        //$('#' + t).keydown(function (evt) {
        //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        //    if (iKeyCode == 8) {
        //        $('#' + t).val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
        //    }
        //    if (iKeyCode == 46) {
        //        $('#' + t).val('');
        //        $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
        //    }
        //});

    });
    $(document).on('keydown', '.clssubLedger', function (evt) {
        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id'); 

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#' + t).val('');
            $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
            return false;
        }
        if (iKeyCode == 46) {
            $('#' + t).val('');
            $('#' + t).closest('tr').find('.clsAccCodeSubLedger').val('');
            return false;
        }
    });
});

function setDate()
{
    $('#txtFromDate, #txtToDate, #txtVFromDate, #txtVToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set From Date
    var a = localStorage.getItem("FinYear");
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
        $('#txtVFromDate').val(validDate);


    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);
    $('#txtVToDate').val(output);
}

function PostingDate(BankCode)
{
    var E = "{TransType: '" + "FileName" + "', SectorID: '" + $("#ddlSector").val() + "', Desc: '" + BankCode + "', AccountCode: '" + "" + "'}";

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BankStatementExcelUpload/Load',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('#ddlPostingDate').empty();
            $('#ddlPostingDate').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('#ddlPostingDate').append('<option value=' + item.PostingDate + '>' + item.PostingDate + '</option>');
                });
            }
        }
    });
}

function Photo() {

    if ($('#hdnAccCode').val() == "") { $('#txtBankName').focus(); return false; }
    if ($('#ddlDRCR').val() == "") { $('#ddlDRCR').focus(); return false; }

    var filename = $('#flPic').val();
    if (filename.length == 0) {
        alert("Please Select (.xls, .xlsx) file.");
        $('#flPic').val("");
        return false;
    }

    var fileExtension = ['xls', 'xlsx'];
    var filename = $('#flPic').val();

    if (filename.length == 0) {
        alert("Please Select (.xls, .xlsx) file.");
        $('#flPic').val("");
        return false;
    }

    var extension = filename.replace(/^.*\./, '');
    if ($.inArray(extension, fileExtension) == -1) {
        alert("Please select only (.xls, .xlsx) file.");
        $('#flPic').val("");
        return false;
    }
    Upload_Item('flPic');
}

function Upload_Item(flPic) {



    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#flPic').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file);
    }

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BankStatementExcelUpload/Upload',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
            var t = response.Data;

            if (t == 0) {
                alert('Sorry ! There is some proble to upload Excel file.'); return false;
            }
            else {
                fileName = t;

                Checking(t);
            }
        }
    });
}

function Checking(fileName) {

    var E = "{FileName: '" + fileName + "', BankCode: '" + $('#hdnAccCode').val() + "'," +
        "SectorID: '" + $('#ddlSector').val() + "', DRCR: '" + $('#ddlDRCR').val() + "'} ";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/BankStatementExcelUpload/Checking",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data; //alert(JSON.stringify(t));
            if (t.length > 0) {

                //$('#txtBankName').val('');
                //$('#hdnAccCode').val('');
                //$('#flPic').val('');
                var MsgCode = t[0].MsgCode;
                var Msg = t[0].Msg;
                if (MsgCode == 1) {
                    $('.isdrcr').text($('#ddlDRCR').val() == "D" ? "(Dr.)" : "(Cr.)");

                    $('#tbl tbody tr.myData').empty();
                    var html = "", Total = 0;
                    for (var i = 0; i < t.length; i++) {
                        Total = Total + parseFloat(t[i]["Amount"]);
                        html += "<tr class='myData'>";
                        html += "<td style='text-align:center; display:none;' class='cls_StatementID'>" + t[i]["StatementID"] + "</td>";
                        html += "<td style='text-align:center; display:none;' class='cls_BankCode'>" + t[i]["BankCode"] + "</td>";
                        html += "<td style='text-align:center' class='cls_TxnDate'>" + t[i]["TxtDate"] + "</td>";
                        html += "<td class='cls_Description'>" + t[i]["Description"] + "</td>";
                        html += "<td style='text-align:right' class='cls_Amount'>" + t[i]["Amount"] + "</td>";

                        html += "<td style='text-align:center;'><input id='dt_" + i + "' class='cls_PostingDate' type='text' value='" + t[i]["PostingDate"] + "' placeholder='DD/MM/YYYY' />";
                        html += "<script type='text/javascript'> $('#dt_" + i + "').datepicker({";
                        html += "showOtherMonths: true,";
                        html += "selectOtherMonths: true,";
                        html += "closeText: 'X',";
                        html += "showAnim: 'drop',";
                        html += "changeYear: true,";
                        html += "changeMonth: true,";
                        html += "duration: 'slow',";
                        html += "dateFormat: 'dd/mm/yy',";
                        html += "});</script></td>";

                        html += "<td>"
                        html += "<input type= 'text' style= 'width:100%' class='clsLedger' id= 'lgr_" + i + "' sub-ledger='N' onkeyup= 'Clear(this, event);' value= '" + (t[i]["AccountDescription"] == null ? "" : t[i]["AccountDescription"]) + "' placeholder='Select Ledger...' />";
                        html += "<input type='hidden' class='clsAccCodeLedger' value='" + t[i]["LedgerCode"] + "' />"

                        html += "<input type= 'text' style= 'width:100%; display:" + ((t[i]["SubLedgerCode"] == "" || t[i]["SubLedgerCode"] == "null" || t[i]["SubLedgerCode"] == null) ? "none" : "") + "' class='clssubLedger' id= 'lgrsub_" + i + "' onkeyup= 'Clear(this, event);' value= '" + (t[i]["SubAccountDescription"] == null ? "" : t[i]["SubAccountDescription"]) + "' placeholder='Select SubLedger...' />";
                        html += "<input type='hidden' class='clsAccCodeSubLedger'  value='" + t[i]["SubLedgerCode"] + "' />"
                        html += "</td> ";

                        if ($('#ddlDRCR').val() == "D") {
                            html += "<td style='text-align:center;'><div ><a class='save not-active' onclick='Save(this,1);'>SAVE</a>&nbsp;&nbsp;&nbsp;";
                            html += "<a class='print' vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='PrintVoucher(this,1);'>PRINT</a></div></td > ";
                        }
                        else {
                            html += "<td style='text-align:center;'><div><a class='save' onclick='Save(this,1);'>SAVE</a>&nbsp;&nbsp;&nbsp;";
                            html += "<a class='print' vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='PrintVoucher(this,1);'>PRINT</a></div></td > ";
                        }


                        html += "</tr> ";
                    }
                    $('#tbl tbody').append(html);

                    $('#tbl tfoot').empty();
                    if (Total > 0) {
                        $('#tbl tfoot').append("<tr><td colspan='2' style='text-align:right; font-weight:bold;'>Total (&#8377)</td><td  style='text-align:right; font-weight:bold;'>" + Total + "</td><td colspan='3'></td></tr>");
                    }
                }
                else
                {
                    alert(Msg); return false;
                }
            }
        }
    });
}

function Clear(ID, e)
{
    //var iKeyCode = (e.which) ? e.which : e.keyCode
    //if (iKeyCode == 8) {
    //     $(ID).val('');
    //    $(ID).closest('tr').find('.clsAccCodeLedger').val('');
    //}
    //if (iKeyCode == 46) {
    //    $(ID).val('');
    //    $(ID).closest('tr').find('.clsAccCodeLedger').val('');
    //}
}

function Save(ID, CON)
{
    var LedgerCode = $(ID).closest('tr').find('.clsAccCodeLedger').val();  
    var SubLedgerCode = $(ID).closest('tr').find('.clsAccCodeSubLedger').val(); 
    var isSubLedr = $(ID).closest('tr').find('.clsLedger').attr('sub-ledger'); 

    if (LedgerCode == "" || LedgerCode == 'null') { $(ID).closest('tr').find('.clsLedger').focus(); return false; }
    if (isSubLedr == "Y" && (SubLedgerCode == "" || SubLedgerCode == null || SubLedgerCode == "null")) {
        $(ID).closest('tr').find('.clssubLedger').focus(); return false;
    }

    var res = confirm('Are you sure want to Save ??');
    if (res == true)
    {
        var PostingDate = "";
        var StatementID = $(ID).closest('tr').find('.cls_StatementID').text(); 
        var TxnDate = $(ID).closest('tr').find('.cls_TxnDate').text(); 
        var LedgerCode = $(ID).closest('tr').find('.clsAccCodeLedger').val();
        var SubLedgerCode = $(ID).closest('tr').find('.clsAccCodeSubLedger').val();
        var BankCode = $(ID).closest('tr').find('.cls_BankCode').text(); 
        var Amount = $(ID).closest('tr').find('.cls_Amount').text(); 
        var Description = $(ID).closest('tr').find('.cls_Description').text(); 
        if (CON == 1)
            PostingDate = $(ID).closest('tr').find('.cls_PostingDate').val(); 
        else
         PostingDate = $(ID).closest('tr').find('.cls_PostingDate1').val(); 

        var E = "{TxnDate: '" + TxnDate + "', Amount: '" + Amount + "', Description: '" + Description + "', SectorID: '" + $('#ddlSector').val() + "'," +
            "BankCode: '" + BankCode + "', LedgerCode: '" + LedgerCode + "', SubLedgerCode: '" + SubLedgerCode + "', StatementID: '" + StatementID + "'," +
            "PostingDate: '" + PostingDate + "', VoucherNumber: '" + "" + "'} ";

        alert(E); return false;
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/BankStatementExcelUpload/Save",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (r) {

                var t = r.Data;
                var code = r.Status;
                var msg = r.Message;

                if (code == 200)
                {
                    var vchNo = t[0].VchNo;
                    var vchRefNo = t[0].RefVoucherSlNo;

                    //$('#save').css('display', 'none');
                    $('.print').css('display', '');
                    $('.print').attr('vchno', vchNo);
                    $('.print').attr('refvchno', vchRefNo);

                    swal({
                        title: "Success",
                        text: 'Account Posting done Successfully !!\nVoucher No. ' + vchRefNo,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                //location.reload();
                                //$('#print').css('display', '');
                                return false;
                            }
                        });
                }
                else
                {
                    swal({
                        title: "Errors",
                        text: r.Message,
                        type: "warning",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                return false;
                            }
                        });
                }
            }
        });
    }
}

function Update(ID) {
    var LedgerCode = $(ID).closest('tr').find('.clsAccCodeLedger').val();
    if (LedgerCode == "") {
        $(ID).closest('tr').find('.clsLedger').focus(); return false;
    }
    var res = confirm('Are you sure want to Save ??');
    if (res == true) {
        var StatementID = $(ID).closest('tr').find('.cls_StatementID').text();
        var TxnDate = $(ID).closest('tr').find('.cls_TxnDate').text();
        var LedgerCode = $(ID).closest('tr').find('.clsAccCodeLedger').val();
        var BankCode = $(ID).closest('tr').find('.cls_BankCode').text();
        var Amount = $(ID).closest('tr').find('.cls_Amount').text();
        var Description = $(ID).closest('tr').find('.cls_Description').text();
        var PostingDate = $(ID).closest('tr').find('.cls_PostingDate').text();

        var E = "{TxnDate: '" + TxnDate + "', Amount: '" + Amount + "', Description: '" + Description + "', SectorID: '" + $('#ddlSector').val() + "'," +
            "BankCode: '" + BankCode + "', LedgerCode: '" + LedgerCode + "', StatementID: '" + StatementID + "', VchNo: '" + VchNo + "'," +
            "PostingDate: '" + PostingDate + "'}";

        $.ajax({
            type: "POST",
            url: "/Accounts_Form/BankStatementExcelUpload/VoucherEntry",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (r) {

                var t = r.Data;
                var a = t.split('#');

                $('#save').css('display', 'none');
                $('#print').css('display', '');
                $('#print').attr('vchno', a[0]);
                $('#print').attr('refvchno', a[1]);

                swal({
                    title: "Success",
                    text: 'Account Posting done Successfully !!\nVoucher No. ' + a[1],
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            //location.reload();
                            $('#print').css('display', '');
                            return false;
                        }
                    });

            }
        });
    }
}

function PrintVoucher(ID, CON)
{
    var final = {}; var master = []; var detail = [];
    if (CON == 1)
    {
        if ($(ID).closest('tr').find('.print').attr('vchno') > 0) {

            master.push({
                ReportName: "VoucherCash.rpt",
                FileName: $(ID).closest('tr').find('.print').attr('refvchno'),
                Database: ''
            });

            detail.push({
                VoucherNumber: $(ID).closest('tr').find('.print').attr('vchno'),
                sectorid: $("#ddlSector").val()
            });

            final = {
                Master: master,
                Detail: detail
            }

            var left = ($(window).width() / 2) - (950 / 2),
                top = ($(window).height() / 2) - (650 / 2),
                popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
            popup.focus();
        }
    }
    else
    {
        if ($(ID).closest('tr').find('.print1').attr('vchno') > 0) {

            master.push({
                ReportName: "VoucherCash.rpt",
                FileName: $(ID).closest('tr').find('.print1').attr('refvchno'),
                Database: ''
            });

            detail.push({
                VoucherNumber: $(ID).closest('tr').find('.print1').attr('vchno'),
                sectorid: $("#ddlSector").val()
            });

            final = {
                Master: master,
                Detail: detail
            }

            var left = ($(window).width() / 2) - (950 / 2),
                top = ($(window).height() / 2) - (650 / 2),
                popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
            popup.focus();
        }

    }


}

function Sample() {

    var res = confirm('Are You Sure Want to download Excel Sample ?');
    if (res == true) {
        location.href = "/Accounts_Form/BankStatementExcelUpload/Excel";
    }

}

function Search()
{
    var fromdt = $("#txtFromDate").val();
    var todt = $("#txtToDate").val();
    var BankCode = $("#hdnSAccCode").val();
    //var PostingDate = $("#ddlPostingDate option:selected").text(); 
    var drcr = $("#ddlSDRCR").val();
    var Rtype = $("#ddlRtype").val();

    if (fromdt == "") { $("#txtFromDate").focus(); return false; }
    if (todt == "") { $("#txtToDate").focus(); return false; }
    if (BankCode == "") { $("#txtSBankName").focus(); return false; }
    //if (PostingDate == "") { $("#ddlPostingDate").focus(); return false; }
    if (drcr == "") { $("#ddlSDRCR").focus(); return false; }
    if (Rtype == "") { $("#ddlRtype").focus(); return false; }

    var E = "{TransType: '" + "Search" + "', BankCode: '" + BankCode + "'," +
        "SectorID: '" + $('#ddlSector').val() + "', DRCR: '" + drcr + "', RType: '" + Rtype + "'," +
        "FromDate: '" + fromdt + "', ToDate: '" + todt + "'} ";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/BankStatementExcelUpload/Search",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data; /*alert(JSON.stringify(t));*/
            if (t.length > 0) {

                $('#tbl1 tbody tr.myData').empty();
                var html = "", Total = 0, color = '';
                for (var i = 0; i < t.length; i++) {
                    Total = Total + parseFloat(t[i]["Amount"]);
                    if (t[i]["Color"] == 'Y') { color = '#FCF3CF'; }
                    else if (t[i]["Color"] == 'G') { color = '#D5F5E3'; }
                    else color = 'white';

                    html += "<tr class='myData' style='background-color:" + color +"'>";
                    html += "<td style='text-align:center; display:none;' class='cls_StatementID'>" + t[i]["StatementID"] + "</td>";
                    html += "<td style='text-align:center; display:none;' class='cls_BankCode'>" + t[i]["BankCode"] + "</td>";
                    html += "<td style='text-align:center; display:none;' class='cls_PostingDate'>" + t[i]["PostingDate"] + "</td>";
                    html += "<td style='text-align:center' class='cls_TxnDate'>" + t[i]["TxtDate"] + "</td>";
                    html += "<td class='cls_Description'>" + t[i]["Description"] + "</td>";
                    html += "<td style='text-align:right' class='cls_Amount'>" + t[i]["Amount"] + "</td>";

                    html += "<td style='text-align:center;'><input id='dtt_" + i + "' class='cls_PostingDate1' type='text' value='" + t[i]["PostingDate"] + "' placeholder='DD/MM/YYYY' />";
                    html += "<script type='text/javascript'> $('#dtt_" + i + "').datepicker({";
                    html += "showOtherMonths: true,";
                    html += "selectOtherMonths: true,";
                    html += "closeText: 'X',";
                    html += "showAnim: 'drop',";
                    html += "changeYear: true,";
                    html += "changeMonth: true,";
                    html += "duration: 'slow',";
                    html += "dateFormat: 'dd/mm/yy',";
                    html += "});</script></td>";

                    html += "<td>"
                    html += "<input type= 'text' style= 'width:100%' class='clsLedger' id= 'lgr_" + i + "' sub-ledger='N' onkeyup='Clear(this, event);' value= '" + (t[i]["AccountDescription"] == null ? "" : t[i]["AccountDescription"]) + "' placeholder='Select Ledger...' />";
                    html += "<input type='hidden' class='clsAccCodeLedger' value='" + t[i]["LedgerCode"] + "' />"
                   
                    html += "<input type= 'text' style= 'width:100%; display:" + ((t[i]["SubLedgerCode"] == "" || t[i]["SubLedgerCode"] == "null" || t[i]["SubLedgerCode"] == null) ? "none" : "") + "' class='clssubLedger' id= 'lgrsub_" + i + "' onkeyup= 'Clear(this, event);' value= '" + (t[i]["SubAccountDescription"] == null ? "" : t[i]["SubAccountDescription"]) + "' placeholder='Select SubLedger...' />";
                    html += "<input type='hidden' class='clsAccCodeSubLedger'  value='" + t[i]["SubLedgerCode"] + "' />"
                    html += "</td> ";

                    if ($('#ddlSDRCR').val() == "D") {
                        html += "<td style='text-align:center;'><div ><a class='save not-active' onclick='Save(this, 2);'>SAVE</a>&nbsp;&nbsp;&nbsp;";
                        html += "<a class='print1' vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='PrintVoucher(this, 2);'>PRINT</a>&nbsp;&nbsp;&nbsp;";
                        html += "<a class='view1'  vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='ShowDialog(this, 2);'>MAPPING</a></div></td > ";
                    }
                    else {
                        html += "<td style='text-align:center;'><div><a class='save' onclick='Save(this,2);'>SAVE</a>&nbsp;&nbsp;&nbsp;";
                        html += "<a class='print1' vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='PrintVoucher(this, 2);'>PRINT</a>&nbsp;&nbsp;&nbsp;";
                        html += "<a class='view1'  vchno='" + t[i]["VchNo"] + "' refvchno='" + t[i]["RefVoucherSlNo"] + "' onclick='ShowDialog(this, 2);'>MAPPING</a></div></td > ";
                    }

                    html += "</tr> ";
                }
                $('#tbl1 tbody').append(html);

                $('#tbl1 tfoot').empty();
                if (Total > 0) {
                    $('#tbl1 tfoot').append("<tr><td colspan='2' style='text-align:right; font-weight:bold;'>Total (&#8377)</td><td  style='text-align:right; font-weight:bold;'>" + Total + "</td><td colspan='3'></td></tr>");
                }
            }
            else
            {
                $('#tbl1 tbody tr.myData').empty();
                $('#tbl1 tfoot').empty();
            }
        }
    });
}

function ShowDialog(ID, CON)
{
    var table = $('#datatable').DataTable();
    table.clear().draw();
    table.destroy();

    var PostingDate = "";
    var LedgerCode = $(ID).closest('tr').find('.clsAccCodeLedger').val(); 
    var SubLedgerCode = $(ID).closest('tr').find('.clsAccCodeSubLedger').val(); 
    var IsSubLedger = $(ID).closest('tr').find('.clsLedger').attr('sub-ledger'); 
    var StatementID = $(ID).closest('tr').find('.cls_StatementID').text(); 
	var BankCode = $(ID).closest('tr').find('.cls_BankCode').text(); 
    if (CON == 1)
        PostingDate = $(ID).closest('tr').find('.cls_PostingDate').val();
    else
        PostingDate = $(ID).closest('tr').find('.cls_PostingDate1').val(); 

    $('#hdnLedgerCode').val(LedgerCode);
    $('#hdnSubLedgerCode').val(SubLedgerCode);
    $('#hdnIsSubLedger').val(IsSubLedger);
    $('#hdnStatementID').val(StatementID);
    $('#hdnPostingDate').val(PostingDate);
	$('#hdnVBankCode').val(BankCode);

    $('#dialog').modal('show');
}
function ShowVoucher()
{
    if ($("#txtVFromDate").val() == "") { $("#txtVFromDate").focus(); return false; }
    if ($("#txtVToDate").val() == "") { $("#txtVToDate").focus(); return false; }
	  //if ($("#hdnAccCode").val() == "") { alert('Sorry ! Please Select Bank Name.'); return false; }

    var E = "{TransType: '" + "VoucherList" + "', SectorID: '" + $("#ddlSector").val() + "', FromDate: '" + $("#txtVFromDate").val() + "', ToDate: '" + $("#txtVToDate").val() + "', BankCode: '" + $("#hdnVBankCode").val() + "'}";

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BankStatementExcelUpload/VchList',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data;

            var columnData = [
                { "mDataProp": "RefVoucherSlNo" },
                { "mDataProp": "VoucherDate" },
                { "mDataProp": "VoucherAmount" },
                { "mDataProp": "Narration" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        return '<a href="javascript:void(0)" title="Confrim" style="text-align:center" class="btn btn-azure btn-xs" onclick="Confrim(\'' + row.VoucherNumber + '\')" ><span class="label label-info pull-right">Confirm</span>  </a>';
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": 2,
                        "className": "text-right",
                        "width": "10%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });
          
        }
    });
}
function Confrim(VoucherNumber)
{
   var res = confirm('Voucher Number will be mapped. Are You Sure ?');
    if (res == true)
    {
        var LedgerCode = $('#hdnLedgerCode').val(); 
        var SubLedgerCode = $('#hdnSubLedgerCode').val();
        var IsSubLedger = $('#hdnIsSubLedger').val();
        var StatementID = $('#hdnStatementID').val();
        var PostingDate = $('#hdnPostingDate').val();
        //alert(LedgerCode); alert(SubLedgerCode); alert(IsSubLedger);
        //if (LedgerCode == "" || LedgerCode == 'null') {
        //    alert('Sorry ! Please Confirm Ledger.');
        //    return false;
        //}
        //if (IsSubLedger == "Y" && (SubLedgerCode == "" || SubLedgerCode == null || SubLedgerCode == "null")) {
        //    alert('Sorry ! Please Confirm Sub-Ledger.'); return false;
        //}
        var E = "{TxnDate: '" + "" + "', Amount: '" + "" + "', Description: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "'," +
            "BankCode: '" + "" + "', LedgerCode: '" + "" + "', SubLedgerCode: '" + "" + "', StatementID: '" + StatementID + "'," +
            "PostingDate: '" + PostingDate + "', VoucherNumber: '" + VoucherNumber + "'} ";

        //alert(E); return false;
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/BankStatementExcelUpload/Save",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (r) {

                var t = r.Data;
                var code = r.Status;
                var msg = r.Message;

                if (code == 200) {
                    var vchNo = t[0].VchNo;
                    var vchRefNo = t[0].RefVoucherSlNo;

                      //$('#save').css('display', 'none');
                    //$('.print').css('display', '');
                    $('.print').attr('vchno', vchNo);
                    $('.print').attr('refvchno', vchRefNo);

                    //$('.print1').css('display', '');
                    $('.print1').attr('vchno', vchNo);
                    $('.print1').attr('refvchno', vchRefNo);

                    swal({
                        title: "Success",
                        text: 'Voucher Number mapped Successfully !!\nVoucher No. ' + vchRefNo,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                //location.reload();
                                //$('#print').css('display', '');
                                return false;
                            }
                        });
                }
                else {
                    swal({
                        title: "Errors",
                        text: r.Message,
                        type: "warning",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                return false;
                            }
                        });
                }
            }
        });
    }
}

