﻿$(document).ready(function () {
    Load();
   
    $(document).on('click', '.txt-teacher', function () {
        AutoComplete_Teacher(this);
    });
    $(document).on('click', '.txt-subject', function () {
        AutoComplete_Subject(this);
    });
});

function Load() {
    var E = "{ClassID: '" + 1 + "', SectionID: '" + 1 + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/Checking/Load",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; //alert(JSON.stringify(t));

            var table_1 = t.Table; //alert(JSON.stringify(table_1));
            var table_2 = t.Table1; //alert(JSON.stringify(table_2));
            var table_3 = t.Table2; //alert(JSON.stringify(table_3));

            Headers(table_2[0].Period);
            Body(table_1, table_2[0].Period, table_3);
        }
    });
}
function Headers(noofcol) {
    var table = $('#tbl');
    var thead = table.find('thead');
    thead.empty();

    var html = "<tr><th cls-period='" + 0 +"' class='header'></th>";
    for (var i = 1; i <= noofcol; i++) {
        html += "<th style='text-align:center;' cls-period='"+ i +"' class='header'>" + "Period-" + i + "</th>";
    }
    html += "</tr>";

    thead.append(html);
}
function Body(weekdetail, noofcol, data) {
    var table = $('#tbl');
    var thead = table.find('thead');
    var tbody = table.find('tbody');
    tbody.empty();

    if (weekdetail.length > 0) {
        $(weekdetail).each(function (index, item) {
            var $tr = getRow(item, data, noofcol);
            tbody.append($tr);
        });
    }
}
function getRow(week, data, noofcol) {
    var $tr = $("<tr cls-weekid='" + week.WeekID +"'/>");
    var $td = $("<td />");
    $td.text(week.WeekName)
    $tr.append($td);
    for (var i = 1; i <= noofcol; i++) {

        var weekData = data.filter(function (v, ii) {
            return v.WeekID == week.WeekID;
        });
       
        $td = $("<td />");
        $td.append("<input type='text' cls-periodid='" + i + "' class='txt-teacher' placeholder='Teacher' />" +
            "<input type='hidden' class='hdn-teacher' value='' />"+
            "</br>"+
            "<input type='text' cls-periodid='" + i + "' placeholder='Subject' class='txt-subject' />" +
            "<input type='hidden' class='hdn-subject' value='' />")
        $tr.append($td);

        var findValue = weekData.find(function (v, ii) {
            return v.PeriodID === i;
        })
       
        if (findValue) {
            $td.find(".txt-teacher").val(findValue.TeacherName);
            $td.find(".txt-subject").val(findValue.SubjectName);

            $td.find(".hdn-teacher").val(findValue.TeacherID);
            $td.find(".hdn-subject").val(findValue.SubjectID);
        }
        $tr.append($td);
    }
    return $tr;
}
function Save(ID, TeacherID, SubjectID)
{
    var WeekID = $(ID).closest('tr').attr('cls-weekid'); //alert(WeekID);
    var PeriodID = $(ID).attr('cls-periodid'); //alert(PeriodID); return false;

    var E = "{ClassID: '" + 1 + "', SectionID: '" + 1 + "', WeekID: '" + WeekID + "', PeriodID: '" + PeriodID + "', TeacherID: '" + TeacherID + "', SubjectID: '" + SubjectID + "', TransType: '" + "Save" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/Checking/Save",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
           
        }
    });
}

function AutoComplete_Teacher(ID)
{
    $(ID).autocomplete({
       
        source: function (request, response) {
           
            var E = "{Desc: '" + $(ID).val() + "', TransType: '" + "Teacher" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/Checking/Auto_Teacher",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {
            var AccCode = i.item.AccountCode;
            $(ID).closest('tr').find('.hdn-teacher').val(AccCode);
            var SubjectID = $(ID).closest('tr').find('.hdn-subject').val();
            Save(ID, AccCode, SubjectID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}
function AutoComplete_Subject(ID) {
    $(ID).autocomplete({

        source: function (request, response) {

            var E = "{Desc: '" + $(ID).val() + "', TransType: '" + "Subject" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/Checking/Auto_Teacher",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {
            var AccCode = i.item.AccountCode;
            $(ID).closest('tr').find('.hdn-subject').val(AccCode);
            var TeacherID = $(ID).closest('tr').find('.hdn-teacher').val();
            Save(ID, TeacherID, AccCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}
