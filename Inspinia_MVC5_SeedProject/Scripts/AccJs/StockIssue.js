﻿$(document).ready(function () {

    Set_StockIssue_DefaultDate();
    Bind_StockIssue_Section();


    //    ITEM AUTOCOMPLETE
    $('#txtStockIssueItemName').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_ParentItem" + "' , Desc:'" + $('#txtStockIssueItemName').val() + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnItemCode').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtStockIssueItemName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtStockIssueItemName").val('');
            $("#hdnItemCode").val('');
        }
        if (iKeyCode == 46) {
            $("#txtStockIssueItemName").val('');
            $("#hdnItemCode").val('');
        }
    });
});
function Set_StockIssue_DefaultDate() {
    $('#txtIssueDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtIssueDate').val(output);
}

// SECTION
function Bind_StockIssue_Section() {
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BillExtract/Bind_Section',
        contentType: "application/json; charset=utf-8",
        data: {},
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('.Section').empty();
            $('.Section').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('.Section').append('<option value=' + item.SectionID + '>' + item.SectionName + '</option>');
                });
            }
        }
    });

}

function Add() {
    if ($("#hdnItemCode").val() == "") { $("#txtStockIssueItemName").focus(); return false; }
    if ($("#txtUnit").val() == "") { $("#txtUnit").focus(); return false; }
    if ($("#txtItemQty").val() == "") { $("#txtItemQty").focus(); return false; }
    var chkAccCode = $("#hdnItemCode").val();

    var l = $('#tblStockIssue tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Item is Already Available.'); $("#txtStockIssueItemName").val(''); $("#hdnItemCode").val(''); return false;
    }

    var html = ""

    var $this = $('#tblStockIssue .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnItemCode").val() + "' >" + $("#hdnItemCode").val() + "</td>"
        + "<td class='itemName'>" + $("#txtStockIssueItemName").val() + "</td>"
        + "<td class='unit'>" + $("#txtUnit").val() + "</td>"
        + "<td class='qty'>" + $("#txtItemQty").val() + "</td>"
        + "<td class='remarks'>" + $("#txtItemRemarks").val() + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_StockIssue(this);' />" 
        + "<img src='/Content/Images/Edit.png' title='Edit' style='width:25px;height:23px;cursor:pointer; text-align:center;' onClick='Edit_StockIssue(this);' /></td > "
    html + "</tr>";

    $parentTR.after(html);

    $("#txtStockIssueItemName").val(''); $("#hdnItemCode").val(''); $("#txtUnit").val(''); $("#txtItemQty").val(''); $("#txtItemRemarks").val('');
}

function Edit_StockIssue(ID)
{
    var itemcode = $(ID).closest('tr').find('.clsAccCode').text();
    var itemName = $(ID).closest('tr').find('.itemName').text();
    var unit = $(ID).closest('tr').find('.unit').text();
    var qty = $(ID).closest('tr').find('.qty').text();
    var remarks = $(ID).closest('tr').find('.remarks').text();

    $("#txtStockIssueItemName").val(itemName); $("#hdnItemCode").val(itemcode); $("#txtUnit").val(unit); $("#txtItemQty").val(qty); $("#txtItemRemarks").val(remarks);
    $(ID).closest('tr').remove(); 
}

function Delete_StockIssue(ID) {
    $(ID).closest('tr').remove();
}

function Refresh()
{
    location.reload();
}

function StockIssueBlank()
{
    $('.stockissue').toggleClass('toggled');
}

function StockIssueSave()
{
    if ($("#txtIssueDate").val() == "") { $("#txtIssueDate").focus(); return false; }
    if ($("#ddlStockFrom").val() == "") { $("#ddlStockFrom").focus(); return false; }
    if ($("#ddlStockTo").val() == "") { $("#ddlStockTo").focus(); return false; }
    if ($("#ddlSectionFrom").val() == "") { $("#ddlSectionFrom").focus(); return false; }
    if ($("#ddlSectionStockIssueTo").val() == "") { $("#ddlSectionStockIssueTo").focus(); return false; }
    if ($("#txtRemarks").val() == "") { $("#txtRemarks").focus(); return false; }

    var l = $("#tblStockIssue tbody tr.myData").length;
    if (l == 0) {
        alert('Please ! add Item details.'); return false;
    }

    var master = {};
    master = {
        IssueId: $('#hdnIssueId').val(),
        IssueNo: $('#txtIssueNo').val(),
        Issuedt: $('#txtIssueDate').val(),
        StockPointIdFrom: $('#ddlStockFrom').val(),
        SectionIdFrom: $('#ddlSectionFrom').val(),
        StockPointIdTo: $('#ddlStockTo').val(),
        SectionIdTo: $('#ddlSectionStockIssueTo').val(),
        //IssueStatus: $('#hdnShipTo_bill').val(),
        Remarks: $('#txtRemarks').val(),
        SectorId: $('#ddlSector').val()
    }
    //alert(JSON.stringify(master));

    var grdLen = $('#tblStockIssue tbody tr.myData').length; var ArrList = [];
    if (grdLen > 0) {
        $('#tblStockIssue tbody tr.myData').each(function () {

            var ItemID = $(this).find('.clsAccCode').text();
            var unit = $(this).find('.unit').text();
            var qty = $(this).find('.qty').text();
            var remarks = $(this).find('.remarks').text();
            var SectorId = $('#ddlSector').val();

            ArrList.push({
                'ItemId': ItemID, 'Uom': unit, 'qty': qty, 'Remarks': remarks, 'SectorId': SectorId
            });
        });
    }

    var E = "{StockIssue: " + JSON.stringify(master) + ", Detail: " + JSON.stringify(ArrList) + "}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/Item/InsertUpdate_StockIssue',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t != "") {

                //$('#hdnBillID').val(t);

               
                    swal({
                        title: "Success",
                        text: 'Stock Issue Details are Saved Successfully !!\n Issue No.  ' + t,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });
                
            }
        }
    });
}