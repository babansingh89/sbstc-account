﻿var pathString = "", DeductionType = "P", boolDesc = "", jLedger="", pLedger="", boolPayDesc = "", isPostBack = 0, SlNo = 0, con = "0";
var isConfirmBudgetPostBack = 0; var Acc_OpenDate = '';
var ArrayOpenListID = [];

$(document).ready(function () {
    $('.modal-dialog').draggable();
    $('.modal-content').resizable({
        alsoResize: ".modal-dialog",
        minHeight: 300,
        minWidth: 300
    });

    Bind_Grid();

    FromDate();
    ToDate();
    Bind_MemoDate();
    Bind_InvoiceDate();
    Bind_BillStatusDate();
    Bind_AllotedDate();
    Bind_Section();
    Bind_ChequeDate();
    //Auto_User();

    Autocomplete_PartyNo();
    Autocomplete_FCNo();
    Autocomplete_CFCNo();
    Autocomplete_MemoNo();
	Autocomplete_PartyName();
	Autocomplete_PendingUser();


        populateDDllFinYear();
     $("#btnSave").click(CheckClosingStatus);  
    $("#btnRefresh").click(Refresh);

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#txtPartyName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtPartyName").val('');
            $("#hdnPartyName").val('');
            $("#txtPayeeName").val('');
        }
        if (iKeyCode == 46) {
            $("#txtPartyName").val('');
            $("#hdnPartyName").val('');
            $("#txtPayeeName").val('');
        }
    });
    $('#txtEfftGL').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtEfftGL").val('');
            $("#hdnEffectAdd").val('');
            $("#hdnEffectAmount").val('');
            $("#hdnEffectTempBudgetID").val('');
            $("#txtEfftAmount").val('');
            $('#lblFCTotalAmt').text('0.00');
        }
        if (iKeyCode == 46) {
            $("#txtEfftGL").val('');
            $("#hdnEffectAdd").val('');
            $("#hdnEffectAmount").val('');
            $("#hdnEffectTempBudgetID").val('');
            $("#txtEfftAmount").val('');
            $('#lblFCTotalAmt').text('0.00');
        }
    });
    $('#txtCEfftGL').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtCEfftGL").val('');
            $("#hdnCEffectAdd").val('');
            $("#hdnCEffectAmount").val('');
            $("#hdnCEffectTempBudgetID").val('');
            $("#txtCEfftAmount").val('');
            $('#lblCFCTotalAmt').text('0.00');
        }
        if (iKeyCode == 46) {
            $("#txtCEfftGL").val('');
            $("#hdnCEffectAdd").val('');
            $("#hdnCEffectAmount").val('');
            $("#hdnCEffectTempBudgetID").val('');
            $("#txtCEfftAmount").val('');
            $('#lblCFCTotalAmt').text('0.00');
        }
    });

    $(".AllotedUserlist").on('click', ".delete", function () {
        $(this).closest(".lstuser").remove();
    });
    $('.AllotedUserlist').on('click', '.select-check-action', function () {

        var UserID = $(this).val(); 

        var isObjection = $('#chkisObjection').prop('checked'); 

      if (isObjection == true) {
            $('.select-check-action').prop('checked', false);
            $('.select-check-action').addClass("notchk");
        }

        if (isObjection == true) {

            if ($(this).hasClass('notchk') == true) {
                $(this).prop("checked", true);
                $(this).removeClass("notchk");
            }
            else {
                $(this).prop("checked", false);
                $(this).addClass("notchk");
            }
           
            $('.clsUserWork').empty();
            $('.clsUserWork').append('<option value="" selected>Select</option');
            $('.clsUserWork').css('display', 'none');

            Bind_UserWork(UserID, this);

        }
        else {
            if ($(this).hasClass('notchk') == false) {
              
                $(this).prop("checked", false);
                $(this).addClass("notchk");
            }
            else {
               
                $(this).prop("checked", true);
                $(this).removeClass("notchk");
            }
        }
    });


    AccountOpeningDate();
});

function Bind_MemoDate()
{
    $('#txtMemoDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText, inst) {
            $('#txtFCNo').val(''); $('#hdnFCNo').val('');
        }
    });

        //Set To Date
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        $('#txtMemoDate').val(output);
      $('#txtjournaldate').val(output);
}
function Bind_RefDate() {
    $('#txtRefDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function FromDate() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function ToDate() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Bind_ChequeDate() {
    $('#txtChequeDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Bind_InvoiceDate() {
    $('#txtInvoiceDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Bind_BillStatusDate() {
    $('#txtBillDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Bind_AllotedDate() {
    $('#txtAllotedDate, #txtSRVDate, #txtRefDate, #txtjournaldate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

  //    PARTY NAME AUTOCOMPLETE
function Autocomplete_PartyNo()
{
    $('#txtPartyName').autocomplete({
        source: function (request, response) {

            var GLS = "S";

            var S = "{BankCash:'" + GLS + "', Desc:'" + $('#txtPartyName').val() + "', TransType:'" + "Subledger" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/BillExtract/BankCash',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = []; 
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode,
								 PayeeName: item.PayeeName
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnPartyName').val(i.item.AccountCode);

            if (i.item.label != "Not Applicable")
               $('#txtPayeeName').val(i.item.PayeeName);

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

//    FC NO. AUTOCOMPLETE
function Autocomplete_FCNo() {
    $('#txtEfftGL').autocomplete({
        source: function (request, response) {

            var S = "{TransType:'" + "FC" + "', TabID:'" + "" + "', Desc:'" + $('#txtEfftGL').val() + "', MemoDate:'" + $('#txtMemoDate').val() + "',"+
                "SectorID: '" + $('#ddlSector').val() + "', BudgetID:'" + "" + "', FinYear:'" + $("#ddlBillAbstractFinYear option:selected").text() + "'," +
                "MainFinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "', Flag:'" + "M" + "'}"; //$('#hdnEffectBudgetID').val()
            $.ajax({
                url: '/Accounts_Form/BillExtract/Auto_FC',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BudgetDesc,
                                ReferanceNo: item.ReferanceNo,
                                BudgetDesc: item.BudgetDesc,
                                ConcurranceAmount: item.ConcurranceAmount,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnEffectAdd').val(i.item.ReferanceNo);
            $('#hdnEffectAmount').val(i.item.ConcurranceAmount);
            $('#lblFCTotalAmt').text(i.item.ConcurranceAmount);
            //$('#txtEfftAmount').val(i.item.ConcurranceAmount);
            $('#hdnEffectTempBudgetID').val(i.item.BudgetID);

            CheckFCLimit(1);
        },
        minLength: 0
    }).click(function () {
        if ($('#txtMemoDate').val() == "")
        {
            alert('Enter Memo Date first.'); $('#txtMemoDate').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });
}

//    CFC NO. AUTOCOMPLETE
function Autocomplete_CFCNo() {
    $('#txtCEfftGL').autocomplete({
        source: function (request, response) {

              var S = "{TransType:'" + "FC" + "', TabID:'" + $("#hdnTabID").val() + "', Desc:'" + $('#txtCEfftGL').val() + "', MemoDate:'" + $('#txtMemoDate').val() + "'," +
                "SectorID: '" + $('#ddlSector').val() + "', BudgetID:'" + "" + "', FinYear:'" + $("#ddlBillAbstractFinYear option:selected").text() + "', " +
                "MainFinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "', Flag:'" + "E" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/BillExtract/Auto_FC',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BudgetDesc,
                                ReferanceNo: item.ReferanceNo,
                                BudgetDesc: item.BudgetDesc,
                                ConcurranceAmount: item.ConcurranceAmount,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnCEffectAdd').val(i.item.ReferanceNo);
            $('#hdnCEffectAmount').val(i.item.ConcurranceAmount); 
            $('#lblCFCTotalAmt').text(i.item.ConcurranceAmount); 
            //$('#txtCEfftAmount').val(i.item.ConcurranceAmount);
            $('#hdnCEffectTempBudgetID').val(i.item.BudgetID);

            CheckCFCLimit(1);
        },
        minLength: 0
    }).click(function () {
	if ($("#ddlBillAbstractFinYear option:selected").text() == "FinYear") {
            alert('Enter Select FinYear first.'); $('#ddlBillAbstractFinYear').focus(); return false;
        }
        if ($('#txtMemoDate').val() == "") {
            alert('Enter Memo Date first.'); $('#txtMemoDate').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });
}

//    BUDGET HEAD AUTOCOMPLETE
function Autocomplete_BudgetHead(ID) {
    $('#txtBudgetHead').autocomplete({
        source: function (request, response) {

            var TabID = $(ID).closest('tr').find('.TabId').text();

            var S = "{TransType:'" + "BudgetHead" + "', TabID:'" + TabID + "', Desc:'" + $('#txtBudgetHead').val() + "', MemoDate:'" + "" + "', SectorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
           // alert(S); return false;
            $.ajax({
                url: '/Accounts_Form/BillExtract/Auto_FC',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.BDesc,
                                TabId: item.TabId
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnBudgetHead').val(i.item.TabId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#txtBudgetHead').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBudgetHead").val('');
            $("#hdnBudgetHead").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBudgetHead").val('');
            $("#hdnBudgetHead").val('');
        }
    });
    Check_isBudgetHeadAvailable(ID);
}

////baban
//    SUBLEDGER AUTOCOMPLETE
function Autocomplete_OptionSubLedger(t) {
    
        $('.OptionSubLedger').autocomplete({
            source: function (request, response) {
               
                var GLS = $('#' + t).closest('tr').find('.cls_hdnAccountDesc').val();

                var S = "{SubCode:'" + GLS + "', Desc:'" + $('.OptionSubLedger').val() + "', TransType:'" + "VoucherSubledger" + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/BillExtract/VoucherSubledger',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
                                    SubLedger: item.SubLedger
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $('#hdnOptionSubLedger').val(i.item.AccountCode);
                $('#' + t).closest('tr').find('.cls_hdnSubCode').val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

        $(document).on('keydown', '.OptionSubLedger', function (evt) {
            var ths = $(this);
            ths.removeClass('selected');
            ths.addClass('selected');
            var t = ths.attr('id');
           
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#' + t).closest('tr').find('.cls_hdnSubCode').val('');
            $('.OptionSubLedger').val('');
        }
        if (iKeyCode == 46) {
            $('#' + t).closest('tr').find('.cls_hdnSubCode').val('');
            $('.OptionSubLedger').val('');
        }
    });
}
//baban
//    SUBLEDGER AUTOCOMPLETE
function Autocomplete_PaymentOptionSubLedger(t) {
   
        $('.PaymentOptionSubLedger').autocomplete({
            source: function (request, response) {

                var GLS = $('#' + t).closest('tr').find('.cls_hdnAccountDesc_payment').val();

                var S = "{SubCode:'" + GLS + "', Desc:'" + $('.PaymentOptionSubLedger').val() + "', TransType:'" + "VoucherSubledger" + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/BillExtract/VoucherSubledger',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
                                    SubLedger: item.SubLedger
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $('#hdnPaymentOptionSubLedger').val(i.item.AccountCode);
                $('#' + t).closest('tr').find('.cls_hdnSubCode_payment').val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
            });


        $(document).on('keydown', '.PaymentOptionSubLedger', function (evt) {
            var ths = $(this);
            ths.removeClass('selected');
            ths.addClass('selected');
            var t = ths.attr('id');

            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 8) {
                $('#' + t).closest('tr').find('.cls_hdnSubCode_payment').val('');
                $('.PaymentOptionSubLedger').val('');
            }
            if (iKeyCode == 46) {
                $('#' + t).closest('tr').find('.cls_hdnSubCode_payment').val('');
                $('.PaymentOptionSubLedger').val('');
            }
    });
}

// SECTION
function Bind_Section()
{
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/Bind_Section',
            contentType: "application/json; charset=utf-8",
            data: {},
            dataType: "json",
            async: false,
            success: function (data, status) {
                var t = data.Data;

                $('#ddlSection').empty();
                $('#ddlSection').append('<option value="" selected>Select</option');
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('#ddlSection').append('<option value=' + item.SectionID + '>' + item.SectionName + '</option>');
                    });
                }
            }
        });
    
}

//REFRESH
function Refresh()
{
    location.reload();
}

//USER
function Auto_User(ID) {
    $('#txtSearchUser').autocomplete({
        source: function (request, response) {
            var TabID = $(ID).closest('tr').find('.TabId').text();
            var S = "{TabId: '" + TabID + "', Desc:'" + $('#txtSearchUser').val() + "', TransType: '" + "SearchUser" + "'}";
            $.ajax({
                url: '/Accounts_Form/BillExtract/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var t = serverResponse.Data; 
                    //var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.UserName,
                                UserID: item.UserID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            //$('#hdnUserID').val(i.item.UserID);
            Add_User(i.item.UserID, i.item.label); 
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchUser').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            Blank();
        }
    });
}
//Pending Job USER
function Autocomplete_PendingUser() {
    $('#txtPendingUser').autocomplete({
        source: function (request, response) {
           
            var S = "{TransactionType: '" + "SearchUser" + "', Desc:'" + $('#txtPendingUser').val() + "', UserID:'" + "" + "'}"; 
            $.ajax({
                url: '/Accounts_Form/UserPermission/Load',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.UserName,
                                UserID: item.UserID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            //$('#hdnUserID').val(i.item.UserID);
            //Add_User(i.item.UserID, i.item.label);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtPendingUser').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            Blank();
        }
    });
}

//SAVE
function Save()
{
    var result = CheckDate(); //alert(result);
    if (result == false)
        return false;
		
    if ($('#txtMemoDate').val() == "")
    {
        $('#txtMemoDate').focus(); return false;
    }
    if ($('#ddlSection').val() == "") {
        $('#ddlSection').focus(); return false;
    }

    if ($('#hdnPartyName').val() == "") {
        $('#txtPartyName').focus(); return false;
    }
    if ($('#hdnPartyName').val() == "0") {
        if ($('#txtPayeeName').val() == "") {
            $('#txtPayeeName').focus(); return false;
        }
    }
    var grdLent = $('#tblFC tbody tr.myData').length; 
    if (grdLent == 0)
    {
        alert('Please add FC Detail.'); return false;
    }

    //if ($('#txtGrossAmount').val() == "" || $('#txtGrossAmount').val() <= 0) {
    //    $('#txtGrossAmount').focus(); return false;
    //}

    var grdLen = $('#tblFC tbody tr.myData').length; var ArrList = []; 
    if (grdLen > 0) {
        $('#tblFC tbody tr.myData').each(function () {
            var accCode = $(this).find('.clsAccCode').text();
            var Amount = $(this).find('.CLSFCAMT').text();
            var BudgetID = $(this).find('.clsBudgetID').text();
            var RefNo = $(this).find('.clsRefNo').text();
            var RefType = $(this).find('.clsRefType').text();
            var RefDate = $(this).find('.clsRefDate').text();
            var SrNo = $(this).find('.clsSrNo').text();
            var SrDate = $(this).find('.clsSrDate').text();
            var InvNo = $(this).find('.clsInvNo').text();
            var InvDate = $(this).find('.clsInvDate').text();
            var FcDetailID = $(this).find('.clsfcDetailID').text();

            ArrList.push({ 'FCNo': accCode, 'Amount': Amount, 'RefType': RefType, 'RefNo': RefNo, 'BudgetID': BudgetID, 'FcDetailID': FcDetailID, 'RefDate': RefDate, 'SrNo': SrNo, 'SrDate': SrDate, 'InvNo': InvNo, 'InvDate': InvDate });
        });
    }

    var multipleFC = JSON.stringify(ArrList);
    var E = "{MemoNo: '" + $('#txtMemoNo').val() + "', MemoDate: '" + $('#txtMemoDate').val() + "',  MemoSrlNo: '" + $('#hdnMemoSrlNo').val() + "', PartyCode: '" + $('#hdnPartyName').val() + "', SectionID: '" + $('#ddlSection').val() + "',  FCNo: '" + $('#hdnFCNo').val() + "', " +
        "RefType: '" + $('#ddlRefType').val() + "', RefNo: '" + $('#txtRefNo').val() + "', SRNo: '" + $('#txtSRNo').val() + "', InvoiceNo: '" + $('#txtInvoiceNo').val() + "', " +
        "InvoiceDate: '" + $('#txtInvoiceDate').val() + "', GrossAmount: '" + $('#txtGrossAmount').val() + "', Remarks: '" + $('#txtMainRemarks').val() + "', SectorID: '" + $('#ddlSector').val() + "', RefDate: '" + $('#txtRefDate').val() + "', SrvDate: '" + $('#txtSRVDate').val() + "', PayeeName: '" + $('#txtPayeeName').val() + "', DocFilePath : '" + "null" + "',  param: " + multipleFC + "}";

    //alert(JSON.stringify(multipleFC));
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {

                //var a = t.split('/'); 
                Upload_Doc('ExpenseflPic_bill', t);

                    swal({
                        title: "Success",
                        text: 'Bill Abstract Details are Saved Successfully !!\nMemo No. is :- ' + t ,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });
                
            }
        }
    });
}
function Upload_Doc(flPic, MemoNo) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#ExpenseflPic_bill').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file, MemoNo + fileNameExt);
    }
  
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BillExtract/UploadPicture',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
        }
    });
}
function ShowImage()
{
    $('#dialog-Image').modal('show');
}
function Expense_Preview_bill(input) {

    var a = input.files[0].name; 
    if (a != "") {
        var validExtensions = ['pdf']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type. Please Select PDF File only.");
            $("#ExpenseflPic_bill").val('');
            return false;
        }
    }

    if (input.files && input.files[0]) {
        path = $('#ExpenseflPic_bill').val().substring(12);
        pathString = '/Upload_BillExtractFile/' + path;
    }
}

function Bind_Grid()
{
    var E = "{SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
		async:false,
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));
      
            var table = $('#tbl');
            var thead = table.find('thead');
            var tbody = table.find('tbody');

            tbody.find('.myData').empty();
              var html = "", isdisable="", rowColor="";
            if (t.length > 0) {
                var k = 0;
                for (var i = 0; i < t.length; i++) {
                    k = k + 1;

                    if (t[i].Condition == "Disable") {
                        isdisable = 'disabled=true'; rowColor = "#F9E79F"; 
                    }
                    else {
                        isdisable = ""; rowColor = "";
                    }
                    if (t[i].PVoucherNo > 0) {
                        rowColor = "#83d8a7";
                    }

                    //for Abstract
                    var isAbstract = "", isDetail = "", isAck = ""; isEdit = "";
                    if (t[i].BillStatus == 1 ) {
                        isAbstract = ''; isDetail = 'none'; isAck = 'none'; 
                    }
                    else if (t[i].BillStatus == 2 && t[i].AckNo == null) {
                        isAbstract = ''; isDetail = 'none'; isAck = 'none'; isEdit = 'none';
                    }
                    else if (t[i].BillStatus == 2 && t[i].AckNo != null) {
                        isAbstract = 'none'; isDetail = ''; isAck = ''; isEdit = 'none';
                    }
                    else
                    {
                        isAbstract = 'none'; isDetail = ''; isAck = ''; isEdit = 'none';
                    }
                    

                   html += "<tr class='myData' style='font-size:11px; background-color:" + rowColor + "'>"
                        + "<td style='display:none;' class='TabId'>" + t[i].TabId + "</td>"
                        + "<td  class='MemoNo' style='text-align:left;'>" + t[i].MemoNo + "</td>"
                        + "<td  class='MemoDt' style='text-align:center;'>" + t[i].MemoDt + "</td>"
                        //+ "<td  class='FcNo' style='text-align:center;'>" + t[i].FcNo + "</td>"
                        + "<td  class='PartyName'>" + (t[i].PartyName == null ? '' : t[i].PartyName) + "</td>"
                        + "<td style='text-align:right;' class='GrossAmt' onClick='UpdateDetail(this);'>" + t[i].GrossAmt + "</td>"
                        + "<td style='display:none;' class='JVoucherNo'>" + t[i].JVoucherNo + "</td>"
                        + "<td style='display:none;' class='RVoucherNo'>" + t[i].RVoucherNo + "</td>"
                        + "<td style='display:none;' class='PVoucherNo'>" + t[i].PVoucherNo + "</td>"
                        + "<td style='display:none;' class='VoucherType'>" + t[i].VoucherType + "</td>"

                        + "<td style='display:none;' class='BillStatus'>" + t[i].BillStatus + "</td>"
                        + "<td class='BillStatusDesc' style='text-align:center;'>" + t[i].BillStatusDesc + "</td>"

                        + "<td style='text-align:center; color:blue; cursor:pointer;' onClick='ViewAppliedDetail(this);'>Click..</td>"

                        + "<td  class='JobWork' style='text-align:center;'>"
                        + "<select style='width:60%' id='JobWork_" + t[i].TabId + "' class='cls_JobWork'" + isdisable + " onchange='Open(this)'>"
                        + "<option value='' >Select</option>"
                        + "</select></td>"

                         //+ "<td style='text-align:center; font-size:11px; color:blue; cursor:pointer; pointer-events:" + isEdit + "'  onClick='Edit(this);' >Edit</td>"
                        + "<td>"
                         + "<select onchange='ActiononRecord(this)'><option value='' >Select</option>"
                        + "<option value='1' style='display:" + isEdit + "' > Edit</option >"
                        + "<option value='2' style='display:" + (t[i].DeleteFlag == "Y" ? "" : "none") + "' > Delete</option >"
                        + "<option value='3' style='display:" + (t[i].DeleteFlag == "Y" ? "" : "none") + "' > Resume</option >"
						  + "<option value='4'> View</option >"
                        + "</td > "

                        + "<td style='text-align:center; color:blue; cursor:pointer;'>"
                        + "<span style='font-size:11px;'> <select onchange='OpenReport(this)'><option value=''>Select</option>"
                        + "<option value='1' style='display:" + isAbstract + "' > Abstract</option >"
                        + "<option value='2' style='display:" + isAck + "' > Ack</option >"
                        + "<option value='3' style='display:" + isDetail + "' > Detail</option >"
                        + "<option value='4' attr-vchno=" + t[i].JVoucherNo + " class='JVoucherNos'  style='display:" + (t[i].JVoucherNo == 0 ? "none" : "") + "' > Journal</option >"
                        + "<option value='5' attr-vchno=" + t[i].RVoucherNo + " class='RVoucherNos'  style='display:" + (t[i].RVoucherNo == 0 ? "none" : "") + "' > Reverse Journal</option >"
                        + "<option value='6' attr-vchno=" + t[i].PVoucherNo + " class='PVoucherNos'  style='display:" + (t[i].PVoucherNo == 0 ? "none" : "") + "' > Payment</option >"
                        + "</select ></span>"
                        + "</td>"
						
                        + "<td style='display:none;' class='SubCode'>" + t[i].SubCode + "</td>"
                        + "<td style='display:none;' class='BankCash'>" + t[i].BankCash + "</td>"

                    html + "</tr>";
                 
                    Bind_AppliedTo(t[i].TabId); 
                    JobWork(t[i].BillStatus, t[i].TabId)
                }
                $('#tbl tbody').append(html);
                //$('.clsEdit').removeAttr('onClick');

            }
        }
    });
}

function Bind_Grids()
{
    var html = "", isdisable = "";
    var table = $('#tbl').DataTable();
    table.destroy();

        $('#tbl').DataTable({
            iDisplayLength: 10,
            paging: true,
            scrollX: true,
            dom: 'Bfrtip',
            "processing": true,
            "aoColumnDefs": [
                {
                    "targets": [11],
                    "className": "text-center",
                    "width": "10%"
                }],
            buttons: [
                {
                    //extend: 'pdf',
                    //title: $("#ddlEmployeeCate option:selected").text() + ' ' + $("#salmaonthID option:selected").text(),
                   // filename: $("#ddlEmployeeCate option:selected").text() + '_' + $("#salmaonthID option:selected").text(),
                    //orientation: 'landscape',
                    //pageSize: 'A4'
                }
            ]
        });
        $('#tbl').DataTable().column(0).visible(false);
        $('#tbl').DataTable().column(5).visible(false);
        $('#tbl').DataTable().column(6).visible(false);
        $('#tbl').DataTable().column(7).visible(false);
        $('#tbl').DataTable().column(8).visible(false);
        $('#tbl').DataTable().column(9).visible(false);

        var E = "{SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/Load',
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                Table1 = D.Data; 
                var dataTable = $("#tbl").DataTable();
                dataTable.clear();
                if (Table1.length > 0) {
                    $.each(Table1, function (index, value) {

                        if (value.Condition == "Disable")
                            isdisable = 'disabled=true';
                        else
                            isdisable = "";

                        //for Abstract
                        var isAbstract = "", isDetail = "", isAck = ""; isEdit = "";
                        if (value.BillStatus == 1) {
                            isAbstract = ''; isDetail = 'none'; isAck = 'none';
                        }
                        else if (value.BillStatus == 2 && value.AckNo == null) {
                            isAbstract = ''; isDetail = 'none'; isAck = 'none'; isEdit = 'none';
                        }
                        else if (value.BillStatus == 2 && value.AckNo != null) {
                            isAbstract = 'none'; isDetail = 'none'; isAck = ''; isEdit = 'none';
                        }
                        else {
                            isAbstract = 'none'; isDetail = ''; isAck = ''; isEdit = 'none';
                        }

                        var rowNode = dataTable.row.add([
                            '' + value.TabId + '',
                            '' + value.MemoNo + '',
                            '' + value.MemoDt + '',
                            '' + value.PartyName + '',
                            '' + value.GrossAmt + '',
                            '' + value.JVoucherNo + '',
                            '' + value.RVoucherNo + '',
                            '' + value.PVoucherNo + '',
                            '' + value.VoucherType + '',
                            '' + value.BillStatus + '',
                            '' + value.BillStatusDesc + '',
                            '<span style="color:#5353ef; text-align:center">Click..</span>',
                            //  '<select id="JobWork_' + value.TabId + '" onchange="Open(this)"' + isdisable + '" style="text-align:center; width:68%;"> <option selected value="">Select</option>  </select>',
                            '<select id="JobWork_' + value.TabId + '" onchange="Open(this)" style="text-align:center; width:68%;"> <option selected value="">Select</option>  </select>',

                            '<span style="text-align:center; font-size:11px;   cursor:pointer; color:blue; pointer-events:' + isEdit + '"  onClick="Edit(this);" >Edit</span>',

                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + isAbstract + '"  onClick="Print(this, 1);" > Abstract </span >' +
                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + isAck + '" onClick="Print(this, 2);" > Ack </span >' +
                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + isDetail + '" onClick="PrintHistory(this, 2);" > | Detail </span >' +
                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + (value.JVoucherNo == null ? "none" : "") + '" onClick="PrintVoucher(this, \'' + value.JVoucherNo + '\');" > | Journal</span >' +
                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + (value.RVoucherNo == null ? "none" : "") + '" onClick="PrintVoucher(this, \'' + value.RVoucherNo + '\');" > | Reverse Journal</span >' +
                            '<span style="font-size:11px; cursor:pointer; color:#5353ef; display:' + (value.PVoucherNo == null ? "none" : "") + '" onClick="PrintVoucher(this, \'' + value.PVoucherNo + '\');" > | Payment</span >',
                        ]).draw(false).node();
                        JobWork(value.BillStatus, value.TabId)
                    });
                }
                else {
                    $("#tbl tbody").html('<tr><td colspan="5">No Data Found</td></tr>')
                }
            }
        });
    
}
function OpenReport(ID)
{
    var Value = $(ID).val();
    if (Value == 1)
    {
        Print(ID, 1);
    }
    if (Value == 2) {
        Print(ID, 2);
    }
    if (Value == 3) {
        PrintHistory(ID, 2);
    }
    if (Value == 4) {
        var a = $(ID).find('.JVoucherNos').attr('attr-vchno');
        PrintVoucher(ID, a);
    }
    if (Value == 5) {
        var a = $(ID).find('.RVoucherNos').attr('attr-vchno');
        PrintVoucher(ID, a);
    }
    if (Value == 6) {
        var a = $(ID).find('.PVoucherNos').attr('attr-vchno');
        PrintVoucher(ID, a);
    }
}
function ActiononRecord(ID)
{
 $('#btnSave').prop("disabled", false);
    var Value = $(ID).val();
    if (Value == 1) {
        Edit(ID);
    }
    if (Value == 2 || Value == 3) {

        swal({
            title: "Warning",
            text: "Are you sure want to Delete this Memo No. ??",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
                    var E = "{MemoNo: '" + MemoNo + "', MemoNoSlNo: '" + (Value == 2 ? 0 : 1) + "'}";
                    //alert(Value);
                    //alert(E);
                    //return false;
                    if (MemoNo != "" || MemoNo != null) {
                        $.ajax({
                            url: '/Accounts_Form/BillExtract/DeleteMemo',
                            type: 'POST',
                            contentType: 'application/json; charset=utf-8',
                            data: E,
                            dataType: 'json',
                            success: function (data) {
                                var t = data.Data;
                                if (t == "success") {
                                    swal({
                                        title: "Success",
                                        text: 'Memo No. Deleted Successfully !!',
                                        type: "success",
                                        confirmButtonColor: "#AEDEF4",
                                        confirmButtonText: "OK",
                                        closeOnConfirm: true,
                                    },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                location.reload();
                                                return false;
                                            }
                                        });
                                }
                            }
                        });
                    }
                    else
                    {
                        alert('Sorry !! Memo No. is Not Available.'); return false;
                    }
                }

            });
    }
	    if (Value == 4) {
        Edit(ID);
        $('#btnSave').prop("disabled", true);
    }
}

//main
function Open(ID)
{
    //var table = $("#tbl").DataTable();
    //var parentRow = $(ID).closest("tr").prev()[0];
    //var rowData = table.row(parentRow).data();
    //var id = rowData[0]; 

    var TabID = $(ID).closest('tr').find('.TabId').text(); 
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); //alert(JobID);
    var TextName = $(ID).closest('tr').find('.cls_JobWork').find('option:selected').text(); //alert(TextName);

    $("#hdnTabId").val(TabID);
    $(".lblHead").text(TextName);
    //$('#dialog-allotedto_' + JobID).modal('show');

    //ACCEPT
    if (TextName == 'Accept')
    {
        $('#dialog-allotedto_Accept').modal('show');
        $("#btnAcceptSave").click(function () {
            Accept_Save(ID);
        });
    }

    //FORWARD  or Forward with Objection
    if (TextName == 'Forword') {

        Check_MandatoryWork(ID);

        //$('#dialog-allotedto_2').modal('show');
       
        Forward_2(ID);
        Auto_User(ID);
        $("#btnForwardSave").click(function () {
            Forward_Save(ID);
        });
    }


    //CONFIRM BUDGET HEAD
    if (JobID == 1) {

        var TabID = $(ID).closest('tr').find('.TabId').text();
        var SubCode = $(ID).closest('tr').find('.SubCode').text();

        if (SubCode == '0')
        {
            $("#ddlVoucherType").val('P');
            $("#ddlVoucherType").prop('disabled', true);
        }
        else
        {
            $("#ddlVoucherType").val('J');
            $("#ddlVoucherType").prop('disabled', false);
        }

       $("#hdnTabID").val(TabID);
        $('#dialog-allotedto_' + JobID).modal('show');
        BindCFC(ID);
       
        if (isConfirmBudgetPostBack == 0) {
            isConfirmBudgetPostBack = 1;
            $("#btnConfirmBudgetSave").click(function () {
                ConfirmBudget_Save(ID);
            });
            //$('#dialog-allotedto_' + JobID).on('hidden.bs.modal', function (e) {
            //    ConfirmBudget_Save(ID);
            //});

            $('.close_' + JobID).click(function (e) {
                ConfirmBudget_Save(ID);
            });
        }
    }

    //VERIFY BILL
    if (JobID == 3) {
        $("#txtBasicAmount").prop('disabled', false); $("#chkpogreater").prop('disabled', false); //$("#txtPOAmount").prop('disabled', false);
        Check_VerifyBill(ID, 'B');
        CheckBaicGrossEqual(ID);
        $('#dialog-allotedto_' + JobID).modal('show');
        $("#btnVerifyBillSave").click(function () {
            VerifyBill_Save(ID, 'B');
        });
        $("#txtBasicAmount").keyup(function () {
            CheckBaicGrossEqual(ID);
        });
    }

    //VERIFY PO/WO
    if (JobID == 4 || JobID == 10 || JobID == 13 || JobID == 14 || JobID == 15) {
        $("#txtBasicAmount").prop('disabled', true); $("#chkpogreater").prop('disabled', true); //$("#txtPOAmount").prop('disabled', true);
        var status = "";
        if (JobID == 4 )
            status = 'O';

        if (JobID == 10)
            status = 'V';

        if (JobID == 13)
            status = 'T';

        //very PAN AND GST 
        if (JobID == 14) {

            Bind_TDSSection(ID);
            $('#dialog-allotedto_' + JobID).modal('show');
            $("#ddlPANTDSSection").change(function () {
                CheckPANTDSPert(ID);
            });
            $("#ddlGSTTDSSection").change(function () {
                CheckGSTTDSPert(ID);
            });
            $(".pangstSave").click(function () {

                if (DeductionType != 'N') {
                    if ($("#ddlPANTDSSection").val() == "") {
                        alert('Please add PAN Calculation.'); return false;
                    }
                    if ($("#ddlGSTTDSSection").val() == "") {
                        alert('Please add GST Calculation.'); return false;
                    }
                }
                PANSave(ID);
                GSTSave(ID);
            });
            status = 'G';
            return false;
        }

        if (JobID == 15)
            status = 'P';

        if (JobID == 16)
            status = 'R';

        Check_VerifyBill(ID, status);

        $('#dialog-allotedto_3').modal('show');
        $("#btnVerifyBillSave").click(function () {
            VerifyBill_Save(ID, status);
        });
    }

    //SELECT DR/CR HEAD ACCOUNT
    if (JobID == 5) {
        Bind_AccountVoucher(ID);
        AutoComplete(ID);
        AutoComplete_Payment(ID);

        var vchType = $(ID).closest('tr').find('.VoucherType').text(); 
        if (vchType != null && vchType != "")
        {
            if (vchType == 'P')
            {
                $(".payment_tab").css('display', '');
                $(".journal_tab").css('display', 'none');

                $(".journal_tab").removeClass('active');
                $(".payment_tab").addClass('active');

                $("#tab_journal").removeClass('active');
                $("#tab_payment").addClass('active');
            }
            else if (vchType == 'L')
            {
                $(".journal_tab").css('display', '');
                $(".payment_tab").css('display', 'none');

                $(".journal_tab").addClass('active');
                $(".payment_tab").removeClass('active');

                $("#tab_journal").addClass('active');
                $("#tab_payment").removeClass('active');
            }
            else if (vchType == 'j') {
                $(".journal_tab").css('display', '');
                $(".payment_tab").css('display', '');

                $(".journal_tab").addClass('active');
                $(".payment_tab").removeClass('active');

                $("#tab_journal").addClass('active');
                $("#tab_payment").removeClass('active');
            }
        }


        $('#dialog-allotedto_' + JobID).modal('show');
        $("#btnDRCRHeadSave").click(function () {
            DRCRHead_Save(ID);
        });
    }

    //APPROVED
    if (TextName == 'Approved') {

        var vchType = $(ID).closest('tr').find('.VoucherType').text();
        var jvchNo = $(ID).closest('tr').find('.JVoucherNo').text(); 
        if (jvchNo == 0) {
		
		    var finYear = $("#ddlGlobalFinYear option:selected").text();
            var FinYearval = $("#ddlGlobalFinYear").val();
            if (FinYearval == "O")
            {
                $("#txtjournaldate").prop('disabled', false);
            }
            else
            {
                                $("#txtjournaldate").prop('disabled', true);
            //------------------------------------------------------------TODAY DATE--------------------------------------
                var today = new Date(); //alert(today);
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
               var datetodayTo = dd + '/' + mm + '/' + yyyy;
            //-------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------CURRENT FINYEAR LAST DATE------------------------
                var sFinYear = finYear.split('-');
                var s = sFinYear[0];
                var t = sFinYear[1];

                var dateTo = "31/03/" + t;
                var from = new Date(t, 03 - 1, 31); //alert(from);  
            //-------------------------------------------------------------------------------------------------------------
                if (today <= from)
                {
                    $("#txtjournaldate").val(datetodayTo);
                }
                else
                {
                    $("#txtjournaldate").val(dateTo);
                }
            }
			
		
            $('#dialog-allotedto_Approved').modal('show');
            $("#btnApprovedSave").click(function () {

                var dt = $(ID).closest('tr').find('.MemoDt').text(); 
                var a = $.trim(dt);
                var b = a.split('/');
                var vchDate = b[2] + '-' + b[1] + '-' + b[0];

                var E = "{MenuId: '" + 84 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/AccountClose/AccountClosingStatus",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        if (t.length > 0) {
                            var Status = t[0].Status;
                            var msg = t[0].Message;
                            if (Status == 1)
                            {
                                var Remarks = $("#txtApprovedRemarks").val();
                                var Postingdate = $("#txtjournaldate").val();
                                if (Remarks == "") {
                                    $("#txtApprovedRemarks").focus(); return false;
                                }
                                if (Postingdate == "") {
                                    $("#txtjournaldate").focus(); return false;
                                }

                                if (vchType == 'P') {
                                    ChangeApproveStatus(ID, Remarks);

                                }
                                else {
                                    Approved_Save(ID, "J", Remarks, Postingdate);
                                }

                            }
                            else {
                                swal("Cancelled", msg, "error");
                            }
                        }
                    }
                });
            });
        }
        else
        {
            if (jvchNo > 0) {
                alert('Sorry ! You have already Approved. Voucher No. is Generated.'); return false;
            }
            else {
                alert('Sorry ! You have already Approved.'); return false;
            }
        }
    }

    //RETURN
    if (TextName == 'Return') {

        $('#dialog-allotedto_return').modal('show');
        //Forward_2(ID);
        $("#btnReturnForwardSave").click(function () {
            ReturnForward_Save(ID);
        });
    }

    //SELECT BANK CHEQUE NO. AND CHEQUE DATE
    if (JobID == 23) {

        $('#dialog-allotedto_' + JobID).modal('show');
       
        if (isPostBack == 0) {
            isPostBack = 1;
            Bind_BankChequeNo(ID);
            BankAutoComplete(ID);
            
            $("#btnSelectChequeSave").click(function (event) {

                var AccCode = $(ID).closest('tr').find('.MemoNo').text().trim();
                var TabID = $(ID).closest('tr').find('.TabId').text();

                var E = "{TabId: '" + TabID + "', Desc: '" + $('#hdnBankName').val() + "', TransType: '" + "PaymentAmount" + "'}";

                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/BillExtract/Account_Description",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data; //alert(JSON);

                        var CurrBal = $("#hdnCurrBal").val(); //alert(CurrBal);
                        var GrossAmt = t[0].Amount; //alert(GrossAmt);
                        if (parseFloat(GrossAmt) > parseFloat(CurrBal)) {
                            alert("Sorry ! Amount is greater than Closing Balance " + CurrBal);
                            return false;
                        }
                        SelectCheque_Save(ID);
                    }
                });
                //var CurrBal = $("#hdnCurrBal").val(); alert(CurrBal);
                //var GrossAmt = $(ID).closest('tr').find('.GrossAmt').text(); alert(GrossAmt);
                //if (parseFloat(GrossAmt) > parseFloat(CurrBal)) {
                //    alert("Sorry ! Amount is greater than Closing Balance " + CurrBal);
                //    return false;
                //}
                //SelectCheque_Save(ID);
            });
            $("#btnApprovedRemarks").click(function () {
                if ($('#txtApprovedFinalRemarks').val() == "") {
                    $('#txtApprovedFinalRemarks').focus(); return false;
                }
                Approved_Save(ID, "P");
            });
        }
    }

}

function Check_MandatoryWork(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); 

    var E = "{JobID: '" + JobID + "', TabId: '" + TabID + "', TransType: '" + "Check_MandatoryWork" + "', SectorID: '"+ $('#ddlSector').val() +"'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/JobWork',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t)); //alert(JobID);
            var TotalCount = t[0].TotalCount; alert(TotalCount);
            var VCHCount = t[0].VCHCount; alert(VCHCount);
            //alert(JobID);
            //confirm budget head
            if (JobID == 11) {
                if (TotalCount > 0 && VCHCount > 0) {
                    $('#dialog-allotedto_2').modal('show'); return false;
                }
                if (TotalCount == 0) {
                    alert('Please Confirm FC Details first.'); $(ID).closest('tr').find('.cls_JobWork').val(''); return false;
                }
                if (VCHCount == 0)
                {
                    alert('Please Confirm Payment Type and Voucher Type from Confirm Budget.'); $(ID).closest('tr').find('.cls_JobWork').val('');   return false;
                }
            }
            else if (JobID == 6) {
                //pan/gst count
                if (TotalCount == 0) {
                    alert('Please Confirm PAN/GST Details first.'); $(ID).closest('tr').find('.cls_JobWork').val(''); return false;
                }
                if (VCHCount == 0) {
                    alert('Please Confirm Dr./Cr. Head and Amount Details first.'); $(ID).closest('tr').find('.cls_JobWork').val(''); return false;
                }
                $('#dialog-allotedto_2').modal('show');
            }
            if (JobID == 21) {
                if (TotalCount == 0) {
                    alert('Please Approve Bill Abstract first.'); $(ID).closest('tr').find('.cls_JobWork').val(''); return false;
                }
                $('#dialog-allotedto_2').modal('show');
            }
            else
                $('#dialog-allotedto_2').modal('show');
        }
    });
}
function ChangeApproveStatus(ID, Remarks) {

    var TabID = $(ID).closest('tr').find('.TabId').text();

    var E = "{TabID: '" + TabID + "', Remarks: '" + Remarks + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/BillExtract/ChangeApproveStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            alert('Memo is Approved Successfully.'); location.reload();
            return false;
        }
    });
}
function Bind_TDSSection(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var GrossAmt = $(ID).closest('tr').find('.GrossAmt').text(); 

    var E = "{TabId: '" + TabID + "', TransType: '" + "TDS_Section" + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/AppliedDetails',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 

            var t1 = t.Table; 
            var t2 = t.Table1; 
            var t3 = t.Table2; 
            var t4 = t.Table3; 

            $(".lblgrossamt").text('Gross : ' + GrossAmt);

            $('#ddlPANTDSSection').empty();
            $('#ddlPANTDSSection').append('<option value="" selected>Select</option');
            if (t1.length > 0) {
                $(t1).each(function (index, item) {
                    $('#ddlPANTDSSection').append('<option value=' + item.TdsId + '>' + item.TdsSec + '</option>');
                });
            }

            $('#ddlGSTTDSSection').empty();
            $('#ddlGSTTDSSection').append('<option value="" selected>Select</option');
            if (t2.length > 0) {
                $(t2).each(function (index, item) {
                    $('#ddlGSTTDSSection').append('<option value=' + item.TdsId + '>' + item.TdsSec + '</option>');
                });
            }

            $("#txtPANBasicAmt").val(t3[0].BASICAmount); $("#txtGSTBasicAmt").val(t3[0].BASICAmount);

            var limitAmt = t3[0].TdsLimit;
            var ispoExceed = t3[0].IsPOAmountExceed;
            $("#lblPANPOGreater").text('PO Amount Exceeds ' + limitAmt);  
            $("#lblGSTPOGreater").text('PO Amount Exceeds ' + limitAmt); 
            if (ispoExceed == "Y") {
                $("#chkpopangreater").prop('checked', true); $("#chkpogstgreater").prop('checked', true);
            }
            else {
                $("#chkpopangreater").prop('checked', false); $("#chkpogstgreater").prop('checked', false);
            }

            //$("#txtPANPOAmt").val(t3[0].POAmount);$("#txtGSTPOAmt").val(t3[0].POAmount);
            var panSectionID = "", gstSectionID = ""; 
            if (t4.length > 0) {
                $(t4).each(function (index, item) {
                    var DeductionType = item.DeductionType; 

                    if (DeductionType == 'P')
                    {
                        panSectionID = t4[index].SectionID;
                        $("#ddlPANTDSSection").val(t4[index].SectionID); $("#txtTDSPert").val(t4[index].TDSPert); $("#txtOtherPANTaxAmt").val(t4[index].TDSTaxAmount); 
                    }
                    if (DeductionType == 'G') {
                        gstSectionID = t4[index].SectionID;
                        $("#ddlGSTTDSSection").val(t4[index].SectionID); $("#txtCGST").val(t4[index].CGSTPert); $("#txtSGST").val(t4[index].SGSTPert); 
                        $("#txtIGST").val(t4[index].IGSTPert); $("#txtCGSTOtherAmt").val(t4[index].CGSTAmount); $("#txtSGSTOtherAmt").val(t4[index].SGSTAmount); 
                        $("#txtIGSTOtherAmt").val(t4[index].IGSTAmount); 
                    }
                  
                    if (panSectionID == null && gstSectionID == null) 
                    {
                        $('#tab_PAN').css('display', 'none'); $('#tab_GST').css('display', 'none'); $('#tab_NOT').css('display', 'none');
                        $("#radioNOT").prop('checked', 'checked');
                    }
                });
            }

        }
    });
}
function CheckPANTDSPert(ID)
{
    var tsssetionPAN = $("#ddlPANTDSSection").val();

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var GrossAmt = $(ID).closest('tr').find('.GrossAmt').text(); 

    if (tsssetionPAN !="")
    {
        var E = "{JobID: '" + tsssetionPAN + "', TabId: '" + TabID + "', TransType: '" + "TDS_PAN" + "'}";

        $.ajax({
            url: '/Accounts_Form/BillExtract/JobWork',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));
                var tdsPert = t[0].TDSPerct; //alert(tdsPert);
                $("#txtTDSPert").val(tdsPert); 
                $(".lblgrossamt").text('Gross : ' + GrossAmt);

                var basicAmt = $("#txtPANBasicAmt").val();
                if (tdsPert > 0) {
                    var a = (basicAmt * tdsPert) / 100;
                    $("#txtOtherPANTaxAmt").val(a.toFixed(2));
                }
            }
        });
    }
    else
    {
        $("#txtTDSPert").val(''); $("#txtOtherPANTaxAmt").val(''); $("#ddlPANTDSSection").val('');
    }

}
function CheckGSTTDSPert(ID) {
   
    var tsssetionGST = $("#ddlGSTTDSSection").val();

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var GrossAmt = $(ID).closest('tr').find('.GrossAmt').text();
    //var POAmt = $("#txtGSTPOAmt").val();
    var ISPOAmtExceed = ($('#chkpogstgreater').prop('checked') == true ? "Y" : "N"); 

    if (tsssetionGST != "") {
        var E = "{JobID: '" + tsssetionGST + "', TabId: '" + TabID + "', TransType: '" + "TDS_GST" + "', ISPOAmtExceed: '" + ISPOAmtExceed + "'}";

        $.ajax({
            url: '/Accounts_Form/BillExtract/GST',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data; 
                var CGST = t[0].CGST; 
                var SGST = t[0].SGST; 
                var IGST = t[0].IGST; 
                $("#txtCGST").val(CGST); $("#txtSGST").val(SGST); $("#txtIGST").val(IGST);
                $(".lblgrossamt").text('Gross : ' + GrossAmt);

                var basicAmt = $("#txtGSTBasicAmt").val();

                var c = (basicAmt * CGST) / 100;
                var s = (basicAmt * SGST) / 100;
                var i = (basicAmt * IGST) / 100;
                $("#txtCGSTOtherAmt").val(c.toFixed(2)); $("#txtSGSTOtherAmt").val(s.toFixed(2)); $("#txtIGSTOtherAmt").val(i.toFixed(2));
            }
        });
    }
    else {
        $("#txtCGST").val(''); $("#txtSGST").val(''); $("#txtIGST").val(''); $("#txtGSTOtherAmt").val('');
        $("#txtCGSTOtherAmt").val(''); $("#txtSGSTOtherAmt").val(''); $("#txtIGSTOtherAmt").val('');
    }
}
function ISPANVerified()
{
    $("#ddlTDSSection").val(''); $("#txtTDSPert").val(''); $("#txtOtherAmt").val('');
}
function GetTabActive(ID)
{
    var value = DeductionType =  $(ID).attr('value');
    if (value == 'G')
    {
        $('#tab_GST').css('display', ''); $('#tab_PAN').css('display', 'none'); $('#tab_NOT').css('display', 'none');
    }
    else if (value == 'P') {
        $('#tab_PAN').css('display', ''); $('#tab_GST').css('display', 'none'); $('#tab_NOT').css('display', 'none');
    }
    else
    {
        $('#tab_GST').css('display', 'none'); $('#tab_PAN').css('display', 'none');
        $('#tab_NOT').css('display', ''); 
    }
}
function PANGSTSave(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();

    var SectionID = (DeductionType == "G" ? $('#ddlGSTTDSSection').val() : $('#ddlPANTDSSection').val());
    var TDSPerct =  (DeductionType == "G" ? "" : $('#txtTDSPert').val());
    var TaxAmount = (DeductionType == "G" ? "" : $('#txtOtherPANTaxAmt').val());

    var CGSTPerct = (DeductionType == "G" ? $('#txtCGST').val() : "");
    var SGSTPerct = (DeductionType == "G" ? $('#txtSGST').val() : "");
    var IGSTPerct = (DeductionType == "G" ? $('#txtIGST').val() : "");
    var CGSTAmount = (DeductionType == "G" ? $('#txtCGSTOtherAmt').val() : "");
    var SGSTAmount = (DeductionType == "G" ? $('#txtSGSTOtherAmt').val() : "");
    var IGSTAmount = (DeductionType == "G" ? $('#txtIGSTOtherAmt').val() : "");

    var E = "{TransType: '" + "Insert_PANGST" + "', TabID: '" + TabID + "', DeductionType: '" + DeductionType + "', SectionID: '" + SectionID + "', TDSPerct: '" + TDSPerct + "'," +
        "TDSTaxAmount: '" + TaxAmount + "', CGSTPerct: '" + CGSTPerct + "', SGSTPerct: '" + SGSTPerct + "', IGSTPerct: '" + IGSTPerct + "', CGSTAmount: '" + CGSTAmount + "'," + 
        "SGSTAmount: '" + SGSTAmount + "', IGSTAmount: '" + IGSTAmount + "'}";

    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_PANGST',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success")
            {
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}
function PANSave(ID) {
    var TabID = $(ID).closest('tr').find('.TabId').text();

    var SectionID = $('#ddlPANTDSSection').val();
    var TDSPerct = $('#txtTDSPert').val();
    var TaxAmount = $('#txtOtherPANTaxAmt').val();

    var CGSTPerct =  "";
    var SGSTPerct = "";
    var IGSTPerct = "";
    var CGSTAmount = "";
    var SGSTAmount =  "";
    var IGSTAmount = "";

    var E = "{TransType: '" + "Insert_PANGST" + "', TabID: '" + TabID + "', DeductionType: '" + "P" + "', SectionID: '" + SectionID + "', TDSPerct: '" + TDSPerct + "'," +
        "TDSTaxAmount: '" + TaxAmount + "', CGSTPerct: '" + CGSTPerct + "', SGSTPerct: '" + SGSTPerct + "', IGSTPerct: '" + IGSTPerct + "', CGSTAmount: '" + CGSTAmount + "'," +
        "SGSTAmount: '" + SGSTAmount + "', IGSTAmount: '" + IGSTAmount + "'}";

    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_PANGST',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success") {
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}
function GSTSave(ID) {
    var TabID = $(ID).closest('tr').find('.TabId').text();

    var SectionID = $('#ddlGSTTDSSection').val();
    var TDSPerct = "";
    var TaxAmount = "";

    var CGSTPerct = $('#txtCGST').val();
    var SGSTPerct = $('#txtSGST').val();
    var IGSTPerct = $('#txtIGST').val();
    var CGSTAmount = $('#txtCGSTOtherAmt').val();
    var SGSTAmount = $('#txtSGSTOtherAmt').val();
    var IGSTAmount = $('#txtIGSTOtherAmt').val();

    var E = "{TransType: '" + "Insert_PANGST" + "', TabID: '" + TabID + "', DeductionType: '" + "G" + "', SectionID: '" + SectionID + "', TDSPerct: '" + TDSPerct + "'," +
        "TDSTaxAmount: '" + TaxAmount + "', CGSTPerct: '" + CGSTPerct + "', SGSTPerct: '" + SGSTPerct + "', IGSTPerct: '" + IGSTPerct + "', CGSTAmount: '" + CGSTAmount + "'," +
        "SGSTAmount: '" + SGSTAmount + "', IGSTAmount: '" + IGSTAmount + "'}";

    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_PANGST',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success") {
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}

function ReferenceValidate()
{
    var RefType = $('#ddlRefType').val();
    if (RefType != "")
    {
        if (RefType == "N")
        {
            $('#txtRefNo').prop('disabled', true); $('#txtRefDate').prop('disabled', true);
            $('.clsPOAmt').css('display', 'none'); $('#txtRefNo').val(''); $('#txtRefDate').val('');
            
        }
        //else if (RefType == "P") {
        //    $('#txtRefNo').prop('disabled', false);
        //    $('.clsPOAmt').css('display', '');
        //}
        else
        {
            $('#txtRefNo').prop('disabled', false); $('#txtRefDate').prop('disabled', false);
            //$('.clsPOAmt').css('display', 'none'); $('#txtPOAmount').val('');
        }
    }
}

function Bind_Status() {
    $.ajax({
        url: '/Accounts_Form/BillExtract/Status',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: {},
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 

            $('.cls_Status').empty();
            $('.cls_Status').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('.cls_Status').append('<option value=' + item.JobId + '>' + item.JobDesc + '</option>');
                });
            }
        }
    });
}
function JobWork(BillStatus, TabId)
{
    //var JobID = $(ID).closest('tr').find('.cls_Status').val(); 
    if (BillStatus != "") {
        var E = "{JobID: '" + BillStatus + "', TabId: '" + TabId + "', TransType: '" + "JobWork" + "', SectorID: '" + $("#ddlSector").val() + "'}";
       
        $.ajax({
            url: '/Accounts_Form/BillExtract/JobWork',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var t = data.Data; //alert(JSON.stringify(t));

                $('#JobWork_' + TabId).empty();
                $('#JobWork_' + TabId).append('<option value="" selected>Select</option');
                if (t.length > 0) {
                        $(t).each(function (index, item) {
                            $('#JobWork_' + TabId).append('<option value=' + item.JwId + '>' + item.JobWorkDesc + '</option>');
                        });

                }
            }
        });
    }
    else {
        $('#JobWork_' + TabId).empty();
        $('#JobWork_' + TabId).append('<option value="" selected>Select</option');
    }
}
function Bind_AppliedTo(TabId)
{
    var E = "{TabId: '" + TabId + "', TransType: '" + "Appliedto" + "'}"; 

    $.ajax({
        url: '/Accounts_Form/BillExtract/AppliedDetail',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            $('#Applied_' + TabId).empty();
            $('#Applied_' + TabId).append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('#Applied_' + TabId).append('<option value=' + item.UserID + '>' + item.FirstName + '</option>');
                });
            }
        }
    });
}
function ViewAppliedDetail(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text(); 
    if (TabID != "") {
        var E = "{TabId: '" + TabID + "', TransType: '" + "AllotedUser" + "', SectorID: '" + $('#ddlSector').val() + "'}";
       
        $.ajax({
            url: '/Accounts_Form/BillExtract/ForwardUser',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var t = data.Data; //alert(JSON.stringify(t));
                $('#tblAllotedTo tbody tr').empty();
                if (t.length > 0)
                {
                    var html = "";
                    for (var i = 0; i < t.length; i++)
                    {
                        html += "<tr><td>" + t[i].FirstName + "</td></tr>"
                    }
                }
              
                $('#tblAllotedTo tbody').append(html);
                $('#dialog-allotedto').modal('show');
            }
        });
    }
}
function Edit(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    if (TabID != "") {
        var E = "{TabId: '" + TabID + "', TransType: '" + "Edit" + "'}";

        $.ajax({
            url: '/Accounts_Form/BillExtract/AppliedDetail',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var t = data.Data; //alert(JSON.stringify(t)); 
                if (t.length > 0)
                {
                    $("#hdnTabId").val(t[0].TabId); $("#txtMemoNo").val(t[0].MemoNo); $("#txtMemoDate").val(t[0].MemoDt); $("#hdnMemoSrlNo").val(t[0].MemoSrlNo); 
                    $("#ddlSection").val(t[0].Section); $("#txtFCNo").val(t[0].FcNo); $("#hdnFCNo").val(t[0].FcNo);
                    $("#ddlRefType").val(t[0].RefType); $("#txtRefNo").val(t[0].RefNo); $("#txtSRNo").val(t[0].SrNo);
                    $("#hdnPartyName").val(t[0].SubCode); $("#txtPartyName").val(t[0].PartyName);
                    $("#txtInvoiceNo").val(t[0].TaxInvNo); $("#txtInvoiceDate").val(t[0].TaxInvDt);
                    $("#txtGrossAmount").val(t[0].GrossAmt); $("#txtMainRemarks").val(t[0].Remarks); 
                    $("#txtRefDate").val(t[0].RefDate); $("#txtSRVDate").val(t[0].SrDate); $("#txtPayeeName").val(t[0].PayeeName); 
                   
                    $('#ExpIframe_bill').attr('src', '');
                    $('#ExpIframe_bill').attr('src', t[0].DocFilePath);
                    pathString = t[0].DocFilePath; 
                    if (t[0].DocFilePath != null)
                        $('.document').css("display", "");
                    else
                        $('.document').css("display", "none");

                    BindFC();
                }
            }
        });
    }
}

function CheckBaicGrossEqual(ID)
{
    var GrossAmt = $(ID).closest('tr').find('.GrossAmt').text();
    var BasicAmt = ($("#txtBasicAmount").val() == "" ? 0 : $("#txtBasicAmount").val());
    if (parseFloat(BasicAmt) > parseFloat(GrossAmt))
    {
        alert('Sorry !! Basic Amount is greater than Gross Amount ' + GrossAmt + '.'); $("#txtBasicAmount").val(0); return false;
    }
}
function Accept_Save(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); //alert(JobID); return false;

    var E = "{TabID: '" + TabID + "', JobID: '" + JobID + "', SectorID: '" + $('#ddlSector').val() + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_BillStatus',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_Accept').modal('hide');
                //$('#dialog-allotedto_' + JobID).modal('hide');
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}
function ConfirmBudget_Save(ID)
{
    //if ($('#hdnBudgetHead').val() == "")
    //{
    //    $('#txtBudgetHead').focus(); return false;
    //}

    var l = $('#tblCFC tr.myData').length;
    if (l == 0) {
        alert('Sorry ! Please Confirm FC Details'); return false;
    }

 
    var grdLen = $('#tblCFC tbody tr.myData').length; var ArrList = []; var TotalAmt = 0; 
    if (grdLen > 0) {
        $('#tblCFC tbody tr.myData').each(function () {
            var accCode = $(this).find('.clsAccCode').text(); //alert(accCode);
            var Amount = $(this).find('.CLSFCAMT').text();
            var BudgetID = $(this).find('.clsBudgetID').text();
            var TaxinvoiceNo = $(this).find('.clsTaxInvoiceNo').text();
            var FcDetailID = $(this).find('.clsfcDetailID').text(); 

            if (accCode == "" || accCode == null)
                con = 1;

            TotalAmt = parseFloat(TotalAmt) + parseFloat(Amount);
            ArrList.push({ 'FCNo': accCode, 'Amount': Amount, 'BudgetID': BudgetID, 'TaxinvoiceNo': TaxinvoiceNo, 'FcDetailID': FcDetailID });
        });
    }
    if (con == 1)
    {
        alert('Sorry ! Please Confirm FC Detail first.'); return false;
    }

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val();
 if (TabID == "" || TabID == null || TabID == 'null')
    {
        alert('Sorry ! Your Memo No. is Blank. Please done the process again.'); return false;
    }
	
    var multipleFC = JSON.stringify(ArrList);
    var E = "{TabID: '" + TabID + "', JobID: '" + JobID + "', SectorID: '" + $('#ddlSector').val() + "', BankCash: '" + $('#ddlBankCash').val() + "', VoucherType: '" + $('#ddlVoucherType').val() + "', param: " + multipleFC + ", TotalAmt: '" + TotalAmt + "'}";

   
    //alert(multipleFC);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_ConfirmBudgetHead',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_' + JobID).modal('hide');
				 if (t != "" || t != null) {
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
 }
                else
                {
                    alert('Sorry ! Your Memo No. is Blank. Please done the process again.'); return false;
                }
            }
        }
    });
}
function Check_isBudgetHeadAvailable(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    if (TabID != "") {
        var E = "{TabId: '" + TabID + "', TransType: '" + "ISBudgetHead" + "'}";

        $.ajax({
            url: '/Accounts_Form/BillExtract/AppliedDetail',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var t = data.Data; //alert(JSON.stringify(t));
                if (t.length > 0) {
                    $('#hdnBudgetHead').val(t[0].TabId);
                    $('#txtBudgetHead').val(t[0].BDesc);
                    $('#ddlBankCash').val(t[0].BankCash);
                    $('#ddlVoucherType').val(t[0].VoucherType);
                }
                else
                {
                    $('#hdnBudgetHead').val('');
                    $('#txtBudgetHead').val('');
                    $('#ddlBankCash').val('B');
                    $('#ddlVoucherType').val('J');
                }
            }
        });
    }
}

function Forward_2(ID)
{
    //var JobID = $(ID).closest('tr').find('.cls_JobWork').val();
    var JobID = $(ID).closest('tr').find('.BillStatus').text(); 

    var E = "{TabId: '" + JobID + "', TransType: '" + "ForwardUser" + "', SectorID: '" + $('#ddlSector').val() + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/ForwardUser',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 

            var html = "";
            $('.AllotedUserlist').empty();
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $(".AllotedUserlist").append("<div class='lstuser'><label style='width:80%'><input type='checkbox'  class='select-check-action notchk' value='" + item.UserID + "'/> " + item.FirstName + " </label>"+
                        "<span><select class='clsUserWork' style='width:80px; display:none'><option selected value=''>Select</option></select></span>"+
                        "<span style= 'float:right; padding-right:10px;' > <img src='/Content/Images/Delete.png' class='delete' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' /></span ></div > ");
                    //$(".AllotedUserlist").append("<div><label style='width:80%'><input type='checkbox' checked class='select-check-action' value='" + item.UserID + "'/> " + item.FirstName + " </label></div>");
                });
            }
        }
    });
}
function Forward_Save(ID) {

    var ArrList = [], strUserID="";
    var ISObjection = "", isJob = "", newJobID="";

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var ActionID = $(ID).closest('tr').find('.cls_JobWork').val();
    var JobID = $(ID).closest('tr').find('.BillStatus').text(); 

    $(".AllotedUserlist").find(".select-check-action").each(function (idx, aid) {
        if ($(this).hasClass('notchk') == false) {
            ArrList.push({ 'UserID': aid.value });

            if (strUserID == "")
            {
                strUserID = aid.value;
            }
            else
            {
                strUserID = strUserID + '#' + aid.value;
            }


            newJobID = $(this).closest('.lstuser').find('.clsUserWork').val(); 
            if (newJobID == "")
                isJob = 1;
        }
    });
    if (ArrList.length == 0)
    {
        alert('Sorry ! Please confirm User.'); return false;
    }

    var isobjection = $('#chkisObjection').prop('checked'); 
    if (isobjection == true)
    {
        ISObjection = "Y";
        if (isJob == 1) {
            alert('Sorry ! Please confirm User Work.'); isJob = 0; return false;
        }
        if ($('#txtForwardRemarks').val() == "")
        {
            $('#txtForwardRemarks').focus(); return false;
        }
    }
    else
        ISObjection = "N";

   
    var Master = JSON.stringify(ArrList);
    var E = "{TabID: '" + TabID + "', NewJobID: '" + newJobID + "', JobID: '" + JobID + "', ActionID: '" + ActionID + "', SectorID: '" + $('#ddlSector').val() + "', Remarks: '" + $('#txtForwardRemarks').val() + "', ISObjection: '" + ISObjection + "', param: " + Master + ", strUserID: '" + strUserID + "'}";

    //alert(E);
    //return false;
    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_Forward',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_' + JobID).modal('hide');
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}
function OpenObjection()
{
    var isobjection = $('#chkisObjection').prop('checked'); 
    if (isobjection == true) {
        $('#txtSearchUser').prop('disabled', false); //$('#txtSearchUser').focus();

        $('.select-check-action').prop('checked', false);
        $('.select-check-action').addClass("notchk");
        $('.select-check-action').prop("disabled", true);
        $('.delete').prop("disabled", true);
    }
    else
    {
        $('.objection').remove();
        $('#txtSearchUser').prop('disabled', true); $('#txtSearchUser').val('');

        //$('.select-check-action').prop('checked', true);
        $('.select-check-action').addClass("notchk");
        $('.select-check-action').prop("disabled", false);
        $('.delete').prop("disabled", false);
    }
}
function Bind_UserWork(UserID, ID) {
   
    var E = "{TabId: '" + UserID + "', TransType: '" + "UserWork" + "', SectorID: '" + $('#ddlSector').val() + "'}";

    //return false;
    $.ajax({
        url: '/Accounts_Form/BillExtract/ForwardUser',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            $(ID).closest('.lstuser').find('.clsUserWork').css('display', '');
            $(ID).closest('.lstuser').find('.clsUserWork').empty();
            $(ID).closest('.lstuser').find('.clsUserWork').append('<option value="" selected>Select</option')
            $(ID).append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $(ID).closest('.lstuser').find('.clsUserWork').append('<option value=' + item.JobId + '>' + item.JobWorkDesc + '</option>');
                });
            }
        }
    });
}

function Approved_Save(ID, VchType, Remarks, Postingdate) {

    var ArrList = [];

    $(".AllotedUserlist").find(".select-check-action").each(function (idx, aid) {
        if ($(this).hasClass('notchk') == false) {
            ArrList.push({ 'UserID': aid.value });
        }
    });

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var ActionID = $(ID).closest('tr').find('.cls_JobWork').val();
    var JobID = $(ID).closest('tr').find('.BillStatus').text();

    var Master = JSON.stringify(ArrList);
    var E = "{TabID: '" + TabID + "', JobID: '" + JobID + "', ActionID: '" + ActionID + "', Remarks: '" + Remarks + "' , PostingDate: '" + Postingdate + "' , SectorID: '" + $('#ddlSector').val() + "', VoucherType: '" + VchType + "', param: " + Master + "}";

    //alert(JSON.stringify(E));
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/VoucherEntry',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        async:false,
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_' + JobID).modal('hide');

                var a = t.split('#');
                var VchNo = a[0]; 
                var RefVchNo = a[1];

                if (VchType == 'J') {
                    if (VchNo > 0) {
                        Approved_Save1(ID, "J", Remarks, Postingdate, VchNo, RefVchNo);
                    }
                    else
                    {
                        swal({
                            title: "Success",
                            text: (a[1] == 0 ? 'Bill Abstract Details are Saved Successfully !!' : 'Bill Abstract Details are Saved Successfully !!\nVoucher No. ' + a[1]),
                            type: "success",
                            confirmButtonColor: "#AEDEF4",
                            confirmButtonText: "OK",
                            closeOnConfirm: true,
                        },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                    return false;
                                }
                            });
                    }
                }
                else
                {
                swal({
                    title: "Success",
                    text: (a[1] == 0 ? 'Bill Abstract Details are Saved Successfully !!' : 'Bill Abstract Details are Saved Successfully !!\nVoucher No. ' + a[1]),
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
                }
            }
        }
    });
}
function Approved_Save1(ID, VchType, Remarks, Postingdate, VchNo, RefVchNo) {

    var ArrList = [];

    var TabID = $(ID).closest('tr').find('.TabId').text();
    var ActionID = $(ID).closest('tr').find('.cls_JobWork').val();
    var JobID = $(ID).closest('tr').find('.BillStatus').text();

    var Master = JSON.stringify(ArrList);
    var E = "{TabID: '" + TabID + "', JobID: '" + JobID + "', ActionID: '" + ActionID + "', Remarks: '" + Remarks + "' , PostingDate: '" + Postingdate + "' , VchNo: '" + VchNo + "' , SectorID: '" + $('#ddlSector').val() + "', VoucherType: '" + VchType + "', param: " + Master + "}";

    //alert(JSON.stringify(E));
    //return false;

    $.ajax({
        url: '/Accounts_Form/BillExtract/VoucherEntry1',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        async: false,
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_' + JobID).modal('hide');
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!\nVoucher No. ' + RefVchNo,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}

function Check_VerifyBill(ID, DocType)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); //alert(JobID); return false;

    var E = "{TabID: '" + TabID + "',  DocType: '" + DocType + "', TransType: '" + "Check_VerifyBill" + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/Check_VerifyBill',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 
            var t1 = t.Table; //alert(JSON.stringify(t1));
            var t2 = t.Table1; //alert(JSON.stringify(t2));
           
            if (t1.length > 0) {
                var isVerified = t1[0].DocVerified;
                var remaks = t1[0].Remarks;
                $("#ddlVerifybill").val(isVerified == true ? 1 : 0); $("#txtVerifybillRemarks").val(remaks); 
                $("#txthdnBEBV").val(t1[0].BvId); 
            }
            else {
                $("#txtVerifybillRemarks").val(''); $("#ddlVerifybill").val(0); 
            }

            if (t2.length > 0) {
                var basicamt = t2[0].BASICAmount;
                var IsPOAmountExceed = t2[0].IsPOAmountExceed;
                var limitAmt = t2[0].TdsLimit; 
                $("#txtBasicAmount").val(basicamt); $("#lblPOGreater").text('Amount Exceeds ' + limitAmt + ' ?');  //$("#txtPOAmount").val(poamt);
                if (IsPOAmountExceed == "Y")
                    $('#chkpogreater').prop('checked', true);
                else
                    $('#chkpogreater').prop('checked', false);
            }
            else {
               $("#txtBasicAmount").val(''); //$("#txtPOAmount").val('');
            }
        }
    });
}
function VerifyBill_Save(ID, DocType)
{
   
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); //alert(JobID); return false;

    if ($('#txtBasicAmount').val() <= 0 || $('#txtBasicAmount').val() == "")
    {
        $('#txtBasicAmount').focus(); return false;
    }

    var ISPOAmtExceed = ($('#chkpogreater').prop('checked') == true ? "Y" : "N"); 
    var BVID = $('#txthdnBEBV').val(); 

    var Remarks = $('#txtVerifybillRemarks').val();
    

    var E = "{TabID: '" + TabID + "', DocType: '" + DocType + "', isDocVerified: '" + $('#ddlVerifybill').val() + "', BVID: '" + BVID + "', Remarks: '" + Remarks + "', BasicAmount: '" + $('#txtBasicAmount').val() + "', ISPOAmtExceed: '" + ISPOAmtExceed + "'}";

    // return false;
    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_VerifyBill',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_3').modal('hide');
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}

// JOURNAL
function Bind_AccountVoucher(ID) {

    var TabID = $(ID).closest('tr').find('.TabId').text();

    var E = "{TabId: '" + TabID + "', TransType: '"+ "Voucher" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BillExtract/VoucherDetail',
        data: E,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var data = response.Data; 
            if (response.Status == 200) {

                var table1 = data.Table;
                var table2 = data.Table1;

                Load_AccountInformation_Journal(table1); //alert(JSON.stringify(table1));
                Load_AccountInformation_Payment(table2); //alert(JSON.stringify(table2));
            }
        }
    });
}
//baban
function Load_AccountInformation_Journal(detail) {
   
    $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').remove();

    if (detail.length > 0) {

        var drcrAmt = 0, isDRdisable = "", isCRdisable = ""; 

        var Remarks =  detail[0].Remarks == null ? '' : detail[0].Remarks; 
        $('#journalRemarks').val(Remarks);

        var table = $('#tbl_Expense_Voucher_journal tbody');

        $.each(detail, function (key, value) {

            if (value.DrCr == "D")
            {
                if (value.Amount > 0)
                {
                    isDRdisable = "";
                    isCRdisable = 'disabled=true';
                }
            }
                
            if (value.DrCr == "C") {
                if (value.Amount > 0) {
                    isCRdisable = "";
                    isDRdisable = 'disabled=true';
                }
            }

            table.append("<tr class='myData_voucher_bill'>"
                + "<td style='display:none' class='cls_Order' srlno='" + value.Vchid +"' orderno=" + value.VchSrlId + ">" + value.VchSrlId + "</td>"

                + "<td style='text-align:center'>"
                + "<input type= 'text' class='cls_AccountDesc form-control'   id='desc_" + key + "'  value='" + (value.AccountDescription == null ? '' : value.AccountDescription) + "'/>"
                + "<input type= 'hidden' class='cls_hdnAccountDesc form-control' value='" + value.AccountCode + "'/>"
                + "<input type= 'hidden' class='cls_hdnSubCode form-control' subledger='' value='" + value.SubCode + "'/>"
                + "<input type= 'hidden' class='cls_hdnSubLedger form-control' value='" + value.Subledger + "'/>"
                + "</td>"

                + "<td style='text-align:center'><select id='drcr_" + key + "' class='cls_drcr form-control' onchange='Enable_DisableVch_bill(this)' >"
                + "<option value=''>Dr/Cr</option>"
                + "<option value='D'>DR</option>"
                + "<option value='C'>CR</option>"
                + "</select></td>"

                + "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal'" + isDRdisable + " id='dr_" + key + "'  value='" + (value.DrCr == "D" ? (value.Amount == null ? 0 : value.Amount) : 0) + "' onkeyup='Vch_Cal_Amount()' /></td>"
                + "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal'" + isCRdisable + " id='cr_" + key + "'  value='" + (value.DrCr == "C" ? (value.Amount == null ? 0 : value.Amount) : 0) + "' onkeyup='Vch_Cal_Amount()' /></td>"

                + "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row(this)' /></td>" +

                + "</tr>");
            $('#drcr_' + key).val(value.DrCr);
        });

        Calculate_TotalVch_bill(Remarks);
    }

    Add_PlusButton();
}
//baban
function Add_Row(ID) {

    var accCode = $(ID).closest('tr').find('.cls_hdnAccountDesc').val(); 
    var isdrcr = $(ID).closest('tr').find('.cls_drcr').val(); 
    var drAmt = $(ID).closest('tr').find('.cls_drAmount').val(); 
    var crAmt = $(ID).closest('tr').find('.cls_crAmount').val(); 

    if (accCode == '') {
        $(ID).closest('tr').find('.cls_AccountDesc').focus(); return false;
    }
    if (isdrcr == '' || isdrcr == 'undefined')
    {
        $(ID).closest('tr').find('.cls_drcr').focus(); return false;
    }
    if (isdrcr == "D" && drAmt <= 0)
    {
        $(ID).closest('tr').find('.cls_drAmount').focus(); return false;
    }
    if (isdrcr == "C" && crAmt <= 0) {
        $(ID).closest('tr').find('.cls_crAmount').focus(); return false;
    }

    var table = "";
    var len = $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').length;
    if (len > 0)
        table = $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill:last');
    else {
        table = $('#tbl_Expense_Voucher_journal tbody');
    }

    var orderNo = 1;
    var arr = $("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill td.cls_Order").map(function (ind, val) { return Number($(val).attr("orderno")) }).sort(function (a, b) { return b - a });
    if (arr.length > 0) {
        orderNo = arr[0] + 1;
    }

    var html = "";
    html += "<tr class='myData_voucher_bill'>";
    html += "<td style='display:none' class='cls_Order' srlno='' orderno=" + orderNo + ">" + orderNo + "</td>";

    html += "<td style='text-align:center'>"
    html += "<input type= 'text' class='cls_AccountDesc form-control' id='desc_" + orderNo + "'  value='' />";
    html += "<input type= 'hidden' class='cls_hdnAccountDesc form-control' value='' />";
    html += "<input type='hidden' class='cls_hdnSubCode form-control' value=''>";
    html += "<input type= 'hidden' class='cls_hdnSubLedger form-control' value=''/>";
    html += "</td>";

    html += "<td style='text-align:center'><select id='drcr_" + orderNo + "' class='cls_drcr form-control' onchange='Enable_DisableVch_bill(this)' >";
    html += "<option value=''>Dr/Cr</option>";
    html += "<option value='D'>DR</option>";
    html += "<option value='C'>CR</option>";
    html += "</select></td>";

    html += "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal' id='dr_" + orderNo + "'  value='' onkeyup='OnlyDecimalAmount(); Vch_Cal_Amount();' /></td>";
    html += "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal' id='cr_" + orderNo + "'  value='' onkeyup='OnlyDecimalAmount(); Vch_Cal_Amount();' /></td>";

    html += "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row(this)' /></td>";
    html += "</tr>";

    if (len > 0)
        table.after(html);
    else
        table.append(html);

    Add_PlusButton(); Calculate_TotalVch_bill();
}
function Add_PlusButton() {
    var html = "";
    $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').closest('tr').find('.cls_btnAdd').html('');

    html = "<img src='/Content/Images/add.png' title='Add' style='height:20px; width: 20px; cursor:pointer;' onclick='Add_Row(this)' />";

    var len = $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').length;
    if (len == 0) {
        $('#tbl_Expense_Voucher_journal tr.trfooter_voucher_bill').remove();
        $('#tbl_Expense_Voucher_journal thead th.cls_headerAdd').html(html);
    }
    else {
        $('#tbl_Expense_Voucher_journal thead th.cls_headerAdd').html('');
        $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill:last td:last>.cls_btnAdd').html(html);
    }
}
//baban
function AutoComplete(ID)
{
   
    $(document).on('keydown', '.cls_AccountDesc', function (e) {
        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id');
        var TabID = $(ID).closest('tr').find('.TabId').text();

        $('#' + t).autocomplete({
      
            source: function (request, response) {
                var V = "{TabID:'" + TabID + "', Desc:'" + $('#' + t).val() + "', TransType: '"+ "AccountDesc" +"'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/BillExtract/Account_Description',
                    type: 'POST',
                    data: V,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
									 SubLedger: item.SubLedger
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                var id = i.item.AccountCode;
                $('#' + t).closest('tr').find('.cls_hdnAccountDesc').val(id);
                $('#' + t).closest('tr').find('.cls_hdnSubLedger').val(i.item.SubLedger);
              
                if (i.item.SubLedger == 'Y') {
                    $('#dialog-subledger').modal('show');
                   
				    $('.OptionSubLedger').val('');
                    $('#hdnOptionSubLedger').val('');
                    Autocomplete_OptionSubLedger(t);
                }
            },
            minLength: 0
        }).click(function () {
            $(t).autocomplete('search', ($(t).val()))
            });
			
			 var iKeyCode = (e.which) ? e.which : e.keyCode
        if (iKeyCode == 8) {
            $('#' + t).closest('tr').find('.cls_AccountDesc').val(''); 
            $('#' + t).closest('tr').find('.cls_hdnAccountDesc').val(''); 
            $('#' + t).closest('tr').find('.cls_hdnSubCode').val(''); 
            $('#' + t).closest('tr').find('.cls_hdnSubLedger').val(''); 
        }
        if (iKeyCode == 46) {
            $('#' + t).closest('tr').find('.cls_AccountDesc').val('');
            $('#' + t).closest('tr').find('.cls_hdnAccountDesc').val('');
            $('#' + t).closest('tr').find('.cls_hdnSubCode').val(''); 
            $('#' + t).closest('tr').find('.cls_hdnSubLedger').val(''); 
        }
    });
}
function Delete_Row(ID) {
    DeleteJournalfrom_Database(ID);
   
}
function OnlyDecimalAmount()
{
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}
function Vch_Cal_Amount() {
    var DRTotal = 0, CRTotal = 0;

    if ($("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val();
            var CRAmount = $(this).find('.cls_crAmount').val();
            if (Math.abs(DRAmount) > 0 || Math.abs(CRAmount) > 0) {
                DRTotal = parseFloat(DRTotal) + parseFloat(Math.abs(DRAmount) == "" ? 0 : Math.abs(DRAmount));
                CRTotal = parseFloat(CRTotal) + parseFloat(Math.abs(CRAmount) == "" ? 0 : Math.abs(CRAmount));
            }
        });
    }

    if ($('#tbl_Expense_Voucher_journal .trfooter_voucher_bill').length > 0) {
        $('#tbl_Expense_Voucher_journal .trfooter_voucher_bill').find('.clsdrTotal').text((DRTotal).toFixed(2));
        $('#tbl_Expense_Voucher_journal .trfooter_voucher_bill').find('.clscrTotal').text((CRTotal).toFixed(2));
        return;
    }
}
function Enable_DisableVch_bill(ID) {
    var drcr = $(ID).closest('tr').find('.cls_drcr').val();
    if (drcr != "") {
        $(ID).closest('tr').find('.cls_drAmount').val(0);
        $(ID).closest('tr').find('.cls_crAmount').val(0);

        $(ID).closest('tr').find('.cls_drAmount').prop('disabled', false);
        $(ID).closest('tr').find('.cls_crAmount').prop('disabled', false);
        if (drcr == "D") {
            $(ID).closest('tr').find('.cls_drAmount').focus(); $(ID).closest('tr').find('.cls_drAmount').select();
            $(ID).closest('tr').find('.cls_crAmount').prop('disabled', true);
        }
        if (drcr == "C") {
            $(ID).closest('tr').find('.cls_crAmount').focus(); $(ID).closest('tr').find('.cls_crAmount').select();
            $(ID).closest('tr').find('.cls_drAmount').prop('disabled', true);
        }
        Vch_Cal_Amount();
    }
}
function Calculate_TotalVch_bill() {

    var DRTotal = 0, CRTotal = 0;
    var html = ""

    if ($("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val();
            var CRAmount = $(this).find('.cls_crAmount').val();
            DRTotal = parseFloat(DRTotal) + parseFloat(DRAmount == "" ? 0 : DRAmount);
            CRTotal = parseFloat(CRTotal) + parseFloat(CRAmount == "" ? 0 : CRAmount);
        });
    }

    $('#tbl_Expense_Voucher_journal .trfooter_voucher_bill').remove();

    html += "<tr class='trfooter_voucher_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='2'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clsdrTotal' > " + (DRTotal).toFixed(2) + "</span></td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clscrTotal' > " + (CRTotal).toFixed(2) + "</span></td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'><span style='color:white'>jghghjgk</span></td>"
    html + "</tr>";

    html += "<tr class='trfooter_voucher_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='5'><input type='text' id='txtJournalRemarks' class='form-control' placeholder='Narration..' value='" + $('#journalRemarks').val() +"'></td>"
    html + "</tr>";

    $('#tbl_Expense_Voucher_journal tr:last').after(html);
}
//baban
function DRCRHead_Save(ID) {
   
    var ArrVchList = []; var ArrVchListPayment = [];
    var grdvchLen = $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').length;
    var grdvchLenPay = $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').length;
    var vchType = $(ID).closest('tr').find('.VoucherType').text(); 

    if (vchType == "J") {
        if (grdvchLen.length == 0) { alert('Please add Journal Voucher details'); return false; }
        if (grdvchLenPay.length == 0) { alert('Please add Payment Voucher details'); return false; }
    }
    if (vchType == "L") {
        if (grdvchLen.length == 0) { alert('Please add Journal Voucher details'); return false; }
    }
    if (vchType == "P") {
        if (grdvchLenPay.length == 0) { alert('Please add Payment Voucher details'); return false; }
    }

    if (vchType == "J" || vchType == "L")
    {
        //journal
        if (grdvchLen > 0) {

            var DRTotal = 0.00, CRTotal = 0, NetAmount = 0, Roudoff = 0, totalCom = 0;

            if ($("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").length > 0) {
                $("#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill").each(function (index, value) {
                    var DRAmount = Number($(this).find('.cls_drAmount').val());
                    var CRAmount = Number($(this).find('.cls_crAmount').val());
                    DRTotal = Number(DRTotal) + (Number(DRAmount) == "" ? 0 : Number(DRAmount));
                    CRTotal = Number(CRTotal) + (Number(CRAmount) == "" ? 0 : Number(CRAmount));
                });
            }
            //alert(DRTotal);
            //alert(CRTotal);
            if (Math.round(DRTotal, 2) != Math.round(CRTotal, 2)) {
                toastr.error('Sorry ! Journal Debit and Credit Amount is not Equal.', 'Warning')
                return false;
            }

            var Amount = 0;
            $('#tbl_Expense_Voucher_journal tbody tr.myData_voucher_bill').each(function () {

                var Order = $(this).find('.cls_Order').text();
                var DRCR = $(this).find('.cls_drcr').val();
                var AccCode = $(this).find('.cls_hdnAccountDesc').val(); //alert(AccCode);
                var SubCode = $(this).find('.cls_hdnSubCode').val();
                var SubLedger = $(this).find('.cls_hdnSubLedger').val();
                if (AccCode == 'null') {
                    boolDesc = 1;
                }
                if (SubLedger == 'Y' && SubCode == "") {
                    jLedger = 1;
                }

                if (DRCR == "D")
                    Amount = $(this).find('.cls_drAmount').val();
                else
                    Amount = $(this).find('.cls_crAmount').val();

                if (Math.abs(Amount) > 0) {
                    ArrVchList.push({
                        'Ord': Order, 'Drcr': DRCR, 'AccountCode': AccCode, 'Amount': Amount, 'VoucherType': "J", 'SubCode': SubCode
                    });
                }
            });

            if (boolDesc == 1) {
                alert('Please, Select Journal Ledger Detail.'); boolDesc = ""; return false;
            }
            if (jLedger == 1) {
                alert('Please, Confirm Sub Ledger Detail.'); jLedger = ""; return false;
            }
        }
        else {
            var vchType = $(ID).closest('tr').find('.VoucherType').text();
            if (vchType == 'J') {
                toastr.error('Sorry ! Add Account Description for Journal Voucher detail.', 'Warning')
                return false;
            }
        }
        var jRemarks = $('#txtJournalRemarks').val();
        if (jRemarks == "") {
            alert('Please, Enter Journal Remarks.'); $('#txtJournalRemarks').focus(); return false;
        }
    }

    if (vchType == "J" || vchType == "P")
    {
        //payment
        if (grdvchLenPay > 0) {

            var DRTotal = 0, CRTotal = 0, NetAmount = 0, Roudoff = 0, totalCom = 0;

            if ($("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").length > 0) {
                $("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").each(function (index, value) {
                    var DRAmount = Number($(this).find('.cls_drAmount').val());
                    var CRAmount = Number($(this).find('.cls_crAmount').val());
                    DRTotal = Number(DRTotal) + (Number(DRAmount) == "" ? 0 : Number(DRAmount));
                    CRTotal = Number(CRTotal) + (Number(CRAmount) == "" ? 0 : Number(CRAmount));
                });
            }
            if (Math.round(DRTotal, 2) != Math.round(CRTotal, 2)) {
                toastr.error('Sorry ! Payment Debit and Credit Amount is not Equal.', 'Warning')
                return false;
            }

            var Amount = 0;
            $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').each(function () {

                var Order = $(this).find('.cls_Order').text();
                var DRCR = $(this).find('.cls_drcr').val();
                var AccCode = $(this).find('.cls_hdnAccountDesc_payment').val();
                var SubCode = $(this).find('.cls_hdnSubCode_payment').val();
                var SubLedger = $(this).find('.cls_hdnSubLedger_payment').val();
                if (AccCode == 'null') {
                    boolPayDesc = 1;
                }
                if (SubLedger == 'Y' && SubCode == "") {
                    pLedger = 1;
                }

                if (DRCR == "D")
                    Amount = $(this).find('.cls_drAmount').val();
                else
                    Amount = $(this).find('.cls_crAmount').val();

                if (Math.abs(Amount) > 0) {
                    ArrVchListPayment.push({
                        'Ord': Order, 'Drcr': DRCR, 'AccountCode': AccCode, 'Amount': Amount, 'VoucherType': "P", 'SubCode': SubCode
                    });
                }
            });
            if (boolPayDesc == 1) {
                alert('Please, Select Payment Ledger Detail.'); boolPayDesc = ""; return false;
            }
            if (pLedger == 1) {
                alert('Please, Confirm Sub Ledger Detail.'); pLedger = ""; return false;
            }
        }
        else {
            toastr.error('Sorry ! Add Account Description for Payment Voucher detail.', 'Warning')
            return false;
        }
        var pRemarks = $('#txtPaymentRemarks').val();
        if (pRemarks == "") {
            alert('Please, Enter Payment Remarks.'); $('#txtPaymentRemarks').focus(); return false;
        }
    }



    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val(); 
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text(); 

    var Master = JSON.stringify(ArrVchList);
    var Master1 = JSON.stringify(ArrVchListPayment);



    var E = "{TabID: '" + TabID + "', MemoNo: '" + MemoNo + "', JournalRemarks: '" + $('#txtJournalRemarks').val() + "', PaymentRemarks: '" + $('#txtPaymentRemarks').val() + "', param: " + Master + ", param1: " + Master1 + "}";

    alert(JSON.stringify(Master));
    //alert(JSON.stringify(ArrVchList));
    //alert(boolDesc);
    return false;
  
    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_DRCRHead',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200) {
                $('#dialog-allotedto_' + JobID).modal('hide');
                swal({
                    title: "Success",
                    text: 'Bill Abstract Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });

            }
        }
    });
}
function DeleteJournalfrom_Database(ID) {

    var TabID = $('#hdnTabId').val();
    var srlno = $(ID).closest('tr').find('.cls_Order').attr('srlno');

    if (srlno != "") {
        var E = "{JobID: '" + srlno + "', TabId: '" + TabID + "', TransType: '" + "VoucherDelete" + "', ISPOAmtExceed: '" + "J" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/GST',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data;
                if (response.Status == 200) {
                    $(ID).closest('tr').remove(); Add_PlusButton();
                }
            }
        });
    }
    else {
        $(ID).closest('tr').remove(); Add_PlusButton();
    }
}

//baban
//PAYMENT
function Load_AccountInformation_Payment(detail) {
   
    $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').remove();
   
    if (detail.length > 0) {

        var drcrAmt = 0, isDRdisable = "", isCRdisable = "";

        var Remarks = detail[0].Remarks == null ? '' : detail[0].Remarks; 
        $('#paymentRemarks').val(Remarks);

        var table = $('#tbl_Expense_Voucher_payment tbody');

        $.each(detail, function (key, value) {

            if (value.DrCr == "D") {
                if (value.Amount > 0) {
                    isDRdisable = "";
                    isCRdisable = 'disabled=true';
                }
            }

            if (value.DrCr == "C") {
                if (value.Amount > 0) {
                    isCRdisable = "";
                    isDRdisable = 'disabled=true';
                }
            }

            table.append("<tr class='myData_voucher_bill'>"
                + "<td style='display:none' class='cls_Order' srlno='" + value.Vchid +"' orderno=" + value.VchSrlId + ">" + value.VchSrlId + "</td>"

                + "<td style='text-align:center'>"
                + "<input type= 'text' class='cls_AccountDesc_payment form-control'   id='descpayment_" + key + "'  value='" + (value.AccountDescription == null ? '' : value.AccountDescription) + "'/>"
                + "<input type= 'hidden' class='cls_hdnAccountDesc_payment form-control' value='" + value.AccountCode + "'/>"
                + "<input type= 'hidden' class='cls_hdnSubCode_payment form-control' value='" + value.SubCode + "'/>"
                + "<input type= 'hidden' class='cls_hdnSubLedger_payment form-control' value='" + value.Subledger + "'/>"
                + "</td>"

                + "<td style='text-align:center'><select id='drcrp_" + key + "' class='cls_drcr form-control' onchange='Enable_DisableVch_Payment(this)' >"
                + "<option value=''>Dr/Cr</option>"
                + "<option value='D'>DR</option>"
                + "<option value='C'>CR</option>"
                + "</select></td>"

                + "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal'" + isDRdisable + " id='dr_" + key + "'  value='" + (value.DrCr == "D" ? (value.Amount == null ? 0 : value.Amount) : 0) + "' onkeyup='Vch_Cal_Amount_Payment()' /></td>"
                + "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal'" + isCRdisable + " id='cr_" + key + "'  value='" + (value.DrCr == "C" ? (value.Amount == null ? 0 : value.Amount) : 0) + "' onkeyup='Vch_Cal_Amount_Payment()' /></td>"

                + "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row_Payment(this)' /></td>" +

                + "</tr>");
            $('#drcrp_' + key).val(value.DrCr);
        });

        Calculate_TotalVch_Payment(Remarks);
    }
    
    Add_PlusButton_Payment();
   
}
function Calculate_TotalVch_Payment() {

    var DRTotal = 0, CRTotal = 0;
    var html = ""

    if ($("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val();
            var CRAmount = $(this).find('.cls_crAmount').val();
            DRTotal = parseFloat(DRTotal) + parseFloat(DRAmount == "" ? 0 : DRAmount);
            CRTotal = parseFloat(CRTotal) + parseFloat(CRAmount == "" ? 0 : CRAmount);
        });
    }

    $('#tbl_Expense_Voucher_payment .trfooter_voucher_bill').remove();

    html += "<tr class='trfooter_voucher_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='2'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clsdrTotal' > " + (DRTotal).toFixed(2) + "</span></td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clscrTotal' > " + (CRTotal).toFixed(2) + "</span></td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'><span style='color:white'>jghghjgk</span></td>"
    html + "</tr>";

    html += "<tr class='trfooter_voucher_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='5'><input type='text' id='txtPaymentRemarks' class='form-control' placeholder='Narration..' value='" + $('#paymentRemarks').val() +"'></td>"
    html + "</tr>";

    $('#tbl_Expense_Voucher_payment tr:last').after(html)
}
function Add_PlusButton_Payment() {
    var html = "";
    $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').closest('tr').find('.cls_btnAdd').html('');

    html = "<img src='/Content/Images/add.png' title='Add' style='height:20px; width: 20px; cursor:pointer;' onclick='Add_Row_Payment(this)' />";

    var len = $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').length;
    if (len == 0) {
        $('#tbl_Expense_Voucher_payment tr.trfooter_voucher_bill').remove();
        $('#tbl_Expense_Voucher_payment thead th.cls_headerAdd').html(html);
    }
    else {
        $('#tbl_Expense_Voucher_payment thead th.cls_headerAdd').html('');
        $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill:last td:last>.cls_btnAdd').html(html);
    }
}
function Delete_Row_Payment(ID) {
    Deletefrom_Database(ID);
    
    
}
//baban
function Add_Row_Payment(ID) {

    var accCode = $(ID).closest('tr').find('.cls_hdnAccountDesc_payment').val(); 
    var isdrcr = $(ID).closest('tr').find('.cls_drcr').val();
    var drAmt = $(ID).closest('tr').find('.cls_drAmount').val();
    var crAmt = $(ID).closest('tr').find('.cls_crAmount').val();

    if (accCode == '') {
        $(ID).closest('tr').find('.cls_AccountDesc_payment').focus(); return false;
    }
    if (isdrcr == '' || isdrcr == 'undefined') {
        $(ID).closest('tr').find('.cls_drcr').focus(); return false;
    }
    if (isdrcr == "D" && drAmt <= 0) {
        $(ID).closest('tr').find('.cls_drAmount').focus(); return false;
    }
    if (isdrcr == "C" && crAmt <= 0) {
        $(ID).closest('tr').find('.cls_crAmount').focus(); return false;
    }

    var table = "";
    var len = $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill').length;
    if (len > 0)
        table = $('#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill:last');
    else {
        table = $('#tbl_Expense_Voucher_payment tbody');
    }

    var orderNo = 1;
    var arr = $("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill td.cls_Order").map(function (ind, val) { return Number($(val).attr("orderno")) }).sort(function (a, b) { return b - a });
    if (arr.length > 0) {
        orderNo = arr[0] + 1;
    }

    var html = "";
    html += "<tr class='myData_voucher_bill'>";
    html += "<td style='display:none' class='cls_Order' srlno='' orderno=" + orderNo + ">" + orderNo + "</td>";

    html += "<td style='text-align:center'>"
    html += "<input type= 'text' class='cls_AccountDesc_payment form-control' id='descpayment_" + orderNo + "'  value='' />";
    html += "<input type= 'hidden' class='cls_hdnAccountDesc_payment form-control' value='' />";
    html += "<input type='hidden' class='cls_hdnSubCode_payment form-control' value=''>";
    html += "<input type= 'hidden' class='cls_hdnSubLedger_payment form-control' value=''/>";
    html += "</td>";

    html += "<td style='text-align:center'><select id='drcrp_" + orderNo + "' class='cls_drcr form-control' onchange='Enable_DisableVch_Payment(this)' >";
    html += "<option value=''>Dr/Cr</option>";
    html += "<option value='D'>DR</option>";
    html += "<option value='C'>CR</option>";
    html += "</select></td>";

    html += "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal' id='drp_" + orderNo + "'  value='' onkeyup='OnlyDecimalAmount(); Vch_Cal_Amount_Payment();' /></td>";
    html += "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal' id='crp_" + orderNo + "'  value='' onkeyup='OnlyDecimalAmount(); Vch_Cal_Amount_Payment();' /></td>";

    html += "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row_Payment(this)' /></td>";
    html += "</tr>";

    if (len > 0)
        table.after(html);
    else
        table.append(html);

    Add_PlusButton_Payment(); Calculate_TotalVch_Payment();
}
function Enable_DisableVch_Payment(ID) {
    var drcr = $(ID).closest('tr').find('.cls_drcr').val();
    if (drcr != "") {
        $(ID).closest('tr').find('.cls_drAmount').val(0);
        $(ID).closest('tr').find('.cls_crAmount').val(0);

        $(ID).closest('tr').find('.cls_drAmount').prop('disabled', false);
        $(ID).closest('tr').find('.cls_crAmount').prop('disabled', false);
        if (drcr == "D") {
            $(ID).closest('tr').find('.cls_drAmount').focus(); $(ID).closest('tr').find('.cls_drAmount').select();
            $(ID).closest('tr').find('.cls_crAmount').prop('disabled', true);
        }
        if (drcr == "C") {
            $(ID).closest('tr').find('.cls_crAmount').focus(); $(ID).closest('tr').find('.cls_crAmount').select();
            $(ID).closest('tr').find('.cls_drAmount').prop('disabled', true);
        }
        Vch_Cal_Amount_Payment();
    }
}
function Vch_Cal_Amount_Payment() {
    var DRTotal = 0, CRTotal = 0;

    if ($("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_payment tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val();
            var CRAmount = $(this).find('.cls_crAmount').val();
            if (Math.abs(DRAmount) > 0 || Math.abs(CRAmount) > 0) {
                DRTotal = parseFloat(DRTotal) + parseFloat(Math.abs(DRAmount) == "" ? 0 : Math.abs(DRAmount));
                CRTotal = parseFloat(CRTotal) + parseFloat(Math.abs(CRAmount) == "" ? 0 : Math.abs(CRAmount));
            }
        });
    }

    if ($('#tbl_Expense_Voucher_payment .trfooter_voucher_bill').length > 0) {
        $('#tbl_Expense_Voucher_payment .trfooter_voucher_bill').find('.clsdrTotal').text((DRTotal).toFixed(2));
        $('#tbl_Expense_Voucher_payment .trfooter_voucher_bill').find('.clscrTotal').text((CRTotal).toFixed(2));
        return;
    }
}
//baban
function AutoComplete_Payment(ID) {
  
    $(document).on('keydown', '.cls_AccountDesc_payment', function (e) {
        var ths = $(this);
        ths.removeClass('selectd');
        ths.addClass('selectd');
        var t = ths.attr('id');
        var TabID = $(ID).closest('tr').find('.TabId').text();

        $('#' + t).autocomplete({

            source: function (request, response) {
                var V = "{TabID:'" + TabID + "', Desc:'" + $('#' + t).val() + "', TransType: '" + "AccountDesc" + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/BillExtract/Account_Description',
                    type: 'POST',
                    data: V,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
									  SubLedger: item.SubLedger
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                var id = i.item.AccountCode;
                $('#' + t).closest('tr').find('.cls_hdnAccountDesc_payment').val(id);
                $('#' + t).closest('tr').find('.cls_hdnSubLedger_payment').val(i.item.SubLedger);
				
				 if (i.item.SubLedger == 'Y') {
                    $('#dialog-payment-subledger').modal('show');
                     $('.PaymentOptionSubLedger').val('');
                    $('#hdnPaymentOptionSubLedger').val('');
                    Autocomplete_PaymentOptionSubLedger(t);
                }
            },
            minLength: 0
        }).click(function () {
            $(t).autocomplete('search', ($(t).val()))
        });
		var iKeyCode = (e.which) ? e.which : e.keyCode
        if (iKeyCode == 8) {
            $('#' + t).closest('tr').find('.cls_AccountDesc_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnAccountDesc_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnSubCode_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnSubLedger_payment').val('');
        }
        if (iKeyCode == 46) {
            $('#' + t).closest('tr').find('.cls_AccountDesc_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnAccountDesc_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnSubCode_payment').val('');
            $('#' + t).closest('tr').find('.cls_hdnSubLedger_payment').val('');
        }
    });
}
function Deletefrom_Database(ID)
    {
    var TabID = $('#hdnTabId').val();
    var srlno = $(ID).closest('tr').find('.cls_Order').attr('srlno'); 

    if (srlno != "") {
        var E = "{JobID: '" + srlno + "', TabId: '" + TabID + "', TransType: '" + "VoucherDelete" + "', ISPOAmtExceed: '" + "P" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/GST',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data;
                if (response.Status == 200) {
                    $(ID).closest('tr').remove(); Add_PlusButton_Payment();
                }
            }
        });
    }
    else
    {
        $(ID).closest('tr').remove(); Add_PlusButton_Payment();
    }
}


function Print(ID, Con)
{
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    var BillStatus = Con; 

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: Con == 1 ? "BillExtract.rpt" : "AckBillExtract.rpt",
        FileName: MemoNo,
		Database:''
    });

    detail.push({
        MemoNo: MemoNo,
        BillStatus: BillStatus,
        ReportType: 1,
        SectorID: $('#ddlSector').val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function PrintHistory(ID, Con) {
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    var BillStatus = Con; 

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "BillExtractHistory.rpt",
        FileName: MemoNo,
		Database:''
    });

    detail.push({
        MemoNo: MemoNo,
        BillStatus: BillStatus,
        ReportType: 2
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function PrintVoucher(ID, VchNo) {
   
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    var VoucherNo = VchNo;//$(ID).closest('tr').find('.VoucherNo').text();

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: MemoNo,
		Database:''
    });

    detail.push({
        VoucherNumber: VoucherNo,
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function AddMultipleFC()
{
    BindFC();
    $('#dialog-MultipleFC').modal('show');
}
function BindFC()
{
    var MemoNo = $("#txtMemoNo").val();
    if (MemoNo != "") {
        var E = "{MemoNo: '" + MemoNo + "', TransType: '" + "FCDetail" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/FCDetail',
            data: E,
            async:false,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data; 
                if (data.length > 0) {

                    var html = ""

                    var $this = $('#tblFC .test_0');
                    $parentTR = $this.closest('tr');
                    $('#tblFC tbody tr.myData').empty();

                    for (var j = 0; j < data.length; j++) {
                      
                        html += "<tr class='myData' >"
                            + "<td style='display:none;' class='clsAccCode' chk-data='" + (data[j].FCNo == 'null' ? '' : data[j].FCNo) + "' >" + (data[j].FCNo == 'null' ? '' : data[j].FCNo) + "</td>"
                            + "<td class='clsAccDesc'>" + (data[j].BudgetDesc == null ? '' : data[j].BudgetDesc)  + "</td>"
                          
                            + "<td style='display:none;' class='clsBudgetID'>" + (data[j].BudgetID == null ? '' : data[j].BudgetID) + "</td>"
                            + "<td style='text-align:center;' >" + (data[j].RefTypeDesc == null ? '' : data[j].RefTypeDesc) + "</td>"
                            + "<td style='text-align:center; display:none' class='clsRefType'>" + (data[j].RefType == null ? '' : data[j].RefType)  + "</td>"
                            + "<td style='text-align:center;' class='clsRefNo'>" + (data[j].RefNo == null ? '' : data[j].RefNo) + "</td>"
                            + "<td style='text-align:center;' class='clsRefDate'>" + (data[j].RefDate == null ? '' : data[j].RefDate) + "</td>"
                            + "<td style='text-align:center;' class='clsSrNo' >" + (data[j].SrvNo == null ? '' : data[j].SrvNo) + "</td>"
                            + "<td style='text-align:center;' class='clsSrDate'>" + (data[j].SrvDate == null ? '' : data[j].SrvDate) + "</td>"
                            + "<td style='text-align:center;' class='clsInvNo'>" + (data[j].TaxInvoiceNo == null ? '' : data[j].TaxInvoiceNo) + "</td>"
                            + "<td style='text-align:center;' class='clsInvDate'>" + (data[j].TaxInvoiceDate == null ? '' : data[j].TaxInvoiceDate) + "</td>"
                            + "<td style='text-align:center; display:none;' class='clsfcDetailID' >" + data[j].FCDetailID + "</td>"
                            + "<td style='text-align:right;' class='CLSFCAMT'>" + (data[j].Amount == null ? 0 : data[j].Amount) + "</td>"

                            + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer;margin-bottom:4px; text-align:center;' onClick='DeleteFC(this);' />"
                            + "<br/><img src='/Content/Images/Edit.png' title= 'Edit' style= 'width:20px;height:20px;cursor:pointer; text-align:center;' onClick= 'EditFC(this);' /></td > "
                        html + "</tr>";
                    }
                    $parentTR.after(html);
                }
            }
        });
    }
}
function AddFC()
{
    //if ($("#hdnEffectAdd").val() == "") { $("#txtEfftGL").focus(); return false; }


    if ($("#ddlRefType").val() == "") { $("#ddlRefType").focus(); return false; }
    var RefType = $('#ddlRefType').val();
    if (RefType != 'N') {
        if ($('#txtRefNo').val() == "") {
            $('#txtRefNo').focus(); return false;
        }
        if ($('#txtRefDate').val() == "") {
            $('#txtRefDate').focus(); return false;
        }
    }

    //if ($("#txtSRNo").val() == "") { $("#txtSRNo").focus(); return false; }
    //if ($("#txtSRVDate").val() == "") { $("#txtSRVDate").focus(); return false; }
    if ($("#txtInvoiceNo").val() == "") { $("#txtInvoiceNo").focus(); return false; }
    if ($("#txtInvoiceDate").val() == "") { $("#txtInvoiceDate").focus(); return false; }

    if ($("#txtEfftAmount").val() <= 0) { $("#txtEfftAmount").focus(); return false; }
    if ($("#txtEfftAmount").val() == "") { $("#txtEfftAmount").focus(); return false; }

    var chkAccCode = $("#hdnEffectAdd").val(); 

    //if (chkAccCode != "" &&  chkAccCode != 'null') {
    //    var l = $('#tblFC tr.myData').find("td[chk-data='" + chkAccCode + "']").length;
    //    if (l > 0) {
    //        alert('Sorry ! This FC No. is Already Available.'); $("#txtEfftGL").val(''); $("#hdnEffectAdd").val(''); $("#txtEfftAmount").val(''); return false;
    //    }
    //}


    var html = ""

    var $this = $('#tblFC .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnEffectAdd").val() + "' >" + $("#hdnEffectAdd").val() + "</td>"
        + "<td class='clsAccDesc'>" + $("#txtEfftGL").val() + "</td>"
        + "<td style='display:none;' class='clsBudgetID'>" + $("#hdnEffectTempBudgetID").val() + "</td>"
        + "<td style='text-align:center;' >" + $("#ddlRefType option:selected").text() + "</td>"
        + "<td style='text-align:center; display:none' class='clsRefType'>" + $("#ddlRefType").val() + "</td>"
        + "<td style='text-align:center;' class='clsRefNo'>" + $("#txtRefNo").val() + "</td>"
        + "<td style='text-align:center;' class='clsRefDate'>" + $("#txtRefDate").val() + "</td>"
        + "<td style='text-align:center;' class='clsSrNo' >" + $("#txtSRNo").val() + "</td>"
        + "<td style='text-align:center;' class='clsSrDate'>" + $("#txtSRVDate").val() + "</td>"
        + "<td style='text-align:center;' class='clsInvNo'>" + $("#txtInvoiceNo").val() + "</td>"
        + "<td style='text-align:center;' class='clsInvDate'>" + $("#txtInvoiceDate").val() + "</td>"
        + "<td style='text-align:center; display:none;' class='clsfcDetailID' >" + $("#hdnEfftFcDetailID").val() + "</td>"
        + "<td style='text-align:right;' class='CLSFCAMT'>" + $("#txtEfftAmount").val() + "</td>"

        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteFC(this);' />"
        + "<br/><img src='/Content/Images/Edit.png' title= 'Edit' style= 'width:20px;height:20px;cursor:pointer; text-align:center;' onClick= 'EditFC(this);' /></td > "

    html + "</tr>";

    $parentTR.after(html);

    Calculate_TotalGross();

    $("#txtEfftGL").val(''); $("#hdnEffectAdd").val(''); $("#txtEfftAmount").val('');

    $("#ddlRefType").val(''); $("#txtRefNo").val(''); $("#txtRefDate").val(''); $("#txtSRNo").val('');
    $("#txtSRVDate").val(''); $("#txtInvoiceNo").val(''); $("#txtInvoiceDate").val('');

    $("#hdnEffectBudgetID").val($("#hdnEffectTempBudgetID").val()); $("#hdnEffectTempBudgetID").val(''); $("#hdnEfftFcDetailID").val(''); $("#hdnEffectTempBudgetID").val('');
    $('#lblFCTotalAmt').text('0.00');
  
}
function DeleteFC(ID)
{
    var amt = (($(ID).closest('tr').find('.CLSFCAMT').text()) == null ? 0 : ($(ID).closest('tr').find('.CLSFCAMT').text()));
    var fcDetailID = $(ID).closest('tr').find('.clsfcDetailID').text();

    if (fcDetailID == "")
    {
        $(ID).closest('tr').remove();
    }
    else
    {
        var E = "{JobID: '" + "" + "', TabId: '" + fcDetailID + "', TransType: '" + "DeleteFCDetail" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/JobWork',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (D) {
                var t = D.Data; 
                if (t[0].Result == 'Success')
                {
                    $(ID).closest('tr').remove();
                }
            }
        });
    }

    if (amt == null)
    {
        $("#txtGrossAmount").val(0);
    }
    else
    {
        $("#txtGrossAmount").val(parseFloat(($("#txtGrossAmount").val()) - parseFloat(amt)));
    }
    

    var l = $('#tblFC tr.myData').length;
    if (l == 0)
    {
        $("#hdnEffectBudgetID").val(''); $("#hdnEffectTempBudgetID").val(''); $("#hdnEffectAmount").val(''); $("#hdnEffectAdd").val('');
    }
}
function EditFC(ID)
{
    var FcNo = $(ID).closest('tr').find('.clsAccCode').text(); 
    var FcNoDesc = $(ID).closest('tr').find('.clsAccDesc').text();

    var Amount = (($(ID).closest('tr').find('.CLSFCAMT').text() == null || $(ID).closest('tr').find('.CLSFCAMT').text() == "") ? 0 : ($(ID).closest('tr').find('.CLSFCAMT').text()));
    var RefType = $(ID).closest('tr').find('.clsRefType').text();
    var RefNo = $(ID).closest('tr').find('.clsRefNo').text();
    var RefDate = $(ID).closest('tr').find('.clsRefDate').text();

    var SrNo = $(ID).closest('tr').find('.clsSrNo').text();
    var SrDate = $(ID).closest('tr').find('.clsSrDate').text(); 
    var InvNo = $(ID).closest('tr').find('.clsInvNo').text();
    var InvDate = $(ID).closest('tr').find('.clsInvDate').text();
    var fcDetailID = $(ID).closest('tr').find('.clsfcDetailID').text();
    var TotalFCAmt = $(ID).closest('tr').find('.clsTotalFCAmt').text();

    $('#txtEfftGL').val(FcNoDesc); $('#hdnEffectAdd').val(FcNo); $('#txtEfftAmount').val(Amount); $('#ddlRefType').val(RefType);
    $('#txtRefNo').val(RefNo); $('#txtRefDate').val(RefDate); $('#txtSRNo').val(SrNo); $('#txtSRVDate').val(SrDate); $('#txtInvoiceNo').val(InvNo); $('#txtInvoiceDate').val(InvDate);
    $('#hdnEfftFcDetailID').val(fcDetailID); 
    //if (Amount == null || Amount) {
    //    $("#txtGrossAmount").val(0);
    //}
    //else {
    //    $("#txtGrossAmount").val(parseFloat(($("#txtGrossAmount").val()) - parseFloat(Amount)));
    //}
   
    
    $(ID).closest('tr').remove();
    CheckFCLimit(3);
    Calculate_TotalGross();
}
function Calculate_TotalGross()
{
    var Amount = 0;
    var grdLen = $('#tblFC tbody tr.myData').length;
    if (grdLen > 0) {
        $('#tblFC tbody tr.myData').each(function () {
            var a = $(this).find('.CLSFCAMT').text();
            Amount = parseFloat(Amount) + parseFloat(a);
        });
    }
    $("#txtGrossAmount").val(Amount);
}
function CheckFCLimit(con)
{
   
    var fcno = $("#hdnEffectAdd").val(); 
    if (fcno != "") {
        var E = "{MemoNo: '" + fcno + "', TabID: '" + $("#hdnTabId").val() + "', TransType: '" + "FCLimit" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/FCDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data; 
               
                var ActualFCAmount = data[0].ActualAmount; //$("#hdnEffectAmount").val(); //alert(ActualFCAmount);

                var TotalFCAmount = data[0].TotalFCAmount; //alert(TotalFCAmount);
                var EnterFCAmount = parseFloat($("#txtEfftAmount").val()) + parseFloat(TotalFCAmount); //alert(EnterFCAmount);

                var RestFCAmount = parseFloat(ActualFCAmount) - parseFloat(TotalFCAmount);
               

                if (con == 2) {
                    if (parseFloat(EnterFCAmount) > parseFloat(ActualFCAmount)) {
                        alert('Sorry ! Enter Total FC Amount is greater than Actual FC Amount - ' + ActualFCAmount); return false;
                    }
                    else {
                        //AddFC();
                        Check_FCWiseAmount($('#hdnEffectAdd').val()); //$("#hdnEffectTempBudgetID").val()
                    }
                }
                else if (con == 3) {
                    $("#lblFCTotalAmt").text(RestFCAmount);
                }
                else
                {
                    $("#txtEfftAmount").val(RestFCAmount);
                }
            }
        });
    }
    else
    {
        if(con !=3)
        AddFC();
        //if ($("#ddlRefType").val() == "") { $("#ddlRefType").focus(); return false; }
        //var RefType = $('#ddlRefType').val();
        //if (RefType != 'N') {
        //    if ($('#txtRefNo').val() == "") {
        //        $('#txtRefNo').focus(); return false;
        //    }
        //    if ($('#txtRefDate').val() == "") {
        //        $('#txtRefDate').focus(); return false;
        //    }
        //}
        //if ($("#txtInvoiceNo").val() == "") { $("#txtInvoiceNo").focus(); return false; }
        //if ($("#txtInvoiceDate").val() == "") { $("#txtInvoiceDate").focus(); return false; }
        //if ($("#txtEfftAmount").val() <= 0) { $("#txtEfftAmount").focus(); return false; }
        //if ($("#txtEfftAmount").val() == "") { $("#txtEfftAmount").focus(); return false; }

        //Check_FCWiseAmount($("#hdnEffectTempBudgetID").val());
    }
}
function Check_FCWiseAmount(FCNo)
{
    var TotalAmount = 0;
    var TotalFCAmount = $('#lblFCTotalAmt').text();
    var EnterFCAmount = $('#txtEfftAmount').val() == "" ? 0 : $('#txtEfftAmount').val();


    var l = $('#tblFC tr.myData').length; 
    if (l > 0) {
        $('#tblFC tbody tr.myData').each(function () {

            var Amount = $(this).find('.CLSFCAMT').text(); 
            var BudgetID = $(this).find('.clsBudgetID').text(); 
            var fcNo = $(this).find('.clsAccCode').attr('chk-data'); 

            if (FCNo == fcNo)
                TotalAmount = parseFloat(TotalAmount) + parseFloat(Amount) ;
        });

        
        if ((parseFloat(TotalAmount) + parseFloat(EnterFCAmount)) > parseFloat(TotalFCAmount))
        {
            alert('Sorry ! Total FC Amount is over than ' + TotalFCAmount); return false;
        }
        else {
            AddFC();
        }
    }
    else
    {
        AddFC();
    }
}


function BindCFC(ID) {
    var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    if (MemoNo != "") {
        var E = "{MemoNo: '" + MemoNo + "', TransType: '" + "FCDetail" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/FCDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data;
                if (data.length > 0) {

                    var html = ""

                    var $this = $('#tblCFC .test_0');
                    $parentTR = $this.closest('tr');
                    $('#tblCFC tbody tr.myData').remove();

                    $('#txtCEfftGL').val(''); $('#hdnCEffectTempBudgetID').val(''); $('#hdnCEffectAmount').val(''); $('#hdnCEffectAdd').val(''); $('#txtCEfftAmount').val(''); $('#txtCEfftInvoiceNo').val(''); $('#hdnCEfftFcDetailID').val('');
                    
                    var BudgetID = data[0].BudgetID;
                    $('#hdnCEffectBudgetID').val(BudgetID);

					 var SubCode = $(ID).closest('tr').find('.SubCode').text(); 
                    for (var j = 0; j < data.length; j++) {
                        var fcno = data[j].FCNo;
						
						
						 if (SubCode == '0') {
						 
                            $('#ddlVoucherType').val(data[j].VoucherType == null ? 'P' : data[j].VoucherType);
                            $("#ddlVoucherType").prop('disabled', true);
                        }
                        else {
                            $('#ddlVoucherType').val(data[j].VoucherType == null ? 'J' : data[j].VoucherType);
                            $("#ddlVoucherType").prop('disabled', false);
                        }
                     
                        $('#ddlBankCash').val(data[j].BankCash == null ? 'B' : data[j].BankCash);
                     
                            html += "<tr class='myData' >"
                                + "<td style='display:none;' class='clsAccCode' chk-data='" + (data[j].FCNo == null ? '' : data[j].FCNo) + "' >" + (data[j].FCNo == null  ? '' : data[j].FCNo) + "</td>"
                                + "<td class='clsDesc'>" + (data[j].BudgetDesc == null ? '' : data[j].BudgetDesc) + "</td>"
                                + "<td style='text-align:right;' class='CLSFCAMT'>" + (data[j].Amount == null ? 0 : data[j].Amount) + "</td>"
                                + "<td style='display:none;' class='clsBudgetID'>" + (data[j].BudgetID == null ? '' : data[j].BudgetID) + "</td>"
                                + "<td style='text-align:center;' class='clsTaxInvoiceNo' chk-inv='" + (data[j].TaxInvoiceNo == null ? '' : data[j].TaxInvoiceNo) + "'>" + (data[j].TaxInvoiceNo == null ? '' : data[j].TaxInvoiceNo) + "</td>"
                                + "<td style='text-align:center; display:none;' class='clsfcDetailID' >" + data[j].FCDetailID + "</td>"
                                + "<td style='text-align:center'>"
                                + "<img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center; display:none;' onClick='DeleteCFC(this);' />"
                                + "<img src='/Content/Images/Edit.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditCFC(this);' />"
                                + "</td > "
                            html + "</tr>";
                       
                    }
                    $parentTR.after(html);
                }
            }
        });
    }
}
function AddCFC() {
   
    if ($("#hdnCEffectAdd").val() == "") { $("#txtCEfftGL").focus(); return false; }
    if ($("#txtCEfftAmount").val() <= 0) { $("#txtCEfftAmount").focus(); return false; }
    if ($("#txtCEfftAmount").val() == "") { $("#txtCEfftAmount").focus(); return false; }
    if ($("#txtCEfftInvoiceNo").val() == "") { $("#txtCEfftInvoiceNo").focus(); return false; }

    var chkAccCode = $("#hdnCEffectAdd").val(); 

    //var l = $('#tblCFC tr.myData').find("td[chk-data='" + chkAccCode + "']").length; 
    //if (l > 0) {
    //    alert('Sorry ! This FC No. is Already Available.'); $("#txtCEfftGL").val(''); $("#hdnCEffectAdd").val(''); $("#txtCEfftAmount").val(''); return false;
    //}


    var html = "";
    var $this = $('#tblCFC .test_0');
    $parentTR = $this.closest('tr');
   
  
    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnCEffectAdd").val() + "' >" + $("#hdnCEffectAdd").val() + "</td>"
        + "<td class='clsDesc'>" + $("#txtCEfftGL").val() + "</td>"
        + "<td style='text-align:right;' class='CLSFCAMT'>" + $("#txtCEfftAmount").val() + "</td>"
        + "<td style='display:none;' class='clsBudgetID'>" + $("#hdnCEffectTempBudgetID").val() + "</td>"
        + "<td style='text-align:center;' class='clsTaxInvoiceNo' chk-inv='" + $("#txtCEfftInvoiceNo").val() + "'>" + $("#txtCEfftInvoiceNo").val() + "</td>"
        + "<td style='text-align:center; display:none;' class='clsfcDetailID' >" + $("#hdnCEfftFcDetailID").val() + "</td>"
        + "<td style='text-align:center'>"
        + "<img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center; display:" + ($("#hdnCEfftFcDetailID").val() == "" ? "" : "none") + "' onClick='DeleteCFC(this);' />"
        + "<img src='/Content/Images/Edit.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditCFC(this);' />"
        + "</td > "
    html + "</tr>";

    $parentTR.after(html);

    $("#txtCEfftGL").val(''); $("#hdnCEffectAdd").val(''); $("#txtCEfftAmount").val(''); $("#hdnCEffectTempBudgetID").val('');
    $("#txtCEfftInvoiceNo").val(''); $("#hdnCEfftFcDetailID").val(''); $("#hdnCEffectTempBudgetID").val(''); $("#lblCFCTotalAmt").text('0.00');

}
function DeleteCFC(ID) {
    var amt = (($(ID).closest('tr').find('.CLSFCAMT').text()) == null ? 0 : ($(ID).closest('tr').find('.CLSFCAMT').text()));

    $(ID).closest('tr').remove();

    var l = $('#tblCFC tbody tr.myData').length;
    if (l == 0) {
        $("#hdnCEffectBudgetID").val(''); $("#hdnCEffectTempBudgetID").val(''); $("#hdnCEffectAmount").val(''); $("#hdnCEffectAdd").val('');
    }
}
function EditCFC(ID)
{
    var fcNo = $(ID).closest('tr').find('.clsAccCode').text(); 
    var fcNoDecsc = $(ID).closest('tr').find('.clsDesc').text();
    var Amount = $(ID).closest('tr').find('.CLSFCAMT').text();
    var InvoiceNo = $(ID).closest('tr').find('.clsTaxInvoiceNo').text();
    var fcDetailID = $(ID).closest('tr').find('.clsfcDetailID').text();
    var BudgetID = $(ID).closest('tr').find('.clsBudgetID').text();

    $(ID).closest('tr').remove(); 

    

    $("#txtCEfftGL").val(fcNoDecsc); $("#hdnCEffectAdd").val(fcNo); $("#txtCEfftAmount").val(Amount); $("#txtCEfftInvoiceNo").val(InvoiceNo); $("#hdnCEfftFcDetailID").val(fcDetailID);
    $("#hdnCEffectTempBudgetID").val(BudgetID);
    CheckCFCLimit(3);
}
function CheckCFCLimit(con) {

    var fcno = $("#hdnCEffectAdd").val(); //alert(fcno);// alert(con);  alert(TabID);
    var TabID = $("#hdnTabID").val();
    if (fcno != "" && fcno != 'null') {
        
        var E = "{MemoNo: '" + fcno + "', TabID: '" + TabID + "', TransType: '" + "FCLimit" + "'}"; // alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BillExtract/FCDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data; //alert(JSON.stringify(data));

                var ActualFCAmount = data[0].ActualAmount; //alert(ActualFCAmount); 

                var TotalFCAmount = data[0].TotalFCAmount; //alert(TotalFCAmount);
                var EnterFCAmount = parseFloat($("#txtCEfftAmount").val()) + parseFloat(TotalFCAmount); //alert(EnterFCAmount);

                var RestFCAmount = parseFloat(ActualFCAmount) - parseFloat(TotalFCAmount); 
               
               
                if (con == 2)
                {
                    if (parseFloat(EnterFCAmount) > parseFloat(ActualFCAmount)) {
                        alert('Sorry ! Enter Total FC Amount is greater than Actual FC Amount - ' + ActualFCAmount); return false;
                    }
                    else {
                        Check_CFCWiseAmount($('#hdnCEffectAdd').val());  // $('#hdnCEffectTempBudgetID').val()
                        //AddCFC();
                    }
                }
                else if (con == 3)
                {
                    $("#lblCFCTotalAmt").text(RestFCAmount); 
                }
                else
                {
                    //$("#txtCEfftAmount").val(RestFCAmount); 
                }
            }
        });
    }
    else {
      
        if ($("#hdnCEffectAdd").val() == "") { $("#txtCEfftGL").focus(); return false; }
        if ($("#txtCEfftAmount").val() <= 0) { $("#txtCEfftAmount").focus(); return false; }
        if ($("#txtCEfftAmount").val() == "") { $("#txtCEfftAmount").focus(); return false; }
        if ($("#txtCEfftInvoiceNo").val() == "") { $("#txtCEfftInvoiceNo").focus(); return false; }
        Check_CFCWiseAmount($('#hdnCEffectAdd').val());  //$('#hdnCEffectTempBudgetID').val()
    }
}
function CheckCFCLimitAmt()
{
    var fcAmt = $("#txtCEfftAmount").val(); 
    var actualfcAmt = $("#hdnCEffectAmount").val();
    if (parseFloat(fcAmt) > parseFloat(actualfcAmt))
    {
        alert('Sorry ! Enter FC Amount is greater than ' + actualfcAmt); $("#txtCEfftAmount").val(0);  return false;
    }
}
function Check_CFCWiseAmount(FCNo) {

    var TotalAmount = 0; 
    var TotalFCAmount = $('#lblCFCTotalAmt').text();
    var EnterFCAmount = $('#txtCEfftAmount').val() == "" ? 0 : $('#txtCEfftAmount').val();

    var l = $('#tblCFC tr.myData').length;
    if (l > 0) {
        $('#tblCFC tbody tr.myData').each(function () {

            var Amount = $(this).find('.CLSFCAMT').text(); 
            var BudgetID = $(this).find('.clsBudgetID').text();
            var fcNo = $(this).find('.clsAccCode').attr('chk-data'); 
            if (FCNo == fcNo)
                TotalAmount = parseFloat(TotalAmount) + parseFloat(Amount); 
        });

       // alert(TotalAmount); alert(TotalFCAmount);
        if ((parseFloat(TotalAmount.toFixed(2)) + parseFloat(EnterFCAmount)) > parseFloat(TotalFCAmount)) {
            alert('Sorry ! Total Amount should be below ' + TotalFCAmount + '.'); return false;
        }
        else {
            AddCFC();
        }
    }
    else {
        AddCFC();
    }
}

function BankAutoComplete(ID) {

    $('#txtBankName').autocomplete({
        source: function (request, response) {

            var MemoDate = $(ID).closest('tr').find('.MemoDt').text();
            //alert(MemoDate);
            //return false;
            var S = "{BankCash:'" + $('#ddlChequeBankCash').val() + "', Desc:'" + $('#txtBankName').val() + "', TransType:'" + "BankCash" + "'," +
                "SectorID: '" + $('#ddlSector').val() + "', MemoDate:'" + $('#txtChequeDate').val() + "'  }"; //alert(S);
         
            $.ajax({
                url: '/Accounts_Form/BillExtract/BankCash',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode,
								  CurrBal: item.CurrBal
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnBankName').val(i.item.AccountCode);
			$('#hdnCurrBal').val(i.item.CurrBal); 
        },
        minLength: 0
    }).click(function () {
	        if ($('#txtChequeDate').val() == "") {
            $('#txtChequeDate').focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#txtBankName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBankName").val('');
            $("#hdnBankName").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBankName").val('');
            $("#hdnBankName").val('');
        }
    });
}
function Bind_BankChequeNo(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
   

    if (TabID != "") {
        var E = "{TabId: '" + TabID + "', TransType: '" + "BankChequeNo" + "'}";

        $.ajax({
            url: '/Accounts_Form/BillExtract/AppliedDetail',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var t = data.Data; 

                if (t.length > 0) {
                    $('#ddlChequeBankCash').val(t[0].BankCash);
                    $('#txtChequeNo').val(t[0].ChequeNo);
                    $('#txtChequeDate').val(t[0].ChequeDate);
                    $('#txtBankName').val(t[0].AccountDescription);
                    $('#hdnBankName').val(t[0].AccountCode);
                    $('#ddlInstType').val(t[0].InstrumentType);

                    var bankcash = t[0].BankCash;
                 
                    if (bankcash == "B") {
                        $('.cls_bankcash').css('display', '');
                    }
                    else {
                        $('.cls_bankcash').css('display', 'none');
                        $('#txtChequeNo').val('');
                        $('#txtChequeDate').val('');
                        $('#txtBankName').val('');
                        $('#hdnBankName').val('');
                    }
                }
            }
        });
    }
}
function BankCash()
{
    var bankcash = $('#ddlChequeBankCash').val();
    if (bankcash == "B")
    {
        $('.cls_bankcash').css('display', '');
        $('#txtChequeNo').val('');
        //$('#txtChequeDate').val('');
        $('#txtBankName').val('');
        $('#hdnBankName').val('');
    }
    else
    {
        $('.cls_bankcash').css('display', 'none');
        $('#txtChequeNo').val('');
        //$('#txtChequeDate').val('');
        $('#txtBankName').val('');
        $('#hdnBankName').val('');
    }
}
function SelectCheque_Save(ID)
{
    var TabID = $(ID).closest('tr').find('.TabId').text();
    var JobID = $(ID).closest('tr').find('.cls_JobWork').val();
    var SubCode = $(ID).closest('tr').find('.SubCode').text();
    var BankCash = $(ID).closest('tr').find('.BankCash').text();

    var bankCode = $('#hdnBankName').val();
    var InstType = $('#ddlInstType').val();
    var ChequeDate = $('#txtChequeDate').val();

    if (BankCash == "B" && SubCode == 0)
    {
        if (InstType == "R")
        {
            alert('Sorry ! NEFT/RTGS is not allowed.'); $('#ddlInstType').val(''); return false;
        }
    }

    if (bankCode == "")
    {
        $('#txtBankName').focus(); return false;
    }
    if (InstType == "") {
        $('#ddlInstType').focus(); return false;
    }
    if (ChequeDate == "") {
        $('#txtChequeDate').focus(); return false;
    }

    //return false;
    var E = "{TabID: '" + TabID + "', BankCash: '" + $('#ddlChequeBankCash').val() + "', ChequeNo: '" + $('#txtChequeNo').val() + "', ChequeDate: '" + $('#txtChequeDate').val() + "', AccountCode: '" + $('#hdnBankName').val() + "', InstType: '" + $('#ddlInstType').val() + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/Insert_BankChequeNo',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        async:false,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success") {
                $('#dialog-allotedto_' + JobID).modal('hide');

                var pvchNo = $(ID).closest('tr').find('.PVoucherNo').text(); 
                if (pvchNo == 0) {
                    Approved_Save(ID, "P");
                }
                else {
                    alert('Sorry ! You have already Paid. Voucher No. is Generated.'); return false;
                }
            }
        }
    });
}
function CheckPaymentMode()
{
    var InstType = $('#ddlInstType').val();
    var bankCash = $('#ddlChequeBankCash').val();
    if (bankCash == 'C')
    {
        if (InstType == "R" || InstType == "Q") {
            alert('Sorry ! NEFT/RTGS and Cheque is not allowed.'); $('#ddlInstType').val(''); return false;
        }
    }
}

function Add_User(UserID, FirstName)
{
    var i = 0;
    $(".AllotedUserlist").find(".select-check-action").each(function (idx, aid) {
        var Uid = aid.value;
        if (Uid == UserID)
            i = 1;
    });
    if (i == 1)
    {
        alert('Sorry ! this user is already available.'); $("#txtSearchUser").val('');   return false;
    }

    $(".AllotedUserlist").append("<div class='lstuser objection'><label style='width:80%'><input type='checkbox'  class='select-check-action notchk' value='" + UserID + "'/> " + FirstName + " </label>" +
        "<span><select class='clsUserWork' style='width:80px; display:none'><option selected value=''>Select</option></select></span>" +
        "<span style= 'float:right; padding-right:10px;' > <img src='/Content/Images/Delete.png' class='delete' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' /></span ></div > ");

    //$(".AllotedUserlist").append("<div class='lstuser'><label style='width:80%'><input type='checkbox'  class='select-check-action' value='" + UserID + "'/> " + FirstName + " </label><span style='float:right; padding-right:10px;'><img src='/Content/Images/Delete.png' class='delete' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;'/></span></div>");
    $("#txtSearchUser").val('');

}
function Search()
{
    var E = "{FromDate: '" + $('#txtFromDate').val() + "', ToDate: '" + $('#txtToDate').val() + "', SectorID: '" + $('#ddlSector').val() + "', Desc: '" + $('#txtSearchMemoNo').val() + "', PartyName: '" + $('#txtSearchPartyName').val() + "', TransType: '" + "Select" + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";

    $.ajax({
        url: '/Accounts_Form/BillExtract/Search',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var table = $('#tbl');
            var thead = table.find('thead');
            var tbody = table.find('tbody');

            tbody.find('.myData').empty();
            var html = "", isdisable = "";
            if (t.length > 0) {
                var k = 0;
                for (var i = 0; i < t.length; i++) {
                    k = k + 1;

                    if (t[i].Condition == "Disable") {
                        isdisable = 'disabled=true'; rowColor = "#F9E79F";
                    }
                    else {
                        isdisable = ""; rowColor = "";
                    }
                    if (t[i].PVoucherNo > 0) {
                        rowColor = "#83d8a7";
                    }

                    //for Abstract
                    var isAbstract = "", isDetail = "", isAck = ""; isEdit = "";
                    if (t[i].BillStatus == 1) {
                        isAbstract = ''; isDetail = 'none'; isAck = 'none';
                    }
                    else if (t[i].BillStatus == 2 && t[i].AckNo == null) {
                        isAbstract = ''; isDetail = 'none'; isAck = 'none'; isEdit = 'none';
                    }
                    else if (t[i].BillStatus == 2 && t[i].AckNo != null) {
                        isAbstract = 'none'; isDetail = ''; isAck = ''; isEdit = 'none';
                    }
                    else {
                        isAbstract = 'none'; isDetail = ''; isAck = ''; isEdit = 'none';
                    }


                    html += "<tr class='myData' style='font-size:11px; background-color:" + rowColor + "'>"
                        + "<td style='display:none;' class='TabId'>" + t[i].TabId + "</td>"
                        + "<td  class='MemoNo' style='text-align:left;'>" + t[i].MemoNo + "</td>"
                        + "<td  class='MemoDt' style='text-align:center;'>" + t[i].MemoDt + "</td>"
                        //+ "<td  class='FcNo' style='text-align:center;'>" + t[i].FcNo + "</td>"
                        + "<td  class='PartyName'>" + (t[i].PartyName == null ? '' : t[i].PartyName) + "</td>"
                        + "<td style='text-align:right;' class='GrossAmt' onClick='UpdateDetail(this);'>" + t[i].GrossAmt + "</td>"
                        + "<td style='display:none;' class='JVoucherNo'>" + t[i].JVoucherNo + "</td>"
                        + "<td style='display:none;' class='RVoucherNo'>" + t[i].RVoucherNo + "</td>"
                        + "<td style='display:none;' class='PVoucherNo'>" + t[i].PVoucherNo + "</td>"
                        + "<td style='display:none;' class='VoucherType'>" + t[i].VoucherType + "</td>"

                        + "<td style='display:none;' class='BillStatus'>" + t[i].BillStatus + "</td>"
                        + "<td class='BillStatusDesc' style='text-align:center;'>" + t[i].BillStatusDesc + "</td>"

                        + "<td style='text-align:center; color:blue; cursor:pointer;' onClick='ViewAppliedDetail(this);'>Click..</td>"

                        + "<td  class='JobWork' style='text-align:center; width:10%'>"
                        + "<select style='width:60%' id='JobWork_" + t[i].TabId + "' class='cls_JobWork'" + isdisable + " onchange='Open(this)'>"
                        + "<option value='' >Select</option>"
                        + "</select></td>"

                        + "<td>"
                        + "<select onchange='ActiononRecord(this)'><option value='' >Select</option>"
                        + "<option value='1' style='display:" + isEdit + "' > Edit</option >"
                        + "<option value='2' style='display:" + (t[i].DeleteFlag == "Y" ? "" : "none") + "' > Delete</option >"
                        + "<option value='3' style='display:" + (t[i].DeleteFlag == "Y" ? "" : "none") + "' > Resume</option >"
                        + "</td > "

                        + "<td style='text-align:center; color:blue; cursor:pointer;'>"
                        + "<span style='font-size:11px;'> <select onchange='OpenReport(this)'><option value=''>Select</option>"
                        + "<option value='1' style='display:" + isAbstract + "' > Abstract</option >"
                        + "<option value='2' style='display:" + isAck + "' > Ack</option >"
                        + "<option value='3' style='display:" + isDetail + "' > Detail</option >"
                        + "<option value='4' attr-vchno=" + t[i].JVoucherNo + " class='JVoucherNos'  style='display:" + (t[i].JVoucherNo == 0 ? "none" : "") + "' > Journal</option >"
                        + "<option value='5' attr-vchno=" + t[i].RVoucherNo + " class='RVoucherNos'  style='display:" + (t[i].RVoucherNo == 0 ? "none" : "") + "' > Reverse Journal</option >"
                        + "<option value='6' attr-vchno=" + t[i].PVoucherNo + " class='PVoucherNos'  style='display:" + (t[i].PVoucherNo == 0 ? "none" : "") + "' > Payment</option >"
                        + "</select ></span>"
                        + "</td>"

                        + "<td style='display:none;' class='SubCode'>" + t[i].SubCode + "</td>"
                        + "<td style='display:none;' class='BankCash'>" + t[i].BankCash + "</td>"

                    html + "</tr>";

                    Bind_AppliedTo(t[i].TabId);
                    JobWork(t[i].BillStatus, t[i].TabId)
                }
                $('#tbl tbody').append(html);
                //$('.clsEdit').removeAttr('onClick');

            }
        }
    });
}
function Autocomplete_MemoNo()
{
    $('#txtSearchMemoNo').autocomplete({
        source: function (request, response) {

            Search();
        },
        select: function (e, i) {
            //$('#hdnBankName').val(i.item.AccountCode);
        },
        minLength: 0
    });
}
function Autocomplete_PartyName() {
    $('#txtSearchPartyName').autocomplete({
        source: function (request, response) {

            Search();
        },
        select: function (e, i) {
            //$('#hdnBankName').val(i.item.AccountCode);
        },
        minLength: 0
    });
}

function HideBack()
{
    $('.clsBack').css('display', 'none');
}
function PendingSector()
{
    $('.clsBack').css('display','');
    var E = "{UserID: '" + "" + "', SectorID: '" + "" + "', TransType: '" + "PendingJob" + "'}";
    $.ajax({
        url: '/Accounts_Form/BillExtract/PendingJob',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var table1 = t['Table']; //alert(JSON.stringify(table1));
            var table2 = t['Table1'];
            var table3 = t['Table2'];

            var table = $("#tbl_Pending");
            table.find('thead').empty();
            table.find('tbody').empty();
            table.find('tfoot').empty();

         
            if (table1.length > 0) {

                var hide_Columns = [0];
                Populate_Headers(table1, hide_Columns);
                var Total = 0;
                for (var i = 0; i < table1.length; i++) {
                    Total = parseInt(Total) + parseInt(table1[i].SectorPending);
                    $('#tbl_Pending tbody').append("<tr style='cursor:pointer;' onclick='Pending_User(\"" + table1[i].sectorId + "\", \"" + table1[i].SectorName +"\");'><td style='display:none;'>" +
                        table1[i].sectorId + "</td><td style='text-align:left;' class='VoucherNumber'>" +
                        table1[i].SectorName + "</td> <td style='text-align:center;' class='AccountDescription'>" +
                        table1[i].SectorPending + "</td></tr>");
                }
                $('#tbl_Pending tfoot').append("<tr><td style='text-align:right; font-weight:bold'>" +
                    "Total" + "</td><td style='text-align:center;font-weight:bold' >" +
                    Total + "</td></tr>");
            }
        }
    });

}
function Pending_User(SectorID, SectorName)
{
    var E = "{UserID: '" + "" + "', SectorID: '" + SectorID + "', TransType: '" + "PendingJob" + "'}"; //alert(E); return false;
    $.ajax({
        url: '/Accounts_Form/BillExtract/PendingJob',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var table1 = t['Table']; //alert(JSON.stringify(table1));
            var table2 = t['Table1'];
            var table3 = t['Table2'];

            ArrayOpenListID.push($('#myDiv').html());

            var table = $("#tbl_Pending");
            table.find('thead').empty();
            table.find('tbody').empty();
            table.find('tfoot').empty();

            if (table2.length > 0) {

                var hide_Columns = [0,1];
                Populate_Headers(table2, hide_Columns);
                var Total = 0;
                for (var i = 0; i < table2.length; i++) {
                    Total = parseInt(Total) + parseInt(table2[i].SectorPending);
                    $('#tbl_Pending tbody').append("<tr style='cursor:pointer;' onclick='Pending_Memo(\"" + table2[i].sectorId + "\", \"" + table2[i].UserID + "\", \"" + table2[i].FirstName + "\");'><td style='display:none;'>" +
                        table2[i].sectorId + "</td><td style='text-align:left; display:none;' class='VoucherNumber'>" +
                        table2[i].UserID + "</td> <td class='AccountDescription'>" +
                        table2[i].FirstName + "</td> <td style='text-align:center;' class='AccountDescription'>" +
                        table2[i].SectorPending + "</td></tr>");
                }
                $('#tbl_Pending tfoot').append("<tr><td style='text-align:right; font-weight:bold'>" +
                    "Total" + "</td><td style='text-align:center;font-weight:bold' >" +
                    Total + "</td></tr>");
           }
        }
    });
}
function Pending_Memo(SectorID, UserID, FirstName) {
    var E = "{UserID: '" + UserID + "', SectorID: '" + SectorID + "', TransType: '" + "PendingJob" + "'}";
    $.ajax({
        url: '/Accounts_Form/BillExtract/PendingJob',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var table1 = t['Table']; //alert(JSON.stringify(table1));
            var table2 = t['Table1'];
            var table3 = t['Table2'];

            ArrayOpenListID.push($('#myDiv').html());

            var table = $("#tbl_Pending");
            table.find('thead').empty();
            table.find('tbody').empty();
            table.find('tfoot').empty();

            if (table3.length > 0) {

                var hide_Columns = [];
                Populate_Headers(table3, hide_Columns);

                for (var i = 0; i < table3.length; i++) {
                    $('#tbl_Pending tbody').append("<tr style='cursor:pointer;' );'><td>" +
                        table3[i].MemoNo + "</td><td>" +
                        table3[i].RefNo + "</td></tr > ");
                }

            }
        }
    });
}
function Populate_Headers(data, hide_Columns) {

   
    var html = "";
    html += "<tr>";

    var table = $("#tbl_Pending");

    //populate header
    for (var j = 0; j < (Object.keys(data[0])).length; j++) {
        var Columns = Object.keys(data[0])[j];

        var res = Check_Array(j, hide_Columns);

        var desc = "";
        if (Object.keys(data[0])[j] == "SectorPending")
            desc = "Pending Job";
        else if (Object.keys(data[0])[j] == "SectorName")
            desc = "Sector Name";
        else
            desc = Object.keys(data[0])[j];

        if (res == true) {
            html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "' style='display:none'>" + desc + "</th>";
        }
        else {
            html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "'>" + desc + "</th>";
        }
    }
    html += "</tr>";
    table.find('thead').append(html);
}
function Check_Array(value, hide_list) {

    if (jQuery.inArray(value, hide_list) != '-1') {
        return true;
    } else {
        return false;
    }
}
function Back() {

    if (ArrayOpenListID.length > 0) {
        var t = ArrayOpenListID.pop();
        if (t != "") {
            $('#myDiv').html(t);
        }
    }
}

function populateDDllFinYear() {
    var sectorid = localStorage.getItem("SectorID");
    var S = "{SectorID:'" + sectorid + "'}";
    $.ajax({
        url: '/Accounts_Form/AccountMaster/FinYear',
        type: 'POST',
        data: S,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = response.Data;

            if (data.length > 0) {
                var ctlr_ddlGlobalFinYear = $('#ddlBillAbstractFinYear');
                ctlr_ddlGlobalFinYear.empty();
                $(data).each(function (index, item) {
                    ctlr_ddlGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                });
            }
        }
    });
}


function AccountOpeningDate() {
    var V = "{MenuID : '" + 84 + "', FinYear: '" + localStorage.getItem("FinYear") + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        url: '/Accounts_Form/VoucherMaster/AccountOpeningDate_MenuWise',
        data: V,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (D) {
            var t = D.Data;
            if (t.length > 0 && D.Status == 200) {

                Acc_OpenDate = t;
                //$('.clsOpeningDate').html(Acc_OpenDate);
            }
        }
    });
}
function CheckDate() {

    var menudateCheck = Acc_OpenDate;  //alert(menudateCheck);
    var dateCheck = $("#txtMemoDate").val();
    var FinYear = $("#ddlGlobalFinYear").val();
    var finyear = $("#ddlGlobalFinYear option:selected").text();
    var dateTo = ''; var dateFrom = '';

        if (finyear != "" || (dateCheck == "" || dateCheck != "")) {
            var sFinYear = finyear.split('-');
            var s = sFinYear[0];
            var t = sFinYear[1];

            var dateFrom = "01/04/" + s; 
            var dateTo = "31/03/" + t;

           // dateFrom = "01/04/" + s;
           // dateTo = "01/04/" + t;



            //------------------------------------------------------------TODAY DATE--------------------------------------
            today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy;
            //-------------------------------------------------------------------------------------------------------------
            var d1 = dateFrom.split("/");
            var d2 = dateTo.split("/");
            var c = dateCheck.split("/");
            var m = menudateCheck.split("/");

            var from = new Date(d1[2], parseInt(d1[1]) - 1, d1[0]); //alert(from);  
            var to = new Date(d2[2], parseInt(d2[1]) - 1, d2[0]); //alert(to);
            var check = new Date(c[2], parseInt(c[1]) - 1, c[0]); //alert(check);
            var menu = new Date(m[2], parseInt(m[1]) - 1, m[0]); //alert(menu);

            //------------------------------------------------------- MENU DATE TO PLUS ONE DATE --------------------------------
            var ss = new Date(menu.getFullYear(), menu.getMonth(), menu.getDate() + 1);
            var ddd = ss.getDate();
            var mmm = ss.getMonth() + 1; //January is 0!
            var yyyyy = ss.getFullYear();
            if (ddd < 10) {
                ddd = '0' + ddd;
            }
            if (mmm < 10) {
                mmm = '0' + mmm;
            }
            ss = ddd + '/' + mmm + '/' + yyyyy;
            //---------------------------------------------------------------------------------------------------------------------
            //checking menu date is, in selected finyear
             //alert(menu); alert(from); alert(to); 
            if (check >= from && check <= to) {
                if (check >= menu && check <= to)
                {
                    //correct
                    //alert(2);
                    return true;
                }
                else {
                    //wrong
                    //alert(3);
                    toastr.error('Sorry ! Enter Memo Date should be between  ' + menudateCheck + '-' + dateTo + '.', 'Warning'); $("#txtMemoDate").val(ss); $("#txtMemoDate").focus(); return false;
                }
            }
            else {
                //alert(1);
                toastr.error('Sorry ! Enter Memo Date should be between  ' + dateFrom + '-' + dateTo + '.', 'Warning'); $("#txtMemoDate").val(ss); $("#txtMemoDate").focus(); return false;
            }
          
        }

}
function CheckClosingStatus() {

    var a = $.trim($("#txtMemoDate").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 84 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Save();
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}
function CheckClosingStatus_OnApproved() {

    var a = $.trim($("#txtMemoDate").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 84 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Save();
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}


