﻿var frmElement = ""; var pathString = "";

$(document).ready(function () {
    frmElement = $('#frmElement');
    FormValidation();

    //Set_AsofDate
    Set_AsofDate();
    Load_Item();
    $("#flPic").on("change", readFile_Photo);



    //=========================================== ITEM PARENT =====================================================
    $('#txtItemParent').autocomplete({
        source: function (request, response) {

            var GLS = "L";

            var S = "{TransactionType:'" + "Vendor_ParentItem" + "' , Desc:'" + $('#txtItemParent').val() + "', MND:'" + GLS + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnItemParent').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtItemParent').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtItemParent").val('');
            $("#hdnItemParent").val('');
        }
        if (iKeyCode == 46) {
            $("#txtItemParent").val('');
            $("#hdnItemParent").val('');
        }
    });

    //=========================================== ITEM PURCHASE =====================================================
    $('#txtItemPurchase').autocomplete({
        source: function (request, response) {

            var GLS = "L";

            var S = "{Desc:'" + $('#txtItemPurchase').val() + "', MND:'" + "L" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnItemPurchase').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtItemPurchase').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtItemPurchase").val('');
            $("#hdnItemPurchase").val('');
        }
        if (iKeyCode == 46) {
            $("#txtItemPurchase").val('');
            $("#hdnItemPurchase").val('');
        }
    });

    //=========================================== ITEM SALE =====================================================
    $('#txtItemSale').autocomplete({
        source: function (request, response) {

            var GLS = "L";

            var S = "{Desc:'" + $('#txtItemSale').val() + "', MND:'" + GLS + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnItemSale').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtItemSale').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtItemSale").val('');
            $("#hdnItemSale").val('');
        }
        if (iKeyCode == 46) {
            $("#txtItemSale").val('');
            $("#hdnItemSale").val('');
        }
    });

    //=========================================== ITEM PREF =====================================================
    $('#txtItemPref').autocomplete({
        source: function (request, response) {

            var GLS = "S";

            var S = "{Desc:'" + $('#txtItemPref').val() + "', MND:'" + GLS + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnItemPref').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtItemPref').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtItemPref").val('');
            $("#hdnItemPref").val('');
        }
        if (iKeyCode == 46) {
            $("#txtItemPref").val('');
            $("#hdnItemPref").val('');
        }
    });

    $('#btnItemSave').click(SaveRecord);
    $('#btnCategoryAdd').click(Add_Category);

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});

//Set_AsofDate
function FormValidation() {
    $('#frmElement').validate({
        rules: {
            txtItemName: "required"
        }
    });
}
function SaveRecord() {
   
    if (!frmElement.valid()) {
        return false;
    }

    var master = {};
    master = {
        AccountCode: $('#hdnItemID').val(),
        AccountTypeID: $('#hdnServiceType').val(),
        AccountDescription: $('#txtItemName').val(),
        OldAccountHead: $('#txtSKU').val(),
        ParentAccountCode: $('#hdnItemParent').val(),
        HsnCode: $('#txtHSN').val(),
        UnitId: $('#ddlItemUnit').val(),
        TaxTypeId: $('#ddlTaxType').val(),

        OpBalance: $('#txtAmount').val(),
        OpbalQty: $('#txtQty').val(),
        OpbalAsOn: $('#txtAofDate').val(),
        LowStkAlert: $('#txtLowStock').val(),
        PurAccCode: $('#hdnItemPurchase').val(),
        SaleAccCode: $('#hdnItemSale').val(),
        PrefVendorCode: $('#hdnItemPref').val(),
        DocFilePath: pathString,
        Sectorid: $('#ddlSector').val()
    }

    var grdLen = $('#tbl tr.myData').length; var ArrList = [];
    if (grdLen > 0) {
        $('#tbl tr.myData').each(function () {
            var ItemCatId = $(this).find('.clsAccCode').text();
            ArrList.push({ 'ItemCatId': ItemCatId });
        });
    }
    var CatDesc = JSON.stringify(ArrList);
    var E = "{item: " + JSON.stringify(master) + ", CatDesc: " + CatDesc + "}";

    $.ajax({
        url: '/Accounts_Form/Item/InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 
            if(data.Status == 200 && t != "")
            {
                Upload_Item('flPic', t);

                swal({
                    title: "Success",
                    text: 'Item Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        location.reload();
                        //Load_Item();
                       
                        return false;
                    }
                });
            }
        }
    });
}
function Set_AsofDate() {
    $('#txtAsofDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

var readFile_Photo = function () {
    var a = this.files[0].name;
    if (a != "") {
        var validExtensions = ['jpg', 'gif']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type");
            $("#fileToUpload").val('');
            document.getElementById("canPhoto").src = '';
            return false;
        }
    }
    if (this.files && this.files[0]) {

        var FR = new FileReader();

        FR.addEventListener("load", function (e) {
            document.getElementById("canPhoto").src = e.target.result;
            $("#txtPhotoStream").val(FR.result);
        });

        FR.readAsDataURL(this.files[0]);
    }

}
function ShowPreview(input) {
    if (input.files && input.files[0]) {
        path = $('#flPic').val().substring(12);
        pathString = '/UploadItemImage/Item/' + path;

        //Upload_Item('flPic');
        //addPictureTable();
    }
}
function Upload_Item(flPic, AccountCode) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#flPic').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file, AccountCode + fileNameExt);
    }
    
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/UploadPicture',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
        }
    });
}

function GetServiceType(serviceType, ID)
{
    $('#lblServiceType').text(serviceType);
    $('#hdnServiceType').val(ID);

    if (ID == 2 || ID == 3) {
        $('#lblHSNSSC').text('SSC Code'); $('#txtHSN').attr('placeholder', 'Enter a valid SSC code');
        $('.clsNonInvServices').css('display', 'none'); $('.blank').val('');
    }
    else
    {
        $('#lblHSNSSC').text('HSN Code'); $('#txtHSN').attr('placeholder', 'Enter a valid HSN code');
        $('.clsNonInvServices').css('display', '');
    }
   
}
function Add_Category()
{
    if ($('#ddlItemCat').val() == "") { $('#ddlItemCat').focus(); return false; }

    var chkAccCode = $("#ddlItemCat").val();

    var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Category is Already Available.'); $("#ddlItemCat").val(''); $("#hdnCategoryAdd").val(''); $('#ddlItemCat').focus(); return false;
    }

    var html = ""

    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#ddlItemCat").val() + "' >" + $("#ddlItemCat").val() + "</td>"
        + "<td>" + $("#ddlItemCat option:selected").text() + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteInstDetail(this);' /></td>"
    html + "</tr>";

    $parentTR.after(html);

    $("#ddlItemCat").val('');
}
function DeleteInstDetail(ID)
{
    $(ID).closest('tr').remove();
}
function Load_Item()
{
    var E = "{AccountCode: '" + "" + "'}";
    $('#page-wrapper').toggleClass('sk-loading');
    $.ajax({
        type: "POST",
        url:'/Accounts_Form/Item/Load_Item',
        contentType: "application/json; charset=utf-8",
        data: null,
        dataType: "json",
        success: function (data, status) {
          
            var t = data.Data; //alert(JSON.stringify(t));
          
            var columnData = [{ "mDataProp": "AccountCode" },
                       { "mDataProp": "AccountDescription" },
                       { "mDataProp": "OldAccountHead" },
                       { "mDataProp": "HsnCode" },
                       { "mDataProp": "AccountType" },
                       {
                          "mDataProp": "Edit",
                          "render": function (data, type, row) {
                              return '<a href="javascript:void(0)" title="Edit" style="text-align:center" class="btn btn-azure btn-xs" onclick="Load_ItemDetail(\'' + row.AccountCode + '\')" ><span class="label label-info pull-right">Edit</span>  </a>';
                          }
                      }];

            var columnDataHide = [0];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                     {
                         "targets": columnDataHide,
                         "visible": false,
                         "searchable": false
                     },
                    {
                        "targets": 5,
                        "className": "text-center",
                        "width": "10%"
                    }],
                'iDisplayLength': 10,
                destroy:true
            });

            $('#page-wrapper').removeClass('sk-loading');
            
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });

   
}
function Load_ItemDetail(AccountCode)
{
    var E = "{AccountCode: '" + AccountCode + "'}";

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_ItemDetail',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data) {

            var master =data.Data;// alert(JSON.stringify(master));
            var detail = data.Data.CatDetail;// alert(JSON.stringify(detail));

            $('#txtItemName').val(master.AccountDescription); $('#hdnItemID').val(master.AccountCode);
            $('#lblServiceType').text(master.AccountType);
            GetServiceType(master.AccountType, master.AccountTypeID);

            $('#hdnServiceType').val(master.AccountTypeID);
            $('#txtItemParent').val(master.PAccountDescription); $('#hdnItemParent').val(master.ParentAccountCode);

            document.getElementById("canPhoto").src = '';
            if (master.DocFilePath != null) {
                $('#canPhoto').attr('src', master.DocFilePath); 
            }
            else {
                document.getElementById("canPhoto").src = '/Content/Images/empty.png';
            }

            $('#txtSKU').val(master.OldAccountHead); $('#txtHSN').val(master.HsnCode);

            $('#ddlItemUnit').val(master.UnitId);

            $('#tbl tr.myData').remove();
            if (detail.length > 0) {
                var html = ""
                var $this = $('#tbl .test_0');
                $parentTR = $this.closest('tr');

                for (var i = 0; i < detail.length; i++) {
                    html += "<tr class='myData' >"
                        + "<td style='display:none;' class='clsAccCode' chk-data='" + detail[i].CategoryID + "' >" + detail[i].CategoryID + "</td>"
                        + "<td>" + detail[i].CatDesc + "</td>"
                        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteInstDetail(this);' /></td>"
                    html + "</tr>";
                }
                $parentTR.after(html);
            }
           
            $('#txtQty').val(master.OpbalQty); $('#txtAmount').val(master.OpBalance); $('#txtAofDate').val(master.OpbalAsOn); $('#txtLowStock').val(master.LowStkAlert);
            $('#ddlTaxType').val(master.TaxTypeId);

            $('#txtItemPurchase').val(master.PurAccountDescription); $('#hdnItemPurchase').val(master.PurAccCode);
            $('#txtItemSale').val(master.SaleAccountDescription); $('#hdnItemSale').val(master.SaleAccCode);
            $('#txtItemPref').val(master.PrefAccountDescription); $('#hdnItemPref').val(master.PrefVendorCode);
           
            $('#wrapperss').toggleClass('toggled');
        }
    });
}
function Blank_Field()
{
    $('.blankField').val(''); $('.blank').val(''); $('#tbl tbody tr.myData').remove();

    if ($('img.canPhoto', this).attr('src') != '') {
        document.getElementById("canPhoto").src = '/Content/Images/empty.png';
    }
    
}
function img_item_delete()
{
    document.getElementById("canPhoto").src = '/Content/Images/empty.png';
    $("#flPic").val('');
    
    var tableName = "", ID = "", urls = "";
    transactionType = "Item";
    ID = $('#hdnItemID').val();
    urls = "~/UploadItemImage/Item/";

    if (ID != "") {
        var E = "{transactionType: '" + transactionType + "', ID: '" + ID + "', urls: '" + urls + "'}";
        $.ajax({
            url: '/Accounts_Form/Item/Delete_Picture',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

            }
        });
    }
}

function LoadOther(ID, t) {

        var i = $("li").hasClass("active");
        if (i == true) {
            $("li").removeClass("active");
            $(t).closest('li').addClass('active');
        }

    var E = "{ID: '" + ID + "'}";
    $('#page-wrapper').toggleClass('sk-loading');
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/CallingForm',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "html",
        success: function (data) {
            $('.form').html(data);
            $('#page-wrapper').removeClass('sk-loading');
        }
    });
}

function ChangeJavacript(pageID)
{
 
    if (pageID == 1)
        $('.item_Javascript').attr('src', '~/Scripts/AccJs/Item.js');
    if (pageID == 2) {
        $('.item_Javascript').attr('src', '~/Scripts/AccJs/Expenses.js');
    }
}
