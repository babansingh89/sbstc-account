﻿


$(document).ready(function () {

    SetFrom_Date();

    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

//    BANK AUTOCOMPLETE
    $('#txtBankName').autocomplete({
    source: function (request, response) {

        var S = "{TransactionType:'" + "Vendor_BankAccountNo" + "' ,Desc:'" + $('#txtBankName').val() + "', MND:'" + "B" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
        $.ajax({
            url: '/Accounts_Form/BankStatement/Auto_Bank',
            type: 'POST',
            data: S,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (serverResponse) {
                var AutoComplete = [];
                if ((serverResponse.Data).length > 0) {

                    $.each(serverResponse.Data, function (index, item) {
                        AutoComplete.push({
                            label: item.AccountDescription,
                            AccountCode: item.AccountCode,
                            AccountNumber: item.AccountNumber
                        });
                    });

                    response(AutoComplete);
                }
            }
        });
    },
    select: function (e, i) {
        $('#hdnBankName').val(i.item.AccountCode);
        var remrks = "Please credit the following A/C with the amount shown against the name of the following details, after debiting the SBSTC A/C No " + i.item.AccountNumber + " maintained at your Branch bearing.";
        if (i.item.AccountNumber != "" && i.item.AccountNumber != null) {
            
            $('#txtRemarks').val(remrks);
        }
        else
        {
            $('#txtRemarks').val(remrks);
        }

        BindGrid(i.item.AccountCode);
    },
    minLength: 0
}).click(function () {
    $(this).autocomplete('search', ($(this).val()));
});
    $('#txtBankName').keydown(function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode == 8) {
        //$("#txtBankName").val('');
        $("#hdnBankName").val('');
    }
    if (iKeyCode == 46) {
        //$("#txtBankName").val('');
        $("#hdnBankName").val('');
    }
    });

    //MEMO NO. AUTOCOMPLETE
    $('#txtSearchMemoNo').autocomplete({
        source: function (request, response) {

            var S = "{TransType: '" + "MemoNo" + "', AccountCode: '" + "" + "', Desc: '" + $('#txtSearchMemoNo').val() + "'}";
            $.ajax({
                url: '/Accounts_Form/BankStatement/BindVendor',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = []; 
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.MemoNo,
                                StatementID: item.StatementID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnSearchMemoNo').val(i.item.StatementID);
            //Print(i.item.StatementID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchMemoNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            //$("#txtSearchMemoNo").val('');
            $("#hdnSearchMemoNo").val('');
        }
        if (iKeyCode == 46) {
            //$("#txtSearchMemoNo").val('');
            $("#hdnSearchMemoNo").val('');
        }
    });
});

function SetFrom_Date() {
    $('#txtMemoDate, #txtChequeDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

function BindGrid(AccountCode)
{
    if (AccountCode != "") {
        var E = "{TransType: '" + "Load" + "', AccountCode: '" + AccountCode + "', Desc: '" + "" + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BankStatement/BindVendor',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            async: false,
            success: function (data, status) {
                var t = data.Data; 

                $('#datatable tbody tr').remove();
                if (data.Status == 200 && t.length > 0) {

                    var columnData = [{ "mDataProp": "VoucherNumber" },
                        { "mDataProp": "RefVoucherSlNo" },
                        { "mDataProp": "ReferenceDate" },
                        { "mDataProp": "AccountCode" },
                        { "mDataProp": "AccountDescription" },
                        { "mDataProp": "Amount" },
                        { "mDataProp": "IFSCCode" },
                        { "mDataProp": "BankName" },
                    {
                        "mDataProp": "Edit",
                        "render": function (data, type, row) {
                            //return '<a href="javascript:void(0)" title="Edit" style="text-align:center" class="btn btn-azure btn-xs" onclick="Load_ItemDetail(\'' + row.VoucherNumber + '\')" ><span class="label label-info pull-right">Edit</span>  </a>';
                            return ' <input type="checkbox" class="chk" value="' + row.VoucherNumber  +'" />';

                        }
                    }];

                    var columnDataHide = [0,3];

                    var oTable = $('#datatable').DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            { extend: 'copy' },
                            { extend: 'csv' },
                            { extend: 'excel', title: 'MyExcel' },
                            { extend: 'pdf', title: 'MyPDF', width: '100%' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');

                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],

                        "oLanguage": {
                            "sSearch": "Search all columns:"
                        },
                        "aaData": t,

                        "aoColumns": columnData,
                        "aoColumnDefs": [
                            {
                                "targets": columnDataHide,
                                "visible": false,
                                "searchable": false
                            },
                            {
                                "targets": [8],
                                "className": "text-center",
                                "width": "10%"
                            },
                            {
                                "targets": [0],
                                "className": "vchno"
                            },
                            {
                                "targets": [2],
                                "className": "text-center vchdate",
                                "width": "11%"
                            },
                            {
                                "targets": [3],
                                "className": "acccode"
                            },
                            {
                                "targets": [4],
                                "className": "vendor"
                            },
                            {
                                "targets": [5],
                                "className": "text-right amount"
                            },
                            {
                                "targets": [6],
                                "className": "ifsccode"
                            },
                            {
                                "targets": [7],
                                "className": "bankname"
                            }
                            ],
                        'iDisplayLength': 10,
                        destroy: true,
                    });
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                AddErrorAlert("error", "request failed");
            }
            
        });
    }
}

function Refresh()
{
    $("#txtBankName").val(''); $("#hdnBankName").val('');
    $("#ddlSignature").val(''); $("#txtMemoNo").val('');
    $("#txtMemoDate").val(''); $("#txtChequeNo").val('');
    $("#txtChequeDate").val(''); $("#txtRemarks").val('');
    $('#datatable tbody tr').remove();
}

function Preview()
{
    var grdLen = $('#datatable tbody tr').length; var ArrList = [];
    if (grdLen > 0) {

        var html = "", html = "";
       

        var oTable = $('#datatable').dataTable();
        var rowcollection = oTable.$(".chk:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val(); 

            var row = $(this).closest('tr');
            var date = $(this).closest('tr').find('.vchdate').text(); 
            var vendor = $(this).closest('tr').find('.vendor').text(); 
            var amount = $(this).closest('tr').find('.amount').text(); 
            var vchno = oTable.dataTable().fnGetData(row).VoucherNumber;

            html += "<tr>";
            html += "<td>" + vendor + "</td>";
            html += "<td style='text-align:center;'>" + date + "</td>";
            html += "<td style='text-align:right;'>" + amount + "</td>";
            html += "</tr>";
        });
      
        var table = $('#tbl tbody');
        table.empty();
        table.append(html);
        $("#dialog").modal('show');
    }
}

function Save()
{
    if ($("#hdnBankName").val() == "") {
        toastr.error('Please, Select Bank Name !!', 'Warning'); $("#txtBankName").focus(); return false;
    }
    if ($("#ddlSignature").val() == "") {
        toastr.error('Please, Select Signature of !!', 'Warning'); $("#ddlSignature").focus(); return false;
    }
    //if ($("#txtMemoNo").val() == "") {
    //    toastr.error('Please, Enter Memo No. !!', 'Warning'); $("#txtMemoNo").focus(); return false;
    //}
    if ($("#txtMemoDate").val() == "") {
        toastr.error('Please, Enter Memo Date !!', 'Warning'); $("#txtMemoDate").focus(); return false;
    }
    if ($("#txtChequeNo").val() == "") {
        toastr.error('Please, Enter Cheque No. !!', 'Warning'); $("#txtChequeNo").focus(); return false;
    }
    if ($("#txtChequeDate").val() == "") {
        toastr.error('Please, Enter Cheque Date !!', 'Warning'); $("#txtChequeDate").focus(); return false;
    }
    if ($("#txtRemarks").val() == "") {
        toastr.error('Please, Enter Remarks !!', 'Warning'); $("#txtRemarks").focus(); return false;
    }

    var grdLen = $('#datatable tbody tr').length; 
    if (grdLen > 0) {
            
        var ArrList = [];
        var oTable = $('#datatable').dataTable();
        var rowcollection = oTable.$(".chk:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val();
            ArrList.push({
                'checkbox_value': checkbox_value
            });
        });
    }
    else
    {
        toastr.error('Sorry !! No Vendors available !!', 'Warning'); return false;
    }
  
    if (ArrList.length == 0)
    {
        toastr.error('Please, Select Vendors !!', 'Warning');  return false;
    }

   
    swal({
        title: "Warning",
        text: "Are you sure want to save selected Vendors ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                Confirm_Save();
            }

        });
}
function Confirm_Save()
{
   
    var ArrList = [];
    var oTable = $('#datatable').dataTable();
    var rowcollection = oTable.$(".chk:checked", { "page": "all" });
    rowcollection.each(function (index, elem) {
        var checkbox_value = $(elem).val();

        var row = $(this).closest('tr');
        var vchno = oTable.dataTable().fnGetData(row).VoucherNumber;
        var acccode = oTable.dataTable().fnGetData(row).AccountCode;
        var date = $(this).closest('tr').find('.vchdate').text();
        var amount = $(this).closest('tr').find('.amount').text();
        var vendor = $(this).closest('tr').find('.vendor').text();
        var ifsc = $(this).closest('tr').find('.ifsccode').text();
        var bankname = $(this).closest('tr').find('.bankname').text();

        ArrList.push({
            'VoucherNo': vchno, 'AccountCode': acccode, 'VendorName': vendor, 'VoucherDate': date, 'Amount': amount, 'IFSC': ifsc, 'BankName': bankname 
        });
    });

    var EffectedGL = JSON.stringify(ArrList);
    var obj = "{ BankAccountCode: '" + $('#hdnBankName').val() + "', Signature: '" + $('#ddlSignature').val() + "', MemoNo: '" + "" + "', MemoDate: '" + $('#txtMemoDate').val() + "', ChequeNo: '" + $('#txtChequeNo').val() + "', ChequeDate: '" + $('#txtChequeDate').val() + "', Remarks: '" + $('#txtRemarks').val() + "', EffectedGL: " + EffectedGL + ", SectorID: '" + $('#ddlSector').val() + "'}";

    //alert(EffectedGL);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BankStatement/Insert',
        type: 'POST',
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var t = response.Data; 

            if (t > 0) {
                swal({
                    title: "Success",
                    text: "Bank Statement has been Successfully Inserted.",
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            Print(t)
                        }
                    });
            }
            else
            {
                swal({
                    title: "Warning",
                    text: "Sorry !! This Memo No. is already available.",
                    type: "warning",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            
                        }
                    });
            }
        }
    });
}
function Print(StatementID)
{
    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "BankStatement.rpt",
        FileName: "PayAdvice-" + StatementID
    });

    detail.push({
        StatementID: StatementID
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();

}

function Search()
{
    $('#dialog-print').modal('show');
}
function SearchPrint() {
    Print($("#hdnSearchMemoNo").val()); 
}
function CloseMemoNo()
{
    swal({
        title: "Warning",
        text: "Are you sure want to Close this Memo No. ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                if ($("#hdnSearchMemoNo").val() != "") {
                    var E = "{TransType: '" + "CloseMemo" + "', StatementID: '" + $("#hdnSearchMemoNo").val() + "'}";
                    $.ajax({
                        type: "POST",
                        url: '/Accounts_Form/BankStatement/CloseMemo',
                        contentType: "application/json; charset=utf-8",
                        data: E,
                        dataType: "json",
                        async: false,
                        success: function (data, status) {
                            var t = data.Data;
                            var result = t[0].Result;

                            if (result == "success") {
                                swal({
                                    title: "Success",
                                    text: "Memo No. Closed Successfully !!",
                                    type: "success",
                                    confirmButtonColor: "#AEDEF4",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: true,
                                },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            $("#txtSearchMemoNo").val(''); $("#hdnSearchMemoNo").val('');
                                        }
                                    });
                            }
                        }
                    });
                }
                else {
                    $("#txtSearchMemoNo").focus(); return false;
                }
            }

        });
}
