﻿var pathString = ""; var oldNetAmt = 0; var TranType = "", IncExp="", boolDesc = "";
var Acc_OpenDate = '';
$(document).ready(function () {

    Set_PurchaseOrder_billDate();
    Set_DefaultDate_bill();
Bind_Section();
     AccountOpeningDate();

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //    ITEM AUTOCOMPLETE
    $('#txtExpItemName_bill').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_ParentItem" + "' , Desc:'" + $('#txtExpItemName_bill').val() + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnExpItemCode_bill').val(i.item.AccountCode);
            if (i.item.AccountCode !="")
            Get_Item_Wise_Tax(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtExpItemName_bill').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtExpItemName_bill").val('');
            $("#hdnExpItemCode_bill").val('');
        }
        if (iKeyCode == 46) {
            $("#txtExpItemName_bill").val('');
            $("#hdnExpItemCode_bill").val('');
        }
    });

    //    VENDOR AUTOCOMPLETE
    $('#txtExpVendorName_bill').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_Bill" + "' ,Desc:'" + $('#txtExpVendorName_bill').val() + "', MND:'" + "L" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode,
								 ParentAccountCode: item.ParentAccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdntxtExpVendorID_bill').val(i.item.AccountCode);
			 $('#txtMailingAddress_bill').val(i.item.ParentAccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtExpVendorName_bill').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#hdntxtExpVendorID_bill").val('');
            $("#txtExpVendorName_bill").val('');
        }
        if (iKeyCode == 46) {
            $("#hdntxtExpVendorID_bill").val('');
            $("#txtExpVendorName_bill").val('');
        }
    });

    //    SHIP TO AUTOCOMPLETE
    $('#txtShipTo_bill').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_Bill" + "' ,Desc:'" + $('#txtShipTo_bill').val() + "', MND:'" + "L" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnShipTo_bill').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtShipTo_bill').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtShipTo_bill").val('');
            $("#hdnShipTo_bill").val('');
        }
        if (iKeyCode == 46) {
            $("#txtShipTo_bill").val('');
            $("#hdnShipTo_bill").val('');
        }
    });

    //    PO NO. AUTOCOMPLETE
    //$('#txtBillPONo').autocomplete({
    //    source: function (request, response) {

    //        var S = "{Desc:'" + $('#txtBillPONo').val() + "', VendorCode:'" + $('#hdntxtExpVendorID_bill').val() +"'}"; //alert(S);
    //        $.ajax({
    //            url: '/Accounts_Form/Item/Load_AutoPO',
    //            type: 'POST',
    //            data: S,
    //            dataType: 'json',
    //            contentType: 'application/json; charset=utf-8',
    //            success: function (serverResponse) {
    //                var AutoComplete = [];
    //                if ((serverResponse.Data).length > 0) {

    //                    $.each(serverResponse.Data, function (index, item) {
    //                        AutoComplete.push({
    //                            label: item.PoNo,
    //                            PoId: item.PoId,
    //                            CreditTerms: item.CreditTerms
    //                        });
    //                    });

    //                    response(AutoComplete);
    //                }
    //            }
    //        });
    //    },
    //    select: function (e, i) {
    //        $('#hdnBillPONo').val(i.item.PoId);

    //        if (i.item.CreditTerms != null && i.item.CreditTerms != "")
    //        {
    //                var currDate = $('#txtExpPurOrderDate_bill').val();
    //                var aa = currDate.split('/');
    //                var ldates = new Date(aa[2], aa[1] - 1, aa[0]);

    //                ldates.setDate(ldates.getDate() + parseInt(i.item.CreditTerms));
    //                var futDate = (ldates.getDate() < 10 ? '0' + ldates.getDate() : ldates.getDate()) + "/" + ((parseInt(ldates.getMonth()) + parseInt(1)) < 10 ? '0' + (parseInt(ldates.getMonth()) + parseInt(1)) : (parseInt(ldates.getMonth()) + parseInt(1))) + "/" + ldates.getFullYear();

    //                $('#txtExpPurOrderDueDate').val(futDate);

    //        }
    //        else
    //        {
    //            $('#txtExpPurOrderDueDate').val($('#txtExpPurOrderDate_bill').val());
    //        }

    //    },
    //    minLength: 0
    //}).click(function () {
    //    $(this).autocomplete('search', ($(this).val()));
    //});
    //$('#txtBillPONo').keydown(function (evt) {
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode == 8) {
    //        $("#txtBillPONo").val('');
    //        $("#hdnBillPONo").val('');
    //    }
    //    if (iKeyCode == 46) {
    //        $("#txtBillPONo").val('');
    //        $("#hdnBillPONo").val('');
    //    }
    //});

    //    FC NO. AUTOCOMPLETE
    $('#txtBillFCNo').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "FC" + "' , Desc:'" + $('#txtBillFCNo').val() + "', MND:'" + "B" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountCode,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnBillFCNo').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtBillFCNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBillFCNo").val('');
            $("#hdnBillFCNo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBillFCNo").val('');
            $("#hdnBillFCNo").val('');
        }
    });

    //BANK AUTOCOMPLETE
    $('#txtBillBank').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_Bank" + "' ,Desc:'" + $('#txtBillBank').val() + "', MND:'" + "B" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnBillBank').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtBillBank').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBillBank").val('');
            $("#hdnBillBank").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBillBank").val('');
            $("#hdnBillBank").val('');
        }
    });

    $('#btnItemAdd_bill').click(Add_Item_bill);

    $('#btnExpSave_bill').click(function (evt) {
         //Save_Bill(1);
        CheckClosingStatus_TaxInvoice();
    });
    $('.collapse-link').click(function (evt) {
        Save_Bill(2);
    });

    $(document).on('keydown', '.cls_AccountDesc', function (e) {
        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id');
        $('#' + t).autocomplete({

            source: function (request, response) {
                var V = "{Desc:'" + $('#' + t).val() + "', MND:'" + "L" + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/Account_Description',
                    type: 'POST',
                    data: V,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                var id = i.item.AccountCode;
                $('#' + t).closest('tr').find('.cls_hdnAccountDesc').val(id);
            },
            minLength: 0
        }).click(function () {
            $(t).autocomplete('search', ($(t).val()))
        });
    });

    $(document).on('mouseenter', '#tbl_Expense_bill tbody tr.myData_bill', function (e) {
        Hide_Show_EditDelete(this, 1);
    });
    $(document).on('mouseleave', '#tbl_Expense_bill tbody tr.myData_bill', function (e) {
        Hide_Show_EditDelete(this, 2);
    });
});

function Add_Item_bill() {

 if ($("#ddlExIncomeType").val() == "") {
        toastr.error('Please, Select Either Income or Expense !!', 'Warning')
        $("#ddlExIncomeType").focus(); return false;
    }
    if ($("#hdntxtExpVendorID_bill").val() == "") {
        toastr.error('Please, Select Vendor Name !!', 'Warning')
        $("#txtExpVendorName_bill").focus(); return false;
    }

    if ($("#txtVendorBillNo_bill").val() == "") {
        toastr.error('Please, Enter Party Bill No. !!', 'Warning')
        $("#txtVendorBillNo_bill").focus(); return false;
    }
    if ($("#txtVendorBillDate_bill").val() == "") {
        toastr.error('Please, Enter Party Bill Date !!', 'Warning')
        $("#txtVendorBillDate_bill").focus(); return false;
    }
    if ($("#txtExpPurOrderDate_bill").val() == "") {
        toastr.error('Please, Enter Bill Date !!', 'Warning')
        $("#txtExpPurOrderDate_bill").focus(); return false;
    }
    if ($("#txtExpRemark_bill").val() == "") {
        toastr.error('Please, Enter Remarks !!', 'Warning')
        $("#txtExpRemark_bill").focus(); return false;
    }
   
    if ($('#hdnExpItemCode_bill').val() == "") { $('#txtExpItemName_bill').focus(); return false; }
    if ($('#txtExpItemQty_bill').val() == "" || $('#txtExpItemQty_bill').val() == 0) { $('#txtExpItemQty_bill').focus(); return false; }
    if ($('#txtExpItemRate_bill').val() == "" || $('#txtExpItemRate_bill').val() == 0) { $('#txtExpItemRate_bill').focus(); return false; }
    if ($('#txtExpItemAmount_bill').val() == "" || $('#txtExpItemAmount_bill').val() == 0) { $('#txtExpItemAmount_bill').focus(); return false; }
    if ($('#ddlExpenseTax_bill').val() == "" || $('#ddlExpenseTax_bill').val() == 0) { $('#ddlExpenseTax_bill').focus(); return false; }
    //$("#hdnExpenseTax_bill").val($('#ddlExpenseTax_bill').val());

    var chkAccCode = $("#hdnExpItemCode_bill").val();

    var l = $('#tbl_Expense_bill tr.myData_bill').find("td[chk-data='" + chkAccCode + "']").length;

    //if (l > 0) {
        //alert('Sorry ! This Item is Already Available.'); $("#txtExpItemName_bill").val(''); $("#hdnExpItemCode_bill").val(''); //$('#txtExpItemName_bill').focus(); return false;
    //}

    Calculate_TaxAmount_bill("");

    var html = ""

    var $this = $('#tbl_Expense_bill .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData_bill'>"
         + '<td colspan="8" chk-data=' + $("#hdnExpItemCode_bill").val() + ' chk-tax=' + $("#ddlExpenseTax_bill").val() + '>'
         + '<div class="forum-item cls_1">'
         + '<div class="row">'
        + '<div class="col-md-4">'

         + '<div class="forum-icon" style="margin-right:3px">'
         + '<i class="fa  del_img"  onclick="Delete_bill(this, \'' + $("#ddlExpenseTax_bill").val() + '\', \'' + $("#txtExpItemAmount_bill").val() + '\')" style="cursor:pointer; color:red; font-size:27px;" title="Delete"></i>'
         + '</div>'
         + '<div class="forum-icon" style="margin-top:3px">'
         + '<i class="fa  edit_img"  onclick="Edit_bill(this, \'' + $("#ddlExpenseTax_bill").val() + '\')" style="cursor:pointer; color:red; font-size:25px; margin-left:4px; " title="Edit"></i>'
         + '</div>'


         + '<a class="forum-item-title cls_ExpItemName">' + $("#txtExpItemName_bill").val() + '</a>'
         + '<div class="forum-sub-title cls_ExpItemDesc" style="margin-left:83px">' + $("#txtExpItemDesc_bill").val() + '</div>'
         + '</div>'

          + '<div class="col-md-1 forum-info" style="display:none">'
         + '<span class="views-number cls_ItemID" >'
         + $("#hdnExpItemCode_bill").val()
         + '</span>'
         + '<div>'
         + '<small>ItemID</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-1 forum-info">'
         + '<span class="views-number cls_ExpItemQty">'
         + $("#txtExpItemQty_bill").val()
         + '</span>'
         + '<div>'
         + '<small>Qty</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info">'
         + '<span class="views-number cls_ExpItemRate">'
         + $("#txtExpItemRate_bill").val()
         + '</span>'
         + '<div>'
         + '<small>Rate</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info">'
         + '<span class="views-number cls_Amount">'
         + $("#txtExpItemAmount_bill").val()
         + '</span>'
         + '<div>'
         + '<small>Amount</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-1 forum-info">'
         + '<span class="views-number cls_ExpItemDiscount">'
         + (parseFloat($("#txtExpItemDiscount_bill").val() == "" ? 0.00 : $("#txtExpItemDiscount_bill").val())).toFixed(2)
         + '</span>'
         + '<div>'
         + '<small>Discount</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info" style="display:none">'
         + '<span class="views-number cls_ExpenseTaxAmount" >'
         + $("#hdnExpenseTax_bill").val()
         + '</span>'
         + '<div>'
         + '<small>CalTaxAmount</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info">'
         + '<span class="views-number">'
         + $("#ddlExpenseTax_bill option:selected").text()
         + '</span>'
         + '<div>'
         + '<small>Tax</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info" style="display:none">'
         + '<span class="views-number cls_ExpenseTax" >'
         + $("#ddlExpenseTax_bill").val()
         + '</span>'
         + '<div>'
         + '<small>TaxID</small>'
         + '</div>'
         + '</div>'

         + '</div>'
         + '</div>'
         + '</td>'
    html + "</tr>";

    $parentTR.after(html);

     Calculate_Tax_bill($("#ddlExpenseTax_bill").val(), $("#txtExpItemAmount_bill").val(), "P");
    $("#txtExpItemName_bill").val(''); $("#hdnExpItemCode_bill").val(''); $("#txtExpItemDesc_bill").val('');
    $("#txtExpItemQty_bill").val(''); $("#txtExpItemRate_bill").val(''); $("#txtExpItemDiscount_bill").val(''); $("#txtExpItemAmount_bill").val('');
    $("#ddlExpenseTax_bill").val(''); //$("#hdnExpenseTax_bill").val('');
    Calculate_Total_bill();
   

}
function Set_DefaultDate_bill() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear(); 
    $('#txtExpPurOrderDate_bill').val(output);
}
function Set_PurchaseOrder_billDate() {
    $('#txtExpPurOrderDate_bill, #txtExpPurOrderDueDate, #txtVendorBillDate_bill').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

}

function Blank_bill() {
    $('#ddlExIncomeType').val(''); $('#lblBillorExpense').text(''); 

    $('.bill').val(''); $('#lblBillNo').text(''); Set_DefaultDate_bill();
    $("#tbl_Expense_bill tbody tr.myData_bill").remove(); $('#tbl_Expense_bill tbody tr.trfooter_bill').remove();
    $("#tbl_SubExpense_bill tbody tr.mysubData_bill").remove(); $("#tbl_SubExpense_bill tbody tr.trfooter_bill").remove(); $('#ExpIframe').attr('src', '');
    $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').remove(); $('#tbl_Expense_Voucher_bill .trfooter_voucher_bill').remove();
    $('.bank').val(''); $('#lblBillVoucherNo').text(''); $('#btnPrintVch').css('display', 'none');
}
function GetType_Bill(serviceType, shortSType, ID, ExpInc)
{
    if (ID == 2) {
        $('.banksection').css('display', ''); TranType = "E"; 
    }
    if (ID == 3) {
        $('.banksection').css('display', 'none'); $('.bank').val(''); TranType = "B";
    }
    if (ExpInc == "I")
        $('.cls_fcno').css('display', 'none');
    else
        $('.cls_fcno').css('display', '');

    $('#ddlExIncomeType').prop('disabled', true); $('#ddlExIncomeType').val(ExpInc);

    //$('#lblBillorExpense').text('(' + serviceType + ')');
    Populate_BillAdjHead(shortSType);
}
function ChangeBillExpHeader() {
    var type = $('#ddlExIncomeType').val();
    if (type == "E") {
        $('#lblBillorExpense').text('(' + (TranType == "B" ? ("Bill - " + "Expense") : ("Expense - " + "Expense")) + ')'); IncExp = "E";
    }
    if (type == "I") {
        $('#lblBillorExpense').text('(' + (TranType == "B" ? ("Bill - " + "Income") : ("Expense - " + "Income")) + ')'); IncExp = "I";
    }
   
}
function Populate_BillAdjHead(shortSType)
{
    var E = "{TransType: '" + shortSType + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_BillAccHead',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async:false,
        success: function (data, status) {
            var t = data.Data;
            if(t.length >0)
            {
                $('#ddlExpAdjHead_bill').empty();
                $('#ddlExpAdjHead_bill').append('<option value="" selected>Select</option');
                $(t).each(function (index, item) {
                    $('#ddlExpAdjHead_bill').append('<option value=' + item.AccountCode + '>' + item.AccountDescription + '</option>');
                });
            }
        }
    });
}

function Delete_bill(ID, TaxID, newAmt) {

    $(ID).closest('tr').remove(); 
    Calculate_Total_bill(); Calculate_Tax_bill(TaxID, newAmt, "M"); Calculate_FinalTotal_bill();
}
function Edit_bill(ID, TaxID)
{
    var itemID = $(ID).closest('tr').find('td:eq(0)').attr('chk-data'); 
    var itemName = $(ID).closest('tr').find('.cls_ExpItemName').text(); 
    var itemProdDesc = $(ID).closest('tr').find('.cls_ExpItemDesc').text();
    var itemQty = $(ID).closest('tr').find('.cls_ExpItemQty').text();
    var itemRate = $(ID).closest('tr').find('.cls_ExpItemRate').text();
    var itemNetAmount = $(ID).closest('tr').find('.cls_Amount').text();
    var itemDiscount = $(ID).closest('tr').find('.cls_ExpItemDiscount').text();
    var itemTaxAmount = $(ID).closest('tr').find('.cls_ExpenseTaxAmount').text();
    var itemTaxTypeID = $(ID).closest('tr').find('.cls_ExpenseTax').text();
   
    $('#txtExpItemName_bill').val(itemName);
    $('#hdnExpItemCode_bill').val(itemID);
    $('#txtExpItemDesc_bill').val(itemProdDesc); $('#txtExpItemQty_bill').val(itemQty);
    $('#txtExpItemRate_bill').val(itemRate); $('#txtExpItemAmount_bill').val(itemNetAmount);
    $('#txtExpItemDiscount_bill').val(itemDiscount); $('#ddlExpenseTax_bill').val(itemTaxTypeID);
    $('#hdnExpenseTax_bill').val(itemTaxAmount); 

    Delete_bill(ID, TaxID, itemNetAmount);
}

function Bill_Cal_Amount() {
    var qty = $('#txtExpItemQty_bill').val();
    var rate = $('#txtExpItemRate_bill').val();
    var dis = $('#txtExpItemDiscount_bill').val() == "" ? 0.00 : $('#txtExpItemDiscount_bill').val();

    var amt = (qty * rate) - dis;
    $('#txtExpItemAmount_bill').val(amt.toFixed(2));
}
function Calculate_Total_bill() {

    var Total = 0;
    var html = ""

    if ($("#tbl_Expense_bill tbody tr.myData_bill").length > 0) {
        $("#tbl_Expense_bill tbody tr.myData_bill").each(function (index, value) {
            var Amount = $(this).find('.cls_Amount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
            $('#hdnExpGrossAmt_bill').val(Total);
        });
    }

    $('#tbl_Expense_bill .trfooter_bill').remove();

    html += "<tr class='trfooter_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='6'>SubTotal : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (Total).toFixed(2) + "</span></td>"
    html + "</tr>";

    $('#tbl_Expense_bill tr:last').after(html)
}

function Calculate_AdjCal()
{
    var a = $('#hdnExpGrossAmt_bill').val() == "" ? 0 : $('#hdnExpGrossAmt_bill').val();     //Detail Total
    var b = $('#hdnExpSubGrossAmt_bill').val() == "" ? 0 : $('#hdnExpSubGrossAmt_bill').val();//SubDetail Total

    var e = $('#txtExpRoundOff_bill').val() == "" ? 0 : $('#txtExpRoundOff_bill').val();     //round off
    var f = ($('#txtExpAdjAmount_bill').val() == "" || $('#txtExpAdjAmount_bill').val() == ".") ? 0 : $('#txtExpAdjAmount_bill').val();   //adjamount

    var adjtype = $('#ddlExpAdjType_bill').val() == "" ? 0 : $('#ddlExpAdjType_bill').val();
	
	    var tcsp = $('#ddlExpenseTCSPerValue_bill').val(); var TCSTotal = 0;
    if ($("#tbl_TCSSubExpense_bill tbody tr.mysubData_bill").length > 0) {
        TCSTotal = $('.clsTCSSubTotalTax').text() == "" ? 0 : $('.clsTCSSubTotalTax').text();
    }
	

    var c = 0, s=0, k=0; 
    if (adjtype != "") {
        if(adjtype == "A")
            c = (parseFloat(a) + parseFloat(b) + parseFloat(f)).toFixed(2);
        else
            c = (parseFloat(a) + parseFloat(b) - parseFloat(f)).toFixed(2);

         s = Math.round(parseFloat(c)).toFixed(2);
         k = parseFloat(s) - parseFloat(c);
    }
    else {
        c = (parseFloat(a) + parseFloat(b)).toFixed(2); 
        s = Math.round(parseFloat(c)).toFixed(2); 
        k = parseFloat(s) - parseFloat(c);
    }

    if ($("#tbl_TCSSubExpense_bill tbody tr.mysubData_bill").length > 0) {
        $('#txtExpNetAmount_bill').val(parseFloat(c));

        s = Math.round(parseFloat(c) + parseFloat(TCSTotal)).toFixed(2);
        k = parseFloat(s) - (parseFloat(c) + parseFloat(TCSTotal)) ;

        $('#txtTcsExpNetAmount_bill').val(Math.round(s).toFixed(2));  
        $('#txtExpRoundOff_bill').val(parseFloat(k).toFixed(2)); 
    }
    else {
        $('#txtExpRoundOff_bill').val(parseFloat(k).toFixed(2));
        $('#txtExpNetAmount_bill').val(Math.round(s).toFixed(2));
        $('#txtTcsExpNetAmount_bill').val(Math.round(s).toFixed(2)); 
    }
    
}
function Calculate_RoundNet()
{
    var adjtype = $('#ddlExpAdjType_bill').val() == "" ? 0 : $('#ddlExpAdjType_bill').val();
    var adjamt = $("#txtExpAdjAmount_bill").val() == "" ? 0 : $("#txtExpAdjAmount_bill").val();
    var adjbillHead = $('#ddlExpAdjHead_bill').val();

    if (adjtype == "") { $('#ddlExpAdjType_bill').focus(); $("#txtExpAdjAmount_bill").val(''); return false; }
    if (adjbillHead == "") { $('#ddlExpAdjHead_bill').focus(); $("#txtExpAdjAmount_bill").val(''); return false; }
   

    var a = $('#hdnExpGrossAmt_bill').val() == "" ? 0 : $('#hdnExpGrossAmt_bill').val();
    var b = $('#hdnExpSubGrossAmt_bill').val() == "" ? 0 : $('#hdnExpSubGrossAmt_bill').val();

    var c = 0, s = 0, k = 0;

   
    if (adjamt > 0) {
      
        if (adjtype == "A")
            c = (parseFloat(a) + parseFloat(b) + parseFloat(adjamt)).toFixed(2);
        else
            c = (parseFloat(a) + parseFloat(b) - parseFloat(adjamt)).toFixed(2);

        s = Math.round(parseFloat(c)).toFixed(2);
        k = parseFloat(s) - parseFloat(c);

    }
    else {
      
        Calculate_AdjCal(); return false;
    }

   $('#txtExpRoundOff_bill').val(parseFloat(k).toFixed(2));
    $('#txtExpNetAmount_bill').val(Math.round(s).toFixed(2));
    $('#txtTcsExpNetAmount_bill').val(Math.round(s).toFixed(2));
}

function Calculate_FinalTotal_bill() {
    var Total = 0;
    var html = ""

    if ($("#tbl_SubExpense_bill tbody tr.mysubData_bill").length > 0) {
        $("#tbl_SubExpense_bill tbody tr.mysubData_bill").each(function (index, value) {
            var Amount = $(this).find('.clsTaxNetAmount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
            $('#hdnExpSubGrossAmt_bill').val(Total);
        });
    }

    $('#tbl_SubExpense_bill .trfooter_bill').remove();

    html += "<tr class='trfooter_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold;'><span class='clsSubTotalTax' > &#x20b9; " + (Total).toFixed(2) + "</span></td>"
    html + "</tr>";

    $('#tbl_SubExpense_bill tr:last').after(html);

    if ($("#tbl_Expense_bill tbody tr.myData_bill").length > 0) 
        Calculate_AdjCal();

    if ($("#ddlExpAdjType_bill").val() != "")
        Calculate_RoundNet();
		
		    if ($("#tbl_SubExpense_bill tbody tr.mysubData_bill").length > 0) {
        if ($("#ddlExpenseTCSPerValue_bill").val() != "")
            CalculateTCS();
    }
}
function Calculate_Tax_bill(TaxID, newAmt, PM) {
    var Total = 0;
    if (TaxID != "") {
       
        if ($("#tbl_Expense_bill tbody tr.myData_bill").length > 0) {
            $("#tbl_Expense_bill tbody tr.myData_bill").each(function (index, value) {
                var TaxIDs = $(this).find('.cls_ExpenseTax').text();
                if (TaxID == TaxIDs) {
                    var Amount = $(this).find('.cls_Amount').text();
                    Total = parseFloat(Total) + parseFloat(Amount);
                }
            });

            var l = $('#tbl_Expense_bill tr.myData_bill').find("td[chk-tax='" + TaxID + "']").length;
            if (l > 0) {
                var E = "{TaxTypeID: '" + TaxID + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/Item/Load_TaxTypeDetail',
                    data: E,
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        var data = response.Data;
                        if (response.Status == 200) {
                            for (var i = 0; i < data.length; i++) {
                                var taxPert = data[i].TaxPer;
                                var taxTypeDet = data[i].TaxTypeDet;
                                var taxTypeDetID = data[i].TaxTypeDetId;
                                //var calAmount = Roundoff((parseFloat(Total) * parseFloat(taxPert) / 100)); //parseFloat(Total) * parseFloat(taxPert) / 100);

                                var calAmount = 0;
                                    if (taxTypeDetID == 1)
                                         calAmount = Roundoff((parseFloat(newAmt) * parseFloat(taxPert) / 100));
                                    else
                                         calAmount = Roundoff((parseFloat(newAmt) * parseFloat(taxPert) / 100));
								
								Bind_TAXCal_bill1(TaxID, taxTypeDetID, taxTypeDet, Total, calAmount, PM);
								//Bind_TAXCal_bill(TaxID, taxTypeDetID, taxTypeDet, Total, calAmount);
                            }
                            Calculate_FinalTotal_bill();
							Save_Bill(3);
                        }
                    }
                });

            }
            else {
                $('#tbl_SubExpense_bill tbody').find("tr[chk-tax='" + TaxID + "']").remove();
            }
        }
        else {
            $('#ddlExpensePerValue_bill').val(''); $('#hdnExpPerValue_bill').val('');
            $('#txtExpRoundOff_bill').val(''); $('#txtExpNetAmount_bill').val('');
            $('#hdnExpGrossAmt_bill').val(''); $('#hdnExpSubGrossAmt_bill').val(''); $('#txtExpAdjAmount_bill').val(''); $('#ddlExpAdjType_bill').val('');
            $("#tbl_SubExpense_bill tbody tr.mysubData_bill").remove();
			
			 $("#tbl_TCSSubExpense_bill tbody tr.mysubData_bill").remove(); $("#tbl_TCSSubExpense_bill tbody tr.trtcsfooter_bill").remove(); 
            $('#txtExpRoundOff_bill').val(''); $('#txtTcsExpNetAmount_bill').val(''); $('#ddlExpenseTCSPerValue_bill').val(''); 
            //$('.clsTcs').css('display', 'none');
        }
    }
}
function Bind_TAXCal_bill1(TaxID, taxTypeDetID, TaxTypeDet, Total, calAmount, PM) {

    var html = ""

    var table = $('#tbl_SubExpense_bill tbody tr.test_1');

    var ee = 0;
    $("#tbl_SubExpense_bill tbody tr.mysubData_bill").each(function (index, value) {
        var taxid = $(this).find('.aa').text();
        var taxtypeid = $(this).find('.bb').text();

        if (TaxID == taxid && taxTypeDetID == taxtypeid) {
            $(this).find('.clsTaxDes').text(TaxTypeDet);

            var a = $(this).find('.clsTaxNetAmount').text() == "" ? 0 : $(this).find('.clsTaxNetAmount').text(); 
            if(PM == "P")
                $(this).find('.clsTaxNetAmount').text((calAmount + parseFloat(a)).toFixed(2)); 
            else
                $(this).find('.clsTaxNetAmount').text((parseFloat(a) - calAmount).toFixed(2)); 

            ee++;
        }
    });
    if (ee > 0)
        return false;



    html += "<tr class='mysubData_bill' cls_" + TaxID + "'  chk-tax=" + TaxID + ">"
        + "<td style='text-align:right; display:none;' class='aa' >" + TaxID + "</td>"
        + "<td style='text-align:right; display:none;' class='bb'>" + taxTypeDetID + "</td>"
        + "<td style='text-align:right' class='clsTaxDes' >" + (TaxTypeDet) + "</td>"
        + "<td style='text-align:right' class='clsTaxNetAmount' >" + (calAmount).toFixed(2) + "</td>"
    html + "</tr>";

    $('#tbl_SubExpense_bill tr:last').after(html);

}
function Bind_TAXCal_bill(TaxID, taxTypeDetID, TaxTypeDet, Total, calAmount) {
   
    var html = ""

    //var $this = $('#tbl_Expense_bill .test_0');
    //$parentTR = $this.closest('tr');

    var table = $('#tbl_SubExpense_bill tbody tr.test_1');

    var ee = 0;
    $("#tbl_SubExpense_bill tbody tr.mysubData_bill").each(function (index, value) {
        var taxid = $(this).find('.aa').text();
        var taxtypeid = $(this).find('.bb').text();
        if (TaxID == taxid && taxTypeDetID == taxtypeid) {
            $(this).find('.clsTaxDes').text(TaxTypeDet);
            $(this).find('.clsTaxNetAmount').text((calAmount).toFixed(2));
            ee++;
        }
    });
    if (ee > 0)
        return false;

    html += "<tr class='mysubData_bill cls_" + TaxID + "'  chk-tax=" + TaxID + ">"
        + "<td style='text-align:right; display:none;' class='aa' >" + TaxID + "</td>"
        + "<td style='text-align:right; display:none;' class='bb'>" + taxTypeDetID + "</td>"
        + "<td style='text-align:right' class='clsTaxDes' >" + (TaxTypeDet) + "</td>"
        + "<td style='text-align:right' class='clsTaxNetAmount' >" + (calAmount).toFixed(2) + "</td>"
    html + "</tr>";

    //table.append(html);
    $('#tbl_SubExpense_bill tr:last').after(html);
   
}



function Calculate_TaxAmount_bill(ID) {
    var taxamount = $('#txtExpItemAmount_bill').val();
    var taxid = $('#ddlExpenseTax_bill').val();
    if (taxid != "") {
        var E = "{TaxTypeID: '" + taxid + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/Item/Load_TaxTypeDetail',
            data: E,
            async:false,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data;
                if (response.Status == 200 && data.length > 0) {
                    var Total = 0, Total1=0, Total2=0;
                    for (var i = 0; i < data.length; i++) {

                        var TaxTypeDetId = data[i].TaxTypeDetId;
                        var taxPert = data[i].TaxPer;

                        if (TaxTypeDetId == 1) {
                            Total1 = Roundoff((parseFloat(taxamount) * parseFloat(taxPert)) / 100);
                            Total = parseFloat(Total) + parseFloat(Total1); 
                        }
                        else {
                            Total2 = Roundoff((parseFloat(taxamount) * parseFloat(taxPert)) / 100);
                            Total = parseFloat(Total) +  parseFloat(Total2); 
                        }
                    }
                   
                    $('#hdnExpenseTax_bill').val(Total);
                    $('#hdnExpenseTax_bill1').val(Total1);
                    $('#hdnExpenseTax_bill2').val(Total2);

                    //alert(taxamount); alert(taxPert); 
                    //var a = Roundoff((parseFloat(taxamount) * parseFloat(taxPert)) / 100); //alert(parseFloat(taxamount) * parseFloat(taxPert));
                    //$('#hdnExpenseTax_bill').val(a); 
                }
            }
        });
    }
}
function Calculate_DueDate() {
    var termID = $("#ddlCreditTerm").val(); 
    var termValue = $("#ddlCreditTerm option:selected").text(); 

    if (termID != 1 && termID !="") {
        var currDate = $('#txtExpPurOrderDate_bill').val();
        var aa = currDate.split('/');
        var ldates = new Date(aa[2], aa[1] - 1, aa[0]);

        ldates.setDate(ldates.getDate() + parseInt(termValue));// alert(ldates.getMonth()); alert(ldates);
        var futDate = (ldates.getDate() < 10 ? '0' + ldates.getDate() : ldates.getDate()) + "/" + ((parseInt(ldates.getMonth()) + parseInt(1)) < 10 ? '0' + (parseInt(ldates.getMonth()) + parseInt(1)) : (parseInt(ldates.getMonth()) + parseInt(1))) + "/" + ldates.getFullYear();

        $('#txtExpPurOrderDueDate').val(futDate);
    }
    else
    $('#txtExpPurOrderDueDate').val($('#txtExpPurOrderDate_bill').val());
}

function Expense_Preview_bill(input) {

    var a = input.files[0].name;
    if (a != "") {
        var validExtensions = ['pdf']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type. Please Select PDF File only.");
            $("#ExpenseflPic_bill").val('');
            return false;
        }
    }

    if (input.files && input.files[0]) {
        path = $('#ExpenseflPic_bill').val().substring(12);
        pathString = '/UploadItemImage/Bill/' + path;
    }
}
// SECTION
function Bind_Section() {
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BillExtract/Bind_Section',
        contentType: "application/json; charset=utf-8",
        data: {},
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('#ddlExpPurOrderSectionName').empty();
            $('#ddlExpPurOrderSectionName').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('#ddlExpPurOrderSectionName').append('<option value=' + item.SectionID + '>' + item.SectionName + '</option>');
                });
            }
        }
    });

}
function Save_Bill(CON) {

    var result = CheckDate(); //alert(result);
    if (result == false)
        return false;
		
    if ($("#ddlExIncomeType").val() == "") {
        toastr.error('Please, Select Either Income or Expense !!', 'Warning')
        $("#ddlExIncomeType").focus(); return false;
    }
    if ($("#hdntxtExpVendorID_bill").val() == "") {
        toastr.error('Please, Select Vendor Name !!', 'Warning')
        $("#txtExpVendorName_bill").focus(); return false;
    }

   if ($("#txtVendorBillNo_bill").val() == "") {
        toastr.error('Please, Enter Supplier Ref. No. !!', 'Warning')
        $("#txtVendorBillNo_bill").focus(); return false;
    }
    if ($("#txtVendorBillDate_bill").val() == "") {
        toastr.error('Please, Enter Supplier Ref. Date !!', 'Warning')
        $("#txtVendorBillDate_bill").focus(); return false;
    }
	    if ($("#txtExpPurOrderDate_bill").val() == "") {
        toastr.error('Please, Enter Bill Date !!', 'Warning')
        $("#txtExpPurOrderDate_bill").focus(); return false;
    }
	    if ($("#txtExpRemark_bill").val() == "") {
        toastr.error('Please, Enter Remarks !!', 'Warning')
        $("#txtExpRemark_bill").focus(); return false;
    }

    var master = {};
    master = {
        BillId: $('#hdnBillID').val(),
        VendorCode: $('#hdntxtExpVendorID_bill').val(),
        VendEmail: $('#txtEmail_bill').val(),
        BillStatus: TranType,
        IncomeExpense: $('#ddlExIncomeType').val(),
		 MailingAddr: $('#txtMailingAddress_bill').val(),
        ShipAddr: $('#txtShippingAddress_bill').val(),
        ShipTo: $('#hdnShipTo_bill').val(),
        ShipVia: $('#txtShipVia_bill').val(),

        BillNo: $('#lblBillNo').text(),
        CreditTerms: $('#ddlCreditTerm').val(),
        BillDate: $('#txtExpPurOrderDate_bill').val(),
        DueDate: $('#txtExpPurOrderDueDate').val(),
		        StockPointId: $('#ddlExpPurOrderStockPoint').val(),
        SectionId: $('#ddlExpPurOrderSectionName').val(),

        VendorBillNo: $('#txtVendorBillNo_bill').val(),
        VendorBillDate: $('#txtVendorBillDate_bill').val(),
        PoId: $('#hdnBillPONo').val(),
        FCNo: $('#hdnBillFCNo').val(),
        TransType: TranType,
        TaxType: 'E',

        BankID: $('#hdnBillBank').val(),
        InstrumentType: $('#ddlExpInstType').val(),
        InstrumentNo: $('#txtExpInstNo').val(),

        GrossAmt: $('#hdnExpGrossAmt_bill').val(),
        TaxTypeonTax: $('#ddlExpensePerValue_bill').val(),
        TaxTypeonTaxAmount: $('#hdnExpPerValue_bill').val(),
        AdjType: $('#ddlExpAdjType_bill').val(),
        AdjAmt: $('#txtExpAdjAmount_bill').val(),
        BillAdjHead: $('#ddlExpAdjHead_bill').val(),
        RoundOff: $('#txtExpRoundOff_bill').val(),
        NetAmt: $('#txtExpNetAmount_bill').val(),
		 NetAmtAfterTCS: $('#txtTcsExpNetAmount_bill').val(),
        Remarks: $('#txtExpRemark_bill').val(),
        DocFilePath: pathString,
        Sectorid: $('#ddlSector').val()
    }
    //alert(JSON.stringify(master));

    var grdLen = $('#tbl_Expense_bill tbody tr.myData_bill').length; var ArrList = [];
    if (grdLen > 0) {
        $('#tbl_Expense_bill tbody tr.myData_bill').each(function () {

            var ItemID = $(this).find('.cls_ItemID').text();
            var ItemDesc = $(this).find('.cls_ExpItemDesc').text();
            var ItemQty = $(this).find('.cls_ExpItemQty').text();
            var ItemRate = $(this).find('.cls_ExpItemRate').text();
            var ItemAmount = $(this).find('.cls_Amount').text();
            var ItemDiscount = $(this).find('.cls_ExpItemDiscount').text();
            var ItemCalTaxAmount = $(this).find('.cls_ExpenseTaxAmount').text();
            var ItemTaxId = $(this).find('.cls_ExpenseTax').text();

            var NetAmount = parseFloat(ItemAmount) + parseFloat(ItemCalTaxAmount);

            ArrList.push({
                'ItemId': ItemID, 'ProductServiseDesc': ItemDesc, 'Qty': ItemQty, 'Rate': ItemRate, 'GrossAmt': ItemAmount,
                'TaxTypeId': ItemTaxId, 'TaxAmt': ItemCalTaxAmount, 'NetAmt': NetAmount, 'sectorid': $('#ddlSector').val(), 'Discount': ItemDiscount
            });
        });
    }

    var grdsubLen = $('#tbl_SubExpense_bill tbody tr.mysubData_bill').length; var ArrSubList = [];
    if (grdsubLen > 0) {
        //$('#tbl_SubExpense_bill tbody tr.mysubData_bill').each(function () {

        //    var TaxID = $(this).find('.aa').text();
        //    var taxTypeDetID = $(this).find('.bb').text();
        //    var TaxDes = $(this).find('.clsTaxDes').text();
        //    var TaxNetAmount = $(this).find('.clsTaxNetAmount').text();

        //    ArrSubList.push({
        //        'TaxID': TaxID, 'taxTypeDetID': taxTypeDetID, 'TaxDes': TaxDes, 'TaxNetAmount': TaxNetAmount, 'GrossAmt': $('#hdnExpSubGrossAmt').val()
        //    });
        //});
    }
	
	    var grdtcssubLen = $('#tbl_TCSSubExpense_bill tbody tr.mysubData_bill').length; //var ArrTCSSubList = [];
    if (grdtcssubLen > 0)
    {
        $('#tbl_TCSSubExpense_bill tbody tr.mysubData_bill').each(function () {
            var TaxID = $(this).find('.aa').text();
            var taxTypeDetID = $(this).find('.bb').text();
            var TaxDes = $(this).find('.clsTaxDes').text();
            var TaxNetAmount = $(this).find('.clsTaxNetAmount').text();

            ArrSubList.push({
                'TaxID': TaxID, 'taxTypeDetID': taxTypeDetID, 'TaxDes': TaxDes, 'TaxNetAmount': TaxNetAmount, 'GrossAmt': $('#hdnExpSubGrossAmt').val()
            });
        });
    }

    var ArrVchList = [];
    var grdvchLen = $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').length;
    if (grdvchLen > 0) {

        var DRTotal = 0, CRTotal = 0, NetAmount = 0, Roudoff = 0, totalCom=0;
        NetAmount = $('#txtTcsExpNetAmount_bill').val(); //$('#txtExpNetAmount_bill').val();
        Roudoff = $('#txtExpRoundOff_bill').val();
        totalCom = NetAmount; //parseFloat(NetAmount) + (-1 * parseFloat(Roudoff)); //alert(totalCom);

        if ($("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").length > 0) {
            $("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").each(function (index, value) {
                var DRAmount = $(this).find('.cls_drAmount').val();
                var CRAmount = $(this).find('.cls_crAmount').val();
                 DRTotal = Roundoff(parseFloat(Math.abs(DRTotal)) + (parseFloat(Math.abs(DRAmount)) == "" ? 0 : Math.abs(DRAmount))); //alert(DRTotal);
                CRTotal = Roundoff(parseFloat(Math.abs(CRTotal)) + (parseFloat(Math.abs(CRAmount)) == "" ? 0 : Math.abs(CRAmount))); //alert(CRTotal);
            });
        }
        //alert(DRTotal);
        //alert(CRTotal);
		if (Roudoff < 0)
        {
            Roudoff = Math.abs(Roudoff);
            totalCom = parseFloat(totalCom) + parseFloat(Roudoff);
        }
		
        if (parseFloat(DRTotal) != parseFloat(CRTotal) || (parseFloat(totalCom) != parseFloat(DRTotal) && parseFloat(totalCom) != parseFloat(CRTotal)))
        {
            toastr.error('Sorry ! Debit and Credit Amount is not Equal.', 'Warning')
            return false;
        }


        var Amount = 0;
        $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').each(function () {

            var Order = $(this).find('.cls_Order').text();
            var DRCR = $(this).find('.cls_drcr').val();
            var AccCode = $(this).find('.cls_hdnAccountDesc').val();
 if (AccCode == 'null') {
                boolDesc = 1;
            }
			
            if(DRCR=="D")
                Amount = $(this).find('.cls_drAmount').val();
            else
                Amount = $(this).find('.cls_crAmount').val();

            if (Math.abs(Amount) > 0) {
                ArrVchList.push({
                    'Ord': Order, 'Drcr': DRCR, 'AccountCode': AccCode, 'Amount': Amount
                });
            }
        });
        if (boolDesc == 1) {
            alert('Please, Select Ledger Detail for all Debit & Credit.'); boolDesc = ""; return false;
        }
    }
    if (CON == 1 && grdvchLen == 0) {
        toastr.error('Please, review your account information !!', 'Warning')
        return false;
    }

    var ItemDesc = JSON.stringify(ArrList);
    var ItemSubDesc = JSON.stringify(ArrSubList);

    var ItemVchDesc = "", blankArr=[];
    if (CON == 1)
         ItemVchDesc = JSON.stringify(ArrVchList);
    else
        ItemVchDesc = JSON.stringify(blankArr);

    var E = "{BillMaster: " + JSON.stringify(master) + ", BillDetail: " + ItemDesc + ", BillSub: " + ItemSubDesc + ",  BillAccVch: " + ItemVchDesc + "}";

    //return false;
    $.ajax({
        url: '/Accounts_Form/Item/BILL_InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; 
            if (data.Status == 200 && t != "") {
               
                $('#hdnBillID').val(t);
                Bind_AccountVoucher(t, $('#hdntxtExpVendorID_bill').val());
                Upload_bill('ExpenseflPic_bill', t);

                if (CON == 1) {
                    swal({
                        title: "Success",
                        text: 'Bill Details are Saved Successfully !!',
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });
                }
            }
        }
    });

}
function Upload_bill(flPic, PONO) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#ExpenseflPic_bill').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file, PONO + fileNameExt);
    }

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/UploadPicture_bill',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
        }
    });
}

function Execute_Action_B(ID) {

    var POID = $(ID).closest('tr').find('.PoId').text();
    var ActionName = $(ID).closest('tr').find('.cls_Action').val();
    var TransType = $(ID).closest('tr').find('.TransType').text();
    $('.cls_Action').val('');


    if (ActionName != "") {
        if (ActionName == "E")  //Edit
            Action_Edit_B(POID, TransType);
        if (ActionName == "D")  //Delete
            Action_Delete_B(POID, TransType);
		 if (ActionName == "P")  //Print
            Action_Print_B(POID, TransType);
    }
$(ID).closest('tr').find('.cls_Action').val('');
    //$(ID).closest('tr').find('.cls_Action').val(ActionName);
    $('.banksection').css('display', 'none'); $('.bank').val('');
}
function Execute_Action_E(ID) {

    Execute_Action_B(ID);
    $('.banksection').css('display', '');
}

function Action_Edit_B(POID, TransType) {
    var E = "{POID: '" + POID + "', TransactionType: '" + "Bill_Detail" + "', TransType: '" + TransType + "', SectorID: '" + $('#ddlSector').val() + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var tt = data.Data; //alert(JSON.stringify(t));

            var t1 = tt["Table"];
            var t2 = tt["Table1"];
            var t3 = tt["Table2"];

            if (t1.length > 0) {
                $('#lblBillNo').text(t1[0].PoNo);
                $('#txtExpVendorName_bill').val(t1[0].VendorName); $('#hdntxtExpVendorID_bill').val(t1[0].VendorCode); $('#txtEmail_bill').val(t1[0].VendEmail);
                $('#txtMailingAddress_bill').val(t1[0].Address); $('#txtShippingAddress_bill').val(t1[0].ShipAddr); $('#txtShipTo_bill').val(t1[0].ShipToName); $('#hdnShipTo_bill').val(t1[0].ShipTo);
                $('#txtShipVia_bill').val(t1[0].ShipVia); $('#txtExpPurOrderDate_bill').val(t1[0].PoDate); $('#ddlCreditTerm').val(t1[0].CreditTerms); $('#txtExpPurOrderDueDate').val(t1[0].DueDate);
                $('#txtVendorBillNo_bill').val(t1[0].VendorBillNo); $('#txtVendorBillDate_bill').val(t1[0].VendorBillDate);
                $('#txtBillPONo').val(t1[0].BillPoNo); $('#hdnBillPONo').val(t1[0].BillPOID); $('#hdnBillFCNo').val(t1[0].FCNo); $('#txtBillFCNo').val(t1[0].FCNo);  $('#ddlExpPurOrderStockPoint').val(t1[0].StockPointId); $('#ddlExpPurOrderSectionName').val(t1[0].SectionId);


                $('#txtExpRemark_bill').val(t1[0].Remarks);
                $('#ddlExpensePerValue_bill').val(t1[0].TaxTypeonTax == null ? 'V' : t1[0].TaxTypeonTax);
                $('#hdnExpPerValue_bill').val(t1[0].TaxTypeonTaxAmount);


                $('#ddlExpAdjType_bill').val(t1[0].AdjType); $('#txtExpAdjAmount_bill').val(t1[0].AdjAmt); $('#txtExpRoundOff_bill').val(t1[0].RoundOff); $('#txtExpNetAmount_bill').val(t1[0].NetAmt); $('#txtTcsExpNetAmount_bill').val(t1[0].NetAmtAfterTcs);
                Populate_BillAdjHead(t1[0].TransType); $('#ddlExpAdjHead_bill').val(t1[0].BillAdjHead);

                $('#hdnExpGrossAmt_bill').val(t1[0].GrossAmt); $('#txtBillBank').val(t1[0].BankName); $('#hdnBillBank').val(t1[0].BankID); $('#ddlExpInstType').val(t1[0].InstrumentType); $('#txtExpInstNo').val(t1[0].InstrumentNo);
                $('#hdnBillID').val(POID); TranType = t1[0].TransType;

                $('#ExpIframe_bill').attr('src', '');
                $('#ExpIframe_bill').attr('src', t1[0].DocFilePath);

                $('#ddlExIncomeType').val(t1[0].IncomeExpense); $('#ddlExIncomeType').prop("disabled", true); IncExp = t1[0].IncomeExpense;
				 t1[0].IncomeExpense == "I" ? $('.cls_fcno').css("display", 'none') : $('.cls_fcno').css("display", '');
                $('#lblBillorExpense').text(TranType == "E" ? "(Expense - " + (t1[0].IncomeExpense == "I" ? "Income" : "Expense") + ")" : "(Bill - " + (t1[0].IncomeExpense == "I" ? "Income" : "Expense") + ")");

                Populate_Expense_bill(t2);
                Populate_SubExpense_bill(t3);
                Bind_AccountVoucher(POID, t1[0].VendorCode);
				
				  $('#lblBillVoucherNo').text(t1[0].RefVoucherSlNO == null ? "" : (" - " + t1[0].RefVoucherSlNO));
                $('#hdnBillVoucherNo').val(t1[0].VoucherNo);
                t1[0].VoucherNo == "" ? ($('#btnPrintVch').css('display', 'none')) : ($('#btnPrintVch').css('display', ''));

                $('.bill').toggleClass('toggled');
            }
        }
    });
}
function Action_Delete_B(POID, TransType) {

    swal({
        title: "Warning",
        text: "Are You Sure want to Delete ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{POID: '" + POID + "', TransactionType: '" + "Delete" + "'}";
                //alert(E);
                //return false;
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/Item/Load_PO',
                    contentType: "application/json; charset=utf-8",
                    data: E,
                    dataType: "json",
                    success: function (data, status) {
                        var t = data.Data;
                        var table = t['Table'];
                        var result = table[0].Result;
                        if (data.Status == 200 && result == "success") {
                            swal("Deleted!", "Your Bill has been deleted successfully.", "success");
                            Load();
                        }
                        else
                            swal("Cancelled", "There is Some Problem. Your Bill is Not Deleted.", "error");
                    }
                });
            } else {
                swal("Cancelled", "Your Data is Safe.", "error");
            }
        });
}
function Action_Print_B(POID, TransType) {

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "Tax_Invoice.rpt",
        FileName: "Tax-Invoice",
		Database:''
    });

    detail.push({
        BillId: POID,
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function Populate_Expense_bill(detail) {

    $('#tbl_Expense_bill tbody tr.myData_bill').remove();
    $('#tbl_Expense_bill tbody tr.trfooter_bill').remove();

    if (detail.length > 0) {
       
            var html = ""

            var $this = $('#tbl_Expense_bill .test_0');
            $parentTR = $this.closest('tr');
            var totalGross = 0;


            for (var i = 0 ; i < detail.length; i++) {
               
                html += "<tr class='myData_bill'>"
                     + '<td colspan="8" chk-data=' + detail[i].ItemId + ' chk-tax=' + detail[i].TaxTypeId + '>'
                     + '<div class="forum-item cls_1">'
                     + '<div class="row">'
                    + '<div class="col-md-4">'

                     + '<div class="forum-icon" style="margin-right:3px">'
                     + '<i class="fa  del_img"  onclick="Delete_bill(this, \'' + detail[i].TaxTypeId + '\')" style="cursor:pointer; color:red; font-size:27px;" title="Delete"></i>'
                    + '</div>'
                    + '<div class="forum-icon" style="margin-top:3px">'
                    + '<i class="fa  edit_img"  onclick="Edit_bill(this, \'' + detail[i].TaxTypeId + '\')" style="cursor:pointer; color:red; font-size:25px; margin-left:4px; " title="Edit"></i>'
                    + '</div>'

                    + '<a class="forum-item-title cls_ExpItemName" >' + detail[i].ItemDescription + '</a>'
                    + '<div class="forum-sub-title cls_ExpItemDesc" style="margin-left:83px">' + ((detail[i].ProductServiseDesc == null || detail[i].ProductServiseDesc == "null") ? "" : detail[i].ProductServiseDesc) + '</div>'
                     + '</div>'

                     + '<div class="col-md-1 forum-info" style="display:none">'
                     + '<span class="views-number cls_ItemID" >'
                     + detail[i].ItemId
                     + '</span>'
                     + '<div>'
                     + '<small>ItemID</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-1 forum-info">'
                     + '<span class="views-number cls_ExpItemQty">'
                     + detail[i].Qty
                     + '</span>'
                     + '<div>'
                     + '<small>Qty</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number cls_ExpItemRate">'
                     + detail[i].Rate
                     + '</span>'
                     + '<div>'
                     + '<small>Rate</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number cls_Amount">'
                     + detail[i].GrossAmt
                     + '</span>'
                     + '<div>'
                     + '<small>Amount</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-1 forum-info">'
                     + '<span class="views-number cls_ExpItemDiscount">'
                     + (parseFloat(detail[i].Discount == "" ? 0.00 : detail[i].Discount)).toFixed(2)
                     + '</span>'
                     + '<div>'
                     + '<small>Discount</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info" style="display:none">'
                     + '<span class="views-number cls_ExpenseTaxAmount" >'
                     + detail[i].TaxAmt
                     + '</span>'
                     + '<div>'
                     + '<small>CalTaxAmount</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number">'
                     + detail[i].TaxTypeDesc
                     + '</span>'
                     + '<div>'
                     + '<small>Tax</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info" style="display:none">'
                     + '<span class="views-number cls_ExpenseTax" >'
                     + detail[i].TaxTypeId
                     + '</span>'
                     + '<div>'
                     + '<small>TaxID</small>'
                     + '</div>'
                     + '</div>'

                     + '</div>'
                     + '</div>'
                     + '</td>'
                html + "</tr>";

                totalGross = parseFloat(totalGross) + parseFloat(detail[i].TaxAmt);

            }

            html += "<tr class='trfooter_bill' style='background-color:#F5F5F6' >"
               + "<td style='text-align:right; font-weight:bold;' colspan='6'>SubTotal : </td>"
               + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (parseFloat($('#hdnExpGrossAmt_bill').val()).toFixed(2)) + "</span></td>"
            html + "</tr>";

            $parentTR.after(html);
    }
}   //wforking
function Populate_SubExpense_bill(detail) {

    $('#tbl_SubExpense_bill tbody tr.mysubData_bill').remove();
    $('#tbl_SubExpense_bill tbody tr.trfooter_bill').remove();
    $('#tbl_TCSSubExpense_bill tbody tr.mysubData_bill').remove();
    $('#tbl_TCSSubExpense_bill tbody tr.trfooter_bill').remove();

    if (detail.length > 0) {

          var html = "", html1 = "", TaxGroup = ""; var totalGross = 0, totaltcsGross = 0;

        var table = $('#tbl_SubExpense_bill tbody');
		var table1 = $('#tbl_TCSSubExpense_bill tbody');

        for (var i = 0 ; i < detail.length; i++) {
		 TaxGroup = detail[i].TaxGroup;
		  if (TaxGroup == 'G') {
                $('#ddlExpenseTCSPerValue_bill').val('');
            html += "<tr class='mysubData_bill cls_" + detail[i].TaxID + "'  chk-tax=" + detail[i].TaxID + ">"
                + "<td style='text-align:right; display:none;' class='aa' >" + detail[i].TaxID + "</td>"
                + "<td style='text-align:right; display:none;' class='bb'>" + detail[i].taxTypeDetID + "</td>"
                + "<td style='text-align:right' class='clsTaxDes' >" + detail[i].TaxDes + "</td>"
                + "<td style='text-align:right' class='clsTaxNetAmount' >" + detail[i].TaxNetAmount + "</td>"
            html + "</tr>";

            totalGross = parseFloat(totalGross) + parseFloat(detail[i].TaxNetAmount);
			}
			else
            {
                $('#ddlExpenseTCSPerValue_bill').val(detail[i].TaxID);
                $('.clsTcs').css('display', '');
                html1 += "<tr class='mysubData_bill row_id=" + detail[i].DatBillId + " cls_" + detail[i].TaxID + "'  chk-tax=" + detail[i].TaxID + ">"
                    + "<td style='text-align:right; display:none;' class='aa' >" + detail[i].TaxID + "</td>"
                    + "<td style='text-align:right; display:none;' class='bb'>" + detail[i].taxTypeDetID + "</td>"
                    + "<td style='text-align:right' class='clsTaxDes' >" + detail[i].TaxDes + "</td>"
                    + "<td style='text-align:right' class='clsTaxNetAmount' >" + detail[i].TaxNetAmount + "</td>"
                html1 + "</tr>";

                totaltcsGross = parseFloat(totaltcsGross) + parseFloat(detail[i].TaxNetAmount);
            }
        }
                if (totalGross > 0) {
            html += "<tr class='trfooter_bill' style='background-color:#F5F5F6' >"
                + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
                + "<td style='text-align:right; font-weight:bold;'><span class='clsSubTotalTax' >&#x20b9; " + (totalGross).toFixed(2) + "</span></td>"
            html + "</tr>";
        }

        if (totaltcsGross > 0) {
            html1 += "<tr class='trfooter_bill' style='background-color:#F5F5F6' >"
                + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
                + "<td style='text-align:right; font-weight:bold;'><span class='clsSubTotalTax' >&#x20b9; " + (totaltcsGross).toFixed(2) + "</span></td>"
            html1 + "</tr>";
        }

        table.append(html); 
        table1.append(html1);
        $('#hdnExpSubGrossAmt_bill').val(totalGross);
        $('.clsTcs').css('display', '');
        $('#lblTcsTax').text('Amount Including Taxes');
    }
}

function ExpSubPerValue_bill()
{
    if ($('#ddlExpensePerValue_bill').val() == "") { $('#ddlExpensePerValue_bill').focus(); return false; }
    //if ($('#hdnExpPerValue_bill').val() == "") { $('#hdnExpPerValue_bill').focus(); return false; }
    var decValue = ($('#hdnExpPerValue_bill').val() == "" ? 0 : $('#hdnExpPerValue_bill').val()); 


    if ($("#tbl_Expense_bill tbody tr.myData_bill").length > 0) {
        $("#tbl_Expense_bill tbody tr.myData_bill").each(function (index, value) {
            var TaxID = $(this).find('.cls_ExpenseTax').text(); 
            if (decValue == "" || decValue == 0)
                Calculate_Tax_bill(TaxID);
            else
                Calculate_TaxOnTax_bill(TaxID, decValue);
        });
    }
}
function Calculate_TaxOnTax_bill(TaxID, decValue) {
    var Total = 0;
    if (TaxID != "") {

        if ($("#tbl_Expense_bill tbody tr.myData_bill").length > 0) {
            $("#tbl_Expense_bill tbody tr.myData_bill").each(function (index, value) {
                var TaxIDs = $(this).find('.cls_ExpenseTax').text();
                if (TaxID == TaxIDs) {
                    var Amount = $(this).find('.cls_Amount').text();
                    Total = parseFloat(Total) + parseFloat(Amount);
                }
            });

            var t1 = (parseFloat(Total) * parseFloat(decValue)) / $('#hdnExpGrossAmt_bill').val();
            var t2 = (parseFloat(Total) - parseFloat(t1)).toFixed(2);


            var l = $('#tbl_Expense_bill tr.myData_bill').find("td[chk-tax='" + TaxID + "']").length;
            if (l > 0) {
                var E = "{TaxTypeID: '" + TaxID + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/Item/Load_TaxTypeDetail',
                    data: E,
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        var data = response.Data;
                        if (response.Status == 200) {
                            for (var i = 0; i < data.length; i++) {
                                var taxPert = data[i].TaxPer;
                                var taxTypeDet = data[i].TaxTypeDet;
                                var taxTypeDetID = data[i].TaxTypeDetId;

                                var calAmount = parseFloat((parseFloat(t2) * parseFloat(taxPert)) / 100);
                                Bind_TAXCal_bill(TaxID, taxTypeDetID, taxTypeDet, t2, calAmount);
                            }
                            Calculate_FinalTotal_bill();
                        }
                    }
                });

            }
            else {
                $('#tbl_SubExpense_bill tbody').find("tr[chk-tax='" + TaxID + "']").remove();
            }
        }
        else {
            $("#tbl_SubExpense_bill tbody tr.mysubData_bill").remove();
        }
    }
}

function img_bill_delete() {

    $('#ExpIframe_bill').attr('src', '');
    $("#ExpenseflPic_bill").val('');

    var tableName = "", ID = "", urls = "";
    transactionType = "Bill";
    ID = $('#hdnBillID').val();
    urls = "~/UploadItemImage/Bill/";

    if (ID != "") {
        var E = "{transactionType: '" + transactionType + "', ID: '" + ID + "', urls: '" + urls + "'}";
        $.ajax({
            url: '/Accounts_Form/Item/Delete_Picture',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

            }
        });
    }
}
function Get_Item_Wise_Tax(AccCode)
{
    var S = "{TransactionType:'" + "ItemWise_Tax" + "' , Desc:'" + "" + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "', VendorCode:'" + AccCode + "'}"; //alert(S);
    $.ajax({
        url: '/Accounts_Form/Item/Account_Description',
        type: 'POST',
        data: S,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (D) {
            var t = D.Data;
            var taxTypeID = t[0].AccountCode; 
            $('#ddlExpenseTax_bill').val('');
            if (taxTypeID != null) {
                $('#ddlExpenseTax_bill').val(taxTypeID);
            }
        }
    });
}
function PrintVouchers() {

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: "Voucher",
		 Database:''
    });

    detail.push({
        VoucherNumber: $('#hdnBillVoucherNo').val(),
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();

}

// VOUCHER GRID CALCULATIONS
function Bind_AccountVoucher(BillID, VendorCode) {
    var E = "{BillID: '" + BillID + "', VendorCode: '" + VendorCode + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_AccountInformation',
        data: E,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var data = response.Data;
            if (response.Status == 200) {
                Load_AccountInformation(data);
            }
        }
    });
}
function Load_AccountInformation(detail) {
   
    $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').remove();

    if (detail.length > 0) {

        var drcrAmt = 0;

        var table = $('#tbl_Expense_Voucher_bill tbody');

        $.each(detail, function (key, value) {

            table.append("<tr class='myData_voucher_bill'>"
                + "<td style='display:none' class='cls_Order' orderno=" + value.ORD +">" + value.ORD + "</td>"

                + "<td style='text-align:center'>"
                + "<input type= 'text' class='cls_AccountDesc form-control' id='desc_" + key + "'  value='" + ((value.AccountDescription == null || value.AccountDescription == 'null') ? "" : value.AccountDescription) + "'/>"
                + "<input type= 'hidden' class='cls_hdnAccountDesc form-control' value='" + value.AccountCode + "'/>"
                + "</td>"

                + "<td style='text-align:center'><select id='drcr_" + key + "' class='cls_drcr form-control' onchange='Enable_DisableVch_bill(this)' >"
                + "<option value=''>Dr/Cr</option>"
                + "<option value='D'>DR</option>"
                + "<option value='C'>CR</option>"
                + "</select></td>"

                + "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal' id='dr_" + key + "'  value='" + (value.DRCR == "D" ? (value.Amt == null ? 0 : value.Amt) : 0) + "' onkeyup='Vch_Cal_Amount()' /></td>"
                + "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal' id='cr_" + key + "'  value='" + (value.DRCR == "C" ? (value.Amt == null ? 0 : value.Amt) : 0) + "' onkeyup='Vch_Cal_Amount()' /></td>"

                + "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row(this)' /></td>" +

                + "</tr>");

            $('#drcr_' + key).val(value.DRCR);
        });
        //$('#lblBillVoucherNo').text(detail[0].VoucherNo == null ? "" : (" - " + detail[0].VoucherNo));
        //$('#hdnBillVoucherNo').val(detail[0].VchNo);
        //detail[0].VchNo == "" ? ($('#btnPrintVch').css('display', 'none')) : ($('#btnPrintVch').css('display', ''));

        Calculate_TotalVch_bill();
        Add_PlusButton();
    }
}
function Add_PlusButton()
{
    var html = "";
    $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').closest('tr').find('.cls_btnAdd').html('');

    html = "<img src='/Content/Images/add.png' title='Add' style='height:20px; width: 20px; cursor:pointer;' onclick='Add_Row(this)' />";
    
    var len = $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').length; 
    if (len == 0)
    {
        $('#tbl_Expense_Voucher_bill tr.trfooter_voucher_bill').remove();
        $('#tbl_Expense_Voucher_bill thead th.cls_headerAdd').html(html);
    }
    else
    {
        $('#tbl_Expense_Voucher_bill thead th.cls_headerAdd').html('');
        $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill:last td:last>.cls_btnAdd').html(html);
    }
}
function Add_Row(ID)
{
    var table = "";
    var len = $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill').length; 
    if (len > 0)
        table = $('#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill:last');
    else {
        table = $('#tbl_Expense_Voucher_bill tbody');
    }

    var orderNo = 1; 
    var arr = $("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill td.cls_Order").map(function (ind, val) { return Number($(val).attr("orderno")) }).sort(function (a, b) { return b - a });
    if (arr.length > 0)
    {
        orderNo = arr[0] + 1;
    }

    var html = "";
    html += "<tr class='myData_voucher_bill'>";
    html += "<td style='display:none' class='cls_Order' orderno=" + orderNo + ">" + orderNo + "</td>";

    html += "<td style='text-align:center'>"
    html += "<input type= 'text' class='cls_AccountDesc form-control' id='desc_" + orderNo + "'  value='' />";
    html += "<input type= 'hidden' class='cls_hdnAccountDesc form-control' value='' />";
    html += "</td>";

    html += "<td style='text-align:center'><select id='drcr_" + orderNo + "' class='cls_drcr form-control' onchange='Enable_DisableVch_bill(this)' >";
    html += "<option value=''>Dr/Cr</option>";
    html += "<option value='D'>DR</option>";
    html += "<option value='C'>CR</option>";
    html += "</select></td>";

    html += "<td><input type='text' style='text-align:right' class='cls_drAmount form-control allownumericwithdecimal' id='dr_" + orderNo + "'  value='' onkeyup='Vch_Cal_Amount()' /></td>";
    html += "<td><input type='text' style='text-align:right' class='cls_crAmount form-control allownumericwithdecimal' id='cr_" + orderNo + "'  value='' onkeyup='Vch_Cal_Amount()' /></td>";

    html += "<td style='text-align:center;'><span class='cls_btnAdd'></span>&nbsp;&nbsp;&nbsp;<img src='/Content/Images/Delete.png' title='Delete' style='height:20px; width: 20px; cursor:pointer; float:right;' onclick='Delete_Row(this)' /></td>";
    html += "</tr>";

    if(len >0)
        table.after(html);
    else
        table.append(html);

    Add_PlusButton(); Calculate_TotalVch_bill();
}
function Delete_Row(ID)
{
    $(ID).closest('tr').remove(); Calculate_TotalVch_bill(); Add_PlusButton(); 
}
function Enable_DisableVch_bill(ID)
{
    var drcr = $(ID).closest('tr').find('.cls_drcr').val();
    if (drcr != "") {
        $(ID).closest('tr').find('.cls_drAmount').val(0);
        $(ID).closest('tr').find('.cls_crAmount').val(0);

        $(ID).closest('tr').find('.cls_drAmount').prop('disabled', false); 
        $(ID).closest('tr').find('.cls_crAmount').prop('disabled', false); 
        if (drcr == "D")
        {
            $(ID).closest('tr').find('.cls_drAmount').focus(); $(ID).closest('tr').find('.cls_drAmount').select(); 
            $(ID).closest('tr').find('.cls_crAmount').prop('disabled', true); 
        }
        if (drcr == "C") {
            $(ID).closest('tr').find('.cls_crAmount').focus(); $(ID).closest('tr').find('.cls_crAmount').select(); 
            $(ID).closest('tr').find('.cls_drAmount').prop('disabled', true); 
        }
        Vch_Cal_Amount();
    }
}
function Calculate_TotalVch_bill() {

    var DRTotal = 0, CRTotal=0;
    var html = ""

    if ($("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val(); 
            var CRAmount = $(this).find('.cls_crAmount').val();
            DRTotal = parseFloat(DRTotal) + parseFloat(DRAmount == "" ? 0 : DRAmount);
            CRTotal = parseFloat(CRTotal) + parseFloat(CRAmount == "" ? 0 : CRAmount);
        });
    }

    $('#tbl_Expense_Voucher_bill .trfooter_voucher_bill').remove();

    html += "<tr class='trfooter_voucher_bill' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='2'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clsdrTotal' > " + (DRTotal).toFixed(2) + "</span></td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'>&#x20b9; <span class='clscrTotal' > " + (CRTotal).toFixed(2) + "</span></td>"
        + "<td></td>"
    html + "</tr>";

    $('#tbl_Expense_Voucher_bill tr:last').after(html)
}
function Vch_Cal_Amount()
{
    var DRTotal = 0, CRTotal = 0;

    if ($("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").length > 0) {
        $("#tbl_Expense_Voucher_bill tbody tr.myData_voucher_bill").each(function (index, value) {
            var DRAmount = $(this).find('.cls_drAmount').val(); 
            var CRAmount = $(this).find('.cls_crAmount').val();
            if (Math.abs(DRAmount) > 0 || Math.abs(CRAmount) > 0) {
                DRTotal = parseFloat(DRTotal) + parseFloat(Math.abs(DRAmount) == "" ? 0 : Math.abs(DRAmount));
                CRTotal = parseFloat(CRTotal) + parseFloat(Math.abs(CRAmount) == "" ? 0 : Math.abs(CRAmount));
            }
        });
    }

    if ($('#tbl_Expense_Voucher_bill .trfooter_voucher_bill').length > 0) {
        $('#tbl_Expense_Voucher_bill .trfooter_voucher_bill').find('.clsdrTotal').text((DRTotal).toFixed(2));
        $('#tbl_Expense_Voucher_bill .trfooter_voucher_bill').find('.clscrTotal').text((CRTotal).toFixed(2));
        return;
    }
}

function Hide_Show_EditDelete(ID, Con)
{
    if (Con == 2) {
        $(ID).closest('tr').find('.del_img').removeClass('fa-trash-o');
        $(ID).closest('tr').find('.edit_img').removeClass('fa-edit');
    }
    else
    {
        $(ID).closest('tr').find('.del_img').addClass('fa-trash-o');
        $(ID).closest('tr').find('.edit_img').addClass('fa-edit');
    }
}
function Get_PO_CreditTerms()
{
    var S = "{TransactionType:'" + "ItemWise_Tax" + "' , Desc:'" + "" + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "', VendorCode:'" + AccCode + "'}"; //alert(S);
    $.ajax({
        url: '/Accounts_Form/Item/Account_Description',
        type: 'POST',
        data: S,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (D) {
            var t = D.Data;
            var taxTypeID = t[0].AccountCode;
            $('#ddlExpenseTax').val('');
            if (taxTypeID != null) {
                $('#ddlExpenseTax').val(taxTypeID);
            }
        }
    });
}


function CalculateTCS()
{
    var TaxID = $('#ddlExpenseTCSPerValue_bill').val();
    var Total = $('#txtExpNetAmount_bill').val();
    if (Total == "" || Total <= 0) { alert('Sorry ! Please Check total Net Amount for TCS Percentage calculation.'); $('#ddlExpenseTCSPerValue_bill').val(''); return false; }

    if (TaxID != "")
    {
        var E = "{TaxTypeID: '" + TaxID + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/Item/Load_TaxTypeDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data; 
                if (response.Status == 200) {
                    if (data.length > 0) {

                        var table = $('#tbl_TCSSubExpense_bill tbody');
                        table.empty();
                        var html = "", FTotal=0;

                        for (var i = 0; i < data.length; i++) {

                            var taxPert = data[i].TaxPer;
                            var taxTypeDet = data[i].TaxTypeDet;
                            var taxTypeDetID = data[i].TaxTypeDetId;
                            var calAmount = parseFloat((parseFloat(Total) * parseFloat(taxPert)) / 100);
                            FTotal = parseFloat(FTotal) + parseFloat(calAmount);

                            html += "<tr class='mysubData_bill  cls_" + TaxID + "'  chk-tax=" + TaxID + ">"
                                + "<td style='text-align:right; display:none;' class='aa' >" + TaxID + "</td>"
                                + "<td style='text-align:right; display:none;' class='bb'>" + taxTypeDetID + "</td>"
                                + "<td style='text-align:right' class='clsTaxDes' >" + (taxTypeDet + Total) + "</td>"
                                + "<td style='text-align:right' class='clsTaxNetAmount' >" + (calAmount).toFixed(2) + "</td>"
                            html + "</tr>";
                           
                        }
						 $('#tbl_TCSSubExpense_bill tbody tr.trtcsfooter_bill').empty();
                        if (parseFloat(FTotal) > 0)
                        {
                            html += "<tr class='trtcsfooter_bill' style='background-color:#F5F5F6' >"
                                + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
                                + "<td style='text-align:right; font-weight:bold;'>&#x20b9;<span class='clsTCSSubTotalTax' >" + (FTotal).toFixed(2) + "</span></td>"
                            html + "</tr>";
                        }
                        table.append(html);
                    }

                    $('.clsTcs').css('display', '');
                     Calculate_AdjCal();
                }
            }
        });
    }
    else
    {
        var table = $('#tbl_TCSSubExpense_bill tbody');
        table.empty();
 $('#tbl_TCSSubExpense_bill tbody tr.trtcsfooter_bill').empty();
       Calculate_AdjCal();
    }
}
function Roundoff(Amount)
{
    var keyValue = ""; 
    var E = "{Amount: '" + Amount + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Roundoff',
        data: E,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var t = data.Data;
            keyValue = t[0].DecimalNumber;
        }
    });
    return keyValue; 
}

function AccountOpeningDate() {
    var V = "{MenuID : '" + 80 + "'}";
    $.ajax({
        url: '/Accounts_Form/VoucherMaster/AccountOpeningDate_MenuWise',
        data: V,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (D) {
            var t = D.Data;
            if (t.length > 0 && D.Status == 200) {

                Acc_OpenDate = t;
                //$('.clsOpeningDate').html(Acc_OpenDate);
            }
        }
    });
}
function CheckDate() {

    var menudateCheck = Acc_OpenDate; //alert(menudateCheck);
    var dateCheck = $("#txtExpPurOrderDate_bill").val();
    var FinYear = $("#ddlGlobalFinYear").val();
    var finyear = $("#ddlGlobalFinYear option:selected").text();
    var dateTo = ''; var dateFrom = '';

    if (finyear != "" || (dateCheck == "" || dateCheck != "")) {
        var sFinYear = finyear.split('-');
        var s = sFinYear[0];
        var t = sFinYear[1];

        var dateFrom = "01/04/" + s;
        var dateTo = "31/03/" + t;

        // dateFrom = "01/04/" + s;
        // dateTo = "01/04/" + t;



        //------------------------------------------------------------TODAY DATE--------------------------------------
        today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;
        //-------------------------------------------------------------------------------------------------------------
        var d1 = dateFrom.split("/");
        var d2 = dateTo.split("/");
        var c = dateCheck.split("/");
        var m = menudateCheck.split("/");

        var from = new Date(d1[2], parseInt(d1[1]) - 1, d1[0]); //alert(from);  
        var to = new Date(d2[2], parseInt(d2[1]) - 1, d2[0]); //alert(to);
        var check = new Date(c[2], parseInt(c[1]) - 1, c[0]); //alert(check);
        var menu = new Date(m[2], parseInt(m[1]) - 1, m[0]); //alert(menu);

        //------------------------------------------------------- MENU DATE TO PLUS ONE DATE --------------------------------
        var ss = new Date(menu.getFullYear(), menu.getMonth(), menu.getDate() + 1);
        var ddd = ss.getDate();
        var mmm = ss.getMonth() + 1; //January is 0!
        var yyyyy = ss.getFullYear();
        if (ddd < 10) {
            ddd = '0' + ddd;
        }
        if (mmm < 10) {
            mmm = '0' + mmm;
        }
        ss = ddd + '/' + mmm + '/' + yyyyy;
        //---------------------------------------------------------------------------------------------------------------------
        //checking menu date is, in selected finyear
        //alert(menu); alert(from); alert(to); 
        if (check >= from && check <= to) {
            if (check >= menu && check <= to) {
                //correct
                //alert(2);
                return true;
            }
            else {
                //wrong
                //alert(3);
                toastr.error('Sorry ! Enter Bill Date should be between  ' + menudateCheck + '-' + dateTo + '.', 'Warning'); $("#txtExpPurOrderDate_bill").val(); $("#txtExpPurOrderDate_bill").focus(); return false;
            }
        }
        else {
            //alert(1);
            toastr.error('Sorry ! Enter Bill Date should be between  ' + dateFrom + '-' + dateTo + '.', 'Warning'); $("#txtExpPurOrderDate_bill").val(); $("#txtExpPurOrderDate_bill").focus(); return false;
        }

    }

}
function CheckClosingStatus_TaxInvoice() {
    var a = $.trim($("#txtExpPurOrderDate_bill").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 80 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Save_Bill(1);
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}

