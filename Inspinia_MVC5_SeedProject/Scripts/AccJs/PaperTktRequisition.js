﻿$(document).ready(function () {
    Auto_Denomination();
    Auto_Requisition();
    BindGrid();
    RequiDate();
    BindSectorTo();

    $('#btnReqAdd').click(function () {
        Add_Requisition();
    });

    $('#btnSave').click(function () {
        Save();
    });

    $('#btnRefresh').click(function () {
        Blank();
    });

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#txtDenomination').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtDenomination").val('');
            $("#hdnDenominationID").val('');
        }
        if (iKeyCode == 46) {
            $("#txtDenomination").val('');
            $("#hdnDenominationID").val('');
        }
    });
});

function RequiDate() {
    $('#txtRequisitionDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtRequisitionDate').val(output);
}

function Add_Requisition()
{
    if ($("#hdnDenominationID").val() == "") { $("#txtDenomination").focus(); return false; }
    if ($("#txtQty").val() == "") { $("#txtQty").focus(); return false; }
    if ($("#ddlUpDown").val() == "") { $("#ddlUpDown").focus(); return false; }
    var chkAccCode = $("#hdnDenominationID").val();

    var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Denomination is Already Available.'); $("#txtDenomination").val(''); $("#hdnDenominationID").val(''); return false;
    }

    var html = ""

    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnDenominationID").val() + "' >" + $("#hdnDenominationID").val() + "</td>"
        + "<td class='denomination'>" + $("#txtDenomination").val() + "</td>"
        + "<td class='qty'>" + $("#txtQty").val() + "</td>"
        + "<td class='updownid' style='display:none;'>" + $("#ddlUpDown").val() + "</td>"
        + "<td class='updown'>" + $("#ddlUpDown option:selected").text() + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_row(this);' /></td>"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtDenomination").val(''); $("#hdnDenominationID").val(''); $("#txtQty").val(''); $("#ddlUpDown").val('');
}

function Delete_row(ID)
{
    $(ID).closest('tr').remove(); 
}

function Auto_Denomination() {
    var TransType = 'Denomination';
    var Denomination = $.trim($('.denomination').val());

    $('.denomination').autocomplete({
        source: function (request, response) {
            var S = "{TransType: '" + TransType + "', Denomination:'" + Denomination + "'}";
            $.ajax({
                url: '/Accounts_Form/PaperTkt/Get_Denomination',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.DenominationRemarks,
                                DenominationId: item.DenominationId
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnDenominationID').val(i.item.DenominationId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function Auto_Requisition() {
    var TransType = 'Requisition';
    var Requisition = $.trim($('#txtReqNo').val());

    $('#txtReqNo').autocomplete({
        source: function (request, response) {
            var S = "{TransType: '" + TransType + "', Requisition:'" + Requisition + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";
            $.ajax({
                url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {
                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.TktRequisitionNo,
                                TktRequisitionId: item.TktRequisitionId
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnRequisitionID').val(i.item.TktRequisitionId);
            Bind(i.item.TktRequisitionId);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function Bind(RequisitionId)
{
    if (RequisitionId != '')
    {
        var S = "{TransType: '" + "RequisitionByID" + "', Requisition:'" + RequisitionId + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";

        $.ajax({
            url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: S,
            dataType: 'json',
            success: function (data) {
                var a = data.Data;
                var t1 = a["Table"]; //alert(JSON.stringify(t1)); alert(t1[0].TktRequisitionIdDt);
                var t2 = a["Table1"];

                $("#txtRequisitionDate").val(t1[0].TktRequisitionIdDt); $("#txtRemarks").val(t1[0].TktRequisitionRemarks); $("#ddlSectorto").val(t1[0].SectorIdTo);

                var $this = $('#tbl .test_0');
                $parentTR = $this.closest('tr');

                if (t2.length > 0)
                {
                    var html = "";
                    for (var i = 0; i < t2.length; i++) {
                        html += "<tr class='myData'>"
                            + "<td style='display:none;' class='clsAccCode' chk-data='" + t2[i].DenominationId + "' >" + t2[i].DenominationId + "</td>"
                            + "<td class='denomination' >" + t2[i].Denomination + "</td>"
                            + "<td class='qty'>" + t2[i].TktRequisitionQty + "</td>"
                            + "<td class='updown' style='text-align:center;'>" + t2[i].UPDN + "</td>"
                            + "<td class='updownid' style='display:none;'>" + t2[i].UPDNID + "</td>"
                            + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='Delete_row(this);' /></td>"
                        html + "</tr>";
                    }
                    $parentTR.after(html);
                }
            }
        });
    }
}

function BindGrid() {
    var E = "{TransType: '" + "AllData" + "', Requisition: '" + "" + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {

            var a = data.Data;
            var t = a["Table"];

            var columnData = [
                { "mDataProp": "TktRequisitionNo" },
                { "mDataProp": "TktRequisitionIdDt" },
                { "mDataProp": "TktRequisitionRemarks" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = '<div><select onchange="Execute(this, \'' + row.TktRequisitionId + '\')">';

                        if (row.Edit == 1)
                        {
                            str += "<option value=''>Select</option>";
                            str += "<option value='1' style='display:none'>Edit</option>";
                            str += "<option value='2' style='display:none'>Delete</option>";
                            str += "<option value='3'>Print</option>";
                            str += "</select></div>";
                        }
                        else
                        {
                            str += "<option value=''>Select</option>";
                            str += "<option value='1'>Edit</option>";
                            str += "<option value='2'>Delete</option>";
                            str += "<option value='3'>Print</option>";
                            str += "</select></div>";
                        }

                        return str;
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": 3,
                        "className": "text-center",
                        "width": "10%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });

        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });
}

function Execute(ID, ReqId) {
    var rID = $(ID).val();
    if (rID != "") {
        //EDIT
        if (rID == 1) {
            Edit(ReqId);
        }
        //DELETE
        if (rID == 2) {
            Delete(ReqId);
        }
        if (rID == 3) {
            Print(ReqId);
        }
    }
}

function Print(ReqId)
{
   // alert(ReqId); return false;
    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "Ticket_Requisition.rpt",
        FileName: "Tickt_Requisition",
        Database: ''
    });

    detail.push({
        TransType: "Print",
        FinYear: '',
        RequisitionID: ReqId,
        Requisition:'',
        RequisitionDate:'',
        DenominationID:0,
        Remarks:'',
        Qty: 0.0,
        UpDown: '',
        SectorId: 0,
        SectorTo: 0,
        UserID:0
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function Edit(ReqId) {
    $("#hdnRequisitionID").val(ReqId);
    Bind(ReqId)
}

function Delete(ReqId) {
    swal({
        title: "Warning",
        text: "Are you sure want to Delete this Requisition ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{TransType: '" + "Cancel" + "', Requisition: '" + ReqId + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + localStorage.getItem("FinYear") + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
                    contentType: "application/json; charset=utf-8",
                    data: E,
                    dataType: "json",
                    success: function (data, status) {
                        var a = data.Data;
                        var t = a["Table"]; //alert(JSON.stringify(t1)); alert(t1[0].TktRequisitionIdDt);
                        var msgcnt = t[0].MSGCOUNT;
                        var msg = t[0].MSG;

                        if (msgcnt == '1') {
                            swal({
                                title: "Success",
                                text: msg,
                                type: "success",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "";
                                    }
                                });
                        }
                        else {
                            swal({
                                title: "Error",
                                text: msg,
                                type: "error",
                                confirmButtonColor: "#AEDEF4",
                                confirmButtonText: "OK",
                                closeOnConfirm: true,
                            });
                        }
                    }
                });
            }

        });
}

function Blank()
{
    location.reload();
}

function Save()
{
    if ($("#txtRequisitionDate").val() == "") { $("#txtRequisitionDate").focus(); return false; }
    if ($("#ddlSectorto").val() == "") { $("#ddlSectorto").focus(); return false; }
    //if ($("#txtRemarks").val() == "") { $("#txtRemarks").focus(); return false; }

    var ArrList = [];
    var grdLen = $('#tbl tbody tr.myData').length; 
    if (grdLen > 0) {
        $('#tbl tbody tr.myData').each(function () {

            var Denomination = $(this).find('.denomination').text();
            var Qty = $(this).find('.qty').text();
            var DenominationID = $(this).find('.clsAccCode').text();
            var UpDown = $(this).find('.updownid').text();

            ArrList.push({
                'Denomination': Denomination, 'DenominationID': DenominationID, 'Qty': Qty, 'UpDown': UpDown
            });
        });
    }
    else {
        alert('Please add denomination details'); return false;
    }

    var E = "{RequisitionID: '" + $("#hdnRequisitionID").val() + "', RequisitionDate: '" + $("#txtRequisitionDate").val() + "', SectorTo: '" + $("#ddlSectorto").val() + "', Remarks: '" + $("#txtRemarks").val() + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "', ReqDetail: " + JSON.stringify(ArrList) + "}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/PaperTktRequisition/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t != "") {

                    swal({
                        title: "Success",
                        text: 'Requisition generated Successfully !!\nRQ No. ' + t ,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });
                
            }
        }
    });
}

function BindSectorTo() {
    var SectorID = localStorage.getItem('SectorID');

    var S = "{TransType: '" + "SectorTo" + "', Requisition:'" + "" + "', SectorID: '" + SectorID + "', FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";

    $.ajax({
        url: '/Accounts_Form/PaperTktRequisition/Get_Requisition',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: S,
        dataType: 'json',
        success: function (data) {
            var a = data.Data;
            var t = a["Table"];
            var sectorid = t[0].ParentRefTagId; 
            $('#ddlSectorto').val(sectorid);
        }
    });
}
