﻿$(document).ready(function () {


   // select DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, 'May 2019') + 1, 0))
    var frm = $('#frmInv');
    //Inventory.validation();
    Inventory.stockdate();
    //Inventory.accountDesAutoComplete();

    $('.detail').css('display', 'none');

    //Allow only Decimal with Integer Value
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#txtAccountDesc').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#hdnAuto").val('');
            $("#hdnPAuto").val('');
        }
        if (iKeyCode == 46) {
            $("#hdnAuto").val('');
            $("#hdnPAuto").val('');
        }
    });

    $('#btnsearch').click(function () {
      
        if ($('#dtStockDate').val() == "") { $('#dtStockDate').focus(); return false; }

        Inventory.getAllInvDetails();

        //if (!frm.valid()) {
        //    return false;
        //} else {
            
           // Inventory.getAllInvDetails();
        //}
    });

    $('#btnAdd').click(function () {

        if ($('#hdnAuto').val() == "") { $('#txtAccountDesc').focus(); return false; }
        if ($('#txtAmount').val() == "" || $('#txtAmount').val() < 0) { $('#txtAmount').focus(); return false; }
       
        //$('#tbl tbody tr').find("td[chk-data='" + $('#hdnAuto').val() + "']").text(parseFloat($('#txtAmount').val()).toFixed(2));

        Inventory.Save($('#txtAmount').val());
        $('#txtAmount').val(''); $('#hdnAuto').val(''); $('#hdnPAuto').val(''); $('#hdnMTabID').val(''); $('#hdnTabID').val(''); $('#txtAccountDesc').val('');

        // Inventory.Buttonval = $(this).attr('data-attrval');
        //if (!frm.valid()) {
        //    return false;
        //} else {
        //    Inventory.save(function (res) {
        //        Inventory.getAllInvDetails();
        //        Inventory.reset();
        //    });
        //}
    });

    $('#btnRefresh').click(function () {
        Inventory.reset();
    });

    $('#btnPost').click(function () {
        Inventory.Post();
    });
    $('.clsVchNo').click(function () {
        Inventory.rowVchPrint(this);
    });

    $(document).on('click', '.btnEdit', function () {
        Inventory.rowEdit(this);
    });
    $(document).on('click', '.btnCalculate', function () {
        Inventory.rowCalculate(this);
    });
    $(document).on('click', '.btnPrint', function () {
        var AccCode = $(this).closest('tr').find('.AccountCode').html();
        if (AccCode == "0000002532")
            Inventory.rowPrint1(this);
        else
            Inventory.rowPrint2(this);
    });

    $(document).on('click', '#btnDel', function () {
        Inventory.rowDel(this, function (response) {
            Inventory.getAllInvDetails();
        });
    });
});


var Inventory = {
    Id: '',
    Buttonval:'',
    stockdate: function () {
        //Only Month and Year
        $('#dtStockDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "M yy",
            showButtonPanel: true,
            currentText: "This Month",
            onChangeMonthYear: function (year, month, inst) {
                $(this).val($.datepicker.formatDate('M yy', new Date(year, month - 1, 1)));
            },
            onClose: function (dateText, inst) {
                var month = $(".ui-datepicker-month :selected").val();
                var year = $(".ui-datepicker-year :selected").val();
                $(this).val($.datepicker.formatDate('M yy', new Date(year, month, 1)));
            }
        }).focus(function () {
            $(".ui-datepicker-calendar").hide();
        });
        
    },

    accountDesAutoComplete: function () {

        var obj = {};
        obj.AccountDescription = $('#txtAccountDesc').val();
        obj.SectorID = $('#ddlSector').val();

        $('#txtAccountDesc').autocomplete({

            source: function (request, response) {
                //var V = "{ AccountDescription :'" + $('#' + ref).val() + "', SectorID: '" + $('#ddlSector').val() + "'}";
                $.ajax({
                    url: '/Accounts_Form/Inventory/AccontHeadAuto',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(obj),
                    dataType: 'json',
                    success: function (serverResponse) {
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {
                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            //console.log(SubledgerAutoComplete);
                            response(SubledgerAutoComplete);
                        }
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                $("#hdnAuto").val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
        
    },
    getFormdata: function () {
        var ctrl_InvDate = $("#dtStockDate");
        var ctrl_txtAccountDesc = $('#txtAccountDesc');
        var ctrl_hdnAuto = $('#hdnAuto');
        var ctrl_txtAmount = $('#txtAmount');
        

        return {
            Id: Inventory.Id,
            InvDate: ctrl_InvDate.val(),
            AccountDescription: ctrl_txtAccountDesc.val(),
            AccountCode: ctrl_hdnAuto.val(),
            Amount: ctrl_txtAmount.val()
        }
    },
    save: function (callback) {
        //var obj = {};
        //obj.Id = '0';
        //obj.AccountDescription = $('#txtAccountDesc').val();
        //obj.AccountCode = $('#hdnAuto').val();
        //obj.Amount = $('#txtAmount').val();
        //obj.InvDate = $('#dtStockDate').val();

        $.ajax({
            url: '/Accounts_Form/Inventory/InsUpdInventorydetails',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Inventory.getFormdata()),
            dataType: "json",
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
            },
            error: function () { }
        });


    },
    rowEdit: function (ref) {
     
        $('#txtAccountDesc').val($(ref).closest('tr').find('.AccountDescription').html());
        $('#hdnAuto').val($(ref).closest('tr').find('.AccountCode').html());
        $('#hdnPAuto').val($(ref).closest('tr').find('.ParentAccountCode').html());
        $('#txtAmount').val($(ref).closest('tr').find('.Amount').html());
        $('#hdnMTabID').val($(ref).closest('tr').find('.MstTabID').html());
        $('#hdnTabID').val($(ref).closest('tr').find('.TabID').html());

        $('.detail').css('display', '');
    },
    rowCalculate: function (ref) {

        $('#txtAccountDesc').val($(ref).closest('tr').find('.AccountDescription').html());
        $('#hdnAuto').val($(ref).closest('tr').find('.AccountCode').html());
        $('#hdnPAuto').val($(ref).closest('tr').find('.ParentAccountCode').html());
        $('#txtAmount').val($(ref).closest('tr').find('.Amount').html());
        $('#hdnMTabID').val($(ref).closest('tr').find('.MstTabID').html());
        $('#hdnTabID').val($(ref).closest('tr').find('.TabID').html());

        //$('.detail').css('display', '');
        Inventory.Calculate(ref);
    },
    rowDel: function (ref,callback) {
        var obj = {};
        
        Inventory.Id = $(ref).closest('tr').find('.Id').html();
        obj.Id = Inventory.Id;
        $.ajax({
            url: '/Accounts_Form/Inventory/DeleteInvDetailsById',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: 'json',
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
            },
            error: function () { }
        });

    },
    rowPrint1: function (ref) {

        var final = {}; var master = []; var detail = [];
      
        master.push({
            ReportName: "AccHSDValuationOpening.rpt", //"AccHSDValuation.rpt"
            FileName: "Inventory",
            Database: 'Inventory'
        });

        detail.push({
            FromDate: $(ref).closest('tr').find('.FromDate').html(),
            ToDate: $(ref).closest('tr').find('.ToDate').html(),
            UNIT_COMBO_CODE: $(ref).closest('tr').find('.InvSectorID').html()
        });

        final = {
            Master: master,
            Detail: detail
        }

        window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final));
    },
    rowPrint2: function (ref) {

        var final = {}; var master = []; var detail = [];
        var AccCode = $(ref).closest('tr').find('.AccountCode').html();
        master.push({
            ReportName: "StoreLedger.rpt",
            FileName: "Inventory",
            Database: 'Inventory'
        });

        detail.push({
            FromDate: $(ref).closest('tr').find('.FromDate').html(),
            ToDate: $(ref).closest('tr').find('.ToDate').html(),
            UNIT_COMBO_CODE: $(ref).closest('tr').find('.InvSectorID').html(),
            ItemCategoryID: AccCode
        });

        final = {
            Master: master,
            Detail: detail
        }

        window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final));
    },
    rowPrint: function (ID)
    {
        var final = {}; var master = []; var detail = [];
        var rpid = $(ID).val(); 
        var AccCode = $(ID).closest('tr').find('.AccountCode').html();

        if (rpid != "")
        {
            //old
            if (rpid == 1)
            {
                var AccCode = $(ID).closest('tr').find('.AccountCode').html(); //alert(AccCode);// return false;
                Inventory.rowPrint1(ID);
            }
            //new
            if (rpid == 2) {
                master.push({
                    ReportName: "HSDOutSideValuation.rpt",   //StoreLedgerNew_SM.rpt
                    FileName: "Inventory",
                    Database: 'Inventory'
                });

                detail.push({
                    FromDate: $(ID).closest('tr').find('.FromDate').html(),
                    ToDate: $(ID).closest('tr').find('.ToDate').html(),
                    UNIT_COMBO_CODE: $(ID).closest('tr').find('.InvSectorID').html()
                });

                final = {
                    Master: master,
                    Detail: detail
                }

                window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final));
            }
            //consolidated
            if (rpid == 3) {
                master.push({
                    ReportName: "StoreLedgerNew_SM.rpt",
                    FileName: "Inventory",
                    Database: 'Inventory'
                });

                detail.push({
                    FromDate: $(ID).closest('tr').find('.FromDate').html(),
                    ToDate: $(ID).closest('tr').find('.ToDate').html(),
                    UNIT_COMBO_CODE: $(ID).closest('tr').find('.InvSectorID').html(),
                    ItemCategoryID: AccCode
                });

                final = {
                    Master: master,
                    Detail: detail
                }

                window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final));
            }
            if (rpid == 4) {
                master.push({
                    ReportName: "ConsolidatedSta.rpt",
                    FileName: "Inventory",
                    Database: 'Inventory'
                });

                detail.push({
                    FromDate: $(ID).closest('tr').find('.FromDate').html(),
                    ToDate: $(ID).closest('tr').find('.ToDate').html(),
                    UNIT_COMBO_CODE: $(ID).closest('tr').find('.InvSectorID').html(),
                    ItemCategoryID: AccCode
                });

                final = {
                    Master: master,
                    Detail: detail
                }

                window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final));
            }
        }
    },
    rowVchPrint: function (ref) {

        var final = {}; var master = []; var detail = [];

        master.push({
            ReportName: "VoucherCash.rpt",
            FileName: $('.clsVchNo').text(),
            Database: ''
        });

        detail.push({
            VoucherNumber: $(ref).attr('vchno'),
            sectorid: $("#ddlSector").val()
        });

        final = {
            Master: master,
            Detail: detail
        }

        var left = ($(window).width() / 2) - (950 / 2),
            top = ($(window).height() / 2) - (650 / 2),
            popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
        popup.focus();
    },

    getAllInvDetails: function () {
      
        var V = "{StockDate : '" + $("#dtStockDate").val() + "', SectorID : '" + $("#ddlSector").val() + "'}";
        $.ajax({
            url: '/Accounts_Form/Inventory/GetAllInventoryDetails',
            type: 'POST',
            contentType:'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (response) {
               
                Inventory.popAllInvDetails(response.Data);
                $('.clsbtn').css('display', '');
            },
            error: function () { }
        });


    },
    popAllInvDetails: function (data) {
     
        var html = "";
        var table = $("#tbl");
        var thead = table.find("thead");
        var tbody = table.find("tbody");

        thead.empty();
        tbody.empty();

        thead.append('<tr><th>AccountDescription</th><th>Amount</th><th>Edit</th><th>Calculate</th><th>Print</th></tr>');
        if (data.length > 0) {
            $(data).each(function (i, element) {
              
                html += "<tr>";
                html += "<td class='AccountDescription'>" + element.AccountDescription + "</td>";

                html += "<td class='MstTabID' style='display:none;'>" + element.MstTabID + "</td>";
                html += "<td class='TabID' style='display:none;'>" + element.TabId + "</td>";
                html += "<td class='AccountCode' style='display:none;'>" + element.AccountCode + "</td>";
                html += "<td class='ParentAccountCode' style='display:none;'>" + element.ParentAccountCode + "</td>";
                html += "<td class='FromDate' style='display:none;'>" + element.FromDate + "</td>";
                html += "<td class='ToDate' style='display:none;'>" + element.ToDate + "</td>";
                html += "<td class='InvSectorID' style='display:none;'>" + element.InvSectorID + "</td>";

                html += "<td class='Amount' style='text-align:right;' chk-data='" + element.AccountCode +"'>" + (element.Amount).toFixed(2) + "</td>";
                html += "<td style='text-align:center; color:blue;' ><span class='btnEdit' style='cursor:pointer; color:blue;'>Edit</span></td>";
                html += "<td style='text-align:center; color:blue; width:5%;' ><span class='btnCalculate' style='cursor:pointer; display:" + (element.IsCalculate == "1" ? "" : 'none') + "'>Calculate</span></td>";
                //html += "<td style='text-align:center; color:blue;' ><span class='btnPrint' style='cursor:pointer; display:" + (element.IsCalculate == "1" ? "" : 'none') + "'>Print</span></td>";

                if (element.AccountCode == "0000002532")
                    html += "<td style='text-align:center;'><select onchange='Inventory.rowPrint(this);'><option value=''>Select</option><option value='1'>HSD Fuel Stock Ledger</option><option value='2'>Out Side HSD Stock Ledger</option></select></td > ";
                else
                    html += "<td style='text-align:center;'><select onchange='Inventory.rowPrint(this);'><option value=''>Select</option><option value='3'>New Stock Ledger</option><option value='4'>Consolidated</option></select></td > ";

                html += "</tr>";
            });

            var RefVchNo = data[0].RefVoucherSlNo; //alert(VchNo);
            var VchNo = data[0].VoucherhNumber;
            $('.vchNo').css('display', '');
            $('.clsVchNo').text(RefVchNo);
            $('.clsVchNo').attr('vchno', VchNo);

            if (VchNo > 0)
            $('.lblPost').text('Re-Post');
        }
        else
        {
            $('.lblPost').text('Post');
        }
        tbody.append(html);
       
    },
    validation: function () {
        jQuery.validator.addMethod("validDate", function (value, element) {
            //return this.optional(element) || moment(value, "DD/MM/YYYY").isValid();
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        });
        $('#frmInv').validate({
            rules: {
               
                dtStockDate: {
                    required: true,
                    validDate: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtAccountDesc: {
                    required: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtAmount: {
                    required: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false
                        }
                    }
                }
                
                
            },
            messages: {
                
                dtStockDate: {
                    required: "Please Enter From date",
                    validDate: "Please enter a valid date in the format DD/MM/YYYY"
                },
                txtAccountDesc: {
                    required: "Please Select Inventory Description",
                },
                txtAmount: {
                    required: "Please Enter Amount",
                }
            }
        });
    },
    reset: function () {
        $('#txtAccountDesc').val('');
        $('#hdnAuto').val(''); $('#hdnPAuto').val('');
        $('#txtAmount').val('');
        $('#hdnMTabID').val(''); $('#hdnTabID').val(''); 
        $('#dtStockDate').val(''); 
        $("#tbl thead").empty();
        $("#tbl tbody").empty();
        $('.detail').css('display', 'none');
        $('.clsbtn').css('display', 'none');

        $('.vchNo').css('display', 'none');
        $('.clsVchNo').text('');
    },
    Save: function (Amount) {

        var V = "{TransType : '" + "Insert" + "', MTabID : '" + $("#hdnMTabID").val() + "', TabID : '" + $("#hdnTabID").val() + "', StockDate : '" + $("#dtStockDate").val() + "', AccountCode : '" + $("#hdnAuto").val() + "', ParentAccountCode : '" + $("#hdnPAuto").val() + "', Amount : '" + Amount + "', SectorID : '" + $("#ddlSector").val() + "'}";
        //alert(V);
        //return false;
        $.ajax({
            url: '/Accounts_Form/Inventory/Insert',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (response) {

                Inventory.getAllInvDetails();

            },
            error: function () { }
        });
    },
    Calculate: function () {

        var V = "{TransType : '" + "Calculate" + "', StockDate : '" + $("#dtStockDate").val() + "', AccountCode : '" + $("#hdnAuto").val() + "', SectorID : '" + $("#ddlSector").val() + "'}";

        //alert(V);
        //return false;

        $.ajax({
            url: '/Accounts_Form/Inventory/Calculate',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (data) {
                var TotalAmount = jQuery.parseJSON(data.Data); //alert(JSON.stringify(TotalAmount));

                Inventory.Save(TotalAmount);

            },
            error: function () { }
        });
    },
    Post: function () {

        var V = "{TransType : '" + "Post" + "', StockDate : '" + $("#dtStockDate").val() + "', SectorID : '" + $("#ddlSector").val() + "'}";

        //alert(V);
        //return false;

        $.ajax({
            url: '/Accounts_Form/Inventory/Post',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (data) {
                var t = data.Data; //alert(JSON.stringify(t));

                var RefVchNo = t[0].RefVoucherSlNo; //alert(VchNo);
                var VchNo = t[0].VoucherNumber;
                $('.vchNo').css('display', '');
                $('.clsVchNo').text(RefVchNo);
                $('.clsVchNo').attr('vchno', VchNo);
                $('.lblPost').text('Re-Post');
            },
            error: function () { }
        });
    }

}