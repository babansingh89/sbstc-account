﻿var rowCount = 0; var SubDetailID = 0;
var ArrayOpenListID = [];

$(document).ready(function () {

    Generate();
    Ledger_AutoComplete();
    $(document).on('keypress keyup blur, keydown', '.clsSubdetail', function (e) {
        var ths = $(this);
        var t = ths.attr('id');
        Autocomplete(t);
    });
});


function Generate() {

    var E = "{TransType:'" + "Mapping" + "', Desc:'" + "" + "', SectorID:'" + $("#ddlSector").val() + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingBudgetMapping/Load",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        success: function (D) {
            var t = D.Data;

            Show(t);

        }
    });
}
function Show(t) {
    var table = $('#datatable');
    var tbody = table.find('tbody');
    var tfoot = table.find('tfoot');
    tbody.empty();
    tfoot.empty();

    if (t.length > 0) {
        var html = "", tbl_Cnt1 = 0, tbl_Cnt2 = 0, tbl_Cnt3 = 0, tbl_Cnt4 = 0, tbl_Cnt5 = 0, TotalInocme = 0, TotalExpense = 0;

        for (var i = 0; i < t.length; i++) {
            var ClaimID = t[i].ClaimID;

            //Income
            if (ClaimID == 1) {
                html += "<tr class='myData'>"
                    + "<td>" + (tbl_Cnt1 == 0 ? "1." : "") + "</td>"
                    + "<td class='SubDetails' data-subdetailid='" + t[i].SubDetailID + "'>" + (tbl_Cnt1 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' >" + t[i].SubDetail + " </td>"
                    + "<td><input type='text' style='float:left; width:94%' class='form-control allownumericwithdecimal clsSubdetail' id='id_" + t[i].SubDetailID + "' value='" + t[i].BDesc + "' /><input type='hidden' class='hdn' value='" + t[i].BudgetID + "' /> <img src= '/Content/Images/Delete.png' class='clsDelete' style= 'width:18px;height:18px; float:right; margin-top:6px; cursor:pointer; display:" + ((t[i].BudgetID == "" || t[i].BudgetID == null) ? "none" : "") + "' title= 'Delete' onClick='Delete(this)' /></td>"
                    + "<td style='color:blue; text-align:center'><span onClick='AccountHead(this)' style='cursor:pointer; display:" + (t[i].isEditView > 0 ? "none" : "" ) + "'>ADD /</span>"
                    + "<span style= 'cursor:pointer;' onClick= 'AccountHead(this)' > VIEW</span></td>"

                html + "</tr>";
                tbl_Cnt1 = 1;
            }
            if (ClaimID == 2) {

                html += "<tr class='myData'>"
                    + "<td>" + (tbl_Cnt2 == 0 ? "" : "") + "</td>"
                    + "<td class='SubDetails' data-subdetailid='" + t[i].SubDetailID + "'>" + (tbl_Cnt2 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' style='cursor:pointer;' >" + t[i].SubDetail + " </td>"
                    + "<td><input type='text' style='float:left; width:94%' class='form-control allownumericwithdecimal clsSubdetail' id='id_" + t[i].SubDetailID + "' value='" + t[i].BDesc + "' /><input type='hidden' class='hdn' value='" + t[i].BudgetID + "' /> <img src= '/Content/Images/Delete.png' class='clsDelete' style= 'width:18px;height:18px; float:right; margin-top:6px; cursor:pointer; display:" + ((t[i].BudgetID == "" || t[i].BudgetID == null) ? "none" : "") + "' title= 'Delete' onClick='Delete(this)' /></td>"
                    + "<td style='color:blue; text-align:center'><span onClick='AccountHead(this)' style='cursor:pointer; display:" + (t[i].isEditView > 0 ? "none" : "") + "'>ADD /</span>"
                    + "<span style= 'cursor:pointer;' onClick= 'AccountHead(this)' > VIEW</span></td>"
                html + "</tr>";

                tbl_Cnt2 = 1;
            }

            //Expense
            if (ClaimID == 3) {

               html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt3 == 0 ? "A" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails + "'>" + (tbl_Cnt3 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td style='cursor:pointer;' ><span class='subdetails'>" + t[i].SubDetail + "</span></td>"
                   + "<td><input type='text' style='float:left; width:94%' class='form-control allownumericwithdecimal clsSubdetail' id='id_" + t[i].SubDetailID + "' value='" + t[i].BDesc + "' /><input type='hidden' class='hdn' value='" + t[i].BudgetID + "' /> <img src= '/Content/Images/Delete.png' class='clsDelete' style= 'width:18px;height:18px; float:right; margin-top:6px; cursor:pointer; display:" + ((t[i].BudgetID == "" || t[i].BudgetID == null) ? "none" : "") + "' title= 'Delete' onClick='Delete(this)' /></td>"
                   + "<td style='color:blue; text-align:center'><span onClick='AccountHead(this)' style='cursor:pointer; display:" + (t[i].isEditView > 0 ? "none" : "") + "'>ADD /</span>"
                   + "<span style= 'cursor:pointer;' onClick= 'AccountHead(this)' > VIEW</span></td>"
                html + "</tr>";
                tbl_Cnt3 = 1;
            }
            if (ClaimID == 4) {
                var display = '';

                html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt4 == 0 ? "B" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails + "'>" + (tbl_Cnt4 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td style='cursor:pointer;' ><span class='subdetails'>" + t[i].SubDetail + "</span></td>"
                    + "<td><input type='text' style='float:left; width:94%' class='form-control allownumericwithdecimal clsSubdetail' id='id_" + t[i].SubDetailID + "' value='" + t[i].BDesc + "' /><input type='hidden' class='hdn' value='" + t[i].BudgetID + "' /> <img src= '/Content/Images/Delete.png' class='clsDelete' style= 'width:18px;height:18px; float:right; margin-top:6px; cursor:pointer; display:" + ((t[i].BudgetID == "" || t[i].BudgetID == null) ? "none" : "") + "' title= 'Delete' onClick='Delete(this)' /></td>"
                    + "<td style='color:blue; text-align:center'><span onClick='AccountHead(this)' style='cursor:pointer; display:" + (t[i].isEditView > 0 ? "none" : "") + "'>ADD /</span>"
                    + "<span style= 'cursor:pointer;' onClick= 'AccountHead(this)' > VIEW</span></td>"
                html + "</tr>";
                tbl_Cnt4 = 1;
            }
            if (ClaimID == 5) {

                html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt5 == 0 ? "C" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails + "'>" + (tbl_Cnt5 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' style='cursor:pointer;' >" + t[i].SubDetail + "</td>"
                    + "<td><input type='text' style='float:left; width:94%' class='form-control allownumericwithdecimal clsSubdetail' id='id_" + t[i].SubDetailID + "' value='" + t[i].BDesc + "' /><input type='hidden' class='hdn' value='" + t[i].BudgetID + "' /> <img src= '/Content/Images/Delete.png' class='clsDelete' style= 'width:18px;height:18px; float:right; margin-top:6px; cursor:pointer; display:" + ((t[i].BudgetID == "" || t[i].BudgetID == null) ? "none" : "") + "' title= 'Delete' onClick='Delete(this)' /></td>"
                    + "<td style='color:blue; text-align:center'><span onClick='AccountHead(this)' style='cursor:pointer; display:" + (t[i].isEditView > 0 ? "none" : "") + "'>ADD /</span>"
                    + "<span style= 'cursor:pointer;' onClick= 'AccountHead(this)' > VIEW</span></td>"
                html + "</tr>";
                tbl_Cnt5 = 1;
            }
        }

        tbody.append(html);
       
    }
}
function Autocomplete(ID)
{
    var textValue = $('#' + ID).val(); 
    $('#' + ID).autocomplete({
        source: function (request, response) {

            var V = "{TransType:'" + "Budget" + "', Desc:'" + textValue + "', SectorID:'" + $("#ddlSector").val() + "'}"; //alert(V);
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/ClaimingBudgetMapping/Load",
                data: V,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.BDesc,
                                TabId: item.TabId
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        minLength: 0,
        select: function (e, i) {
            var TabId = i.item.TabId;
            $('#' + ID).closest('tr').find('.hdn').val(TabId);

            var SubdetailID = $('#' + ID).closest('tr').find('.SubDetails').attr('data-subdetailid'); 

            var E = "{TransType:'" + "BudgetMapping" + "', SubdetailID:'" + SubdetailID + "', TabId:'" + TabId + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/ClaimingBudgetMapping/Update",
                data: E,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (D) {
                    $('#' + ID).closest('tr').find('.clsDelete').css('display', '');
                }
            });
        },
    }).click(function () {
        $(ID).autocomplete('search', ($(this).val()));
    });
}
function Delete(ID)
{
    var SubDetailID = $(ID).closest('tr').find('.SubDetails').attr('data-subdetailid'); 
    //var TabID = $(ID).closest('tr').find('.hdn').val(); alert(claim);

    var E = "{TransType:'" + "BudgetMapping" + "', SubdetailID:'" + SubDetailID + "', TabId:'" + "" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingBudgetMapping/Update",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (D) {
            $(ID).closest('tr').find('.clsSubdetail').val('');
            $(ID).closest('tr').find('.hdn').val('');
            $(ID).closest('tr').find('.clsDelete').css('display', 'none');
        }
    });
}

function AccountHead(ID)
{
    SubDetailID = $(ID).closest('tr').find('.SubDetails').attr('data-subdetailid'); 
    var E = "{TransType:'" + "AccountHead" + "', SubdetailID:'" + SubDetailID + "', TabId:'" + "" + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingBudgetMapping/Update",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (D) {
            var t = D.Data; 
            var html = ""

            var $this = $('#tbl .test_0');
            $parentTR = $this.closest('tr');
            $('#tbl tbody tr.myData').empty();
            for (var i = 0; i < t.length; i++) {
                html += "<tr class='myData' >"
                    + "<td style='display:none;' class='clsAccCode' chk-data='" + t[i].AccountCode + "' >" + t[i].AccountCode + "</td>"
                    + "<td>" + t[i].AccountDescription + "</td>"
                    + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteInstDetail(this);' /></td>"
                html + "</tr>";
            }
            $parentTR.after(html);
            $('#dialog').modal('show');
        }
    });
  
}
function Ledger_AutoComplete()
{
    $('#txtEfftGL').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{'Desc':'" + $('#txtEfftGL').val() + "', MND: '" + 'L' + "'}",
                dataType: 'json',
                success: function (serverResponse) {

                    var SubledgerAutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            SubledgerAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(SubledgerAutoComplete);
                    }

                },
                error: function () { }
            });
        },
        select: function (e, i) {
            $('#hdnEffectAdd').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()))
    });
}
function EffectAdd()
{
    if ($("#hdnEffectAdd").val() == "") { $("#txtEfftGL").focus(); return false; }
    var chkAccCode = $("#hdnEffectAdd").val();

    var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Account Description is Already Available.'); $("#txtEfftGL").val(''); $("#hdnEffectAdd").val(''); return false;
    }
    var E = "{TransType:'" + "InsertAccountHead" + "', SubdetailID:'" + SubDetailID + "', AccCode:'" + chkAccCode + "'}";
   // alert(E); return false;
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingBudgetMapping/UpdateAccountHead",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (D) {
            var t = D.Data; //alert(JSON.stringify(t));
            var html = ""

            var $this = $('#tbl .test_0');
            $parentTR = $this.closest('tr');

            $('#tbl tbody tr.myData').empty();
            for (var i = 0; i < t.length; i++) {
                html += "<tr class='myData' >"
                    + "<td style='display:none;' class='clsAccCode' chk-data='" + t[i].AccountCode + "' >" + t[i].AccountCode + "</td>"
                    + "<td>" + t[i].AccountDescription + "</td>"
                    + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteInstDetail(this);' /></td>"
                html + "</tr>";
            }
            $parentTR.after(html);
        }
    });
    $("#txtEfftGL").val(''); $("#hdnEffectAdd").val('');
}
function DeleteInstDetail(ID) {
    var AccCode = $(ID).closest('tr').find('.clsAccCode').attr('chk-data'); 
    var E = "{TransType:'" + "DeleteAccountHead" + "', SubdetailID:'" + SubDetailID + "', AccCode:'" + AccCode + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingBudgetMapping/UpdateAccountHead",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (D) {
            var t = D.Data;
            var html = ""

            var $this = $('#tbl .test_0');
            $parentTR = $this.closest('tr');

            $('#tbl tbody tr.myData').empty();
            for (var i = 0; i < t.length; i++) {
                html += "<tr class='myData' >"
                    + "<td style='display:none;' class='clsAccCode' chk-data='" + t[i].AccountCode + "' >" + t[i].AccountCode + "</td>"
                    + "<td>" + t[i].AccountDescription + "</td>"
                    + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Delete' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteInstDetail(this);' /></td>"
                html + "</tr>";
            }
            $parentTR.after(html);
        }
    });
    //$(ID).closest('tr').remove();
}





