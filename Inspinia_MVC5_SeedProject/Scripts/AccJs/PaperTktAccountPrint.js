﻿var MNDOldCode = "";
var selectors = {
    'tree': '#treeview, #subtree',
    'input': '#input-search',
    'reset': '#btn-clear-search'
};
var lastPattern = '';



$(function () {
    var formhead = $('#Acheadtree');
    app.AccountRule();

    if ($(".pageName").val() === "AccountMasterPage") {
        app.populateAccount();
    } 
    app.validation();
    app.nodeClick();
    $(document).on('click', '.tt', function () {
        $('.tt').not(this).prop('checked', false);
        app.IsChecked();
    })

    $('input:radio[name=optradio]').click(function () {
        app.radiochange(this);
    });

    $('#btnsubmit').click(function () {

        if (app.ParentId == "") { $('#txtparent').focus(); return false; }
        if ($('#txtacdesc').val() == "") { $('#txtacdesc').focus(); return false; }
            
        app.save();
        
    });
    app.accountHeadAutoComplete();
    //app.accountHeadSearchAutoComplete(); //FOR SEARCH LEFT SIDE TREEVIEW

    app.test111(); //FOR ACCOUNT MASTER AUTOCOMPLETE
    //app.subledgerAutoComplete();
    app.EffectiveLedgerAuto();


    $('#txtparent').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtparent').val('');
            app.ParentId = "";
        }
        if (iKeyCode == 46) {
            $('#txtparent').val('');
            app.ParentId = "";
        }
    });
    $('#btncancel').click(function () {
        location.reload();
    });
    $('#ddlSector').change(function () {
        app.AccountRule();
    });
    $('#btnEffectAdd').click(function () {
        app.EffectAdd();
    });

    $('form input:text, input:radio,input:checkbox,select,button').bind("keydown", function (e) {
        var n = $('form input:text, input:radio,input:checkbox,select,button').length;
        //
        var c = $('form input:text, input:radio,input:checkbox,select,button').length;
        $('form input:text, input:radio,input:checkbox,button,select').each(function (index, element) {
            if ($(element).prop('readonly') || $(element).prop('disabled') || $(element).prop('type') == "button") {
            }

        });

        var keyCode = (window.event) ? e.which : e.keyCode;
        //
        if (keyCode == 13) { //Enter key
            e.preventDefault(); //Skip default behavior of the enter key
            var nextIndex = $('form input:text, input:radio,input:checkbox,select,button').not(":disabled").index(this) + 1;
            if (nextIndex < n) {
                //$('form input:text, input:radio,input:checkbox,select')[nextIndex].focus();
                $('form input:text, input:radio,input:checkbox,select,button').not(":disabled")[nextIndex].focus();
            }
            else {
                $('form input:text, input:radio,input:checkbox,select').not(":disabled")[nextIndex - 1].focus();
                //$('input:text')[nextIndex - 1].blur();
                $('#btnSubmit').click();
            }
        }
    });
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    app.getAccConfigBySectorId();

    app.RightClick();
    $(selectors.input).on('keyup', search);
});

var app = {
    IELA: '',
    ParentId: '',
    subParentId: '',
    branchId: '',
    childCount: '',
    group: '',
    UnitType: '',
    SectorName: '',
    MNDOldCode: '',

    populateAccount: function () {
        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/GetAccountHead',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {

                app.treeview1(response.Data);
            },
            error: function () { }
        });
    },
    AccountRule: function () {
        var V = "{SectorID : '" + $("#ddlSector").val() + "'}";
        $.ajax({
            url: '/Accounts_Form/AccountMaster/AccountRule',
            data: V,
            type: 'POST',
            async: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {


                    MNDOldCode = app.MNDOldCode = t[0].MNDOldcode;
                    if (MNDOldCode == "Y") { $("#panel-1").hide(); $("#panel-2").show(); $('#lblLedger').html('Detail'); }
                    else { $("#panel-1").show(); $("#panel-2").hide(); $('#lblLedger').html('Ledger'); }

                    //disable enable AccountHead field
                    //var maintainAccCode = t[0].MaintainAccCode;
                    //if (maintainAccCode == "Y" && MNDOldCode == "N") { $("#txtachead").prop("disabled", false);  }
                    //else if (maintainAccCode == "Y" && MNDOldCode == "Y") { $("#txtachead").prop("disabled", true); }
                    //else { $("#txtachead").prop("disabled", false); }
                }
            },
            error: function () { }
        });
    },
    subledger: function () {
        $("#overlay_Common").css('display', 'block');;
        $.ajax({
            url: '/Accounts_Form/AccountMaster/GetSubledger',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                app.subledgerPop(response.Data);
                $("#overlay_Common").css('display', 'none');;
            },
            error: function () { }
        });
    },

    gethead: function (parent, items) {
        app.treeview(items, 0);
    },
    treeviewsasasa: function (list, parent) {

        $.each(list, function (i, element) {
            if (element.ParentAccountCode == '0') {
                var li = $('<li style="color:white;"><a href="' + this.Url + '">' + this.AccountDescription + '</a></li>');
                li.appendTo($('#root'));
            }
            else {

            }

        });

    },


    treeview1: function (data1) {

        //alert(JSON.stringify(data1));
        $('#treeview').treeview({
            data: data1,
            levels: 1,
            onNodeSelected: function (event, data) { app.accClick(data); }
        });

    },   //Treeview node click event
    nodeClick: function () {
        $('.node-selected').click(function () {
            alert(this.id);
        });
    },
    parent: function (data) {
        app.child(data, 0);
    },

    radiochange: function (ref) {
        //var value = $(this).val();
        var value = $(ref).val();
        //alert(value)
        if (value == "Ledger") {
            $('input[type=checkbox]').prop('disabled', false);
            //$('#txtachead').prop('readonly', false);
            $('#chkIsSubLedger').prop('readonly', true);
            $('#unit').prop('readonly', true);

            //$('#ddlPL').prop('readonly', true);
            $('#ddlPL').prop("disabled", true);
            $('#txtschedule').prop('readonly', true);
            $('#txtschedule').val('');
            $('#ddlPL').val(0);
            $('#ddlunit').prop('disabled', false);


        }
        else {
            $('input[type=checkbox]').prop('disabled', true);
            $('input[type=checkbox]').prop('checked', false);
            //$('#txtachead').prop('readonly', true);
            //$('#txtachead').val('');
            $('#chkIsSubLedger').prop('readonly', false);
            $('#unit').prop('readonly', false);

            $('#txtschedule').prop('readonly', false);
            $('#ddlPL').prop("disabled", false);

            $('#ddlunit').prop('disabled', true);
            $('#ddlunit').val('');
            app.IsChecked()
        }
    },
    IsChecked: function () {
        var ckk = $('input[type="checkbox"]').is(":checked");
        var ischk = $('input[type=checkbox]').prop('checked');
        var ddlbank = $('#ddlbank');
        var ddlbranch = $('#ddlbranch');
        var accno = $('#txtacno');
        if (ischk) {
            //$('#pizza_kind').prop('disabled', false);
            ddlbank.prop('disabled', false);
            ddlbranch.prop('disabled', false);
            //accno.prop('disabled', false);
        } else {
            ddlbank.prop('disabled', true);
            ddlbranch.prop('disabled', 'disabled');
            // accno.prop('disabled', 'disabled');
            ddlbank.val(0);
            ddlbranch.val(0);
            accno.val('');

        }
    },
    accClick: function (ref) {       //Treeview node click event
       
        app.ParentId = ref.AccountCode;
        $('#hdnAccountSearchID').val(ref.AccountCode);

        app.autoChangeEvent(ref.AccountCode);
    },

    save: function () {
        var group = $('#radiogroup');
        var ledger = $('#radionLedger');

        var GLS = '';
        if (group.prop('checked')) {
            GLS = 'G'
        } else if (ledger.prop('checked')) {
            GLS = 'L';
        }

        var grdLen = $('#tbl tr.myData').length; var ArrList = [];
        if (grdLen > 0) {

            $('#tbl tr.myData').each(function () {
                var accCode = $(this).find('.clsAccCode').text();
                var OpBalance = $(this).find('.OpBalance').text();
                var OpDrCr = $(this).find('.OpDrCr').text();
                ArrList.push({ 'accCode': accCode, 'OpBalance': OpBalance, 'OpDrCr': OpDrCr });
            });

        }

        var obj = {
            "AccountCode": $('#hdnAccountSearchID').val(), "ParentAccountCode": app.ParentId, "AccountDescription": $('#txtacdesc').val(), "GLS": GLS, "IELA": app.IELA,
            "Schedule": $('#txtSchedule').val(), "PL": $('#ddlPL').val(), "Active": $('#ddlActive').val(), "MenuOrder": $('#txtMenuOrder').val(),
            "SectorID": $('#ddlSector').val(), "FinYear": localStorage.getItem("FinYear"), "Detail": ArrList
        }

       // alert(JSON.stringify(obj)); return false;

        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/SaveAccountHead',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var t = response.Data;

                swal({
                    title: "Success",
                    text: $('#hdnAccountSearchID').val() == "" ? "Account Detail Inserted Successfully !!" : "Account Detail Updated Successfully !!",
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                                //app.populateAccount();
                                //app.reset();
                                location.reload();
                        }
                    });
            },
            error: function () { }
        });
    },
    update: function () {
        var group = $('#radiogroup');
        var ledger = $('#radionLedger');
        var major = $('#radioMajor');
        var minor = $('#radionMinor');

        var id = $('#hdnAccountSearchID').val();
        var acDesc = $('#txtacdesc').val();
        var parentname = $('#txtparent').val();
        var achead = $('#txtachead').val();
        var branchId = ($('#ddlbranch option:selected').val() != null && $('#ddlbranch').val() != '') ? $('#ddlbranch').val() : 0;
        var acno = $('#txtacno').val();

        var IsSubLedger = $('#chkIsSubLedger').prop('checked') ? "Y" : null
        //var groupLedger = $('input:radio[name=optradio]:checked').val().substr(0, 1);
        if (group.prop('checked')) {
            GLS = 'G'
        } else if (ledger.prop('checked')) {
            GLS = 'L';
        } else if (major.prop('checked')) {
            GLS = 'N';
        } else if (minor.prop('checked')) {
            GLS = 'L';
        }

        var PGroup = $('#txtparent').val().substr(0, 1).replace('(', '').replace(')', '');

        var PL = $('#ddlPL').val();
        var schedule = $('#txtschedule').val();
        //var sec = $('#unit').prop('checked');
        var centerid = $('#ddlunit').val();
        var AccountType = $('#ddlaccounttype').val();
        var Active = $('#ddlActive').val();
        var IFSC = $('#txtIFSC').val();
        var MICR = $('#txtMICR').val();
        var BankName = $('#txtbank').val();
        var BranchName = $('#txtbranch').val();

        if ($('#chkbank').prop('checked')) {

            ISBank = "B";
            app.IELA = PGroup;
        }
        else if ($('#chkcash').prop('checked')) {

            ISBank = "C";
            app.IELA = PGroup;
        }
        else {
            ISBank = null;
        }

        var obj = {
            "AccountID": id, "parent": app.ParentId, "acDesc": acDesc, "GLS": GLS, "IELA": app.IELA, "IsSubLedger": IsSubLedger, "AccountType": AccountType, "branchId": branchId,
            "acno": acno, "ISBank": ISBank, "achead": achead, "centerid": centerid, "schedule": schedule, "PL": PL, "Active": Active, "IFSC": IFSC, "MICR": MICR, "BankName": BankName, "BranchName": BranchName
        }
        //alert(JSON.stringify(obj));
        //return false;

        $.ajax({
            url: '/Accounts_Form/AccountMaster/SaveAccountHead',
            type: 'POST',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var t = response.Data;
                var msgType = "", titles = "";
                if (t == "1" || t == "2") { msgType = "success"; titles = "Success"; }
                else
                { msgType = "warning"; titles = "Warning"; }

                swal({
                    title: titles,
                    text: response.Message,
                    type: msgType,
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            if (t != 4) {
                                //app.populateAccount();
                                //app.reset();
                                location.reload();
                            }
                        }
                    });
                //alert(response.Message);
                //app.populateAccount();
            },
            error: function () { }
        });

    },

    validation: function () {
        $.validator.addMethod('Root', function (value, element) {
            //return parseInt(value) % 5 == 0
            return value != '(G)NA(0)' && value != 'NA'
        });

        $('#Acheadtree').validate({
            rules: {
                txtparent: {
                    required: true,
                    Root: true
                },
                txtacdesc: "required",
            },
            messages: {
                txtparent: {
                    required: "Parent Account Should be not Blank !!",
                    Root: "You cant update the Top Root."
                },
                txtacdesc: "Enter Account Description"
            }
        });
    },
    accountHeadAutoComplete: function () {
        $('#txtparent').autocomplete({
            source: function (request, response) {

                var GLS = "";
                var group = $('#radiogroup');
                var ledger = $('#radionLedger');

                if (group.prop('checked')) {
                    GLS = 'G'
                } else if (ledger.prop('checked')) {
                    GLS = 'L';
                } 

                var S = "{Desc:'" + $('#txtparent').val() + "', MND:'" + "M" + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/PaperTktAccountPrint/Account_Description',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                AutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                app.ParentId = i.item.AccountCode;
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    accountHeadSearchAutoComplete: function () {
        $('#txtAccountSearch').autocomplete({
            source: function (request, response) {

                var S = "{Desc:'" + $('#txtAccountSearch').val() + "', Subledger:'" + "A" + "', SectorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
                $.ajax({
                    url: '/Accounts_Form/AccountMaster/Account_DescriptionAuto',
                    type: 'POST',
                    data: S,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function (serverResponse) {
                        var AutoComplete = [];


                        if ((serverResponse.Data).length > 0) {

                            app.treeview1(serverResponse.Data);

                            //$.each(serverResponse.Data, function (index, item) {
                            //    AutoComplete.push({
                            //        label: item.AccountDescription,
                            //        AccountCode: item.AccountCode
                            //    });
                            //});

                            //response(AutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                app.ParentId = i.item.AccountCode;
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
    },

    test111: function () {
        $("#txtacAuto").autocomplete({
            source: function (request, response) {

                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/PaperTktAccountPrint/Load_AccountAutoComplete',
                    data: "{ AccountDescription: '" + $('#txtacAuto').val() + "',  AccountORSubLedger: '" + "A" + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (serverResponse) {
                        var ledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                ledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    Val: item.AccountCode
                                });
                            });
                            response(ledgerAutoComplete);
                        }
                    }
                });
            },
            select: function (e, i) {
                $("#hdnAccountSearchID").val(i.item.Val);
                app.autoChangeEvent($('#hdnAccountSearchID').val());
                if ($('#hdnAccountSearchID').val() != '') {
                    $('#btnsubmit').text('Update');
                }
                else {
                    $('#btnsubmit').text('Save');
                }

            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });

    },
    autoChangeEvent: function (id) {

        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/GetAccuntHeadById',
            type: 'POST',
            data: "{ 'id': '" + id + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data.Table;
                var b = response.Data.Table1;

                app.ParentId = a[0]["ParentAccountCode"];
                app.IELA = a[0]["IELA"];


                if (a.length > 0) {  //app.ParentId

                    var group = $('#radiogroup'); var ledger = $('#radionLedger'); 
                    group.prop('disabled', false); ledger.prop('disabled', false); 

                    $('#hdnAccountSearchID').val(a[0]["AccountCode"]);
                    $('#txtacdesc').val(a[0]["AccountDescription"]);
                    $('#txtparent').val(a[0]["ParentName"]);
                    
                    app.childCount = a[0]["ChildCount"];
                    app.group = a[0]["GroupLedger"];

                    if (a[0]["GroupLedger"] == "G") {
                        $('#radiogroup').prop('checked', true);
                    } else if (a[0]["GroupLedger"] == "L") {
                        $('#radionLedger').prop('checked', true)
                    }
                   
                    $('#ddlPL').val(a[0]["PLDirectIndirect"]);
                    $('#txtSchedule').val(a[0]["Schedule"]);
                    $('#txtMenuOrder').val(a[0]["MenuOrder"]);
                    $('#ddlActive').val(a[0]["Active"] == null ? 'A' : a[0]["Active"]);

                    $('#tbl tr.myData').remove();
                    if (b.length > 0)
                    {
                        var html = ""
                        var $this = $('#tbl .test_0');
                        $parentTR = $this.closest('tr');

                        for (var i = 0; i < b.length; i++) {
                            html += "<tr class='myData' >"
                                + "<td style='display:none;' class='clsAccCode' chk-data='" + b[i].AccountCode + "' >" + b[i].AccountCode  + "</td>"
                                + "<td class='OpAccCode'>" + b[i].AccountDescription  + "</td>"
                                + "<td class='OpBalance'>" + b[i].OpBalance  + "</td>"
                                + "<td class='OpDrCr'>" + b[i].DRCR  + "</td>"
                                + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='app.DeleteDetail(this);' /></td>"
                            html + "</tr>";
                        }
                        $parentTR.after(html);
                        
                    }

                }
                else {
                    $('#radiogroup').prop('checked', false); $('#radionLedger').prop('checked', false);
                    app.reset();
                }
            },
            error: function () { }
        });
    },
    reset: function () {
        app.ParentId = '',
            app.IELA = '';
        $('#txtacdesc').val('');
        $('#txtparent').val('');
        $('#hdnAccountSearchID').val('');
        $('#txtSchedule').val('');
        $('#txtMenuOrder').val('');
        $('#ddlPL').val();
        $('#ddlActive').val('A');

    },

    EffectiveLedgerAuto: function () {
        $('#txtOpAccCode').autocomplete({
            source: function (request, response) {
               
                $.ajax({
                    url: '/Accounts_Form/PaperTktAccountPrint/Account_Description',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'Desc':'" + $('#txtOpAccCode').val() + "', MND: '" + app.IELA  + "'}",
                    dataType: 'json',
                    success: function (serverResponse) {

                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {

                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });

                            response(SubledgerAutoComplete);
                        }

                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                $('#hdnOpAccCode').val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            if (app.IELA == "") {
                alert('Please Select Ledger Type under which you want to create Group/Ledger');
                $('#txtOpAccCode').val('');
                $('#hdnOpAccCode').val('');
                return false;
            }
            $(this).autocomplete('search', ($(this).val()))
        });
    },

    subReset: function () {
        app.subParentId = '';
        $('#txtsubparent').val('');
        $('#txtpayee').val('');
        $('#txtgst').val('');
        $('#txtpan').val('');
        $('#txtmob').val('');
        $('#payeeContactPerson').val('');
        $('#hdnIFSC').val('');
        $('#txtIFSC').val('');
        $('#ddlaccounttype').val('0')
        var groupLedger = 'S';
        var address = $('#txtaddress').val('');
        $('#txtbank').val('');
        $('#txtbranch').val('');
        $('#txtMICR').val('');
        $('#hdnParentID').val('');
        $('#ddlstate').val(0);
        $('#ddldistrict').val(0);
        $('#txtpin').val('');
        $('#ddlSubActive').val('A');
        $('#txtAccountNo').val('');
        $('#btnsubsubmit').text('Save');
        $('#tbl tr.myData').remove();
    },
    onlyonecheck: function () {

        //$(this).siblings('input:checkbox').prop('checked', false);
        //$(this).siblings('.tt').prop('checked', false);
        //$('input[type="checkbox"]').not(this).prop('checked', false); 
        //$('.tt').not(this).prop('checked', false);
        //$('input[type="checkbox"]').not(this).prop('checked', false); 

        //$('.tt').not(this).prop('checked', false);  



    },
    getAccConfigBySectorId: function () {

        var E = "{SectorID : '" + $('#ddlSector').val() + "'}";
        $.ajax({
            url: '/Accounts_Form/AccountMaster/getAccConfig',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: E,
            success: function (response) {

                app.UnitType = response.Data[0]["UnitType"];
                app.SectorName = response.Data[0]["SectorName"];

                if (app.UnitType != "C") {
                    $('#txtachead').prop('readonly', false);
                    $('#btnsubmit').prop('disabled', false);
                }
                else {
                    $('#txtachead').prop('readonly', true);
                    $('#btnsubmit').prop('disabled', true);
                }

            },
            error: function () { }
        });
    },
    EffectAdd: function () {
        if ($("#hdnOpAccCode").val() == "") { $("#txtOpAccCode").focus(); return false; }
        if ($("#txtOpBalance").val() == "") { $("#txtOpBalance").focus(); return false; }
        if ($("#ddlOpDrCr").val() == "") { $("#ddlOpDrCr").focus(); return false; }
        var chkAccCode = $("#hdnOpAccCode").val();


        var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

        if (l > 0) {
            alert('Sorry ! This Account Description is Already Available.'); $("#txtOpAccCode").val(''); $("#hdnOpAccCode").val(''); return false;
        }

        var html = ""

        var $this = $('#tbl .test_0');
        $parentTR = $this.closest('tr');

        html += "<tr class='myData' >"
            + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#hdnOpAccCode").val() + "' >" + $("#hdnOpAccCode").val() + "</td>"
            + "<td class='OpAccCode'>" + $("#txtOpAccCode").val() + "</td>"
            + "<td class='OpBalance'>" + $("#txtOpBalance").val() + "</td>"
            + "<td class='OpDrCr'>" + $("#ddlOpDrCr").val() + "</td>"
            + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='app.DeleteDetail(this);' /></td>"
        html + "</tr>";

        $parentTR.after(html);

        $("#txtOpAccCode").val(''); $("#hdnOpAccCode").val(''); $("#txtOpBalance").val(''); $("#ddlOpDrCr").val('');
    },
    DeleteDetail: function (ID) {
        var AccCode = $(ID).closest('tr').find('.clsAccCode').text();
        app.Check_InOpeningBalance(AccCode, ID);
    },
    Check_InOpeningBalance: function (AccCode, ID) {
        var E = "{AccountCode: '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}";
        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/CheckInOpeningBalance',
            type: 'POST',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data;
                var Count = a[0].Count;
                if (Count > 0) {
                        swal({
                            title: "Warning",
                            text: "Sorry ! This Ledger is available in Opening Balance.",
                            type: "warning",
                            confirmButtonColor: "#AEDEF4",
                            confirmButtonText: "OK",
                            closeOnConfirm: true,
                        },
                            function (isConfirm) {
                                if (isConfirm) {

                                }
                            });
                }
                else {
                        $(ID).closest('tr').remove();
                }

            },
            error: function () { }
        });
    },

    setAccParetnOnly: function (AccCode, GL) {
        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/GetAccuntHeadById',
            type: 'POST',
            data: "{ 'id': '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data.Table;
                var b = response.Data.Table1;

                app.ParentId = a[0]["AccountCode"]; 
                app.IELA = a[0]["IELA"];
                var groupLedger = a[0]["GroupLedger"];

                if (groupLedger == "L") {
                    alert('Sorry !! You can not Create Account Under Ledger/Detail.'); return false;
                }

                //$('#hdnAccountSearchID').val(a[0]["AccountCode"]);
                $('#txtparent').val(a[0]["AccountDescription"]);

                if (GL == "G") {
                    $('#radiogroup').prop('checked', true);
                } else if (GL == "L") {
                    $('#radionLedger').prop('checked', true)
                }
            },
            error: function () { }
        });
    },

    DeleteAccount: function (AccCode) {
        $.ajax({
            url: '/Accounts_Form/PaperTktAccountPrint/DeleteAccountCode',
            type: 'POST',
            data: "{ 'AccCode': '" + AccCode + "', SectorID: '" + $('#ddlSector').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                var a = response.Data;
                if (a == "Yes") {
                    alert("Account Description is Deleted Successfully !!");
                    app.populateAccount(); app.reset();
                }
                else if (a == "No") {
                    alert("Sorry ! You can not Delete Account Description."); return false;
                }
                else {
                    alert("There is Some Problem !!"); return false;
                }
            },
            error: function () { }
        });
    },
    RightClick: function () {

        var $contextMenu = $("#contextMenu");
        $("body").on("contextmenu", function (e) {
            return false;
        });

        var t = "";
        $(document).on('mousedown', ".right-clicked", function (e) {
            if (e.which == 3) {

                var ths = $(this);
                ths.removeClass('selected');
                ths.addClass('selected');
                t = ths.attr('id');
                var fordata = $("#hdnfor").val();

                if (fordata == "account") {
                    $contextMenu.css({
                        display: "block",
                        left: e.pageX - 75,
                        top: e.pageY - 80
                    });
                }
                if (fordata == "subledger") {
                    $contextMenu.css({
                        display: "block",
                        left: e.pageX - 60,
                        top: e.pageY - 20
                    });
                }
            }
            return false;
        });

        //Add
        $contextMenu.on("click", ".lnkAdd", function () {
            var ths = $(this);
            var acc_sub = ths.attr('acc-sub');
            var acc_gl = ths.attr('acc-gl');
            if (acc_sub == "A") //Account Form
            {
                app.reset();
                app.setAccParetnOnly(t, acc_gl);
            }
        });
        //Delete
        $contextMenu.on("click", ".lnkDelete", function () {
            var ths = $(this);
            var acc_sub = ths.attr('acc-sub');
            var res = confirm('Are You Sure want to Delete this Account Description.');
            if (res == true)
                app.DeleteAccount(t);

        });

        $contextMenu.mouseleave(function () {
            $contextMenu.css({
                display: "none"
            });
            return false;
        });

    }
}

// ========================================================================== TreeView Search ==========================================================================
function reset(tree) {
    tree.collapseAll();
    tree.enableAll();
}

function collectUnrelated(nodes) {
    var unrelated = [];
    $.each(nodes, function (i, n) {
        if (!n.searchResult && !n.state.expanded) { // no hit, no parent
            unrelated.push(n.nodeId);
        }
        if (!n.searchResult && n.nodes) { // recurse for non-result children
            $.merge(unrelated, collectUnrelated(n.nodes));
        }
    });
    return unrelated;
}

var search = function (e) {

    var pattern = $(selectors.input).val();
    if (pattern === lastPattern) {
        return;
    }
    lastPattern = pattern;
    var tree = $(selectors.tree).treeview(true);
    reset(tree);
    if (pattern.length < 3) { // avoid heavy operation
        tree.clearSearch();
    } else {
        tree.search(pattern);
        var roots = tree.getSiblings(0);
        roots.push(tree.getNode(0));
        var unrelated = collectUnrelated(roots);
        tree.disableNode(unrelated, { silent: true });
    }
};

$(selectors.reset).on('click', function (e) {
    $(selectors.input).val('');
    var tree = $(selectors.tree).treeview(true);
    reset(tree);
    tree.clearSearch();
});

