﻿$(document).ready(function () {

    ReceiveDate();

    Load();

    $('#btnEdit').click(function () {
        Grid();
    });
});

function ReceiveDate() {
    $('#txtReceiveDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtReceiveDate').val(output);
}

function Load()
{
    var E = "{TransType: '" + "AllData" + "', SectorID: '" + $('#ddlSector').val() + "', IssueId: '" + "" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTktReceived/Get_AllData',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {

            var t = data.Data; //alert(JSON.stringify(t));

            var columnData = [
                { "mDataProp": "TktIssueNo" },
                { "mDataProp": "TktIssueDt" },
                { "mDataProp": "Remarks" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = ' <input type="text" id=td_' + row.MstTktIssueId +' class="recDate" id="txtRecRemarks" placeholder="DD/MM/YYYY" autocomplete="off" />';

                        $('#td_' + row.MstTktIssueId).datepicker({
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            closeText: 'X',
                            showAnim: 'drop',
                            changeYear: true,
                            changeMonth: true,
                            duration: 'slow',
                            dateFormat: 'dd/mm/yy',
                            todayHighlight: true,
                            onSelect: function (d)
                            {
                                var recdt = d;
                                var issudt = row.TktIssueDt;

                                var d1 = recdt.split("/");
                                var d2 = issudt.split("/");

                                var d3 = new Date(d1[2], parseInt(d1[1]) - 1, d1[0]); //alert(from);  
                                var d4 = new Date(d2[2], parseInt(d2[1]) - 1, d2[0]); //alert(to);

                                if (d3 < d4)
                                {
                                    alert('Sorry ! Receive date should not less than Issue Date ' + issudt);
                                    $('#td_' + row.MstTktIssueId).val('');
                                    return false;
                                }
                            }
                        });

                        return str;
                    }
                },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = ' <input type="text" class="recRemarks" id="txtRecRemarks" placeholder="Remarks" style="width:100%" autocomplete="off" />';
                        return str;
                    }
                },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                      
                        var str = '<button type="button" id="btnSave" onClick="Save(this, \'' + row.MstTktIssueId + '\')" ><span class="glyphicon glyphicon-floppy-save" aria-hidden="true" style="padding-right:4px"></span>Save</button>';
                        str += '<button type="button" id="btnPrint" onClick="Print(\'' + row.MstTktIssueId + '\')"><span class="glyphicon glyphicon-print" aria-hidden="true" style="padding-right:4px"></span>Print</button>';
                        return str;
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#datatable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0],
                        "className": "text-center",
                        "width": "15%"
                    },
                    {
                        "targets": [1,3],
                        "className": "text-center",
                        "width": "10%"
                    },
                    {
                        "targets": [4],
                        "width": "25%"
                    },
                    {
                        "targets": [5],
                        "width": "12%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });

        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });
}

function Print(IssueId) {
    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "Ticket_Issue.rpt",
        FileName: "Tickt_Receive",
        Database: ''
    });

    detail.push({
        TransType: "PrintReceive",
        TransFrom: '',
        IssueID: IssueId,
        IssueDate: '',
        DenominationID: 0,
        Desc: '',
        IssueType: '',
        IssueTo: 0,
        ReferanceNo: 0,
        TicketSeries: '',
        TicketFrom: 0,
        TicketTo: 0,
        Remarks: '',
        SectorId: $('#ddlSector').val(),
        SectorTo: 0,
        UPDN: '',
        FinYear: '',
        UserID: 0,
        CON: 2
    });

    final = {
        Master: master,
        Detail: detail
    }


    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function View(IssueId)
{
    if (IssueId != "")
    {

        var E = "{TransType: '" + "IssueByID" + "', SectorID: '" + $('#ddlSector').val() + "', IssueId: '" + IssueId + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/PaperTktReceived/Get_AllData',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                if (t.length > 0) {

                    $('#dialog').modal('show');

                    var columnData = [
                        { "mDataProp": "TktSeries" },
                        { "mDataProp": "TktSrlIdFrom" },
                        { "mDataProp": "TktSrlIdTo" },
                        { "mDataProp": "Denomination" },
                        { "mDataProp": "DenominationRemarks" }
                        ];

                    var columnDataHide = [];

                    var oTable = $('#detail').DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [],

                        "oLanguage": {
                            "sSearch": "Search all columns:"
                        },
                        "aaData": t,

                        "aoColumns": columnData,
                        "aoColumnDefs": [
                            {
                                "targets": columnDataHide,
                                "visible": false,
                                "searchable": false
                            }
                        ],
                        'iDisplayLength': 10,
                        destroy: true
                    });


                }
            }
        });
    }
}

function Save(ID, IssueID)
{
    var recDate = $(ID).closest('tr').find('.recDate').val(); 
    var remarks = $(ID).closest('tr').find('.recRemarks').val(); 

    var E = "{TransType:'" + "Insert" + "', Remarks:'" + remarks + "', RecDate:'" + recDate + "', IssueID:'" + IssueID + "'}";
    //alert(E);
    //return false;
    $.ajax({
        url: '/Accounts_Form/PaperTktReceived/Save',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == 'success') {
                swal({
                    title: "Success",
                    text: "Data Updated successfully !!",
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = "";
                        }
                    });
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function Delete(IssueID) {

    var res = confirm("Are you sure want to delete ??");
    if (res == true) {
        var E = "{TransType:'" + "DeleteReceive" + "', Remarks:'" + "" + "', RecDate:'" + "" + "', IssueID:'" + IssueID + "'}";
        $.ajax({
            url: '/Accounts_Form/PaperTktReceived/Save',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data;
                if (t == 'success') {
                    swal({
                        title: "Success",
                        text: "Data Deleted Successfully !!",
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                Grid();
                            }
                        });
                }
            },
            error: function (data, status, e) {
                alert(e);
            }
        });
    }
}

function Grid() {

    var E = "{TransType: '" + "Detail" + "', SectorID: '" + $('#ddlSector').val() + "', IssueId: '" + "" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/PaperTktReceived/Get_AllData',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {

            var t = data.Data; //alert(JSON.stringify(t));

            var columnData = [
                { "mDataProp": "TktIssueNo" },
                { "mDataProp": "TktIssueDt" },
                { "mDataProp": "Remarks" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = ' <input type="text" ' + (row.EditDelete == 0 ? "disabled" : "") + ' value=' + row.TktReceivedDt + ' id=td_' + row.MstTktIssueId + ' class="recDate" id="txtRecRemarks" placeholder="DD/MM/YYYY" autocomplete="off" />';

                        $('#td_' + row.MstTktIssueId).datepicker({
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            closeText: 'X',
                            showAnim: 'drop',
                            changeYear: true,
                            changeMonth: true,
                            duration: 'slow',
                            dateFormat: 'dd/mm/yy',
                            todayHighlight: true
                        });

                        return str;
                    }
                },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        var str = ' <input type="text" ' + (row.EditDelete == 0 ? "disabled" : "") + ' class="recRemarks"  value="' + row.ReceivedRemarks + '"  id="txtRecRemarks" placeholder="Remarks" style="width:100%" autocomplete="off" />';
                        return str;
                    }
                },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {

                        var str = '<button type="button" id="btnSave" style="display:' + (row.EditDelete == 0 ? "none" : "") + '" onClick="Save(this, \'' + row.MstTktIssueId + '\')" ><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span></button>';
                        //str += '<button type="button" id="btnEdit"   style="display:' + (row.EditDelete == 0 ? "none" : "") + '" onClick="Edit(\'' + row.MstTktIssueId + '\')"><span class="glyphicon glyphicon-edit" aria-hidden="true" style="padding-right:4px"></span>Edit</button>';
                        str += '<button type="button" id="btnDelete"   style="display:' + (row.EditDelete == 0 ? "none" : "") + '" onClick="Delete(\'' + row.MstTktIssueId + '\')"><span class="glyphicon glyphicon-remove" aria-hidden="true" ></span></button>';
                        str += '<button type="button" id="btnPrint"   onClick="Print(\'' + row.MstTktIssueId + '\')"><span class="glyphicon glyphicon-print" aria-hidden="true" ></span></button>';
                        return str;
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#detail').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,

                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0],
                        "width": "15%"
                    },
                    {
                        "targets": [1,3],
                        "className": "text-center",
                        "width": "10%"
                    },
                    {
                        "targets": [4],
                        "width": "25%"
                    },
                    {
                        "targets": [5],
                        "width": "18%"
                    }
                ],
                'iDisplayLength': 10,
                destroy: true
            });
            $('#dialog').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });

}


