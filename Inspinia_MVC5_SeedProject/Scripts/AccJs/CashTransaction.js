﻿var MAIN = {};
var vchslr = 1;
var isEdit = true;
var Modifyflg = 'Add';

$(document).ready(function () {

    window.voucherInfo = new MAIN.VoucherInfo();
    window.voucherInfo.init();

    Bind_Todaydate();
    Bind_TransactionDate();
    AutoComplete();
    AutoCompleteSearch();
    SubLedger();
    Navigation();
    SetVoucherDate();

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.modal').on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    });
});


MAIN.VoucherInfo = function () {
    var A = this;
    this.voucherForm;
    this.init = function () {
        this.voucherForm = JSON.parse($("#hdnPopulateJSON").val());
        //alert(JSON.stringify(this.voucherForm));
        this.masterInfo = new MAIN.VoucherMaster();
        this.masterInfo.init();

    };

    this.findAndSetDeleteFlag = function (VchDetData, value, VohDetailID) {

       
        for (var i = 0; i < VchDetData.length; i++) {

            if (value == VchDetData[i].VchSrl && VohDetailID == VchDetData[i].VoucherDetailID) {

                VchDetailID = VchDetData[i].VchSrl; 
                isdel = VchDetData[i].IsDelete;

                if (VchDetailID == "") {
                    var B = window.voucherInfo.voucherForm;
                    window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', value);
                }
                else {
                    var B = window.voucherInfo.voucherForm;

                    //FROM MAIN VOUCHER DETAIL
                    VchDetData[i].IsDelete = 'Y';

                    //FROM SUBLEDGER VOUCHER DETAIL
                    var b = VchDetData[i].VchSubLedger;
                    if (b.length > 0) {
                        for (var k = 0; k < b.length; k++) {
                            b[k].IsDelete = 'Y';
                        }
                    }
                }
            }
        }
    };
    this.findAndRemove = function (array, property, value) {
        //alert(JSON.stringify(array)); alert(property); alert(value);
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.findAndSetSLDeleteFlag = function (VchSLData, value) {

        var a = VchSLData;
        if (a.length > 0) {
            for (var j = 0; j < a.length; j++) {
                if (value == a[j].SLSlNo) {
                    if (a[j].BillID == "") {
                        var B = window.voucherInfo.voucherForm;
                        window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', value);
                    }
                    else {
                        a[j].IsDelete = 'Y';
                    }
                }
            }
        }
    };
    this.findAndSetInstDeleteFlag = function (VchInstData, value) {

        var a = VchInstData;
        if (a.length > 0) {
            for (var j = 0; j < a.length; j++) {
                if (value == a[j].InstSrNo) {
                    if (a[j].BillID == "") {
                        var B = window.voucherInfo.voucherForm;
                        window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', value);
                    }
                    else {
                        a[j].IsDelete = 'Y';
                    }
                }
            }
        }
    };
};
MAIN.VoucherMaster = function () {

    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {

        //$('#txtDebit_0').bind("keydown", function (e) {
               // if (e.which == 13) {  //Enter
                 //   DebitKeyDown();
                //}
        //});

        //$('#btnSubLedgerAdd').bind("click", function (e) {
        //    AddSubLedger(1);
        //});
        $('#btnSubLedgerFinal').bind("click", function (e) {
            AddSubLedger(2);
        });
        $("#btnSave").click(CheckClosingStatus);  //SaveClicked

        $("#btnConfYes").click(ConfYes);
        $("#btnConfNo").click(ConfNo);

        $("#btnNarrOK").click(NarrOK);

        $('#btnRefresh').click(function () {
            location.reload();
        });
        $("#btnInstAdd").click(InstrumentAdd);
        $("#btnInstOK").click(CloseInstrumentType);

    }
};
MAIN.Utils = function () {
    return {
        /* ReatEasyFix for Arrays START */
        fixArray: function (array) {
            if ($.isArray(array)) {
                return array;
            } else {
                return [array];
            }
        }
        /* ReatEasyFix for Arrays END */
    };
}();

function Bind_Todaydate()
{
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtTranDate').val(output);
}
function Bind_TransactionDate() {
    $('#txtTranDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}


function AutoComplete() {

    $("#lblAccDesc_0").autocomplete({

        source: function (request, response) {

            var Type = "";
            var Value = $.trim($("#lblAccDesc_0").val());
            var isNumorStr = $.isNumeric(Value);

            if (isNumorStr)
                Type = "C";  //FOR CODE
            else
                Type = "D";  //FOR DESCRIPTION

            var DRCR = "";
            var TranType = $("#ddlTransactionType").val(); 
            if (TranType == 4 || TranType == 5)
                DRCR = "D";
            else
                DRCR = "C";

         
            var E = "{SearchValue: '" + Value + "', SearchType: '" + Type + "', VoucherTypeID: '" + $("#ddlTransactionType").val() + "', SectorID: '" + $("#ddlSector").val() + "', DRCR: '" + DRCR + "', TransType: '" + "C" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/CashTransaction/Search_AccountDetails",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data; 
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.GL_NAME,
                                GL_ID: item.GL_ID,
                                GL_TYPE: item.GL_TYPE,
                                OLD_SL_ID: item.OLD_SL_ID,
                                SubLedger: item.MaintainItemDetail,
                                CurrBal: item.CurrBal
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {

            $("#hdnlblAccCode").val(i.item.GL_ID);
            $("#lblGlID").val(i.item.GL_ID);
            $("#lblAccDesc_0").val(i.item.label);
            $("#lblGlType").val(i.item.GL_TYPE);
            $("#txtDebit_0").focus();
            $("#hdnlblCurrBal").val(i.item.CurrBal);

            alert($("#hdnlblCurrBal").val());

            var isSubLedger = i.item.SubLedger;
            var glType = i.item.GL_TYPE;

            var found = false;
            var B = window.voucherInfo.voucherForm;
            var C = MAIN.Utils.fixArray(B.VoucherDetail); 
            for (var i in C) {
                if (C[i].GLID == $("#hdnlblAccCode").val()){
                    alert("Account Code Already Entered. Select Another Account Code.");
                    found = true;
                    ClearText();
                    return false;
                }
            }
          
            if (isSubLedger == "Y" || glType == "S")
            {
                $('#dialog').modal('show'); $("#txtSubLedger").focus(); return false;
            }
            if (glType == "B") {
                $('#dialog-Instrument').modal('show'); return false;
            }
        },
        appendTo: "#AutoComplete",
        minLength: 0
    }).click(function () {
        if ($('#ddlTransactionType').val() == "") {
            $('#ddlTransactionType').focus();
            return false;
        }
        if ($('#txtTranDate').val() == "") {
            $('#txtTranDate').focus();
            return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

}
function AutoCompleteSearch() {

    $("#txtVoucherNo").autocomplete({

        source: function (request, response) {
            //$("#ddlTransactionType").val() == 4 ? "D" : "C",
            var E = "{VoucherNo: '" + $("#txtVoucherNo").val() + "', VoucherTypeID: '" + $("#ddlTransactionType").val() + "', VoucherDate: '" + $("#txtTranDate").val() + "', SectorID: '" + $("#ddlSector").val() + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/CashTransaction/Search_VoucherNo",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.RefVoucherSlNo,
                                VoucherNumber: item.VoucherNumber
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnVoucherNo").val(i.item.VoucherNumber);

            var E = "{VoucherTypeID: '" + $("#ddlTransactionType").val() + "', VoucherDate: '" + $("#txtTranDate").val() + "', VoucherNo: '" + $("#hdnVoucherNo").val() + "', AccFinYear: '" + $.trim($("#ddlGlobalFinYear option:selected").text()) + "',  SectorID: '" + $.trim($("#ddlSector").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Accounts_Form/CashTransaction/Get_VoucherDetails",
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data; 
                    
                    window.voucherInfo.voucherForm = t;
                    var B = window.voucherInfo.voucherForm; //alert(JSON.stringify(B));

                    var M = MAIN.Utils.fixArray(B.VoucherMast);
                    var C = MAIN.Utils.fixArray(B.VoucherDetail); //alert(JSON.stringify(C));
                    var D = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(D));
                    var S = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(S));

                    $("#ddlTransactionType").val(M[0].VoucherType);
                    $("#txtTranDate").val(M[0].VCHDATE);
                    $("#txtNarrationval").val(M[0].NARRATION);
                    ShowVoucherDetail(C, "I");

                }
            });

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

}
function SubLedger() {

    $("#txtSubLedger").autocomplete({

        source: function (request, response) {

            var E = "{TransType: '" + "SubLedger" + "', Desc: '" + $("#txtSubLedger").val() + "', ParentAccCode: '" + $("#hdnlblAccCode").val() + "', SectorID: '" + $("#ddlSector").val() + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/CashTransaction/SubLedger",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {

            $("#hdnSubLedgerCode").val(i.item.AccountCode);
        },
        appendTo: "#AutoComplete",
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    }).focus(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

}

DebitKeyDown = function (evt) {

            var B = window.voucherInfo.voucherForm;
            var C = MAIN.Utils.fixArray(B.VoucherDetail);
            var D = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(D.length); alert(JSON.stringify(D));
            var E = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(E));
            var found = false;

            var AccCode = $("#hdnlblAccCode").val();
            var Amount = $("#txtDebit_0").val();



            if (AccCode == "")
            {
                $("#lblAccDesc_0").focus(); return false;
            }
            if (Amount == "" || Amount <= 0) {
                $("#txtDebit_0").focus(); return false;
            }

            alert($("#lblGlType").val()); alert(JSON.stringify(E));
            if ($("#lblGlType").val() == "S") {

                var slamt = parseFloat(0);
                if (E == "") {
                    alert("Sub Ledger Details Entry is required."); $("#txtDebit_0").val(''); $('#dialog').modal('show'); $("#txtSubLedger").focus(); return false;
                }
                for (var v in E) {
                    if (E[v].IsDelete != 'Y')
                        slamt += parseFloat(E[v].Amount);
                }
                alert(Amount); alert(slamt);
                if (parseFloat(Amount) != parseFloat(slamt.toFixed(2))) {
                    alert("Total Amount of SubLedger Details are not equal.");
                    $('#dialog').modal('show');
                    return;
                }
            }
           
            if ($("#lblGlType").val() == "B") {
                var instamt = parseFloat(0);
                if (D == "") {
                    alert("Instrument Details Entry is required."); $("#txtDebit_0").val(''); $('#dialog-Instrument').modal('show'); return false;
                }
                for (var k in D) {
                    if (D[k].IsDelete != 'Y')
                        instamt += parseFloat(D[k].Amount);
                }
                if (parseFloat(Amount) != parseFloat(instamt.toFixed(2))) {
                    alert("Total Amount of Bank Details are not equal.");
                    $('#dialog-Instrument').modal('show');
                    return;
                }
            }
           
            if (Modifyflg == 'Add') {
                vchslr = 1;
                var arr = $("#tblLedger tbody tr.cls_test_Detail").map(function (ind, val) { return Number($(val).attr("max-vch-detail")) }).sort(function (a, b) { return b - a });
              
                if (arr.length > 0) { vchslr = arr[0] + 1; } 
            }
            else {
                vchslr = glvrno; 
            }

            var CurrDrCr = ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C";
            var CurrBal = $("#hdnlblCurrBal").val();
            if ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5)
            {
                if (CurrDrCr == "C") {
                    if (parseFloat(Amount) > parseFloat(CurrBal)) {
                        alert("Sorry ! Amount is greater than Closing Balance " + CurrBal);
                        if ($("#lblGlType").val() == "S") {
                            $('#dialog').modal('show'); return false;
                        }
                        if ($("#lblGlType").val() == "B") {
                            $('#dialog-Instrument').modal('show'); return false;
                        }
                        return false;
                    }
                }
            }



            if (C == "") {

                if (!found) {
                    
                    C.push({
                        VoucherDetailID: $("#lblVoucherDetailID").val(),
                        VoucherType: $("#ddlTransactionType").val(),
                        GLID: $("#hdnlblAccCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",
                        AMOUNT: $("#txtDebit_0").val(),
                        SectorID: $("#ddlSector").val(),
                        IsDelete: null,
                        VchSubLedger: E,
                        VchInstType: D
                    });
                }
            }
            else {

                for (var i in C) {

                    if (C[i].GLID == $("#hdnlblAccCode").val()) {
                        alert("Account Code Already Entered. Select Another Account Code.");
                        found = true;
                    }

                }

                if (!found) {

                    C.push({
                        VoucherDetailID: $("#lblVoucherDetailID").val(),
                        VoucherType: $("#ddlTransactionType").val(),
                        GLID: $("#hdnlblAccCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",
                        AMOUNT: $("#txtDebit_0").val(),
                        SectorID: $("#ddlSector").val(),
                        IsDelete: null,
                        VchSubLedger: E,
                        VchInstType: D
                    });
                }
            }

            D = [];
            E = [];
            B.tmpVchSubLedger = [];
            B.tmpVchInstType = [];
            ClearText();
            //alert(JSON.stringify(C));
            ShowVoucherDetail(C);
            Modifyflg = 'Add';
            isEdit = true;
            $('#lblAccDesc_0').focus();

}

ShowVoucherDetail = function (C) {
    var html = "";
    var slno = 0;
  
    $('.cls_test_Detail').remove();
    var $this = $('#tblLedger .test_0');
    $parentTR = $this.closest('tr');

    if (C.length > 0) {
        //alert(JSON.stringify(C));
        for (var i in C) {
            slno = slno + 1;
           
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {
                //DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",

                if ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) 
                {
                    if (C[i].DRCR == 'D')
                    {
                        html += "<tr class='cls_test_Detail' id='tr_" + C[i].VchSrl + "' max-vch-detail='" + C[i].VchSrl + "' style='cursor:pointer' >"  //onclick='Get_Details(this, \"" + C[i].VchSrl + "\")'
                            + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                            + "<img src='/Content/Images/Delete.png' alt='Delete Ledger' style='width:20px;height:20px;cursor:pointer;' title='Delete Voucher' id='deleteVchSID_" + C[i].VchSrl + "' onClick='removeVoucherDetail(" + C[i].VchSrl + ", \"" + C[i].VoucherDetailID + "\");' />"
                            + "</div></td>"
                            + "<td style='width: 40%; font-family:Verdana;'>" + C[i].GLName + "<br/>"
                            + "<td style='width: 10%; font-family:Verdana; text-align:right;'>" + C[i].AMOUNT + "</td>"
                            + "<td style='width: 10%; font-family:Verdana; text-align:right; display:none;'>" + C[i].GLID + "</td>"
                            + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                            + "<img src='/Content/Images/Edit.png' style='width:20px;height:20px;cursor:pointer;'  title='Edit Voucher' alt='Edit Ledger' id='editVchSID_" + C[i].VchSrl + "' onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");' />"  //onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");'
                            + "</div></td>"
                        html + "</tr>";
                    }
                }
                else
                {
                    if (C[i].DRCR == 'C') {
                        html += "<tr class='cls_test_Detail' id='tr_" + C[i].VchSrl + "' max-vch-detail='" + C[i].VchSrl + "' style='cursor:pointer' >"  //onclick='Get_Details(this, \"" + C[i].VchSrl + "\")'
                            + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                            + "<img src='/Content/Images/Delete.png' alt='Delete Ledger' style='width:20px;height:20px;cursor:pointer;' title='Delete Voucher' id='deleteVchSID_" + C[i].VchSrl + "' onClick='removeVoucherDetail(" + C[i].VchSrl + ", \"" + C[i].VoucherDetailID + "\");' />"
                            + "</div></td>"
                            + "<td style='width: 40%; font-family:Verdana;'>" + C[i].GLName + "<br/>"
                            + "<td style='width: 10%; font-family:Verdana; text-align:right;'>" + C[i].AMOUNT + "</td>"
                            + "<td style='width: 10%; font-family:Verdana; text-align:right; display:none;'>" + C[i].GLID + "</td>"
                            + "<td id='td_" + C[i].VchSrl + "'><div style='text-align:center;'>"
                            + "<img src='/Content/Images/Edit.png' style='width:20px;height:20px;cursor:pointer;'  title='Edit Voucher' alt='Edit Ledger' id='editVchSID_" + C[i].VchSrl + "' onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");' />"  //onClick='editVoucherDetail(this, \"" + C[i].VchSrl + "\");'
                            + "</div></td>"
                        html + "</tr>";
                    }
                }
            }
        }
        $parentTR.after(html);
    }
    CalCulateAmount(C);
};
CalCulateAmount = function (C) {

    var TotalDebit = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            if ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) {
                if (C[i].DRCR == 'D') {
                    TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
                }
            }
            else
            {
                if (C[i].DRCR == 'C') {
                    TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
                }
            }
        }
    }
    $("#txtTotalDebit").val(TotalDebit);

};
removeVoucherDetail = function (VchSRL, VoucherDetailID) {
    var res = confirm('Are You Sure want to Delete this Ledger ??');
    if (res == true) {

        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetDeleteFlag(B.VoucherDetail, VchSRL, VoucherDetailID); 

        ShowVoucherDetail(B.VoucherDetail);
        //$("#lblAccDesc_0").focus();
        return false;
    }
}
editVoucherDetail = function (h, VchSRL) {

    if (isEdit == true) {
       
        var B = {};
        B = window.voucherInfo.voucherForm;
        var ValAccOldSlCode = '';

        var VchMstData = {};
        var VchDetData = {};
        var VchInstData = {};
        var VchSLData = {};

        VchMstData = B.VoucherMast;
        VchDetData = B.VoucherDetail; //alert(JSON.stringify(VchDetData));
        VchInstData = B.tmpVchInstType; //alert(JSON.stringify(VchInstData));
        //VchSLData = B.tmpVchSubLedger;

        var ValVchDetailID = '';
        var VallblGlID = '';
        var VallblGlType = '';
        var VallblSlID = '';
        var VallblSubID = '';
        var ValOldSlCode = '';
        var ValAccDesc = '';
        var ValueDRCR = '';
        var ValueAmount = '';
        alert(JSON.stringify(VchDetData));
        for (var i = 0; i < VchDetData.length; i++) {
            if (VchSRL == VchDetData[i].VchSrl) {

                ValVchDetailID = VchDetData[i].VoucherDetailID; //VchDetData[i].vchslr;
                VallblGlID = VchDetData[i].GLID;
                //VallblGlType = VchDetData[i].VoucherType;
                VallblGlType = VchDetData[i].GlType;
                ValAccDesc = VchDetData[i].GLName;
                ValueDRCR = VchDetData[i].DRCR;
                ValueAmount = VchDetData[i].AMOUNT;
                VallblSlID = VchDetData[i].SLID;
            }
        }
        //alert(VallblGlType);
        $("#lblGlID").val(VallblGlID); 
        $("#lblGlType").val(VallblGlType);
        $("#lblSlID").val(VallblSlID);
        $("#lblAccDesc_0").val(ValAccDesc);
        $("#hdnlblAccCode").val(VallblGlID);
        $("#lblVoucherDetailID").val(ValVchDetailID); 

        $("#txtDebit_0").val(ValueAmount);

        B.tmpVchSubLedger = [];
        B.tmpVchInstType = [];

        glvrno = VchSRL;
        var SectorID = B.VoucherMast.SectorID;
        var C = MAIN.Utils.fixArray(B.tmpVchInstType); //alert(JSON.stringify(C));   //FOR INSTRUMENT POPUP
        var S = MAIN.Utils.fixArray(B.tmpVchSubLedger); //alert(JSON.stringify(S));    //FOR SUBLEDGER POPUP 
        var rowid_S = 1;
        var rowid_C = 1;

        //alert($("#lblGlID").val()); 
        //alert(JSON.stringify(VchDetData));
        $.each(VchDetData, function (index, result) {
            if (result['VchSrl'] == glvrno) {

                $.each(result, function (index1, DataInst) {

                    for (var k in DataInst) {
                        var glid = DataInst[k].GLID;
                        if ($("#lblGlID").val() == glid) {
                          
                            C.push({
                                InstSrNo: rowid_C,
                                YearMonth: '',
                                BillID: DataInst[k].BillID,
                                VoucherType: $("#ddlTransactionType").val(),
                                VCHNo: DataInst[k].VCHNo,
                                GLID: $("#lblGlID").val(),
                                SLID: $("#lblSlID").val(),
                                DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",
                                InstType: DataInst[k].InstType,
                                InstTypeName: DataInst[k].InstTypeName,
                                InstNo: DataInst[k].InstNo,
                                InstDT: DataInst[k].InstDT,
                                Amount: DataInst[k].Amount,
                                ActualAmount: DataInst[k].Amount,
                                Drawee: DataInst[k].Drawee,
                                SectorID: DataInst[k].SectorID,
                                IsDelete: DataInst[k].IsDelete
                            });

                            rowid_C = rowid_C + 1;
                            ShowInstDetail(C);
                        }
                    }
                });
            }
        });

        $.each(VchDetData, function (index, result) {
           
            if (result['VchSrl'] == glvrno) {

                $.each(result, function (index1, DataInst) {
                    
                    for (var k in DataInst) {
                        var AccountCODE = DataInst[k].AccountCODE;
                        if ($("#lblGlID").val() == AccountCODE) {
                          
                            S.push({
                                SLSlNo: rowid_S,
                                BillID: DataInst[k].BillID,
                                SubLedgerTypeID: 'O',
                                DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "C" : "D",
                                SubLedger: DataInst[k].SubLedger,
                                AccountCODE: DataInst[k].AccountCODE,
                                SubLedgerCODE: DataInst[k].SubLedgerCODE,
                                Amount: DataInst[k].Amount,
                                IsDelete: null
                            });
                          
                            rowid_S = rowid_S + 1;
                            ShowSubLedgerDetail(S);
                        }
                    }
                });
            }
        });

        //alert(JSON.stringify(C)); alert($("#lblGlType").val());
        window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
        ShowVoucherDetail(B.VoucherDetail);

        //alert(JSON.stringify(S)); alert(S.length);
        if (S.length > 0) {
            $('#dialog').modal('show');
            //ShowSubLedgerDetail(S);
        }
        if (C.length > 0) {
            $('#dialog-Instrument').modal('show');
            //ShowModalPopupInstType();
        }

        Modifyflg = 'Modify';
        isEdit = false;
        return false;
    }
    else {
        alert("Sorry ! One record is already in Edit Mode !!");
        $('#txtDebit_0').focus();
        return false;
    }
}

ClearText = function () {

    $("#hdnlblAccCode").val("");
    $("#lblAccOldSlCode").val("");
    $("#lblAccDesc_0").val("");
    $("#lblGlID").val("");
    $("#lblSlID").val("");
    $("#lblSubID").val("");
    $("#lblOldSubCode").val("");
    $("#lblSubAccDesc_0").val("");
    $("#ddlDRCR").val("");
    $("#txtDebit_0").val("");
    $("#txtCredit_0").val("");
    $("#lblGlType").val("");
    $(".tblInst").find("tr:gt(1)").remove();
    $("#lblVoucherDetailID").val('');
    //$("#lblAccDesc_0").focus();
}

function SetVoucherDate() {
    $('#txtInstDate').datepicker({
        autoOpen: false,
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
    $("#txtInstDate").datepicker("option", "maxDate", $('#txtVoucherDate').val())
}
InstrumentAdd = function () {

    if ($("#ddlInstType").val() == "") {
        alert("Please Select Instrument Type !!");
        $("#ddlInstType").focus();
        return false;
    }

    //if ($("#ddlVchType").val() != '2' && $("#ddlInstType").val() != 'O')
    if ($("#ddlInstType").val() == 'Q') {

        if ($("#txtInstNo").val() == "") {  // && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B') && ($("#ddlInstType").val() != "N")
            alert("Please Enter Instrument Number !!");
            $("#txtInstNo").focus();
            return false;
        }
    }
    if ($("#txtInstDate").val() == "") {
        alert("Please Enter Instrument Date !!");
        $("#txtInstDate").focus();
        return false;
    }
    if ($("#txtInstAmount").val() <= 0) {
        alert("Please Enter Amount !!");
        $("#txtInstAmount").focus();
        return false;
    }
    if ($("#txtDBankBR").val() == "") {
        alert("Please Enter INFavour !!");
        $("#txtDBankBR").focus();
        return false;
    }
    var InstDate = $("#txtInstDate").val();

    var B = window.voucherInfo.voucherForm;
    var SectorID = B.VoucherMast.SectorID;
    var C = MAIN.Utils.fixArray(B.tmpVchInstType);

    var instsrno = C.length + 1;
    C.push({
        InstSrNo: instsrno,
        YearMonth: "", //$("#txtYearMonth").val(),
        BillID: $("#lblInstBillID").val(),
        VoucherType: $("#ddlTransactionType").val(),
        VCHNo: 0,
        GLID: $("#lblGlID").val(),
        SLID: $("#lblSlID").val(),
        DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",
        InstType: $("#ddlInstType").val(),
        InstTypeName: $("#ddlInstType option:selected").text(),
        InstNo: $("#txtInstNo").val(),
        InstDT: InstDate,
        Amount: $("#txtInstAmount").val(),
        ActualAmount: $("#txtInstAmount").val(),
        Drawee: $("#txtDBankBR").val().toString().toUpperCase(),
        SectorID: SectorID,
        IsDelete: null
    });

    //alert(JSON.stringify(C));

    ShowInstDetail(C);
    ClearInstText();
    //CallModalConfirm();
    ModifyflgInst = 'Add';
}
ShowInstDetail = function (C) {
    var html = ""

    if (C.length > 0) {

        for (var i in C) {
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {
                html += "<tr class='all_Inst' id='trInst_" + C[i].InstSrNo + "'>"
                    + "<td id='tdInst_" + C[i].InstSrNo + "' style='text-align:center;'><img src='/Content/Images/Delete.png' title='Delete' id='deleteInstID_" + C[i].InstSrNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='removeInstDetail(" + C[i].InstSrNo + ");' /></td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + C[i].InstTypeName + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].InstNo) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center;'>" + (C[i].InstDT) + "</td>"
                    + "<td  style='border:solid 1px black;text-align:right;font-family:Verdana; text-align:center;'>" + (C[i].Amount) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; '>" + (C[i].Drawee) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].BillID + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:center; display:none;'>" + C[i].GLID + "</td>"
                    + "<td id='tdInst_" + C[i].InstSrNo + "' style='text-align:center;'><img src='/Content/Images/Edit.png' title='Edit' id='EditInstID_" + C[i].InstSrNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='EditInstDetail(this, \"" + C[i].InstSrNo + "\");' /></td>"
                html + "</tr>";
            }
        }
    }


    $(".tblInst").find("tr:gt(1)").remove();
    $(".tblInst").append(html);
};
ClearInstText = function () {
    $("#ddlInstType").val("");
    $("#txtInstNo").val("");
    $("#txtInstDate").val("")
    $("#txtInstAmount").val("")
    $("#txtDBankBR").val("");
    $("#lblInstBillID").val('');
};
CloseInstrumentType = function () {
    $("#dialog-Instrument").modal('hide');
    CallculateInstAmount();
}
CallculateInstAmount = function () {

    var B = window.voucherInfo.voucherForm;

    var SectorID = B.VoucherMast.SectorID;

    var C = MAIN.Utils.fixArray(B.tmpVchInstType);

    var InstAmount = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }
    }

    $("#txtDebit_0").val("");

        $("#txtDebit_0").val(InstAmount);
        $("#txtDebit_0").focus();

};
removeInstDetail = function (InstSrno) {
    var res = confirm('Are You Sure want to Delete this Record ??');
    if (res == true) {
        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetInstDeleteFlag(B.tmpVchInstType, InstSrno);
        //window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);

        ShowInstDetail(B.tmpVchInstType);
        $("#ddlInstType").focus();
        return false;
    }
};
EditInstDetail = function (h, InstSrno) {

    var B = {};
    B = window.voucherInfo.voucherForm;

    var ValInstBillID = '';
    var ValType = '';
    var ValInstType = '';
    var ValSlipNo = '';
    var ValNumber = '';
    var ValDate = '';
    var ValAmount = '';
    var ValBank = '';
    ValType = $(h).closest('tr').find('td:eq(1)').text();
    ValNumber = $(h).closest('tr').find('td:eq(2)').text();
    ValDate = $(h).closest('tr').find('td:eq(3)').text();
    ValAmount = $(h).closest('tr').find('td:eq(4)').text();
    ValBank = $(h).closest('tr').find('td:eq(5)').text();
    ValInstBillID = $(h).closest('tr').find('td:eq(6)').text();

    $('#ddlInstType option').filter(function () {
        var textValue = $.trim($(this).text());
        var textSearch = $.trim(ValType);

        if (textValue == textSearch)
            return $.trim(textValue) == textSearch;
    }).prop('selected', 'selected');

    $("#lblInstBillID").val(ValInstBillID);
    $("#txtInstNo").val(ValNumber);
    $("#txtInstDate").val(ValDate);
    $("#txtInstAmount").val(ValAmount);
    $("#txtDBankBR").val(ValBank);

    window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);
    ShowInstDetail(B.tmpVchInstType);
    $("#ddlInstType").focus();
    ModifyflgInst = 'Modify';
    return false;

};

AddSubLedger = function (con)
{
    var B = window.voucherInfo.voucherForm;
    var C = MAIN.Utils.fixArray(B.tmpVchSubLedger); 


    var l = C.length;
    if (con == 2)
    {
        if (l == 0)
        {
            alert('Sorry ! Please add SubLedger.'); return false;
        }
        else
        {
            $('#dialog').modal('hide'); $('#txtDebit_0').focus(); return false;
        }
    }

    var SubLedger = $('#txtSubLedger').val();
    var SubCode = $('#hdnSubLedgerCode').val();
    var SubLedgerAmount = $('#txtSubLedgerAmount').val() == "" ? 0 : $('#txtSubLedgerAmount').val();

    if (SubCode == "") { $('#txtSubLedger').focus(); return false; }
    if (SubLedgerAmount == "" || SubLedgerAmount <= 0) { $('#txtSubLedgerAmount').focus(); return false; }

    var srno = C.length + 1;
    C.push({
        SLSlNo: srno,
        BillID: $("#lblSLBillID").val(), 
        SubLedgerTypeID: 'O',
        Amount: SubLedgerAmount,
        DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "C" : "D",
        SubLedger: SubLedger,
        AccountCODE: $("#hdnlblAccCode").val(),
        SubLedgerCODE: SubCode,
        IsDelete: null
    });
   
    ShowSubLedgerDetail(C);
    SumSubLedgerTotal();
    ClearSubLedgerText();
    //Call_SubLedger_ModalConfirm();
    ModifyflgSL = 'Add';

}
ShowSubLedgerDetail = function (C) {

    var html = ""; var isItemDetail = '';
    if (C.length > 0) {
       
        for (var i in C) {
            var isDelete = C[i].IsDelete;
            if (isDelete != 'Y') {

                html += "<tr class='SubLedger' id='trSL_" + C[i].SLSlNo + "'>"
                    + "<td  style='font-family:Verdana;'>" + (C[i].SubLedger) + "</td>"
                    + "<td  style='font-family:Verdana; text-align:center; display:none;'>" + (C[i].SubLedgerCODE) + "</td>"
                    + "<td  style='font-family:Verdana; text-align:right;'>" + (C[i].Amount) + "</td>"
                    + "<td  style='border:solid 1px black; font-family:Verdana; text-align:right; display:none;'>" + (C[i].BillID) + "</td>"
                    + "<td id='tdSL_" + C[i].SLSlNo + "' style='text-align:center;'>"
                    + "<img src= '/Content/Images/Edit.png' title= 'Edit' id= 'EditSLID_" + C[i].SLSlNo + "' style= 'width:20px;height:20px;cursor:pointer; text-align:center;' onClick= 'EditSLDetail(this, \"" + C[i].SLSlNo + "\");' />"
                    + "<img src='/Content/Images/Delete.png' title='Delete' id='deleteSLID_" + C[i].SLSlNo + "' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='removeSLDetail(" + C[i].SLSlNo + ");' /></td > "
                html + "</tr>";
            }
        }
    }

    $("#tbl_SubLedger").find("tr:gt(1)").remove();
    $("#tbl_SubLedger").append(html);
};
SumSubLedgerTotal = function ()
{
    var B = window.voucherInfo.voucherForm;
    var C = MAIN.Utils.fixArray(B.tmpVchSubLedger);
    var SLAmount = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y')
            SLAmount = (parseFloat(SLAmount) + parseFloat(C[i].Amount)).toFixed(2);
    }
    if (SLAmount <= 0) {
        $("#txtDebit_0").val(0);
    }
    else {
        $("#txtDebit_0").val(SLAmount);
    }
}
ClearSubLedgerText = function () {
    $("#txtSubLedger").val("");
    $("#hdnSubLedgerCode").val("");
    $("#txtSubLedgerAmount").val("");
};
EditSLDetail = function (h, SLSrno) {

    var B = {};
    B = window.voucherInfo.voucherForm;

    var Amount = $(h).closest('tr').find('td:eq(2)').text();
    var SubLedger = $(h).closest('tr').find('td:eq(0)').text();
    var SubLedgerCode = $(h).closest('tr').find('td:eq(1)').text();
    var BillID = $(h).closest('tr').find('td:eq(3)').text();

    $("#txtSubLedgerAmount").val(Amount);
    $("#txtSubLedger").val(SubLedger);
    $("#hdnSubLedgerCode").val(SubLedgerCode);
    $("#lblSLBillID").val(BillID);


    window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', SLSrno);

    ShowSubLedgerDetail(B.tmpVchSubLedger);
    ModifyflgSL = 'Modify';
    return false;

};
removeSLDetail = function (SLSrno) {
    var res = confirm('Are You Sure want to Delete this Record ??');
    if (res == true) {
        var B = {};
        B = window.voucherInfo.voucherForm;

        var a = window.voucherInfo.findAndSetSLDeleteFlag(B.tmpVchSubLedger, SLSrno);
        //window.voucherInfo.findAndRemove(B.tmpVchSubLedger, 'SLSlNo', SLSrno);

        ShowSubLedgerDetail(B.tmpVchSubLedger);
        return false;
    }
};

SaveClicked = function () {

    if (isEdit == false) {
        alert("Sorry ! One record is already in Edit Mode !!");
        $('#txtDebit_0').focus();
        return false;
    }

    if ($('#ddlTransactionType').val() == "") {
        $('#ddlTransactionType').focus();
        return false;
    }
    if ($('#txtTranDate').val() == "") {
        $('#txtTranDate').focus();
        return false;
    }

    var B = window.voucherInfo.voucherForm;
    var SectorID = $('#ddlSector').val();
    var C = MAIN.Utils.fixArray(B.VoucherDetail); 
    var D = MAIN.Utils.fixArray(B.tmpVchInstType);

    if (C == "") {
        alert("Please Add Voucher Detail before Saving !!");
        $("#lblAccDesc_0").focus();
        return false;
    }

    //var TotalDebit = 0;
    //for (var i in C) {
    //    if (C[i].IsDelete != 'Y') {
    //            TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
    //    }
    //}

    //$("#txtTotalDebit").val(TotalDebit);

    //alert(JSON.stringify(C));
    //return false;
    var TotalDebit = 0;
    var TotalCredit = 0;
    for (var i in C) {
        if (C[i].IsDelete != 'Y') {
            if (C[i].DRCR == "D") {
                TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            } else if (C[i].DRCR == "C") {
                TotalCredit = (parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT)).toFixed(2);
            }
        }
    }
    //alert(TotalDebit); alert(TotalCredit);
   

    if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit) {
     
        ShowConfirmSaveDialog();
    } 
    else
    {
        //DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "D" : "C",
        var drcr = '';
        if ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5)
            drcr = 'C';
        else
            drcr = 'D';

       
        for (var i = 0; i < C.length; i++) {
            if (drcr == C[i].DRCR) {
                C[i].AMOUNT = $("#txtTotalDebit").val();
            }
        }
        ShowConfirmSaveDialog();
    }
   
};
ShowConfirmSaveDialog = function () {
    $("#lblMSG").html("Are you sure want to save the voucher ??");
    //$("#dialog-confirms").show();
    $("#dialog-confirms").fadeToggle(1000);
    $("#btnConfYes").focus();
};
ConfNo = function () {
    $("#dialog-confirms").hide();
};
ConfYes = function () {
    $('#dialog-confirms').hide();
    $('#dialog-Narration').show();
    $('#txtPopNarration').focus();

    $('#txtPopNarration').val($("#txtNarrationval").val());
};
NarrOK = function () {
    if ($('#txtPopNarration').val() != "") {
        $('#dialog-Narration').hide();
        SetMasterFormValues();
    }
    else {
        $('#txtPopNarration').focus(); return false;
    }
};
SetMasterFormValues = function () {

    var VoucherDate = $.trim($("#txtTranDate").val());
    var B = window.voucherInfo.voucherForm;
    var C = B.VoucherMast;
    var D = B.VoucherDetail; 
    //var S = B.tmpVchSubLedger; alert(S);

    //setting master values
    C.NARRATION = $.trim($("#txtPopNarration").val()).toString().toUpperCase();
    C.YearMonth = VoucherDate; //$.trim($("#txtAccFinYear").val());   //FinYear
    C.VoucherType = $.trim($("#ddlTransactionType").val());
    C.VCHDATE = VoucherDate;
    C.STATUS = "D";
    C.AutoTrans = "C";
    C.SectorID = $("#ddlSector").val();
    C.VCHAMOUNT = $.trim($("#txtTotalDebit").val());
    $("#txtNarrationval").val(C.NARRATION);

    //setting detail values
    if(B.EntryType != "E")
        SetDetbitCreditVendor(D);
    else
        CallSaveVoucher();
  
};
SetDetbitCreditVendor = function (D) {
    var C = "{SectorID: '" + $("#ddlSector").val() + "', VoucherTypeID: '" + $("#ddlTransactionType").val() + "', VchDate: '" + $("#txtTranDate").val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/CashTransaction/SetDetbitCreditVendor",
        data: C,
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (res) {
            var t = res.Data;

            if (t.length > 0) {

                var CurrBal = t[0].CurrBal; alert(CurrBal);
                var CurrDrCr = ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "C" : "D"; alert(CurrDrCr);
                if ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) {
                    if (CurrDrCr == "C") {
                        alert($("#txtTotalDebit").val());
                        if (parseFloat($("#txtTotalDebit").val()) > parseFloat(CurrBal)) {
                            alert("Sorry ! Ledger " + t[0].AccountDescription + " Amount is greater than Closing Balance " + CurrBal);
                            return false;
                        }
                    }
                }



                var C = MAIN.Utils.fixArray(D);
                C.push({
                    VoucherType: $("#ddlTransactionType").val(),
                    GLID: t[0].AccountCode,
                    GLName: t[0].AccountDescription,
                    VchSrl: D.length + 1,
                    DRCR: ($("#ddlTransactionType").val() == 4 || $("#ddlTransactionType").val() == 5) ? "C" : "D",
                    AMOUNT: $("#txtTotalDebit").val(),
                    SectorID: $("#ddlSector").val(),
                    IsDelete: null,
                    VchSubLedger: []
                });
               
                CallSaveVoucher();
            }
            else
            {
                alert('Sorry ! Cash Account not found.'); return false;
            }
           
        }
    });
};
CallSaveVoucher = function () {

    var B = {};
    B = window.voucherInfo.voucherForm;
    var C = JSON.stringify(B);

    alert(JSON.stringify(C));
    return false;
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/CashTransaction/SaveVoucher",
        data: C,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            window.voucherInfo.voucherForm = D.Data;
            var B = window.voucherInfo.voucherForm;


            if (t.SessionExpired == "N") {
                if (t.EntryType == "I") {
                    $("#txtVoucherNo").val(t.VoucherMast.VoucherNo);
                    //alert("Voucher Saved Successfully. Voucher No is '" + t.VoucherMast.VoucherNo + '(' + t.VoucherMast.VCHNo + ')' + "'.");
                    //location.reload();
                    //Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);

                    swal({
                        title: "Success",
                        text: "Voucher Saved Successfully. Voucher No is '" + t.VoucherMast.VoucherNo + '(' + t.VoucherMast.VCHNo + ')' + "'.",
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: false,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);
                            }
                        });
                    return;
                }

                else if (t.EntryType == "E") {
                    if (t.SearchResult == "U") {
                        //alert(t.Message);
                        //location.reload();
                        //Print_Voucher_AfterSaveUpdate(t.VoucherNo, t.VoucherID);
                        swal({
                            title: "Success",
                            text: t.Message,
                            type: "success",
                            confirmButtonColor: "#AEDEF4",
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                        },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                    Print_Voucher_AfterSaveUpdate(t.VoucherMast.VoucherNo, t.VoucherMast.VCHNo);
                                }
                            });
                        return;
                    } else if (t.SearchResult == "F") {
                        alert(t.Message);
                        location.reload();
                        return;
                    } else if (t.SearchResult == "R") {
                        alert(t.Message);
                        location.reload();
                        return;
                    }
                }
            } else {
                alert("Your Session Expired. Login Again");
                location.reload();
                return;
            }
        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
};   //This is for Final Save
CheckClosingStatus = function () {
    var a = $.trim($("#txtTranDate").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 88 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                //if (Status == 1)
                    SaveClicked();
               // else {
                   // swal("Cancelled", msg, "error");
               // }
            }
        }
    });
};


Print_Voucher = function () {

    if ($("#hdnVoucherNo").val() == "") {
        alert('Please, First Select Voucher !!'); $("#txtVoucherNo").focus(); return false;
    }

    if ($("#txtTranDate").val() == "") {
        alert("Please Select Voucher Date !!"); $("#txtTranDate").focus(); return false;
    }

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: $("#txtVoucherNo").val()
    });

    detail.push({
        VoucherNumber: $("#hdnVoucherNo").val(),
        sectorid: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

//Tab Navigation on Enter KeyPress
Navigation = function ()
{
    $('#ddlTransactionType').bind("keydown", function (e) {
        var n = $("#ddlTransactionType").val();
        if (n != "") {
            if (e.which == 13 || e.which == 39) {
                $('#txtTranDate').focus();
                $('#ddlTransactionType').css('background-color', '');
                $('#txtTranDate').css('background-color', '#DAD8D6'); return false;
            }
        }
    });
    $('#txtTranDate').bind("keydown", function (e) {
        var n = $("#txtTranDate").val();
        if (n != "") {
            if (e.which == 13 || e.which == 39) {
                $('#lblAccDesc_0').focus();
                $('#txtTranDate').css('background-color', '');
                $('#lblAccDesc_0').css('background-color', '#DAD8D6'); return false;
            }
        }
    });
    $('#txtDebit_0').bind("keydown", function (e) {
        var n = $("#txtDebit_0").val();
        if (n != "") {
           
            if (e.which == 39 || e.which == 13) { //right arrow key
                $('#btnSubAdd').focus(); return false;
            }
            if (e.which == 37) { //left arrow key
                $('#lblAccDesc_0').focus(); return false;
            }
        }
    });

    $('#txtSubLedger').bind("keydown", function (e) {
        var n = $("#txtSubLedger").val();
        if (n != "") {
            if (e.which == 13 || e.which == 39) {  //Enter
                $('#txtSubLedgerAmount').focus();
                $('#txtSubLedger').css('background-color', '');
                $('#txtSubLedgerAmount').css('background-color', '#DAD8D6'); return false;
            }
        }
    });
    $('#txtSubLedgerAmount').bind("keydown", function (e) {
        var n = $("#txtSubLedgerAmount").val();
        if (n != "") {
            if (e.which == 13 || e.which == 39) {  //Enter
                $('#btnSubLedgerAdd').focus();
                $('#txtSubLedgerAmount').css('background-color', '');
                //$('#txtSubLedgerAmount').css('background-color', '#DAD8D6'); return false;
            }
        }
    });
}
