﻿

$(document).ready(function () {
    Bind_Sector();
    Auto_User();
    Auto_Designation();

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Auto_MenuUser();
    Auto_Menu();
});

function Bind_Sector() {

    var E = "{TransactionType: '" + "Sector" + "', Desc:'" + "" + "', UserID:'" + $('#hdnUserID').val() + "'}";
        $.ajax({
            url: '/Accounts_Form/UserPermission/Load',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {

                var a = data.Data;
                var t = a["Table"]; //alert(JSON.stringify(a));
                $('#tblSector tbody tr').empty();
                if (t.length > 0) {
                    var html = "";
                    for (var i = 0; i < t.length; i++) {
                        html += "<tr class='myData'>"
                        html += "<td style='display:none;' class='clsSectorID'>" + t[i].SectorID + "</td>"
                        html += "<td>" + t[i].SectorName + "</td>"
                        html += "<td style='text-align:center;'><div class='checkbox checkbox-danger checkbox-inline' >"
                        html += "<input type='checkbox' class='tt notchk' id='chkSector' name='Sector' value='" + t[i].SectorID + "' onclick='aa(this)'>"
                        html += "<label for='chkSector' id='lblSector' class='control-label' style='font-weight:bold'></label>"
                        html += "</div></td>"
                        html += "</tr>";
                    }
                }
                $('#tblSector tbody').append(html);

                //$('#tblSector tbody tr.myData.tt').on('change', function () {
                //    if ($(this).hasClass('notchk') == false) {
                //        $(this).prop("checked", false);
                //        $(this).addClass("notchk");
                //    }
                //    else {
                //        $(this).prop("checked", true);
                //        $(this).removeClass("notchk");
                //    }

                //});
            }
        });
    
}
function aa(ID) {
   
    if ($(ID).hasClass('notchk') == false) {
        $(ID).prop("checked", false);
        $(ID).addClass("notchk");
    }
    else {
        $(ID).prop("checked", true);
        $(ID).removeClass("notchk");
    }
}

function Auto_User()
{
    $('#txtSearchUser').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType: '" + "SearchUser" + "', Desc:'" + $('#txtSearchUser').val() + "', UserID:'" + $('#hdnUserID').val() + "'}"; 
            $.ajax({
                url: '/Accounts_Form/UserPermission/Load',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.UserName,
                                UserID: item.UserID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnUserID').val(i.item.UserID);
            User_Detail(i.item.UserID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
        });
    $('#txtSearchUser').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            Blank();
        }
    });
}

function Auto_Designation() {
    $('#txtDesignation').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType: '" + "Designation" + "', Desc:'" + $('#txtDesignation').val() + "', UserID:'" + "" + "'}";
            $.ajax({
                url: '/Accounts_Form/UserPermission/Load',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.Designation,
                                DesignationID: item.DesignationID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnDesignationID').val(i.item.DesignationID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function User_Detail(UserID)
{
    var E = "{TransactionType: '" + "UserDetail" + "', Desc:'" + "" + "', UserID:'" + UserID + "'}";
    $.ajax({
        url: '/Accounts_Form/UserPermission/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {

            var t = data.Data; //alert(JSON.stringify(t));
            var t1 = t["Table"]; //alert(JSON.stringify(t1));
            var t2 = t["Table1"]; //alert(JSON.stringify(t2));

            if (t1.length > 0) {
                $('#txtFirstName').val(t1[0].FirstName); $('#txtLastName').val(t1[0].LastName);
                $('#txtEmail').val(t1[0].Email); $('#txtMobile').val(t1[0].PhoneNo);
                $('#ddlActive').val(t1[0].isActive); $('#txtDesignation').val(t1[0].Designation); $('#hdnDesignationID').val(t1[0].DesignationID);
                $('#txtUserName').val(t1[0].UserName);
            }

            if (t2.length > 0) {
                //$('#tblSector').find([arr].map(function (t) { return ".clsSectorID_" + t }).join()).prop('checked', true);
                var grdLen = $('#tblSector tbody tr.myData').length; 
                if (grdLen > 0) {
                    $.each(t2, function (key, value) {
                        var SectorID = value.SectorID; 
                        $('#tblSector tr.myData').each(function () {
                            var SerSectorID = $(this).closest('tr').find('.clsSectorID').text();
                            if (SectorID == SerSectorID) {
                                $(this).closest('tr').find('.tt').prop('checked', true);
                                $(this).closest('tr').find('.tt').removeClass('notchk');
                            }
                        });
                    });

                }
            }
        }
    });
}

function Blank()
{
    $('#txtFirstName').val(''); $('#txtLastName').val('');
    $('#txtEmail').val(''); $('#txtMobile').val('');
    $('#ddlActive').val('Y'); $('#txtDesignation').val(''); $('#hdnDesignationID').val('');
    $('#txtUserName').val(''); $('#hdnUserID').val(''); $('#txtSearchUser').val('');

    $(".tt").prop('checked', false);
}

function SaveUser()
{
    var UserID = $('#hdnUserID').val();
    var firstName = $('#txtFirstName').val();
    var lastName = $('#txtLastName').val();
    var Email = $('#txtEmail').val();
    var Mobile = $('#txtMobile').val();
    var Active = $('#ddlActive').val();
    var DesignationID = $('#hdnDesignationID').val();
    var txtUserName = $('#txtUserName').val();

    if (firstName == "") { $("#txtFirstName").attr("placeholder", "First Name *"); $("#txtFirstName").addClass("Red"); $('#txtFirstName').focus(); return false; }
    if (lastName == "") { $("#txtLastName").attr("placeholder", "Last Name *"); $("#txtLastName").addClass("Red"); $('#txtLastName').focus(); return false; }
    if (Email == "") { $("#txtEmail").attr("placeholder", "Email *"); $("#txtEmail").addClass("Red"); $('#txtEmail').focus(); return false; }
    if (Mobile == "") { $("#txtMobile").attr("placeholder", "10-digit Mobile No. *"); $("#txtMobile").addClass("Red"); $('#txtMobile').focus(); return false; }
    if (DesignationID == "") { $("#txtDesignation").attr("placeholder", "Designation *"); $("#txtDesignation").addClass("Red"); $('#txtDesignation').val(''); $('#txtDesignation').focus(); return false; }
    if (txtUserName == "") { $("#txtUserName").attr("placeholder", "User Name *"); $("#txtUserName").addClass("Red"); $('#txtUserName').focus(); return false; }

    var grdLen = $('#tblSector tbody tr.myData').length; var ArrList = []; 
    if (grdLen > 0) {
        $("#tblSector tbody tr.myData").find(".tt").each(function (idx, aid) {
          
            if ($(this).hasClass('notchk') == false) {
                ArrList.push({ 'SectorID': aid.value });
            }
        });
    }

    var Master = JSON.stringify(ArrList); 
    var E = "{TransactionType: '" + "Insert" + "', UserID:'" + UserID + "',  firstName:'" + firstName + "',  lastName:'" + lastName + "', " +
        "Email: '" + Email + "', Mobile:'" + Mobile + "', Active:'" + Active + "',  DesignationID:'" + DesignationID + "', UserName:'" + txtUserName + "', param: " + Master + "}";

    //alert(JSON.stringify(Master));
    //return false;
    $.ajax({
        url: '/Accounts_Form/UserPermission/SaveUser',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {

            var t = data.Data; //alert(JSON.stringify(t));
            if (t == "success")
            {
                swal({
                    title: "Success",
                    text: 'User details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            Blank();
                            return false;
                        }
                    });
            }
        }
    });
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                                                                  MENU PERMISSION
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function ShowMenuButtons(con)
{
    if(con==1)
        $('.clsButton').css('display', 'none');
    else
        $('.clsButton').css('display', '');
}

function Blank_Menu() {
    $('#txtMenuUserName').val(''); $('#hdnMenuUserID').val('');
    $('#txtMenuName').val(''); $('#hdnMenuID').val('');
    $('#ddlInsert').val('Y'); $('#ddlEdit').val('Y'); $('#ddlDelete').val('Y'); $('#ddlRemove').val('Y');
}

function Auto_MenuUser() {
    $('#txtMenuUserName').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType: '" + "SearchUser" + "', Desc:'" + $('#txtMenuUserName').val() + "', UserID:'" + $('#hdnMenuUserID').val() + "'}";
            $.ajax({
                url: '/Accounts_Form/UserPermission/Load',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.UserName,
                                UserID: item.UserID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnMenuUserID').val(i.item.UserID);
            User_MenuDetail(i.item.UserID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function Auto_Menu() {
    $('#txtMenuName').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType: '" + "SearchMenu" + "', Desc:'" + $.trim($('#txtMenuName').val()) + "', UserID:'" + "" + "'}";
            $.ajax({
                url: '/Accounts_Form/UserPermission/Load',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var a = serverResponse.Data;
                    var t = a["Table"];
                    var AutoComplete = [];
                    if ((t).length > 0) {

                        $.each(t, function (index, item) {
                            AutoComplete.push({
                                label: item.MenuName,
                                MenuID: item.MenuID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnMenuID').val(i.item.MenuID);
            //User_Detail(i.item.UserID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function SaveMenu() {

    var MenuUserID = $('#hdnMenuUserID').val();
    var MenuID = $('#hdnMenuID').val();
    var isRemove = $('#ddlRemove').val();
    var isInsert = $('#ddlInsert').val();
    var isEdit = $('#ddlEdit').val();
    var isDelete = $('#ddlDelete').val();

    if (MenuUserID == "") { $('#txtMenuUserName').focus(); return false; }
    if (MenuID == "") { $('#txtMenuName').focus(); return false; }

    var E = "{TransactionType: '" + "InsertMenu" + "', MenuUserID:'" + MenuUserID + "',  MenuID:'" + MenuID + "',  isRemove:'" + isRemove + "', " +
        "isInsert: '" + isInsert + "', isEdit:'" + isEdit + "', isDelete:'" + isDelete + "'}";

    //alert(JSON.stringify(Master));
    //return false;
    $.ajax({
        url: '/Accounts_Form/UserPermission/SaveMenu',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {

            var t = data.Data; //alert(JSON.stringify(t));
            if (t == "success") {
                swal({
                    title: "Success",
                    text: 'Menu details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}

function User_MenuDetail(UserID)
{
    var E = "{TransactionType: '" + "UserMenuDetail" + "', Desc:'" + "" + "', UserID:'" + UserID + "'}";
    $.ajax({
        url: '/Accounts_Form/UserPermission/Load',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {

            var a = data.Data; 
            var t = a["Table"]; 
            $('#tblUserMenu tbody tr').empty();
            if (t.length > 0) {
                var html = "", color="";

                for (var i = 0; i < t.length; i++) {
                    if (t[i].Active == 'N')
                        color = '#f3d6d6';
                    else
                        color = '';

                    html += "<tr class='myData' style='background-color:" + color + "'>"
                    html += "<td style='display:none;' class='clsMenuID'>" + t[i].MenuID + "</td>"
                    html += "<td class='clsMenuName'>" + t[i].MenuName + "</td>"
                    html += "<td class='clsActive' style='text-align:center'>" + t[i].Active + "</td>"
                    html += "<td class='clsInsert' style='text-align:center'>" + t[i].InsertFlag + "</td>"
                    html += "<td class='clsEdit' style='text-align:center'>" + t[i].EditFlag + "</td>"
                    html += "<td class='clsDelete' style='text-align:center'>" + t[i].DeleteFlag + "</td>"
                    html += "<td style='text-align:center'><img src= '/Content/Images/Edit.png' title='Edit' style='width:18px;height:19px;cursor:pointer; text-align:center;' onClick='EditMenu(this);' /></td > "
                    html += "</tr>";
                }
            }
            $('#tblUserMenu tbody').append(html);

        }
    });
}

function EditMenu(ID)
{
    var MenuID = $(ID).closest('tr').find('.clsMenuID').text();
    var MenuName = $(ID).closest('tr').find('.clsMenuName').text();
    var Active = $(ID).closest('tr').find('.clsActive').text();
    var Insert = $(ID).closest('tr').find('.clsInsert').text();
    var Edit = $(ID).closest('tr').find('.clsEdit').text();
    var Delete = $(ID).closest('tr').find('.clsDelete').text(); 

    $('#txtMenuName').val(MenuName); $('#hdnMenuID').val(MenuID);
    $('#ddlInsert').val(Insert); $('#ddlEdit').val(Edit); $('#ddlDelete').val(Delete); $('#ddlRemove').val(Active);
}















