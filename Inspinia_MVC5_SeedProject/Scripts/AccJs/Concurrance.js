﻿var master = []; var k = 0;

$(document).ready(function () {

       Set_ConDefaultDate();
       CheckLimit();
    $("#txtConReceiptType").autocomplete({

        source: function (request, response) {

            var budgetTypeID = $('#ddlConBudgetType').val();
            var E = "{TrasactionType: '" + "ConParticulars" + "', BudgetTypeID: '" + budgetTypeID + "',  Desc: '" + $.trim($('#txtConReceiptType').val()) + "'}";

            $.ajax({
                type: "POST",
                url: '/Accounts_Form/BudgetMaster/Get_ConParticulars',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {

                            AutoComplete.push({
                                label: item.BudgetDescription,
                                BudgetID: item.BudgetID
                            });
                        });

                        response(AutoComplete);
                        PopulateConGrid(serverResponse.Data);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#txtConReceiptType').val(i.item.label);
            Get_ConParticulars();
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
           });

    $('#btnConBudgetAdd').click(function () {
        //Add_SaveConDetail();
        CheckClosingStatus_Concurrance();
    });

    $('#dialog-ConBudgetdetail').on('hidden.bs.modal', function () {
        Get_ConParticulars();
    })
});


function Set_ConDefaultDate() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtConEffectiveDate').val(output);

    $('#txtConEffectiveDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });
}
function Get_ConCondition() {
    //var budgetTypeID = $('#ddlConBudgetType').val();
    //if (budgetTypeID == 'R') {
    //    $('.clsConCatSubCat').css('display', '');
    //    $('#ddlConCategory').val('');
    //    $('#ddlConSubCategory').val('');
    //    $('#txtConReceiptType').val('');
    //}
    //else {
    //    $('.clsConCatSubCat').css('display', 'none');
    //    $('#ddlConCategory').val('E');
    //    $('#ddlConSubCategory').val('');
    //    $('#txtConReceiptType').val('');
    //}
    Get_ConParticulars();
}
function Get_ConParticulars() {

    var budgetTypeID = $('#ddlConBudgetType').val();
    var catID = $('#ddlConCategory').val();
    var subcatID = $('#ddlConSubCategory').val();
    var SectorID = $('#ddlSector').val();
    if (SectorID == '') { alert('Please Select Sector.'); $('#ddlSector').focus(); return false; }

    var FinYear = ($("#ddlFFinYear option:selected").text() == "" || $("#ddlFFinYear option:selected").text() == "FinYear") ? localStorage.getItem("FinYear") : $("#ddlFFinYear option:selected").text();
   
    if (budgetTypeID != "") {
        var E = "{TrasactionType: '" + "ConParticulars" + "', BudgetTypeID: '" + budgetTypeID + "', CategoryID: '" + catID + "', SubCategoryID: '" + subcatID + "', SectorID: '" + $('#ddlSector').val() + "',  Desc: '" + $.trim($('#txtConReceiptType').val()) + "', FinYear: '" + $.trim(FinYear) + "'}";// alert(E);

        alert(E);

        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_ConParticulars',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                PopulateConGrid(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }
}
function PopulateConGrid(detail) {
    var html = "";

    var table = $('#myConTable');
    $('#myConTable tbody tr.myData').remove();

    if (detail.length > 0) {
        for (var i = 0; i < detail.length; i++) {
            html += "<tr class='myData'>"
                + "<td style='text-align:right; display:none;' class='BudgetID' >" + detail[i].BudgetID + "</td>"
                + "<td style='text-align:left; '>" + detail[i].BudgetDescription + "</td>"
                + "<td style='text-align:center'><select class='clsStatusCode' onchange='PrintAnother(this, " + detail[i].BudgetID +")'><option selected value=''>Select</option><option value='L'>All</option><option value='A'>Used</option><option value='U'>Unused</option></select></td>"   //(detail[i].BudgetCode == null ? "" : detail[i].BudgetCode)
                + "<td style='text-align:right;' class='clsBudgetAmt' >" + (parseFloat(detail[i].BudgetAmount)).toFixed(2) + "</td>"
                + "<td style='text-align:center;' ><div style='clear:both;'><div style='float:left; text-align:right; width:80%'><span  class='clsConAmount'>" + (parseFloat(detail[i].ConcurranceAmount)).toFixed(2) +  "</span></div><div style='float:right; text-align:right; width:20%'><img src='/Content/Images/add.png' title='Add' style='margin-left:20px;height:20px; width: 20px; cursor:pointer;' onclick='PopUP_ConBudgetDetail(this)' /></div></div></td>"
                + "<td style='text-align:right;' class='clsBalAmt' >" + (parseFloat(detail[i].BalanceAmount)).toFixed(2) + "</td>"
            html + "</tr>";
        }

        table.append(html);
    }
}


//POPUP DATA CALCULALTION
function PopUP_ConBudgetDetail(ID) {

    $('#txtConReferanceNo').val(''); $('#txtConBudgetAmt').val(''); $('#txtConRemarks').val('');

    var BudgetID = $(ID).closest('tr').find('.BudgetID').text();
    var BudgetCode = $(ID).closest('tr').find('.clsBudgetCode').val();
    var BudgetAmount = $(ID).closest('tr').find('.clsBudgetAmt').text();
    var ConAmount = $(ID).closest('tr').find('.clsConAmount').text();
    var BalAmount = $(ID).closest('tr').find('.clsBalAmt').text();

    $('#hdnConBudgetID').val(BudgetID);
    $('#hdnConBudgetCode').val(BudgetCode);
    $('#hdnConBudgetAmount').val(BudgetAmount);
    $('#hdnConAmount').val(ConAmount);
    $('#hdnConBalAmount').val(BalAmount);

    Bind_ConBudgetDetail(BudgetID);
    $("#dialog-ConBudgetdetail").modal("show");
}
function Show_ConBudgetDetail(data) {
    var html = "";

    $("#tbl_ConBudgetDetail tbody tr.myData").remove();
    $('.dvErrorMsg').css('display', 'none');
    $('#errorMsg').text('');

    if (data.length > 0) {

        
        var $this = $('#tbl_ConBudgetDetail .test_0');
        $parentTR = $this.closest('tr');

        for (var i = 0; i < data.length; i++) {
            html += "<tr class='myData' >"
                + "<td class='conSlNo' style='display:none;'>" + data[i].ConcurranceSlNo + "</td>"
                + "<td class='effDate' style='text-align:center;'>" + data[i].ConcurranceDate + "</td>"
                + "<td class='refNo' style='text-align:center;'>" + data[i].ReferanceNo + "</td>"
                + "<td  class='bugtAmt' style='text-align:right;'>" + data[i].ConcurranceAmount + "</td>"
                + "<td class='remarks'>" + (data[i].Remarks == null ? "" : data[i].Remarks) + "</td>"
                + "<td style='text-align:center'>"
                + "<img src= '/Content/Images/Edit.png' title= 'Edit' style= 'width:23px;height:21px;cursor:pointer; text-align:center;' onClick= 'EditConBudgetDetail(this);' />&nbsp;&nbsp;"
                + "<img src= '/Content/Images/printer.png' title='Print' style='width:18px;height:19px;cursor:pointer; text-align:center;' onClick='PrintConBudgetDetail(this);' />&nbsp;&nbsp;"
                + "<img src= '/Content/Images/Transfer.png' title='Transfer' style='width:18px;height:18px;cursor:pointer; text-align:center;' onClick='TransferConBudgetDetail(this);' /><br/>"
                + "<select selected value='' style='margin-top:6px; width:100%; display:none;' class='clsSector' onchange='BindBudget(this)'><option>Select</option></select>"
                + "<select selected value='' style='margin-top:6px; width:100%; display:none;' class='clsBudget' onchange='Confirm_Transfer(this)'><option>Select</option></select> </td > "
               
            html + "</tr>";
        }
        $parentTR.after(html);
        BindSector();
       // BindBudget();
    }
}
function DeleteConBudgetDetail(ID) {
    $(ID).closest('tr').remove(); $("#txtConBudgetAmt").focus();

    var grdLen = $('#tbl_ConBudgetDetail tbody tr.myData').length; var TotalAmount = 0;
    if (grdLen > 0) {
        $('#tbl_ConBudgetDetail tbody tr.myData').each(function () {

            var bugtAmt = $(this).find('.bugtAmt').text();

            TotalAmount = parseFloat(TotalAmount) + parseFloat(bugtAmt);

        });
    }

    var BudgetAmt = $('#hdnConBudgetAmount').val(); 
    if (parseFloat(TotalAmount) > parseFloat(BudgetAmt)) {
        $('.dvErrorMsg').css('display', '');
        $('#errorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
    }
    else {
        $('.dvErrorMsg').css('display', 'none');
        $('#errorMsg').text('');
    }
    return false;
}
function Bind_ConBudgetDetail(BudgetID) {
    if (BudgetID != "") {
        var E = "{TrasactionType: '" + "ConSelect" + "', BudgetID: '" + BudgetID + "', SectorID: '" + $('#ddlSector').val() + "', FinYear: '" + $("#ddlFFinYear option:selected").text() + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data;
                $('#page-wrapper').toggleClass('sk-loading');
                Show_ConBudgetDetail(t);
                $('#page-wrapper').removeClass('sk-loading');
            }
        });
    }

}
function AddConBudgetDetail() {

    var bugtAmt = $('#txtConBudgetAmt').val();
    var effDate = $('#txtConEffectiveDate').val();
    var refNo = $('#txtConReferanceNo').val();
    var remarks = $('#txtConRemarks').val();

    if (bugtAmt == "" && bugtAmt <= 0) {
        $("#txtConBudgetAmt").attr("placeholder", "Enter amount"); $("#txtConBudgetAmt").addClass("Red"); $('#txtConBudgetAmt').focus(); return false;
    }
    if (effDate == "") {
        $("#txtConEffectiveDate").attr("placeholder", "Enter date"); $("#txtConEffectiveDate").addClass("Red"); $('#txtConEffectiveDate').focus(); return false;
    }
    //if (refNo == "") {
    //    $("#txtConReferanceNo").attr("placeholder", "Enter refNo."); $("#txtConReferanceNo").addClass("Red"); $('#txtConReferanceNo').focus(); return false;
    //}

    var html = "";

    var $this = $('#tbl_ConBudgetDetail .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td  class='bugtAmt' style='text-align:right;'>" + bugtAmt + "</td>"
        + "<td class='effDate' style='text-align:center;'>" + effDate + "</td>"
        + "<td class='refNo' style='text-align:center;'>" + refNo + "</td>"
        + "<td class='remarks'>" + remarks + "</td>"
        + "<td style='text-align:center'>"
        + "<img src= '/Content/Images/Edit.png' title= 'Edit' style= 'width:23px;height:21px;cursor:pointer; text-align:center;' onClick= 'EditConBudgetDetail(this);' />&nbsp;&nbsp;"
        + "<img src= '/Content/Images/printer.png' title='Print' style='width:18px;height:19px;cursor:pointer; text-align:center;' onClick='PrintConBudgetDetail(this);' /></td >"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtConReferanceNo").val(''); $("#txtConRemarks").val(''); $("#txtConBudgetAmt").focus();
    return false;
}

function ConSave() {

    var grdLen = $('#tbl_ConBudgetDetail tbody tr.myData').length; var ArrList = []; var master = {}; var TotalAmount = 0;
    if (grdLen > 0) {
        $('#tbl_ConBudgetDetail tbody tr.myData').each(function () {

            var bugtAmt = $(this).find('.bugtAmt').text();
            var effDate = $(this).find('.effDate').text();
            var refNo = $(this).find('.refNo').text();
            var remarks = $(this).find('.remarks').text();

            if (effDate != "") {
                var a = effDate.split('/');
                effDate = a[2] + "-" + a[1] + "-" + a[0];
            }

            TotalAmount = parseFloat(TotalAmount) + parseFloat(bugtAmt);

            ArrList.push({
                'BudgetID': $('#hdnConBudgetID').val(), 'ConcurranceAmount': bugtAmt, 'ConcurranceDate': effDate, 'ReferanceNo': refNo, 'Remarks': remarks
            });
        });
    }

    var BudgetAmt = $('#hdnConBudgetAmount').val(); //alert(BudgetAmt); alert(TotalAmount);
    if (parseFloat(TotalAmount) > parseFloat(BudgetAmt)) 
    {
        $('.dvErrorMsg').css('display', '');
        $('#errorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
    }
    else
    {
        $('.dvErrorMsg').css('display', 'none');
        $('#errorMsg').text('');
    }

    var E = "{BudgetID: '" + $('#hdnConBudgetID').val() + "', SectorID: '" + $('#ddlSector').val() + "',  Master: " + JSON.stringify(ArrList) + "}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BudgetMaster/InsertUpdate_Concurrance',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (t == "success" || t == "updated") {

                swal({
                    title: "Success",
                    text: 'Concurrance Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $('#tab-1').removeClass('active');
                            $('#tab-2').removeClass('active');
                            $('#tab-3').addClass('active');
                            $("#dialog-ConBudgetdetail").modal("hide");
                            Get_ConParticulars();
                            return false;
                        }
                    });
            }
        }
    });
}



//New logic
function CheckLimit()
{
    var limitAmt = $("#hdnFinancialLimit").val();
    var budgtAmt = $("#txtConBudgetAmt").val();
    if (parseFloat(limitAmt) > 0) {
        $("#txtConBudgetAmt").prop('disabled', false);
        if (parseFloat(budgtAmt) > parseFloat(limitAmt)) {
            $('.msgLimitExceeded').css('display', ""); $("#txtConBudgetAmt").val(limitAmt);
        }

        else {
            $('.msgLimitExceeded').css('display', "none");
        }
    }
    else {
        $("#txtConBudgetAmt").prop('disabled', true);
    }
}
function EditConBudgetDetail(ID) {

    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var bugtAmt = $(ID).closest('tr').find('.bugtAmt').text();
    var effDate = $(ID).closest('tr').find('.effDate').text();
    var refNo =   $(ID).closest('tr').find('.refNo').text();
    var remarks = $(ID).closest('tr').find('.remarks').text();


    $("#hdnConSlNo").val(conSlNo); $("#txtConBudgetAmt").val(bugtAmt); $("#txtConEffectiveDate").val(effDate); $("#txtConReferanceNo").val(refNo); $("#txtConRemarks").val(remarks);
    $(ID).closest('tr').remove();
}
function PrintConBudgetDetail(ID)
{
    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var refNo = $(ID).closest('tr').find('.refNo').text();

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "ConcurranceReport.rpt",
        FileName: refNo
    });

    detail.push({
        BudgetID: $("#hdnConBudgetID").val(),
        SectorID: $("#ddlSector").val(),
        ConcurranceSlNo: conSlNo,
        FinYear: $("#ddlFFinYear option:selected").text()
    });

    final = {
        Master: master,
        Detail: detail
    }
    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function Add_SaveConDetail()
{
    alert(4545);
    var TotalAmount = 0;
    var limitAmt = $("#hdnFinancialLimit").val(); 
    var bugtAmt = $('#txtConBudgetAmt').val();
    var effDate = $('#txtConEffectiveDate').val();
    var refNo = $('#txtConReferanceNo').val();
    var remarks = $('#txtConRemarks').val();
    var ConSlNo = $('#hdnConSlNo').val();

   
    if (parseFloat(limitAmt) <= 0) {
        $("#txtConBudgetAmt").prop('disabled', true);  return false;
    }

    
    if (bugtAmt >= 0) {
        //if (bugtAmt == "" || bugtAmt <= 0) {
        //    $("#txtConBudgetAmt").attr("placeholder", "Enter amount"); $("#txtConBudgetAmt").addClass("Red"); $('#txtConBudgetAmt').focus();
        //    $('.msgLimitExceeded').css('display', "none"); return false;
        //}
        alert(1);
        if (effDate == "") {
            $("#txtConEffectiveDate").attr("placeholder", "Enter date"); $("#txtConEffectiveDate").addClass("Red"); $('#txtConEffectiveDate').focus(); return false;
        }

        var grdLen = $('#tbl_ConBudgetDetail tbody tr.myData').length; 
        if (grdLen > 0) {
            $('#tbl_ConBudgetDetail tbody tr.myData').each(function () {

                var buAmt = $(this).find('.bugtAmt').text();
                TotalAmount = parseFloat(TotalAmount) + parseFloat(buAmt);

            });
        }
        //else {
        //    alert(2);
        //    TotalAmount = parseFloat(bugtAmt);
        //}
        alert(2);
        var BudgetAmt = $('#hdnConBudgetAmount').val(); alert(TotalAmount); alert(BudgetAmt);
        if ((parseFloat(TotalAmount) + parseFloat(bugtAmt)) > parseFloat(BudgetAmt)) {
            alert(3);
            $('.dvErrorMsg').css('display', '');
            $('#errorMsg').text('Sorry ! Total Concurrance amount is greater than ' + BudgetAmt); return false;
        }
        else {
            $('.dvErrorMsg').css('display', 'none');
            $('#errorMsg').text('');
        }
        var FinYear = $("#ddlFFinYear option:selected").text();
        var E = "{BudgetID: '" + $('#hdnConBudgetID').val() + "', SectorID: '" + $('#ddlSector').val() + "', ConSlNo: '" + ConSlNo + "',   bugtAmt: '" + bugtAmt + "', effDate: '" + effDate + "', refNo: '" + refNo + "', remarks: '" + remarks + "', FinYear: '" + FinYear + "'}";
        alert(E);
        //return false;

        $.ajax({
            url: '/Accounts_Form/BudgetMaster/InsertUpdate_Concurrance',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (data) {
                var t = data.Data;
                var Code = data.Message;
                if (Code == 0) {
                    $("#hdnConSlNo").val(''); $("#txtConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtConReferanceNo").val(''); $("#txtConRemarks").val(''); $("#txtConBudgetAmt").focus();
                    Show_ConBudgetDetail(t);
                }
                else
                {
                    alert('Sorry ! You can not enter less amount.');
                    return false;
                }

                //$("#dialog-ConBudgetdetail").modal("hide");
            }
        });
    }
}
function CheckClosingStatus_Concurrance() {

    var a = $.trim($("#txtConEffectiveDate").val());
    var b = a.split('/');
    var vchDate = b[2] + '-' + b[1] + '-' + b[0];

    var E = "{MenuId: '" + 82 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Add_SaveConDetail();
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}


//Print Section details
function Print() {

    var finYear = $("#ddlPFinYear option:selected").text();
    var budgetType = $("#ddlPrintConBudgetType").val();
    if (finYear == "") {
        $("#ddlPFinYear").focus(); return false;
    }
    if (budgetType == "")
    {
        $("#ddlPrintConBudgetType").focus(); return false;
    }

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: budgetType == "L" ? "BudgetTypewisePhysicalRevenueReport.rpt" :  "BudgetTypewiseReport.rpt",
        FileName: $("#ddlPrintConBudgetType option:selected").text(),
        Database:''
    });

    detail.push({
        SectorID: $("#ddlSector").val(),
        BudgetType: budgetType,
        SFinYear: finYear
    });

    final = {
        Master: master,
        Detail: detail
    }
    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function PrintAnother(ID, BudgetID)
{
    var finYear = $("#ddlFFinYear option:selected").text(); 
    var StatusCode = $(ID).closest('tr').find('.clsStatusCode').val(); 
    $('.clsStatusCode').val(''); 

    if (StatusCode != "") {

        var final = {}; var master = []; var detail = [];

        master.push({
            ReportName: "FCStatus.rpt",
            FileName: "FCStatus",
            Database: ''
        });

        detail.push({
            SectorID: $("#ddlSector").val(),
            BudgetType: StatusCode,
            FinYear: finYear,
            BudgetId: BudgetID
        });

        final = {
            Master: master,
            Detail: detail
        }
        var left = ($(window).width() / 2) - (950 / 2),
            top = ($(window).height() / 2) - (650 / 2),
            popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
        popup.focus();
    }
}
function Excel() {

    var finYear = $("#ddlPFinYear option:selected").text();
    var budgetType = $("#ddlPrintConBudgetType").val();
    if (finYear == "") {
        $("#ddlPFinYear").focus(); return false;
    }
    if (budgetType == "") {
        $("#ddlPrintConBudgetType").focus(); return false;
    }

    var master = []; var final = {};

    master.push({
        SectorID: $('#ddlSector').val(),
        BudgetType: budgetType,
        SFinYear: finYear,
        BTypeName: $("#ddlPrintConBudgetType option:selected").text()
    });
    final = {
        Master: master
    }
   
    location.href = "/Accounts_Form/BudgetMaster/Excel?ReportName=" + JSON.stringify(final);
}
function BindSector()
{
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BudgetMaster/Bind_Sector',
        contentType: "application/json; charset=utf-8",
        data: {},
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('.clsSector').empty();
            $('.clsSector').append('<option value="" selected>Select</option');
            if (t.length > 0) {
                $(t).each(function (index, item) {
                    $('.clsSector').append('<option value=' + item.SectorID + '>' + item.SectorName + '</option>');
                });
            }
        }
    });
}
function BindBudget(ID)
{
    var secid = $(ID).val(); 
    if (secid != "") {
        var FinYear = ($("#ddlFFinYear option:selected").text() == "" || $("#ddlFFinYear option:selected").text() == "FinYear") ? localStorage.getItem("FinYear") : $("#ddlFFinYear option:selected").text();
        var E = "{TrasactionType: '" + "TransBudgetHead" + "', BudgetID: '" + "" + "', SectorID: '" + secid + "', FinYear: '" + FinYear + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/BudgetMaster/Get_BudgetDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('.clsBudget').empty();
                $('.clsBudget').append('<option value="" selected>Select</option');
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('.clsBudget').append('<option value=' + item.BudgetID + '>' + item.BudgetDescription + '</option>');
                    });
                    $('.clsBudget').val($('#hdnConBudgetID').val());
                }
            }
        });
    }
}
function TransferConBudgetDetail(ID) {


    $('#tbl_ConBudgetDetail tbody tr.myData').find('.clsSector').css('display', 'none');
    $('#tbl_ConBudgetDetail tbody tr.myData').find('.clsBudget').css('display', 'none');
    $('.clsBudget').empty();
    $('.clsBudget').append('<option value="" selected>Select</option');

    $(ID).closest('tr').find('.clsSector').css('display', '');
    $(ID).closest('tr').find('.clsBudget').css('display', '');
}
function Confirm_Transfer(ID)
{
  
    var refNo = $(ID).closest('tr').find('.refNo').text();

    swal({
        title: "Warning",
        text: "Are you sure want to Transfer FCNo. \n" + refNo,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                Transfer_FC(ID);
            }

        });
}
function Transfer_FC(ID) {

    var BudgetID = $(ID).closest('tr').find('.clsBudget').val(); 
    var conSlNo = $(ID).closest('tr').find('.conSlNo').text();
    var refNo = $(ID).closest('tr').find('.refNo').text();
    var secID = $(ID).closest('tr').find('.clsSector').val();

    
    if (BudgetID == ""){ $(ID).closest('tr').find('.clsBudget').focus(); return false; }

    var E = "{BudgetID: '" + $('#hdnConBudgetID').val() + "', TransBudgetID: '" + BudgetID + "', SectorID: '" + secID + "', CurrSectorID: '" + $('#ddlSector').val() + "', conSlNo: '" + conSlNo + "', refNo: '" + refNo + "', FinYear: '" + $.trim($("#ddlFFinYear option:selected").text()) + "'}";
    //alert(E);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BudgetMaster/Transfer_Concurrance',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data; //alert(JSON.stringify(t));

            var msg = t[0]["MSG"]; //alert(msg);
            var msgCode = t[0]["ResultCount"]; //alert(msgCode);

            //$("#hdnConSlNo").val(''); $("#txtConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtConReferanceNo").val(''); $("#txtConRemarks").val(''); $("#txtConBudgetAmt").focus();
            //Show_ConBudgetDetail(t);

            swal({
                title: "Success",
                text: msgCode == 2 ? msg : "FC No. transfered successfully.",
                type: "success",
                confirmButtonColor: "#AEDEF4",
                confirmButtonText: "OK",
                closeOnConfirm: true,
            },
                function (isConfirm) {
                    if (isConfirm) {
                        $("#hdnConSlNo").val(''); $("#txtConBudgetAmt").val(''); Set_ConDefaultDate(); $("#txtConReferanceNo").val(''); $("#txtConRemarks").val(''); $("#txtConBudgetAmt").focus();
                        Show_ConBudgetDetail(t);
                        return false;
                    }
                });


        }
    });

}



