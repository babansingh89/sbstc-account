﻿var rowCount = 0; 
var ArrayOpenListID = [];

$(document).ready(function () {

    //Load();
    var OverLayContainers = $("#overlay_Common");
    //$("#overlay_Common").css('display', 'block');

    //Only Month and Year
    $('#txtClaimMonth, #txtAgainstClaimMonth').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "M yy",
        showButtonPanel: true,
        currentText: "This Month",
        onChangeMonthYear: function (year, month, inst) {
            $(this).val($.datepicker.formatDate('M yy', new Date(year, month - 1, 1)));
        },
        onClose: function (dateText, inst) {
            var month = $(".ui-datepicker-month :selected").val();
            var year = $(".ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('M yy', new Date(year, month, 1)));
        }
    }).focus(function () {
        $(".ui-datepicker-calendar").hide();
    });

    //Allow only Decimal with Integer Value
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#myDiv').on('click', '.subdetails', function () {
       
        if ($("#txtClaimMonth").val() == "") {
            $("#txtClaimMonth").focus(); return false;
        }
        if ($("#txtSanctionAmt").val() == "") {
            $("#txtSanctionAmt").focus(); return false;
        }
        if ($("#txtOpeningBal").val() == "") {
            $("#txtOpeningBal").focus(); return false;
        }
        if ($("#txtReimBal").val() == "") {
            $("#txtReimBal").focus(); return false;
        }
        if ($("#txtExpIncurred").val() == "") {
            $("#txtExpIncurred").focus(); return false;
        }
        if ($("#txtEffKm").val() == "") {
            $("#txtEffKm").focus(); return false;
        }
        if ($("#txtEffPerKm").val() == "") {
            $("#txtEffPerKm").focus(); return false;
        }
        if ($("#txtCostPerKm").val() == "") {
            $("#txtCostPerKm").focus(); return false;
        }
        if ($("#txtAgainstClaimMonth").val() == "") {
            $("#txtAgainstClaimMonth").focus(); return false;
        }

        var l = $("#datatable tbody tr.myData").length;
        if (l == 0) {
            alert('Please generate Reimbursement details.'); return false;
        }

       
        var subdetail = $(this).closest('tr').find('.SubDetails').attr('data-subdetail'); 
        var subdetailid = $(this).closest('tr').find('.SubDetails').attr('data-subdetailid'); 
        var claim = $(this).closest('tr').find('.subdetails').text(); 
		 var ClaimID = $(this).closest('tr').find('.SubDetails').attr('data-claimid'); 
        $('#hdnSubDetailID').val(subdetailid);
		 $('#hdnClaimID').val(ClaimID); 
        $('.clsHead').text(subdetail +  ' > ' + claim);
            ShowVoucher(this, 1);
    });
    $('#myDiv').on('click', '.btngeneratefc', function () {
        var FCNo = $(this).attr('FCNo'); 
        var BudgetID = $(this).attr('budgetid'); 

        if (BudgetID != '' && BudgetID != null && BudgetID != 'null') {
            if (FCNo == '' || FCNo == null || FCNo == 'null') {
                var cnf = confirm('Are you sure want to Generate FC ?');
                if (cnf == true) {
                    SaveBudget(this);
                }
            }
        }
        else
        {
            alert('Sorry ! Subdetail is not mapped with Budget.'); return false;
        }
    });

    $('#btnGenerate').click(function (event) {
        var result = Validation(1); 
        if (result == true) {
            //Save(1);
            CheckClosingStatus();
        }
    });
    $('#btnSave').click(function (event) {
        var result = Validation(2);
        if (result == true) {
             UpdateFlag();
        }
    });
});
function validateEmail(emailField) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(emailField)) {
        alert('not valid');
    } else {
        alert('valid');
    }
}
function Blank()
{
    location.reload();
}

function Generate()
{
    if ($("#txtAgainstClaimMonth").val() == "")
    {
        $("#txtAgainstClaimMonth").focus(); return false;
    }
    var E = "{TransType:'" + "Fetch" + "', ClaimMonth:'" + $("#txtAgainstClaimMonth").val() + "'," +
        "MemoNo:'" + $("#hdnMemoNo").val() + "', SectorID:'" + $("#ddlSector").val() + "', ClaimRemimID:'" + $("#hdnTabID").val() + "', FinYear:'" + localStorage.getItem("FinYear") + "'} ";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/Load",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; 
           
            Show(t);
           
        }
    });
}
function Show(t) {
    var table = $('#datatable');
    var tbody = table.find('tbody');
    var tfoot = table.find('tfoot');
    tbody.empty();
    tfoot.empty();

    if (t.length > 0) {
        var html = "", tbl_Cnt1 = 0, tbl_Cnt2 = 0, tbl_Cnt3 = 0, tbl_Cnt4 = 0, tbl_Cnt5 = 0, TotalInocme = 0, TotalExpense=0, TotalA = 0, TotalB=0;

        for (var i = 0; i < t.length; i++) {
            var ClaimID = t[i].ClaimID;

            //Income
            if (ClaimID == 1)
            {
                html += "<tr class='myData'>"
                    + "<td>" + (tbl_Cnt1 == 0 ? "1." : "") + "</td>"
                    + "<td class='SubDetails' data-claimid='" + ClaimID +"' data-subdetailid='" + t[i].SubDetailID +"'>" + (tbl_Cnt1 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' style='cursor:pointer;' >" + t[i].SubDetail + " </td>"
                    + "<td><input type='text' disabled class='form-control allownumericwithdecimal climIncomeAmt climAmt' autocomplete='off' placeholder='0.00' onkeyup='Calulate_IncomeTotal(this);' style='text-align:right;' value='" + t[i].Amount +"' /></td>"
                    + "<td class='" + (tbl_Cnt1 == 0 ? "remarks" : "") + "' >" + (tbl_Cnt1 == 0 ? "<textarea type='text' class='form-control remark' autocomplete='off' placeholder='Remarks' >" + t[i].Remarks +"</textarea>" : "") + "</td>"

                html + "</tr>";
                tbl_Cnt1 = 1;
                TotalInocme = parseFloat(TotalInocme) + parseFloat(t[i].Amount);
            }
            if (ClaimID == 2) {

                html += "<tr class='myData'>"
                    + "<td>" + (tbl_Cnt2 == 0 ? "" : "") + "</td>"
                    + "<td class='SubDetails' data-claimid='" + ClaimID +"' data-subdetailid='" + t[i].SubDetailID +"'>" + (tbl_Cnt2 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' style='cursor:pointer;' >" + t[i].SubDetail + " </td>"
                    + "<td><input type='text' disabled class='form-control allownumericwithdecimal climIncomeAmt climAmt' autocomplete='off' placeholder='0.00' onkeyup='Calulate_IncomeTotal(this);' style='text-align:right;' value='" + t[i].Amount +"' /></td>"
                    + "<td class='" + (tbl_Cnt2 == 0 ? "remarks" : "") + "'>" + (tbl_Cnt2 == 0 ? "<textarea type='text' class='form-control remark' autocomplete='off' placeholder='Remarks' >" + t[i].Remarks +"</textarea>" : "") + "</td>"
                html + "</tr>";

                tbl_Cnt2 = 1;
                TotalInocme = parseFloat(TotalInocme) + parseFloat(t[i].Amount);
            }

            //Expense
            if (ClaimID == 3) {
                
                if (tbl_Cnt3 == 0)
                {
                    html += "<tr class='myDatas' style='background-color:#e8dede;'>"
                    + "<td colspan='3' style='text-align:right; font-weight:bold'>Total Income</td>"
                        + "<td style='text-align:right; font-weight:bold' class='ITotal'>" + TotalInocme.toFixed(2) + "</td>"
                    + "<td></td>"
                    html + "</tr>";

                    html += "<tr class='myDatas'>"
                        + "<td colspan='4' style='text-align:center; font-weight:bold'>Monthly Claim of Imprest</td>"
                        + "<td colspan='6' style='text-align:center; font-weight:bold'>Remarks</td>"
                    html + "</tr>";
                }
                html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt3 == 0 ? "A" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-claimid='" + ClaimID +"' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails +"'>" + (tbl_Cnt3 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td style='cursor:pointer;' ><span class='subdetails'>" + t[i].SubDetail + "</span>"
                    + "<span class='btngeneratefc' Budgetid= '" + t[i].BudgetID + "' FCNo= '" + t[i].FCNo + "'  style= 'display:" + (t[i].isClaimReim == 0 ? "none" : "") + "' > <br /> <label style='color:#7878c7'>Generate FC</label></span>"
                    + "<br/><span style='color:darkcyan; font-size:10px'>" + ((t[i].FCNo == "" || t[i].FCNo == null) ? "" : ("FCNo: [ " + t[i].FCNo + " ]")) + "</span>"
                    + "<br/><span style='color:darkcyan; font-size:10px'>" + ((t[i].MemoNo == "" || t[i].MemoNo == null) ? "" : ("MemoNo: [ " + t[i].MemoNo + " ]")) + "</span></td > "
                    + "<td><input type='text' disabled class='form-control allownumericwithdecimal climExpenseAmt climAmt' autocomplete='off' placeholder='0.00' onkeyup='Calulate_ExpenseTotal(this);' style='text-align:right;' value='" + t[i].Amount +"' /></td>"
                    + "<td class='" + (tbl_Cnt3 == 0 ? "remarks" : "") + "' colspan='6'>" + (tbl_Cnt3 == 0 ? "<textarea type='text' class='form-control remark' autocomplete='off' placeholder='Remarks' ></textarea>" : "") + "</td>"
                html + "</tr>";
                tbl_Cnt3 = 1;
                TotalExpense = parseFloat(TotalExpense) + parseFloat(t[i].Amount);
				 TotalA = parseFloat(TotalA) + parseFloat(t[i].Amount);
            }
            if (ClaimID == 4) {
                var display = '';

                html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt4 == 0 ? "B" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-claimid='" + ClaimID +"' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails +"'>" + (tbl_Cnt4 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td style='cursor:pointer;' ><span class='subdetails'>" + t[i].SubDetail + "</span>"
                    + "<span class='btngeneratefc' Budgetid= '" + t[i].BudgetID + "' FCNo= '" + t[i].FCNo + "'  style= 'display:" + (t[i].isClaimReim == 0 ? "none" : "") + "' > <br /> <label style='color:#7878c7'>Generate FC</label></span>"
                    + "<br/><span style='color:darkcyan; font-size:10px'>" + ((t[i].FCNo == "" || t[i].FCNo == null) ? "" : ("FCNo: [ " + t[i].FCNo + " ]")) + "</span>"
                    + "<br/><span style='color:darkcyan; font-size:10px'>" + ((t[i].MemoNo == "" || t[i].MemoNo == null) ? "" : ("MemoNo: [ " + t[i].MemoNo + " ]")) +"</span></td > "
                    + "<td><input type='text' disabled class='form-control allownumericwithdecimal climExpenseAmt climAmt' autocomplete='off' placeholder='0.00' onkeyup='Calulate_ExpenseTotal(this);' style='text-align:right;' value='" + t[i].Amount +"' /></td>"
                    + "<td class='" + (tbl_Cnt4 == 0 ? "remarks" : "") + "' colspan='6'>" + (tbl_Cnt4 == 0 ? "<textarea type='text' class='form-control remark' autocomplete='off' placeholder='Remarks' ></textarea>" : "") +"</td>"
                html + "</tr>";
                tbl_Cnt4 = 1;
                TotalExpense = parseFloat(TotalExpense) + parseFloat(t[i].Amount);
				 TotalB = parseFloat(TotalB) + parseFloat(t[i].Amount);
            }
            if (ClaimID == 5) {

                html += "<tr class='myData'>"
                    + "<td style='font-weight:bold;'>" + (tbl_Cnt5 == 0 ? "C" : "") + "</td>"
                    + "<td style='font-weight:bold;' class='SubDetails' data-claimid='" + ClaimID +"' data-subdetailid='" + t[i].SubDetailID + "' data-subdetail='" + t[i].SubDetails +"'>" + (tbl_Cnt5 == 0 ? t[i].SubDetails : "") + "</td>"
                    + "<td class='subdetails' style='cursor:pointer;' >" + t[i].SubDetail + "</td>"
                    + "<td><input type='text' disabled class='form-control allownumericwithdecimal climExpenseAmt climAmt' autocomplete='off' placeholder='0.00' onkeyup='Calulate_ExpenseTotal(this);' style='text-align:right;' readonly='true' value='" + t[i].Amount +"' /></td>"
                    + "<td class='" + (tbl_Cnt5 == 0 ? "remarks" : "") + "' colspan='6'>" + (tbl_Cnt5 == 0 ? "<textarea type='text' class='form-control remark' autocomplete='off' placeholder='Remarks' ></textarea>" : "") +"</td>"
                html + "</tr>";
                tbl_Cnt5 = 1;
                TotalExpense = parseFloat(TotalExpense) + parseFloat(t[i].Amount);
            }
        }
        tfoot.append("<tr style='background-color:#e8dede;'>"
            + "<td colspan='3' style='text-align:right; font-weight:bold'>Total Expense</td>"
            + "<td style='text-align:right; font-weight:bold' class='ETotal'>" + TotalExpense.toFixed(2) + "</td>"
            + "<td></td>"
            + "</tr>");

        tbody.append(html);

        $("#txtExpIncurred").val(parseFloat(TotalA) + parseFloat(TotalB));

        var t1 = $("#txtExpIncurred").val() == "" ? 0 : $("#txtExpIncurred").val();
        var t2 = $("#txtReimBal").val() == "" ? 0 : $("#txtReimBal").val(); 
        var t3 = $("#txtOpeningBal").val() == "" ? 0 : $("#txtOpeningBal").val(); 
        var a = parseFloat(t1) - (parseFloat(t2) + parseFloat(t3)); 
        $("#txtExpIncurredoutsale").val(a); 

        Calculate_Effective();
    }
}
function ShowVoucher(ID, con)
{
   
    var ClaimID = $(ID).closest('tr').find('.SubDetails').attr('data-claimid'); 
    var E = "{TransType:'" + "VoucherDetails" + "', SubDetailID:'" + $('#hdnSubDetailID').val() + "', ClaimID:'" + $('#hdnClaimID').val() + "', MonYear:'" + $("#txtAgainstClaimMonth").val() + "', MemoNo:'" + $("#hdnMemoNo").val() + "', SectorID:'" + $("#ddlSector").val() + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/VoucherDetails",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; //alert(JSON.stringify(t));

            if (t.length > 0) {
                if(con == 1)
                    ArrayOpenListID.push($('#myDiv').html());

                var table = $('#datatable');
                var thead = table.find('thead');
                var tbody = table.find('tbody');
                var tfoot = table.find('tfoot');
                thead.empty();
                tbody.empty();
                tfoot.empty();
                var html = '', Total=0;
                for (var i = 0; i < t.length; i++) {
                    Total = Total + t[i].VoucherAmount;
                    html += "<tr class='myData'>"
                        + "<td style='font-weight:bold; display:none;' class='TabID' >" + t[i].TabID + "</td>"
                        + "<td style='font-weight:bold; display:none;' class='VoucherNumber' >" + t[i].VoucherNumber + "</td>"
                       + "<td style='cursor:pointer; color:blue;' onclick='GotoVoucher(" + t[i].VoucherNumber + ")' >" + t[i].VchNo + "</td>"
                        + "<td style='cursor:pointer;' >" + t[i].VchDate + "</td>"
                        + "<td style='cursor:pointer;' >" + t[i].Narration + "</td>"
                        + "<td style='text-align:right;' class='VoucherAmount'>" + t[i].VoucherAmount + "</td>"
                        + "<td><select selected value='' style='margin-top:6px; width:100%;' class='clsClaimgroup' onchange= 'ClaimGroupDetails(this)' > <option>Select</option></select >"
                        + "<select selected value='' style='margin-top:6px; width:100%; display:none; ' class='clsClaimgroupdetail' onchange='Confirm_Transfer(this)'><option>Select</option></select></td > "
                    html + "</tr>";
                }
                thead.append("<tr><th>VchNo</th><th>VchDate</th><th>Narration</th><th>VoucherAmount</th><th><button type='button' id='btnBack class='btn btn-info' onclick='Back();'><span class='glyphicon glyphicon-arrow-left' aria-hidden='true' style='padding-right:4px'></span>Back</button></th></tr>");
                tbody.append(html);
                tfoot.append("<tr style='background-color:#e8dede;'>"
                    + "<td colspan='3' style='text-align:right; font-weight:bold'>Total</td>"
                    + "<td style='text-align:right; font-weight:bold' class='Total'>" + Total.toFixed(2) + "</td>"
                    + "<td></td>"
                    + "</tr>");
                ClaimGroup();
            }
        }
    });
}
function GotoVoucher(VoucherNumber)
{
    window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank');
}
function ClaimGroup() {
   
    var E = "{TransType:'" + "ClaimGroup" + "', ClaimMonth:'" + "" + "', MemoNo:'" + "" + "', SectorID:'" + $("#ddlSector").val() + "', ClaimRemimID:'" + "" + "', FinYear:'" + localStorage.getItem("FinYear") + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/ClaimingReimbursement/Load',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; //alert(JSON.stringify(t));

                $('.clsClaimgroup').empty();
                $('.clsClaimgroup').append('<option value="" selected>Select</option');
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('.clsClaimgroup').append('<option value=' + item.ClaimID + '>' + item.SubDetails + '</option>');
                    });
                }
            }
        });
    
}
function ClaimGroupDetails(ID) {

    var claimID = $(ID).val();
    if (claimID != "") {
        var E = "{TransType: '" + "ClaimGroupDetail" + "', SubDetailID: '" + claimID + "', MonYear: '" + "" + "', SectorID: '" + $("#ddlSector").val() + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/ClaimingReimbursement/VoucherDetails',
            contentType: "application/json; charset=utf-8",
            data: E,
            dataType: "json",
            success: function (data, status) {
                var t = data.Data; 

                $('.clsClaimgroupdetail').empty();
                $('.clsClaimgroupdetail').append('<option value="" selected>Select</option');
                if (t.length > 0) {
                    $(t).each(function (index, item) {
                        $('.clsClaimgroupdetail').append('<option value=' + item.SubDetailID + '>' + item.SubDetail + '</option>');
                    });

                    $('.clsClaimgroup').val('');
                    $(ID).val(claimID);

                    $('.clsClaimgroupdetail').css('display', 'none');
                    $('.clsClaimgroupdetail').val('');
                    $(ID).closest('tr').find('.clsClaimgroupdetail').css('display','');
                }
            }
        });
    }

}
function Confirm_Transfer(ID) {

    swal({
        title: "Warning",
        text: "Are you sure want to Transfer.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                Transfer_Claim(ID);
            }

        });
}
function Transfer_Claim(ID) {

    var TabID = $(ID).closest('tr').find('.TabID').text(); //alert(TabID);
    var subdetailID = $(ID).closest('tr').find('.clsClaimgroupdetail').val(); //alert(subdetailID);
    var VoucherNumber = $(ID).closest('tr').find('.VoucherNumber').text(); //alert(VoucherNumber);

    var E = "{TransType: '" + "UpdateClaimVoucher" + "',  TabID: '" + TabID + "', VoucherNumber: '" + VoucherNumber + "', SubDetailID: '" + subdetailID + "', MemoNo: '" + $("#hdnMemoNo").val() + "', DetailID: '" + $("#hdnTabID").val() + "', SectorID: '" + $("#ddlSector").val() + "'}";// alert(E);

    //alert(E);
    //return false;
    
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/Update",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            t = data.Data;
          
            ShowVoucher(ID, 2);

        }
    });
}

function Calulate_ExpenseTotal(ID)
{
    var EffKM = $("#txtEffKm").val();
    if (EffKM == "" || EffKM <= 0) {
        alert('Please Enter Effective KM.'); $(ID).val('');
        $("#txtEffKm").focus(); return false;
    }

    var calculated_total_sum = 0;
    $("#datatable .climExpenseAmt").each(function () {
        var get_textbox_value = $(this).val();

        if ($.isNumeric(get_textbox_value)) {
            calculated_total_sum += parseFloat(get_textbox_value);
        }
    });
    $('.ETotal').text(calculated_total_sum.toFixed(2));
    Calculate_Effective();
    Check_VoucherLimit(ID);
}
function Calulate_IncomeTotal(ID) {

    var EffKM = $("#txtEffKm").val();
    if (EffKM == "" || EffKM <= 0) {
        alert('Please Enter Effective KM.'); $(ID).val('');
        $("#txtEffKm").focus(); return false;
    }

    var calculated_total_sum = 0;
    $("#datatable .climIncomeAmt").each(function () {
        var get_textbox_value = $(this).val();

        if ($.isNumeric(get_textbox_value)) {
            calculated_total_sum += parseFloat(get_textbox_value);
        }
    });
    $('.ITotal').text(calculated_total_sum.toFixed(2));
    Calculate_Effective();
}
function Calculate_Effective()
{
    var EffKM = ($("#txtEffKm").val() == "" || $("#txtEffKm").val() <= 0) ? 0 : $("#txtEffKm").val(); 
    var TotalIncome = ($('.ITotal').text() == "" || $('.ITotal').text() <= 0) ? 0 : $('.ITotal').text(); 
    var TotalExpense = ($('.ETotal').text() == "" || $('.ETotal').text() <= 0) ? 0 : $('.ETotal').text(); 

    var cal_Income = TotalIncome / EffKM;
    var cal_Expense = TotalExpense / EffKM;

    $("#txtEffPerKm").val(parseFloat(isNaN(cal_Income) == true ? 0 : cal_Income).toFixed(2));
    $("#txtCostPerKm").val(parseFloat(isNaN(cal_Expense) == true ? 0 : cal_Expense).toFixed(2));

}
function Check_VoucherLimit(ID)
{
    var Amt = $(ID).val(); 
    var subdetailid = $(ID).closest('tr').find('.SubDetails').attr('data-subdetailid'); 
    var E = "{TransType:'" + "VoucherDetails" + "', SubDetailID:'" + subdetailid + "', MonYear:'" + $("#txtAgainstClaimMonth").val() + "', MemoNo:'" + $("#hdnMemoNo").val() + "', SectorID:'" + $("#ddlSector").val() + "'}";
    //alert(E);
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/VoucherDetails",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; //alert(JSON.stringify(t));
            var Total = 0;
            for (var i = 0; i < t.length; i++) {
                Total = Total + t[i].VoucherAmount;
            }
            if (parseFloat(Amt) > parseFloat(Total) || parseFloat(Amt) < parseFloat(Total))
            {
                alert('Sorry ! Total Voucher amount is ' + Total + '.\nYou can not enter less or more than Total Voucher Amount.'); $(ID).val(Total);  return false;
            }
        }
    });
}

function EditGrid()
{

    var E = "{TransType:'" + "Edit" + "', ClaimMonth:'" + "" + "', MemoNo:'" + "" + "', SectorID:'" + $("#ddlSector").val() + "', ClaimRemimID:'" + "" + "', FinYear:'" + localStorage.getItem("FinYear") + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/Load",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; 
           

            var table = $('#tbl_edit');
            var tbody = table.find('tbody');
            tbody.empty();
            var html = "";

            if (t.length > 0)
            {
                                $('#tbl_edit tbody tr').remove();

                var columnData = [
                    { "mDataProp": "MemoNo" },
                    { "mDataProp": "MemoDate" },
                    { "mDataProp": "ClaimforMonth" },
                    { "mDataProp": "TabID" },
                    { "mDataProp": "TotalIncome" },
                    { "mDataProp": "TotalExpense" },
                    { "mDataProp": "Status" },
                    {
                        "mDataProp": "Edit",
                        "render": function (data, type, row) {
                           //if (row.Status == "A")
                               //return "<img src='/Content/Images/Edit.png' title='Edit' style='width:23px;height:25px;cursor:pointer; text-align:center;' //onClick='EditDetail(\"" + row.TabID + "\", \"" + row.MemoNo + "\", \"" + row.Status +  "\");' />";
                            //else
                               return "<img src='/Content/Images/Edit.png' title='Edit' style='width:23px;height:25px;cursor:pointer; text-align:center; pointer-events:" + (row.ISEdit == 'A' ? "" : "none") + "' onClick='EditDetail(\"" + row.TabID + "\", \"" + row.MemoNo + "\", \"" + row.Status +  "\", \"" + row.ClaimforMonth +  "\");' />";

                        }
                    },
                    {
                        "mDataProp": "Delete",
                        "render": function (data, type, row) {
                            //if (row.Status == "A")
                                //return "<img src= '/Content/Images/Delete.png' title= 'Delete' style= 'width:22px;height:20px;cursor:pointer; //text-align:center;' onClick= 'Delete(\"" + row.TabID + "\", \"" + row.MemoNo + "\");' />";
                            //else
                                return "<img src= '/Content/Images/Delete.png' title= 'Delete' style= 'width:22px;height:20px;cursor:pointer; text-align:center; pointer-events:" + (row.ISEdit == 'A' ? "" : "none") + "' onClick= 'Delete(\"" + row.TabID + "\", \"" + row.MemoNo + "\");' />";

                        }
                    },
                    {
                        "mDataProp": "Print",
                        "render": function (data, type, row) {
                            return "<img src='/Content/Images/printer.png' title='Print' style='width:22px; height:20px; cursor:pointer; text-align:center;' onClick='Print(\"" + row.TabID + "\", \"" + row.MemoNo + "\");' />";

                        }
                    }];

                var columnDataHide = [3];

                var oTable = $('#tbl_edit').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,
                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": [0],
                            "className": "BillId"
                        },
                        {
                            "targets": [1],
                            "className": "text-center BillNo"
                        },
                        {
                            "targets": [2],
                            "className": "text-center BillDate"
                        },
                        {
                            "targets": [4,5],
                            "className": "text-right"
                        },
                        {
                            "targets": [7, 8, 9],
                            "className": "text-center"
                        }
                    ],
                    'iDisplayLength': 10,
                    destroy: true,
                });

                $('#dialog').modal('show');
            }
        }
    });
}
function EditDetail(TabID, MemoNo, Status, ClaimforMonth)
{
    $('#dialog').modal('hide');

    //var TabID = $(ID).closest('tr').find('.TabID').text();
    //var MemoNo = $(ID).closest('tr').find('.MemoNo').text();
    //var Status = $(ID).closest('tr').find('.Status').text();


    var E = "{TransType:'" + "EditDetail" + "', TabID:'" + TabID + "', MemoNo:'" + MemoNo + "', ClaimMonth:'" + ClaimforMonth + "', SectorID:'" + $("#ddlSector").val() + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/EditDetail",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; //alert(JSON.stringify(t));

            var table1 = t["Table"]; //alert(JSON.stringify(table1));
            var table2 = t["Table1"]; //alert(JSON.stringify(table1));

            if (table1.length > 0)
            {
                $("#hdnTabID").val(table1[0].TabID); $("#hdnMemoNo").val(table1[0].MemoNo);
                $("#txtClaimMonth").val(table1[0].ClaimingMonth); $("#txtAgainstClaimMonth").val(table1[0].ClaimforMonth); $("#txtSanctionAmt").val(table1[0].SanctionedAmt);
                $("#txtOpeningBal").val(table1[0].OpeningBal); $("#txtReimBal").val(table1[0].ReimbursementBal); $("#txtExpIncurred").val(table1[0].ExpenditureIncurred);
                $("#txtExpIncurredoutsale").val(table1[0].ExpenditureIncurredOutSale);
                $("#txtEffKm").val(table1[0].EffectiveKM); $("#txtEffPerKm").val(table1[0].EarningPerKM); $("#txtCostPerKm").val(table1[0].CostPerKM);
                $(".ITotal").text(table1[0].TotalIncome); $(".ETotal").val(table1[0].TotalExpense);
            }

            if (table2.length > 0) {
                Show(table2);
            }
            Calculate_Effective();
        }
    });
}
function Delete(TabID, MemoNo)
{
    swal({
        title: "Warning",
        text: "Are Your Sure want to Delete this record ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                //var TabID = $(ID).closest('tr').find('.TabID').text();
                //var MemoNo = $(ID).closest('tr').find('.MemoNo').text();

                var E = "{TransType:'" + "FullDelete" + "', TabID:'" + TabID + "', MemoNo:'" + MemoNo + "', SectorID:'" + $("#ddlSector").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/ClaimingReimbursement/EditDetail",
                    data: E,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (D) {
                        var t = D.Data; //alert(JSON.stringify(t));

                                Edit();
                    }
                });
            }
        });
}
function Print(TabID, MemoNo)
{
    //var TabID = $(ID).closest('tr').find('.TabID').text();
    //var MemoNo = $(ID).closest('tr').find('.MemoNo').text();

    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "Claim_ReimbursementReport.rpt",
        FileName: MemoNo,
        Database:''
    });

    detail.push({
        MemoNo: MemoNo,
        SectorID: $("#ddlSector").val()
    });

    final = {
        Master: master,
        Detail: detail
    }
    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}

function Back() {
    $('.clsHead').text('');
    if (ArrayOpenListID.length > 0) {
        var t = ArrayOpenListID.pop();
        if (t != "") {
            $('#myDiv').html(t);
        }
        //Generate();
    }
}
function Validation(con)
{
    if ($("#txtClaimMonth").val() == "") {
        $("#txtClaimMonth").focus(); return false;
    }
    if ($("#txtSanctionAmt").val() == "") {
        $("#txtSanctionAmt").focus(); return false;
    }
    if ($("#txtOpeningBal").val() == "") {
        $("#txtOpeningBal").focus(); return false;
    }
    if ($("#txtReimBal").val() == "") {
        $("#txtReimBal").focus(); return false;
    }
    //if ($("#txtExpIncurred").val() == "") {
        //$("#txtExpIncurred").focus(); return false;
   // }
    if ($("#txtExpIncurredoutsale").val() == "") {
        $("#txtExpIncurredoutsale").focus(); return false;
    }
    if ($("#txtEffKm").val() == "") {
        $("#txtEffKm").focus(); return false;
    }
    if ($("#txtEffPerKm").val() == "") {
        $("#txtEffPerKm").focus(); return false;
    }
    if ($("#txtCostPerKm").val() == "") {
        $("#txtCostPerKm").focus(); return false;
    }
    if ($("#txtAgainstClaimMonth").val() == "") {
        $("#txtAgainstClaimMonth").focus(); return false;
    }
  


    if (con == 2) {
        var l = $("#datatable tbody tr.myData").length;
        if (l == 0) {
            alert('Please generate Reimbursement details.'); return false;
        }
    }
    return true;
}
function Save(con)
{
    var ArrList = [];
    $("#datatable tbody tr.myData").each(function () {
        var Amt = $(this).closest('tr').find('.climAmt').val();
        var SubdetailID = $(this).closest('tr').find('.SubDetails').attr('data-subdetailid');

        var a = $(this).closest('tr').find('.remarks').length; var remarks = ''; 
        if (a > 0)
            remarks= $(this).closest('tr').find('.remarks').find('.remark').val(); 

       
        ArrList.push({ 'SubdetailID': SubdetailID, 'Amount': Amt, 'Remarks': remarks });
    });

    var status = '';
    if ($('#hdnStatus').val() == '')
        status = 'D';
    else if (con == 2)
        status = 'A';
    else
        status = $('#hdnStatus').val();

    var E = "{ClaimMonth:'" + $("#txtClaimMonth").val() + "', ClaimforMonth:'" + $("#txtAgainstClaimMonth").val() + "', SanctionAmt:'" + $("#txtSanctionAmt").val() + "', " +
        "OpeningBal:'" + $("#txtOpeningBal").val() + "', ReimbursementBal: '" + $("#txtReimBal").val() + "', ExpIncurred:'" + $("#txtExpIncurred").val() + "'," +
        "ExpIncurredOutSale:'" + $("#txtExpIncurredoutsale").val() + "', EffKm:'" + $("#txtEffKm").val() + "', EffPerKm: '" + $("#txtEffPerKm").val() + "', CostPerKm: '" + $("#txtCostPerKm").val() + "', " +
        "TotalIncome: '" + $(".ITotal").text() + "', TotalExpense: '" + $(".ETotal").text() + "', SectorID: '" + $("#ddlSector").val() + "', " +
        "TabID:'" + $("#hdnTabID").val() + "', MemoNo:'" + $("#hdnMemoNo").val() + "', Status:'" + "D" + "', Param: " + JSON.stringify(ArrList) + "} ";
    //(con==1 ? "" :'A')
    //alert(E);
    //return false;
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/Save",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            t = data.Data; //alert(JSON.stringify(t));

            var table1 = t["Table"]; //alert(JSON.stringify(table1));
            var table2 = t["Table1"]; //alert(JSON.stringify(table2));

            if (data.Status == 200) {
               
                $("#hdnMemoNo").val(table1[0].MemoNo);
                $("#hdnTabID").val(table1[0].ID);

                //if (a[1] == 0)
                //{
                //    alert('Sorry ! this Month is already available.'); return false;
                //}

                if (con == 2) {
                    swal({
                        title: "Success",
                        text: 'Claim Reimbursement are Saved Successfully !!\nMemo No. is :- ' + table1[0].MemoNo,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });
                }
                else
                {
                    //Generate();
                    Show(table2);
                }
            }
        }
    });
}
function UpdateFlag()
{
    var E = "{MemoNo:'" + $("#hdnMemoNo").val() + "'} ";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/ClaimingReimbursement/UpdateFlag",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            t = data.Data; //alert(JSON.stringify(t));

            if (data.Status == 200) {


                    swal({
                        title: "Success",
                        text: 'Claim Reimbursement are Saved Successfully !!\nMemo No. is :- ' + t,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                location.reload();
                                return false;
                            }
                        });

            }
        }
    });
}
function SaveBudget(ID) {

    var BudgetID = $(ID).attr('Budgetid'); 
    var FCNo = $(ID).attr('FCNo'); 
    var Amount = $(ID).closest('tr').find('.climAmt').val();
    var SubdetailID = $(ID).closest('tr').find('.SubDetails').attr('data-subdetailid'); 
   
    if (FCNo == '' || FCNo == null || FCNo == 'null') {
        var E = "{BudgetID:'" + BudgetID + "', SectorID:'" + $("#ddlSector").val() + "', FinYear:'" + $.trim($("#ddlGlobalFinYear option:selected").text()) + "', Amount:'" + Amount + "', SubdetailID:'" + SubdetailID + "'," +
            "ClaimRemimID:'" + $("#hdnTabID").val() + "', MemoNo:'" + $("#hdnMemoNo").val() + "', MonYear:'" + $("#txtAgainstClaimMonth").val() + "'}";

        //alert(E);
        //return false;

        $.ajax({
            type: "POST",
            url: "/Accounts_Form/ClaimingReimbursement/SaveBudget",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                t = data.Data;
                Generate();
            }
        });
    }
}
function CheckClosingStatus() {

    var vchDate = Month($.trim($("#txtAgainstClaimMonth").val())); 

    var E = "{MenuId: '" + 94 + "', TransDt: '" + vchDate + "', SectId: '" + $.trim($("#ddlSector").val()) + "', FromModule: '" + "A" + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/AccountClose/AccountClosingStatus",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var Status = t[0].Status;
                var msg = t[0].Message;
                if (Status == 1)
                    Save(1);
                else {
                    swal("Cancelled", msg, "error");
                }
            }
        }
    });
}
function Month(month)
{
    var Date = "";
    var a = month;
    var b = a.split(' '); alert(b);
    //var vchDate = b[1] + '-' + b[1] + '-' + b[0];

    if (b[0] == "Jan")
        Date = b[1] + '-' + '01' + '-' + '31';
    if (b[0] == "Feb")
        Date = b[1] + '-' + '02' + '-' + '28';
    if (b[0] == "Mar")
        Date = b[1] + '-' + '03' + '-' + '31';
    if (b[0] == "Apr")
        Date = b[1] + '-' + '04' + '-' + '30';
    if (b[0] == "May")
        Date = b[1] + '-' + '05' + '-' + '31';
    if (b[0] == "Jun")
        Date = b[1] + '-' + '06' + '-' + '30';
    if (b[0] == "Jul")
        Date = b[1] + '-' + '07' + '-' + '31';
    if (b[0] == "Aug")
        Date = b[1] + '-' + '08' + '-' + '31';
    if (b[0] == "Sep")
        Date = b[1] + '-' + '09' + '-' + '30';
    if (b[0] == "Oct")
        Date = b[1] + '-' + '10' + '-' + '31';
    if (b[0] == "Nov")
        Date = b[1] + '-' + '11' + '-' + '30';
    if (b[0] == "Dec")
        Date = b[1] + '-' + '12' + '-' + '31';

    return Date;
}



 