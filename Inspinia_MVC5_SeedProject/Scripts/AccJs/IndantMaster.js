﻿

$(document).ready(function () {

    Set_DefaultDate_Indant();
    Populate_Section();

    //    ITEM AUTOCOMPLETE
    $('#txtIndantName').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_ParentItem" + "' , Desc:'" + $('#txtIndantName').val() + "', MND:'" + "I" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/Item/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnIndantNameCode').val(i.item.AccountCode);
            if (i.item.AccountCode != "")
                Get_Item_Wise_Tax(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtIndantName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtIndantName").val('');
            $("#hdnIndantNameCode").val('');
        }
        if (iKeyCode == 46) {
            $("#txtIndantName").val('');
            $("#hdnIndantNameCode").val('');
        }
    });

    $('#btnIndantAdd').click(IndantItemAdd);
    $('#btnIndantSave').click(Save);


});

function Set_DefaultDate_Indant() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtIndantDate').val(output);

    $('#txtIndantDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });
}

function Populate_Section() {

    var E = "{TransactionType:'" + "Section" + "' , Desc:'" + "" + "', MND:'" + "" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Account_Description',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;
            if (t.length > 0) {
                $('#ddlSection').empty();
                $('#ddlSection').append('<option value="" selected>Select</option');
                $(t).each(function (index, item) {
                    $('#ddlSection').append('<option value=' + item.AccountCode + '>' + item.AccountDescription + '</option>');
                });
            }
        }
    });
}

function IndantItemAdd()
{
    if ($("#hdnIndantNameCode").val() == "") { $("#txtIndantName").focus(); return false; }
    if ($("#txtIndantQty").val() == "") { $("#txtIndantQty").focus(); return false; }
    var chkAccCode = $("#hdnIndantNameCode").val();

    var l = $('#tbl_Indant tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Item is Already Available.'); $("#txtIndantName").val(''); $("#hdnIndantNameCode").val(''); return false;
    }

    var html = ""

    var $this = $('#tbl_Indant .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='ItemID' chk-data='" + $("#hdnIndantNameCode").val() + "' >" + $("#hdnIndantNameCode").val() + "</td>"
        + "<td >" + $("#txtIndantName").val() + "</td>"
        + "<td class='ItemQty' style='text-align:right;'>" + $("#txtIndantQty").val() + "</td>"
		  + "<td class='ItemRemarks' >" + $("#txtIndantRemarks").val() + "</td>"
        + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteIndantDetail(this);' /></td>"
    html + "</tr>";

    $parentTR.after(html);

    $("#txtIndantName").val(''); $("#hdnIndantNameCode").val(''); $("#txtIndantQty").val('');
    Calculate_Qty();
}

function Calculate_Qty() {

    var TotalQty = 0;
    var html = ""

    if ($("#tbl_Indant tbody tr.myData").length > 0) {
        $("#tbl_Indant tbody tr.myData").each(function (index, value) {
            var qty = $(this).find('.ItemQty').text();
            TotalQty = parseInt(TotalQty) + parseInt(qty == "" ? 0 : qty);
        });
    }

    $('#tbl_Indant .trfooter').remove();

    html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%'><span class='clsdrTotal' > " + TotalQty + "</span></td>"
        + "<td></td>"
    html + "</tr>";

    $('#tbl_Indant tr:last').after(html)
}

function DeleteIndantDetail(ID)
{
    $(ID).closest('tr').remove();
    Calculate_Qty();
}

function IndantBlank()
{
    $('.indt').val(''); $('#lblIndantNo').text(''); Set_DefaultDate_Indant();
    $("#tbl_Indant tbody tr.myData").remove();
}

function Save()
{
    if ($("#ddlSection").val() == "") {
        toastr.error('Please, Select Section Name !!', 'Warning')
        $("#ddlSection").focus(); return false;
    }
    if ($("#txtIndantDate").val() == "") {
        toastr.error('Please, Enter Indant Date !!', 'Warning')
        $("#txtIndantDate").focus(); return false;
    }

    var grdLen = $('#tbl_Indant tbody tr.myData').length; var ArrList = []; var i = 0;
    if (grdLen > 0) {
        $('#tbl_Indant tbody tr.myData').each(function () {
            i = i + 1;
            var ItemID = $(this).find('.ItemID').text();
            var ItemQty = $(this).find('.ItemQty').text();

             var ItemRemarks = $(this).find('.ItemRemarks').text();

            ArrList.push({
                'ItemID': ItemID, 'Qty': ItemQty, 'IndantSlNo': i, 'ItemRemarks': ItemRemarks
            });
        });
    }

    var master = {};
    master = {
        IndantID: $('#hdnIndantID').val(),
        SectionID: $('#ddlSection').val(),
		 SectionToID: $('#ddlSectionTo').val() == "" ? $('#ddlSection').val() : $('#ddlSectionTo').val(),
        IndantDate: $('#txtIndantDate').val(),
        Remark: $('#txtIndantRemark').val(),
		  SectorID: $('#ddlSector').val(),
        Itemdetail: ArrList
    }

    var E = "{Master: " + JSON.stringify(master) + "}"; //alert(E); return false;

    $.ajax({
        url: '/Accounts_Form/Item/InsertUpdate_Indant',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t == "success") {

                swal({
                    title: "Success",
                    text: 'Indant Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                            return false;
                        }
                    });
            }
        }
    });
}

function Execute_Action_I(ID) {

    var IndantID = $(ID).closest('tr').find('.PoId').text();
    var ActionName = $(ID).closest('tr').find('.cls_Action').val();
    var TransType = $(ID).closest('tr').find('.TransType').text();
    $('.cls_Action').val('');


    if (ActionName != "") {
        if (ActionName == "E")  //Edit
            Action_Edit_I(IndantID, TransType);
        //if (ActionName == "D")  //Delete
        //    Action_Delete(POID, TransType);

    }

    $(ID).closest('tr').find('.cls_Action').val(ActionName);
}

function Action_Edit_I(IndantID, TransType) {
   
    var E = "{POID: '" + IndantID + "', TransactionType: '" + "Indant_Detail" + "', TransType: '" + TransType + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var tt = data.Data; //alert(JSON.stringify(t));

            var t1 = tt["Table"];
            var t2 = tt["Table1"];

            $('.indant').toggleClass('toggled');

            if (t1.length > 0) {
                $('#lblIndantNo').text(t1[0].IndantNo); $('#hdnIndantID').val(t1[0].IndantID);
                $('#ddlSection').val(t1[0].SectionID); $('#txtIndantDate').val(t1[0].IndantDate);
                $('#txtIndantRemark').val(t1[0].Remark);
            }
            if (t2.length > 0) {
                Populate_IndantDetail(t2);
            }
        }
    });
}

function Populate_IndantDetail(detail) {

    $('#tbl_Indant tbody tr.myData').remove();
    $('#tbl_Indant tbody tr.trfooter').remove();

    if (detail.length > 0) {

        var html = ""; var TotalQty = 0;

        var table = $('#tbl_Indant tbody');

        for (var i = 0; i < detail.length; i++) {
            html += "<tr class='myData' >"
                + "<td style='display:none;' class='ItemID' chk-data='" + detail[i].ItemID + "' >" + detail[i].ItemID  + "</td>"
                + "<td >" + detail[i].AccountDescription + "</td>"
                + "<td class='ItemQty' style='text-align:right;'>" + detail[i].Qty + "</td>"
				 + "<td class='ItemRemarks'>" + detail[i].Remarks + "</td>"
                + "<td style='text-align:center'><img src='/Content/Images/Delete.png' title='Edit' style='width:20px;height:20px;cursor:pointer; text-align:center;' onClick='DeleteIndantDetail(this);' /></td>"
            html + "</tr>";

            TotalQty = parseInt(TotalQty) + parseInt(detail[i].Qty);
        }
        html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
            + "<td style='text-align:right; font-weight:bold;'>Total : </td>"
            + "<td style='text-align:right; font-weight:bold; width:20%'><span class='clsdrTotal' > " + TotalQty + "</span></td>"
            + "<td></td>"
        html + "</tr>";

        table.append(html);
    }
}