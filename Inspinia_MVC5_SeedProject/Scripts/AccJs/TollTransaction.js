﻿var fileName = "",  DArrList = [];

$(document).ready(function () {


    $('#txtFromDate, #txtDepFromDate, #txtDepToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });

    $('.chkall').on('click', function () {
       
        if ($(this).is(":checked")) {
            $('.chk').prop('checked', true);
        } else {
            $('.chk').prop('checked', false);
        }
        //// Check/uncheck all checkboxes in the table
        //var rows = table.rows({ 'search': 'applied' }).nodes();
        //$('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    $("#txtBusNo").autocomplete({

        source: function (request, response) {

            var E = "{TransType: '" + "BusSearch" + "', SectorID: '" + $("#ddlSector").val() + "', TransDate: '" + "" + "', FileName: '" + $("#txtBusNo").val() + "', ID: '" + "" + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/TollTransaction/Post",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    if (t.length > 0) {

                        $('#tbl_bus tbody tr').remove();

                        var columnData = [
                            { "mDataProp": "SectorName" },
                            { "mDataProp": "BusNo" }];

                        var columnDataHide = [];

                        var oTable = $('#tbl_bus').DataTable({
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [],

                            "oLanguage": {
                                "sSearch": "Search all columns:"
                            },
                            "aaData": t,
                            "aoColumns": columnData,
                            "aoColumnDefs": [
                                {
                                    "targets": columnDataHide,
                                    "visible": false,
                                    "searchable": false
                                }
                            ],
                            'iDisplayLength': 10,
                            "scrollX": true,
                            destroy: true,
                            searching: false
                        });
                        var table = oTable;
                    }
                }
            });
        },
        autocomplete: true,
        select: function (e, i) {

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
});

function Photo() {
  
   var fileExtension = ['xls', 'xlsx'];
    var filename = $('#flPic').val();

    if (filename.length == 0) {
        alert("Please Select .xls, .xlsx file.");
        $('#flPic').val("");
        return false;
    }

    var extension = filename.replace(/^.*\./, '');
    if ($.inArray(extension, fileExtension) == -1) {
        alert("Please select only (.xls, .xlsx) files.");
        $('#flPic').val("");
        return false;
    }
    Upload_Item('flPic');
}

function Upload_Item(flPic) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#flPic').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file);
    }

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/TollTransaction/UploadPicture',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
            var t= response.Data;
           
            if (t == 0)
            {
                alert('Sorry ! There is some proble to upload Excel file.'); return false;
            }
            else if (t == 1) {
                alert('Sorry ! First complete account posting for already uploaded record.\nOR, Delete Record.'); return false;
            }
            else
            {
                fileName = t;
              
                Checking(t);
            }
        }
    });
}

function Checking(fileName)
{
    var E = "{FileName: '" + fileName + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/Checking",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var msg = r.Message;
            var t = r.Data; 
            if (msg == "upload")
            {
                var ID = t[0].ID; 
                var fName = t[0].FileName;

                $('.lblfileName').attr('data-file', fileName);
                $('.lblfileName').attr('data-file-id', ID);
                Show(t);
            }
        }
    });
}
function Show(t)
{
    $('#tbl tbody tr').remove();

    var columnData = [
        { "mDataProp": "TabId" },
        {
            "mDataProp": "CheckBox",
            "render": function (data, type, row) {
                return ' <input type="checkbox"  checked class="chk" value="' + row.VehicleNumber + '" onclick="GetDecline( \'' + row.TabId + '\'  , \'' + "chk" +'\', this );" />';
            }
        },
        {
            "mDataProp": "VehicleNumber",
            "render": function (data, type, row) {
                return '<input type="text" class="clsvchNo" value="' + row.VehicleNumber + '" onblur="GetDecline(\'' + row.TabId + '\' ,  \'' + "veh" +'\', this );" />';
            }
        },
        { "mDataProp": "TagId" },
        { "mDataProp": "BarCode" },
        { "mDataProp": "TransactionId" },
        { "mDataProp": "TollreaderTime" },
        { "mDataProp": "TransactionTime" },
        { "mDataProp": "TollplazaId" },
        { "mDataProp": "TollplazaName" },
        { "mDataProp": "TransAmount" },
        { "mDataProp": "AcqId" },
        { "mDataProp": "Remarks" },
        { "mDataProp": "ID" }];

    var columnDataHide = [0, 13];

    var oTable = $('#tbl').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [],

        "oLanguage": {
            "sSearch": "Search all columns:"
        },
        "aaData": t,
        "aoColumns": columnData,
        "aoColumnDefs": [
            {
                "targets": columnDataHide,
                "visible": false,
                "searchable": false
            },
            {
                "targets": [0],
                "className": "tabid"
            },
            {
                "targets": [5],
                "className": "text-center transid"
            }
        ],
        'iDisplayLength': 25,
        "scrollX": true,
        destroy: true
    });
    var table = oTable;

    // #myInput is a <input type="text"> element
    $('#tbl').on('keyup', function () {
        table.search(this.value).draw();
    });
    $('.chkall').on('click', function () {
        var rows = oTable.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    $('#tbl tbody').on('change', 'input[type="checkbox"]', function () {
        // If checkbox is not checked
        if (!this.checked) {
            var el = $('.chkall').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control 
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });
}
function GetDecline(TabID, chkVeh, ID)
{
    var row = $(ID).closest('tr'); 
    var decline = $('.chk', row).prop('checked'); 
    var vechicleno = $('.clsvchNo', row).val();
    var E = "{TransType: '" + "Dat_Update" + "', TabID: '" + TabID + "', VehicleNo: '" + vechicleno + "', chkVeh: '" + chkVeh + "', Decline: '" + (decline == true ? "N" : "Y") + "'}"; 

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/Decline",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
        }
    });
}

function NewSave()
{
    swal({
        title: "Warning",
        text: "Are you sure want to Save ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
              
                var grdLen = $('#tbl tbody tr td.transid').length; 
                if (grdLen > 0) {

                    var ArrList = [];
                    var oTable = $('#tbl').dataTable();
                    var rowcollection = oTable.$(".chk", { "page": "all" });
                    //var rowcollection = oTable.$(".chk:checked", { "page": "all" });
                    rowcollection.each(function (index, elem) {
                        var checkbox_value = $(elem).val();

                        var row = $(this).closest('tr');
                        var TabId = oTable.dataTable().fnGetData(row).TabId;
                        var ID = oTable.dataTable().fnGetData(row).ID;
                        var vechicleno = $(this).closest('tr').find('.clsvchNo').val();
                        var transid = $(this).closest('tr').find('.transid').text();
                        var decline = $('.chk', row).prop('checked');

                        if (decline == false) {
                            ArrList.push({
                                'TabId': TabId, 'VehicleNo': vechicleno, 'TransactionID': transid, 'ID': ID, 'Decline': (decline == true ? "N" : "Y")
                            });
                        }
                    });

                    var TollFDetail = JSON.stringify(ArrList);
                    var E = "{TollDetail: " + TollFDetail + "}";

                   alert(E); return false;

                    $.ajax({
                        type: "POST",
                        url: "/Accounts_Form/TollTransaction/TollDetail",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (r) {
                            var t = r.Data;
                            if (t == "success")
                            {
                                alert("Toll Details are Saved Successfully.");
                                //$('#btnGenAmount').css("display", '');
                                $('#flPic').prop("disabled", true);
                                return false;
                            }
                            else
                            {
                                alert("There is some problem."); return false;

                            }
                        }
                    });
                }
                else {
                    toastr.error('Sorry !! No data available !!', 'Warning'); return false;
                }
            }

        });
}
function DeleteFile(ID)
{
    var res = confirm('Are You sure want to delete file ??');
    if (res == true) {
        var FileNames = $(ID).attr('data-file');
        var IDS = $(ID).attr('data-file-id');
        if (FileNames != "") {
            var E = "{FileName: '" + FileNames + "', ID: '" + IDS + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/TollTransaction/DeleteFile",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    var t = r.Data;
                    if (t == "success") {
                        alert('File Deleted Successfully.'); location.reload();
                    }
                }
            });
        }
    }
}

function GenSecAmount()
{
        var E = "{TransType: '" + "Insert" + "', SectorID: '" + $('#ddlSector').val() + "', TransDate: '" + "" + "', Type: '" + "" + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TollTransaction/Load",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (r) {
                var msg = r.Message;
                var t = r.Data;

                var table = $('#tbl1');
                var thead = table.find('thead');
                var tbody = table.find('tbody tr.mydata');
                var html = ""; var Total = 0;
                tbody.empty();
                if (t.length > 0) {
                  
                    for (var i = 0; i < t.length; i++) {
                        var Sectorid = t[i].SectorId; 
                        if (Sectorid == 1) {
                            $('.clsMasterVch').text(t[i].RefVoucherSlNo);
                            $('.clsMasterVch').attr("data-id", t[i].VoucherNumber);
                            $('.clsMasterVch').attr("data-value", t[i].RefVoucherSlNo);
                        }
                        else {
                            Total = parseFloat(Total) + parseFloat(t[i].TransAmount);
                            html += "<tr class='mydata'>"
                                + "<td style='display:none;' class='SectorId'>" + t[i].SectorId + "</td>"
                                + "<td style='display:none;' class='FileName'>" + t[i].FileName + "</td>"
                                + "<td style='display:none;' class='ID'>" + t[i].ID + "</td>"
                                + "<td class='TransDate'>" + t[i].TransDate + "</td>"
                                + "<td >" + t[i].SectorName + "</td>"
                                + "<td style='text-align:right;'>" + t[i].TransAmount + "</td>"
                                + "<td style='text-align:center;'><select onchange='ViewDetail(this)'><option selected value=''>Select</option><option value='B'>Bus Detail</option><option value='S' style='display:none;'>Service Detail</option></select> <span style='padding-left:20px; color:blue; cursor:pointer' data-id=" + t[i].VoucherNumber + " data-value=" + t[i].RefVoucherSlNo + " onclick='PrintSectorWise(this);'>" + t[i].RefVoucherSlNo + "</span></td>"
                            html + "</tr>";
                        }
                    }
                    if (Total > 0)
                    {
                        html += "<tr class='mydata' >"
                            + "<td colspan='2' style='text-align:right; font-weight:bold;'> " + "Total" + "</td>"
                            + "<td style='text-align:right; font-weight:bold;'>  &#x20b9; " + Total + "</td>"
                            + "<td></td>"
                        html + "</tr>";
                    }

                    $('#tbl1 tbody').append(html);
                    $('#tbl1').css('display', '');
                }
            }
        });
    
}
function Sample()
{
    var table = $("#tbl").DataTable();

        var res = confirm('Are You Sure Want to download Excel Sample ?');
        if (res == true) {
            var ID = $('.lblfileName').attr('data-file-id'); 
            location.href = "/Accounts_Form/TollTransaction/Excel?Decline=" + $('#ddlViewRecord').val() + "&ID=" + ID;
        }

}
function ViewDetail(ID)
{
    var Type = $(ID).val(); 
    var SectorId = $(ID).closest('tr').find('.SectorId').text(); 
    var TransDate = $(ID).closest('tr').find('.TransDate').text();
    if (Type != "") {
        var E = "{TransType: '" + "Detail" + "', SectorID: '" + SectorId + "', TransDate: '" + TransDate + "', Type: '" + Type + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TollTransaction/Load",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                var table = $('#tbl_edit');
                var tbody = table.find('tbody');
                tbody.empty();
                var html = "";

             
                if (t.length > 0) {
                    $('#tbl_edit tbody tr').remove();

                    var columnData = [
                        { "mDataProp": "VehicleNumber" },
                        { "mDataProp": "TransAmount" },
                        { "mDataProp": "TollplazaName" }];

                    var columnDataHide = [];

                    var oTable = $('#tbl_edit').DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [],

                        "oLanguage": {
                            "sSearch": "Search all columns:"
                        },
                        "aaData": t,
                        "aoColumns": columnData,
                        "aoColumnDefs": [
                            {
                                "targets": columnDataHide,
                                "visible": false,
                                "searchable": false
                            },
                            {
                                "targets": [0],
                                "className": "text-center"
                            },
                            {
                                "targets": [1],
                                "className": "text-right"
                            }
                        ],
                        'iDisplayLength': 10,
                        "footerCallback": function (row, data, start, end, display) {
                            var api = this.api(), data;

                            // converting to interger to find total
                            var intVal = function (i) {
                                return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                        i : 0;
                            };

                            var netTotal = api
                                .column(1)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            $(api.column(0).footer()).html('Total');
                            $(api.column(1).footer()).html('(&#x20b9;) ' + netTotal);
                        },
                        destroy: true,
                    });

                    $('#dialog').modal('show');
                }
            }
        });
    }
}
function Post()
{
    var FileNmae = "", ID="";
    var fromDate = $('#txtFromDate').val();
    if (fromDate == "") { $('#txtFromDate').focus(); return false; }

    var l = $("#tbl1 tbody tr.mydata").length;
    if (l > 0)
    {
        var t = document.getElementById('tbl1');
        FileNmae = $(t.rows[1].cells[1]).text();  
        ID = $(t.rows[1].cells[2]).text();  
    }
    else {
        alert('Sorry ! Sector Wise Amount details are not available.'); return false;
    }

   
    var E = "{TransType: '" + "Post" + "', SectorID: '" + $('#ddlSector').val() + "', TransDate: '" + fromDate + "', FileName: '" + FileNmae + "', ID: '" + ID + "'}";

    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/Post",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            GenSecAmount();
        }
    });
}
function ViewRecord()
{
    if ($('#ddlViewRecord').val() != "") {
        var ID = $('.lblfileName').attr('data-file-id'); 
        var E = "{TransType: '" + "Show" + "', ID: '" + ID + "', Decline: '" + $('#ddlViewRecord').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TollTransaction/ViewRecord",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (r) {
                var t = r.Data; 
                    Show(t);
            }
        });
    }
    else {
        var table = $('#tbl').DataTable();

        table
            .clear()
            .draw();
    }
}
function PrintSectorWise(ID)
{
    var final = {}; var master = []; var detail = [];

    var fileName = $(ID).attr('data-value');
    var vchNo = $(ID).attr('data-id');
    var sectorid = $(ID).closest('tr').find('.SectorId').text();

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: fileName,
        Database:""
    });

    detail.push({
        VoucherNumber: vchNo,
        sectorid: 0
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
} 
function Print(ID) {
    var final = {}; var master = []; var detail = [];

    var fileName = $(ID).attr('data-value');
    var vchNo = $(ID).attr('data-id');
    var sectorid = $(ID).closest('tr').find('.SectorId').text();

    master.push({
        ReportName: "VoucherCash.rpt",
        FileName: fileName,
        Database: ""
    });

    detail.push({
        VoucherNumber: vchNo,
        sectorid: 1
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();
}
function SearchBus()
{
    $('#dialog-bus').modal('show');
}

function GenMissingDate()
{
    var E = "{FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/MissingDate",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data;

            var table = $('#tbl2');
            var thead = table.find('thead');
            var tbody = table.find('tbody tr.mydata');
            var html = ""; var Total = 0;
            tbody.empty();

            if (t.length > 0) {

                for (var i = 0; i < t.length; i++) {

                    html += "<tr class='mydata'>"
                        + "<td class='MissingDate' style='text-align:center; width:12%'>" + t[i].MissingDate + "</td>"
                        + "<td><input type='text' class='form-control remarks' style='width:100%' /></td>"
                        + "<td style='text-align:center; width:12%'><input type='checkbox'  class='chk' onclick='SaveMissingDate(this);' /></td>"
                    html + "</tr>";

                }
                $('#tbl2 tbody').append(html);
            }

        }
    });

}
function GenMissingDate1() {
    var E = "{FinYear: '" + $("#ddlGlobalFinYear option:selected").text() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/MissingDate",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data;

            var columnData = [
                { "mDataProp": "MissingDate" },
                {
                    "render": function (data, type, row) {
                        return '<input type="text" class="form-control remarks" style="width:100%" />';
                    }
                },
                {
                    "render": function (data, type, row) {
                        return '<input type="checkbox"  class="chk" onclick="SaveMissingDate(this);" />';
                    }
                }];

            var columnDataHide = [];

            var oTable = $('#tbl2').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,
                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [2],
                        "className": "text-center",
                        "width": "12%"
                    },
                    {
                        "targets": [0],
                        "className": "text-center date",
                        "width": "12%"
                    }
                ],
                destroy: true
            });
        }
    });
}
function SaveMissingDate(ID)
{
    var decline = $(ID).prop('checked'); 
    var remarks = $(ID).closest('tr').find('.remarks').val(); 
    var date = $(ID).closest('tr').find('.MissingDate').text(); 

    if (remarks == "") { $(ID).closest('tr').find('.remarks').focus(); $(ID).prop('checked',  false); return false;    }
    if (decline == false) { return false; }
    
    swal({
        title: "Warning",
        text: "Are you sure want to Sattle Missing Date - " + date + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                Confirm(date, remarks);
            }

        });
}
function Confirm(date, remarks)
{
    var E = "{TransType: '" + "MissingDate" + "', Sattledate: '" + date + "', Remarks: '" + remarks + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/SaveMissingDate",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data;
            if (t == "Success") {
                alert("Your Missing Date - " + date + " has been satteled Successfully.");
                GenMissingDate();
                return false;
            }
        }
    });
}

function DisputedShow()
{
    if ($("#txtDepFromDate").val() == "") { $("#txtDepFromDate").focus(); return false; }
    if ($("#txtDepToDate").val() == "") { $("#txtDepToDate").focus(); return false; }

    var E = "{TransType: '" + "DisputedTrans" + "', FromDate:'" + $("#txtDepFromDate").val() + "', ToDate:'" + $("#txtDepToDate").val() + "', VehicleNo:'" + "" + "', TollID:'" + "" + "', WayBillID:'" + "" +"'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/DeclinedTrans",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data; 

           var columnData = [{ "mDataProp": "Depot" },
                            { "mDataProp": "VehicleNumber" },
                            { "mDataProp": "TransactionDate" },
                            { "mDataProp": "FastagAmt" },
                            { "mDataProp": "Amount" },
                            { "mDataProp": "TollPlazaName" },
                            { "mDataProp": "WayBillNo" },
                            { "mDataProp": "waybillId" },
                            { "mDataProp": "tollId" },
                            {
                                "mDataProp": "Edit",
                                "render": function (data, type, row) {
                                    return '<div><a href="javascript:void(0)" title="View" style="text-align:center" class="btn btn-azure btn-xs" onclick="View_Settle(\'' + row.VehicleNumber + '\', \'' + row.TransactionDate + '\')" ><span class="label label-info pull-right">View</span>  </a>' +
                                           '<a href="javascript:void(0)" title="Settle" style="text-align:center" class="btn btn-azure btn-xs" onclick="Save_Settle(\'' + row.tollId + '\', \'' + row.waybillId + '\')" ><span class="label label-info pull-right">Settle</span>  </a></div>';
                                }
                            }];

            var columnDataHide = [6,7,8];

            var oTable = $('#tbl3').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,
                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [1, 2],
                        "className": "text-center",
                        "width": "15%"
                    },
                    {
                        "targets": [3,4],
                        "className": "text-right",
                        "width": "15%"
                    },
                    {
                        "targets": [9],
                        "className": "text-center",
                        "width": "10%"
                    }
                ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var netFast = api
                        .column(3)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    var netAmt = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    $(api.column(2).footer()).html('Total');
                    $(api.column(3).footer()).html('(&#x20b9;) ' + netFast);
                    $(api.column(4).footer()).html('(&#x20b9;) ' + netAmt);
                },
                destroy: true
            });
        }
    });
}
function DisputedExcel() {

    if ($("#txtDepFromDate").val() == "") { $("#txtDepFromDate").focus(); return false; }
    if ($("#txtDepToDate").val() == "") { $("#txtDepToDate").focus(); return false; }

    var master = []; var final = {};

    master.push({
        TransType: "DisputedTrans",
        FromDate: $("#txtDepFromDate").val(),
        ToDate: $("#txtDepToDate").val(),
        VehicleNo: "",
        TollID: "",
        WayBillID: "",
        FileName : "DisputedTollTransaction"
    });
    final = {
        Master: master
    }
    //alert(JSON.stringify(final)); return false;
    location.href = "/Accounts_Form/TollTransaction/DisputedExcel?ReportName=" + JSON.stringify(final);
}
function Save_Settle(tollId, waybillId)
{
    swal({
        title: "Warning",
        text: "Are you sure want to Settle ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {

                var E = "{TransType: '" + "InsertDisputedTrans" + "', FromDate:'" + $("#txtDepFromDate").val() + "', ToDate:'" + $("#txtDepToDate").val() + "', VehicleNo:'" + "" + "', TollID:'" + tollId + "', WayBillID:'" + waybillId + "'}";
                    $.ajax({
                        type: "POST",
                        url: "/Accounts_Form/TollTransaction/DeclinedTrans",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (r) {
                            var t = r.Data; 
                            if (t[0].Result == "success") {
                                alert("Transaction has been Setteled Successfully.");
                                DisputedShow();
                                return false;
                            }
                            else {
                                alert("There is some problem."); return false;

                            }
                        }
                    });

            }

        });
}
function View_Settle(VehicleNo, TransactionDate)
{
    var E = "{TransType: '" + "DisputedTransDetl" + "', FromDate:'" + TransactionDate + "', ToDate:'" + TransactionDate + "', VehicleNo:'" + VehicleNo + "', TollID:'" + "" + "', WayBillID:'" + "" + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/DeclinedTrans",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data;

            var columnData = [{ "mDataProp": "TransactionDate" },
                { "mDataProp": "TransAmount" },
                { "mDataProp": "TollplazaName" }
            ];

            var columnDataHide = [];

            var oTable = $('#tbl_settle').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,
                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0],
                        "className": "text-center",
                        "width": "10%"
                    },
                    {
                        "targets": [1],
                        "className": "text-right",
                        "width": "10%"
                    }
             ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var netFast = api
                        .column(1)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    $(api.column(0).footer()).html('Total');
                    $(api.column(1).footer()).html('(&#x20b9;) ' + netFast);
                },
                destroy: true
            });

            $('.lblSettlbus').text(VehicleNo);
            $('#dialog-Settle').modal('show');
        }
    });
}

function BuswithoutFastag()
{
    var E = "{TransType: '" + "BuswithoutFastag" + "', FromDate:'" + "" + "', ToDate:'" + "" + "', VehicleNo:'" + "" + "', TollID:'" + "" + "', WayBillID:'" + "" + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TollTransaction/DeclinedTrans",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var t = r.Data;

            var columnData = [{ "mDataProp": "BusNo" },
                { "mDataProp": "SectorName" },
                { "mDataProp": "BusCategory" },
                { "mDataProp": "ServiceName" }
            ];

            var columnDataHide = [];

            var oTable = $('#tbl4').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'  }
                ],

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": t,
                "aoColumns": columnData,
                "aoColumnDefs": [
                    {
                        "targets": columnDataHide,
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0, 2],
                        "className": "text-center",
                        "width": "13%"
                    }
                ],
               destroy: true
            });
        }
    });
}

