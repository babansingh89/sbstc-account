﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;


namespace wbEcsc.App_Start
{
    public class Session :  FilterAttribute
    {
        AuthenticationContext filterContext;

        public void OnAuthentication()

        {
            try
            {
                    //must be authenticated. else throw 401
                    if (SessionContext.IsAuthenticated)
                    {
                       
                    }
                    else
                    {
                        throw new InvalidOperationException("401$Authentication Required");
                    }
                }
            
            catch (Exception ex)
            {
                Action<Exception> handeException = (exception) =>
                {
                    int statusCode = 500;
                    string message = ex.Message; //"Unknown Application Error. Error source-'UrlFilter'";
                    if (exception.GetType() == typeof(InvalidOperationException))
                    {
                        statusCode = Convert.ToInt32(ex.Message.Split('$')[0]);
                        message = ex.Message.Split('$')[1];
                    }

                        filterContext.Result = new RedirectToRouteResult("ErrorRouter",
                               new System.Web.Routing.RouteValueDictionary{
                                {"controller", string.Format("Error{0}",statusCode)},
                                {"action", "Index"},
                                {"errorCode", statusCode},
                                {"Message", message},
                               });
                    
                };

                handeException(ex);
            }

        }
    }
}