﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class ReportRoute:IRoute
    {
        private RouteCollection _routes { get; set; }
        public ReportRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "ReportRouter",
                url: "Report/{controller}/{action}/{extension}",
               //defaults: new { extention = UrlParameter.Optional },
                namespaces: new[] { "wbEcsc.Controllers.Report" }
            );
        }
    }
}