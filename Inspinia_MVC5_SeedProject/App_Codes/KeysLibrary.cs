﻿namespace wbEcsc.App_Codes
{
    internal class KeysLibrary
    {
        public class SessionKeys
        {
            public static string CurrentUser_Key
            {
                get
                {
                    return "{C67B8BAB-F639-4ED2-BD98-72AE0EFC419C}";
                }
            }
            public static string SessionData_Key
            {
                get
                {
                    return "{86BC5D22-4202-464F-8B40-945ECB055BF6}";
                }
            }
        }
        public class GlobalKeys
        {
            public static string LoggedInUsers_Key
            {
                get
                {
                    return "{2D387E82-D86E-4923-8B0B-F617AF4F7FD2}";
                }
            }
        }
    }
}