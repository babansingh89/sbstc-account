﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels.Accounts;

namespace wbEcsc.App_Codes.BLL
{
    public class Services_BLL : BllBase
    {
        public int SaveVoucher(DataTable dt, SqlConnection dbConn, SqlTransaction transaction, decimal TotalIncExp)
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr;

            int VCHNo = 0; string VoucherNo = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    string TR_Type = "";

                    cmd = new SqlCommand("account.Get_Acc_VchTypeByID", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@VoucherTypeID", dt.Rows[0]["VoucherTypeID"].ToString());

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TR_Type = dr["Abbv"].ToString();
                    }
                    dr.Close();

                    cmd = new SqlCommand("account.Insert_Acc_VoucherMaster_ModulePost", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AutoTrans", "N");
                    cmd.Parameters.AddWithValue("@VCH_TYPE", dt.Rows[0]["VoucherTypeID"].ToString());
                    cmd.Parameters.AddWithValue("@VOUCHER_DATE", dt.Rows[0]["WayBillDate"].ToString());
                    cmd.Parameters.AddWithValue("@VCH_AMOUNT", TotalIncExp);
                    cmd.Parameters.AddWithValue("@NARRATION", "Being amount paid for WayBillNo-" + dt.Rows[0]["WayBillNo"].ToString());
                    cmd.Parameters.AddWithValue("@STATUS", "D");
                    cmd.Parameters.AddWithValue("@NP_SP", "SP");
                    cmd.Parameters.AddWithValue("@TR_TYPE", TR_Type);
                    cmd.Parameters.AddWithValue("@SectID", dt.Rows[0]["SectorID"].ToString());
                    cmd.Parameters.AddWithValue("@CenterID", dt.Rows[0]["CenterId"].ToString());
                    cmd.Parameters.AddWithValue("@InsertedBy", dt.Rows[0]["InsertedBy"].ToString());
                    cmd.Parameters.Add("@Vch_NoResult", SqlDbType.Int);
                    cmd.Parameters.Add("@VoucherNoResult", SqlDbType.VarChar, 25);
                    cmd.Parameters["@Vch_NoResult"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@VoucherNoResult"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    VCHNo = Convert.ToInt32(cmd.Parameters["@Vch_NoResult"].Value.ToString());
                    VoucherNo = cmd.Parameters["@VoucherNoResult"].Value.ToString();

                    if (VCHNo > 0)
                    {
                        int i = 0;
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            i = i + 1;

                            string AccountCode =  dt.Rows[j]["Accountcode"].ToString();
                            string DRCR = dt.Rows[j]["DRCR"].ToString();
                            string TicketFare = dt.Rows[j]["TicketFare"].ToString();
                            string SectorID = dt.Rows[j]["SectorID"].ToString();
                            string InsertedBy = dt.Rows[j]["InsertedBy"].ToString();

                            cmd = new SqlCommand("account.Insert_Acc_VoucherDetail_ModulePost", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@MaxVoucherNumber", VCHNo);
                            cmd.Parameters.AddWithValue("@SerialNumber", i);
                            cmd.Parameters.AddWithValue("@Accountcode", dt.Rows[j]["Accountcode"]);
                            cmd.Parameters.AddWithValue("@DRCR", dt.Rows[j]["DRCR"]);
                            cmd.Parameters.AddWithValue("@Amount", dt.Rows[j]["TicketFare"]);
                            cmd.Parameters.AddWithValue("@SectID", dt.Rows[j]["SectorID"]);
                            cmd.Parameters.AddWithValue("@CenterID", dt.Rows[j]["CenterId"]);
                            cmd.Parameters.AddWithValue("@insertedBy", dt.Rows[j]["InsertedBy"]);
                            cmd.ExecuteNonQuery();

                            cmd = new SqlCommand("account.Insert_Acc_WayBillDetail", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@VoucherNumber", VCHNo);
                            cmd.Parameters.AddWithValue("@SerialNumber", i);
                            cmd.Parameters.AddWithValue("@WayBillID", dt.Rows[0]["WayBillID"]);
                            cmd.Parameters.AddWithValue("@WayBillDate", dt.Rows[0]["WayBillDate"]);
                            cmd.Parameters.AddWithValue("@Amount", dt.Rows[0]["TicketFare"]);
                            cmd.Parameters.AddWithValue("@Status", "D");
                            cmd.Parameters.AddWithValue("@InsertedBy", dt.Rows[j]["InsertedBy"]);
                            cmd.ExecuteNonQuery();
                        }
                    }

                }
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                throw new Exception(sqlError.Message);
            }
            return VCHNo;
        }

    }
}