﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels.Accounts;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class CashTransaction_BLL: BllBase
    {
        public VoucherInfo SaveVoucher(string VoucherForm, long UserID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            string noNewLines = VoucherForm.Replace("\n", "");
            VoucherInfo voucherInfo = JsonConvert.DeserializeObject<VoucherInfo>(noNewLines);

            SqlDataReader dr;

            try
            {
                try
                {
                    VoucherMaster voucherMast = voucherInfo.VoucherMast;
                    List<VoucherDetail> voucherDetail = voucherInfo.VoucherDetail;

                    if (UserID > 0)
                    {
                        if (voucherMast != null)
                        {
                            if (voucherInfo.EntryType == "I")
                            {
                                SaveVoucherMaster(voucherMast, UserID, dbConn, transaction);

                                if (voucherMast.VCHNo > 0)
                                {
                                    if (voucherDetail.Count > 0)
                                    {
                                        SaveVoucherDetail(voucherDetail, voucherMast, UserID, dbConn, transaction);
                                    }
                                }
                            }
                            else if (voucherInfo.EntryType == "E")
                            {
                                string Result = UpdateVoucherMast(voucherMast, UserID, dbConn, transaction);
                                if (Result == "Y")
                                {
                                    //cmd = new SqlCommand("account.Delete_VoucherDetail", dbConn, transaction);

                                    //cmd.CommandType = CommandType.StoredProcedure;
                                    //cmd.Parameters.AddWithValue("@YearMonth", voucherMast.YearMonth);
                                    //cmd.Parameters.AddWithValue("@VchNo", voucherMast.VCHNo);
                                    //cmd.Parameters.AddWithValue("@VchType", voucherMast.VoucherType);
                                    //cmd.Parameters.AddWithValue("@SectorID", voucherMast.SectorID);

                                    //cmd.ExecuteNonQuery();

                                    //SaveVoucherDetail(voucherDetail, voucherMast, UserID, dbConn, transaction);
                                    Update_VoucherDetail(voucherDetail, voucherMast, UserID, dbConn, transaction);


                                    voucherInfo.SearchResult = "U";
                                    voucherInfo.Message = "Voucher No " + voucherMast.VoucherNo + '(' + voucherMast.VCHNo + ')' + "" + " Updated Successfully !!";
                                }
                                else if (Result == "N")
                                {
                                    voucherInfo.SearchResult = "F";
                                    voucherInfo.Message = "Voucher has been finalized cannot update the voucher.";
                                }
                                else if (Result == "R")
                                {
                                    voucherInfo.SearchResult = "R";
                                    voucherInfo.Message = "Error in Deleting Voucher";
                                }
                            }
                            transaction.Commit();

                        }
                    }
                    else
                    {
                        voucherInfo.SessionExpired = "Y";
                    }
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return voucherInfo;
        }

        private static void SaveVoucherMaster(VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            string TR_Type = "";
            SqlDataReader dr;
            try
            {
                if (voucherMast != null)
                {
                    SqlCommand cmd = new SqlCommand("account.Get_Acc_VchTypeByID", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@VoucherTypeID", voucherMast.VoucherType);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TR_Type = dr["Abbv"].ToString();
                    }
                    dr.Close();

                    cmd = new SqlCommand("account.Insert_Acc_VoucherMaster", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AutoTrans", "E");
                    cmd.Parameters.AddWithValue("@VCH_TYPE", voucherMast.VoucherType);
                    cmd.Parameters.AddWithValue("@VOUCHER_DATE", voucherMast.VCHDATE);
                    cmd.Parameters.AddWithValue("@VCH_AMOUNT", voucherMast.VCHAMOUNT);
                    cmd.Parameters.AddWithValue("@NARRATION", voucherMast.NARRATION);
                    cmd.Parameters.AddWithValue("@STATUS", voucherMast.STATUS);
                    cmd.Parameters.AddWithValue("@NP_SP", "SP");
                    cmd.Parameters.AddWithValue("@TR_TYPE", TR_Type);
                    cmd.Parameters.AddWithValue("@SectorID", voucherMast.SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                    cmd.Parameters.Add("@Vch_NoResult", SqlDbType.Int);
                    cmd.Parameters.Add("@VoucherNoResult", SqlDbType.VarChar, 25);
                    cmd.Parameters["@Vch_NoResult"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@VoucherNoResult"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    voucherMast.VCHNo = Convert.ToInt32(cmd.Parameters["@Vch_NoResult"].Value.ToString());
                    voucherMast.VoucherNo = cmd.Parameters["@VoucherNoResult"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void SaveVoucherDetail(List<VoucherDetail> voucherDetail, VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (voucherDetail.Count > 0)
                {
                    int i = 0;
                    foreach (VoucherDetail vchdet in voucherDetail)
                    {
                        i = i + 1;
                        cmd = new SqlCommand("account.Insert_Acc_VoucherDetail", db, transaction);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                        cmd.Parameters.AddWithValue("@SerialNumber", i);
                        cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                        cmd.Parameters.AddWithValue("@DRCR", vchdet.DRCR);
                        cmd.Parameters.AddWithValue("@Amount", vchdet.AMOUNT);
                        cmd.Parameters.AddWithValue("@sectorID", vchdet.SectorID);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            vchdet.VchSrl = Convert.ToInt32(dt.Rows[0]["VCHSRL"].ToString());
                        }


                        if (vchdet.VchInstType.Count > 0)
                        {
                            foreach (VoucherInstType vchinst in vchdet.VchInstType)
                            {
                                cmd = new SqlCommand("account.Updt_AccDatBills", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@InFavourof", vchinst.Drawee == "" ? DBNull.Value : (object)vchinst.Drawee);
                                cmd.Parameters.AddWithValue("@InstrumentType", vchinst.InstType == "" ? DBNull.Value : (object)vchinst.InstType);
                                cmd.Parameters.AddWithValue("@InstrumentNo", vchinst.InstNo == "" ? DBNull.Value : (object)vchinst.InstNo);
                                cmd.Parameters.AddWithValue("@InstrumentDate", vchinst.InstDT == "" ? voucherMast.VCHDATE : vchinst.InstDT);
                                cmd.Parameters.AddWithValue("@insertedBy", UserID);
                                cmd.Parameters.AddWithValue("@sectorID", vchinst.SectorID);
                                cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                                cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@AdjustmentAmount", vchinst.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vchinst.DRCR);
                                cmd.Parameters.AddWithValue("@VoucherSerial", vchdet.VchSrl);
                                cmd.Parameters.AddWithValue("@AccDeptCode", vchinst.AccDeptCode == "" ? DBNull.Value : (object)vchinst.AccDeptCode);
                                cmd.Parameters.AddWithValue("@AccCostCenterCode", vchinst.AccCostCenterCode == "" ? DBNull.Value : (object)vchinst.AccCostCenterCode);
                                cmd.ExecuteNonQuery();
                            }
                        }
                        if (vchdet.VchSubLedger.Count > 0)
                        {
                            foreach (VoucherSubLedgerType vsl in vchdet.VchSubLedger)
                            {
                                cmd = new SqlCommand("account.Insert_Acc_SubLedgerType", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@VCHSlNo", vchdet.VchSrl);   //vsl.SLSlNo
                                cmd.Parameters.AddWithValue("@AccountCode", vsl.AccountCODE);
                                cmd.Parameters.AddWithValue("@SubCode", vsl.SubLedgerCODE);
                                cmd.Parameters.AddWithValue("@ModeofAdj", vsl.SubLedgerTypeID);
                                cmd.Parameters.AddWithValue("@AdjNumber", vsl.Number);
                                cmd.Parameters.AddWithValue("@AdjDate", voucherMast.VCHDATE);
                                cmd.Parameters.AddWithValue("@AdjAmount", vsl.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vsl.DRCR);
                                cmd.Parameters.AddWithValue("@SectorID", vchdet.SectorID);
                                cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                                SqlDataAdapter daSL = new SqlDataAdapter(cmd);
                                DataTable dtSL = new DataTable();
                                daSL.Fill(dtSL);

                                if (dtSL.Rows.Count > 0)
                                {
                                    vsl.BillID = Convert.ToInt32(dtSL.Rows[0]["BillID"].ToString());
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string UpdateVoucherMast(VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            string Result = "";
            SqlDataReader dr;
            try
            {
                if (voucherMast != null)
                {
                    SqlCommand cmd = new SqlCommand("account.Update_Acc_VoucherMaster", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                    cmd.Parameters.AddWithValue("@FIN_YEAR", voucherMast.YearMonth);
                    cmd.Parameters.AddWithValue("@VCH_TYPE", voucherMast.VoucherType);
                    cmd.Parameters.AddWithValue("@VCH_DATE", voucherMast.VCHDATE);
                    cmd.Parameters.AddWithValue("@VCH_AMOUNT", voucherMast.VCHAMOUNT);
                    cmd.Parameters.AddWithValue("@NARRATION", voucherMast.NARRATION);
                    cmd.Parameters.AddWithValue("@SectorID", voucherMast.SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Result = dr["Result"].ToString();
                        }
                    }
                    else
                    {
                        Result = "R";
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return Result;
        }

        private static void Update_VoucherDetail(List<VoucherDetail> voucherDetail, VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (voucherDetail.Count > 0)
                {
                    int i = 0;
                    foreach (VoucherDetail vchdet in voucherDetail)
                    {
                        i = i + 1;
                        cmd = new SqlCommand("account.Update_Acc_VoucherDetail", db, transaction);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VoucherDetailID", vchdet.VoucherDetailID == null ? 0 : (object)vchdet.VoucherDetailID);
                        cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                        cmd.Parameters.AddWithValue("@SerialNumber", vchdet.VchSrl);
                        cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                        cmd.Parameters.AddWithValue("@DRCR", vchdet.DRCR);
                        cmd.Parameters.AddWithValue("@Amount", vchdet.AMOUNT);
                        cmd.Parameters.AddWithValue("@sectorID", vchdet.SectorID);
                        cmd.Parameters.AddWithValue("@IsDelete", vchdet.IsDelete == null ? DBNull.Value : (object)vchdet.IsDelete);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            vchdet.VchSrl = Convert.ToInt32(dt.Rows[0]["VCHSRL"].ToString());
                        }


                        if (vchdet.VchInstType.Count > 0)
                        {
                            foreach (VoucherInstType vchinst in vchdet.VchInstType)
                            {
                                cmd = new SqlCommand("account.Update_AccDatInstBills", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@BillID", vchinst.BillID == null ? 0 : (object)vchinst.BillID);
                                cmd.Parameters.AddWithValue("@InFavourof", vchinst.Drawee == "" ? DBNull.Value : (object)vchinst.Drawee);
                                cmd.Parameters.AddWithValue("@InstrumentType", vchinst.InstType == "" ? DBNull.Value : (object)vchinst.InstType);
                                cmd.Parameters.AddWithValue("@InstrumentNo", vchinst.InstNo == "" ? DBNull.Value : (object)vchinst.InstNo);
                                cmd.Parameters.AddWithValue("@InstrumentDate", vchinst.InstDT == "" ? voucherMast.VCHDATE : vchinst.InstDT);
                                cmd.Parameters.AddWithValue("@insertedBy", UserID);
                                cmd.Parameters.AddWithValue("@sectorID", vchinst.SectorID);
                                cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                                cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@AdjustmentAmount", vchinst.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vchinst.DRCR);
                                cmd.Parameters.AddWithValue("@VoucherSerial", vchdet.VchSrl);
                                cmd.Parameters.AddWithValue("@IsDelete", vchinst.IsDelete == null ? DBNull.Value : (object)vchinst.IsDelete);
                                cmd.Parameters.AddWithValue("@DeptCode", (vchinst.AccDeptCode == null || vchinst.AccDeptCode == "") ? DBNull.Value : (object)vchinst.AccDeptCode);
                                cmd.Parameters.AddWithValue("@CostCenterCode", (vchinst.AccCostCenterCode == null || vchinst.AccCostCenterCode == "") ? DBNull.Value : (object)vchinst.AccCostCenterCode);

                                cmd.ExecuteNonQuery();
                            }
                        }
                        if (vchdet.VchSubLedger.Count > 0)
                        {
                            foreach (VoucherSubLedgerType vsl in vchdet.VchSubLedger)
                            {
                                cmd = new SqlCommand("account.Update_Acc_SubLedgerType", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@BillID", vsl.BillID == null ? 0 : (object)vsl.BillID);
                                cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@VCHSlNo", vchdet.VchSrl);   //vsl.SLSlNo
                                cmd.Parameters.AddWithValue("@AccountCode", vsl.AccountCODE);
                                cmd.Parameters.AddWithValue("@SubCode", vsl.SubLedgerCODE);
                                cmd.Parameters.AddWithValue("@ModeofAdj", vsl.SubLedgerTypeID);
                                cmd.Parameters.AddWithValue("@AdjNumber", vsl.Number);
                                cmd.Parameters.AddWithValue("@AdjDate", voucherMast.VCHDATE);
                                cmd.Parameters.AddWithValue("@AdjAmount", vsl.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vsl.DRCR);
                                cmd.Parameters.AddWithValue("@SectorID", vchdet.SectorID);
                                cmd.Parameters.AddWithValue("@IsDelete", vsl.IsDelete == null ? DBNull.Value : (object)vsl.IsDelete);
                                cmd.Parameters.AddWithValue("@InsertedBy", UserID);

                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}