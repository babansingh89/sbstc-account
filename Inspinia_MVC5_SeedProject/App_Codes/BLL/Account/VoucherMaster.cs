﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherMaster
/// </summary>
public class VoucherMaster
{

    public string YearMonth { get; set; }
    public int VoucherType { get; set; }
    public int VCHNo { get; set; }
    public string VoucherNo { get; set; }
    public string VCHDATE { get; set; }
    public string VCHAMOUNT { get; set; }
    public string NARRATION { get; set; }
    public string STATUS { get; set; }
    public int SectorID { get; set; }
    public string AutoTrans { get; set; }

    public VoucherMaster()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}