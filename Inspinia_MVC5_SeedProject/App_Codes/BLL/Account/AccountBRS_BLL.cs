﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class AccountBRS_BLL:BllBase
    {
        public List<AccountBRS> Auto_Bank(string Desc, string SectorID, string Type)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_AccountBank";
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Type", Type);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Convert_AutoBank(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Show_Detail(string AccountCode, string CorresOption, string InstRecIss, string FromDate, string ToDate, string InstrumentNo, string SectorID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_InstrumentDetail";
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@DR_CR", InstRecIss);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@INST_NO", InstrumentNo);
            cmd.Parameters.AddWithValue("@NormalClear", CorresOption);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Save(List<AccountBRS> master, long UserID)
        {
            try
            {
                cn.Open();
                foreach (var list in master)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "account.Update_Bill";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BillID", list.BillID);
                    cmd.Parameters.AddWithValue("@Cleared", list.Cleared);
                    cmd.Parameters.AddWithValue("@ClearedOn", list.ClearedOn == null ? DBNull.Value : (object)list.ClearedOn);
                    cmd.Parameters.AddWithValue("@VoucherAmount", list.VchAmount);
                    cmd.Parameters.AddWithValue("@InFavour", list.InFavour);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.ExecuteNonQuery();
                }
                
                return "success";
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<AccountBRS> Convert_AutoBank(DataTable dt)
        {
            List<AccountBRS> lst = new List<AccountBRS>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new AccountBRS();
                obj.AccountCode = t.Field<string>("AccountCode");
                obj.AccountDescription = t.Field<string>("AccountDescription");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}