﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Inventory_BLL: BllBase
    {
        public object GetAuto(string AccountDescription, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.AccountHeaderAuto", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Where(r => r.Field<string>("GroupLedger") == "L").Select(t => new
                {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).Take(10).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void InsUpdInventorydetails(InventoryDetails InvDetails) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Sp_InsUpdInventorydetails]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Id", InvDetails.Id);
                cmd.Parameters.AddWithValue("@stockdate", InvDetails.InvDate);
                cmd.Parameters.AddWithValue("@AccountCode", InvDetails.AccountCode);
                cmd.Parameters.AddWithValue("@Amount", InvDetails.Amount);
                cmd.Parameters.AddWithValue("@SectorID", InvDetails.SectorID);
                //cmd.Parameters.AddWithValue("@ScoreDesc", !string.IsNullOrEmpty(assmtBeltSheetInsUp.ToRange) ? assmtBeltSheetInsUp.ToRange : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", InvDetails.InsertedBy);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable GetAllInventoryDetails(string stockdate, string SectorID, long? UserID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_AccHeadForInventry";  //sp_getInventoryDetails
            cmd.Parameters.AddWithValue("@TransType", "Fetch");
            cmd.Parameters.AddWithValue("@StockDate", stockdate);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            try
            {
                cn.Open();
                sda.Fill(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public void DeleteInvDetailsById(int Id) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Sp_DeleteInvDetailsById]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Id",Id);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public int Insert(string TransType, string MTabID, string TabID, string StockDate, string AccountCode, string ParentAccountCode, string Amount, string SectorID, long? UserID)
        {
            SqlDataReader dr; int RTabID = 0;
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Get_AccHeadForInventry]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@TransType", ((MTabID == "" || MTabID == null || MTabID == "null") ? "Insert" : "Update"));
                cmd.Parameters.AddWithValue("@InvId", ((MTabID == "" || MTabID == null || MTabID == "null") ? DBNull.Value : (object)MTabID));
                cmd.Parameters.AddWithValue("@TabID", ((TabID == "" || TabID == null || TabID == "null") ? DBNull.Value : (object)TabID));
                cmd.Parameters.AddWithValue("@StockDate", StockDate == "" ? DBNull.Value : (object)StockDate);
                cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
                cmd.Parameters.AddWithValue("@ParentAccountCode", ParentAccountCode == "" ? DBNull.Value : (object)ParentAccountCode);
                cmd.Parameters.AddWithValue("@Amount", Amount == "" ? DBNull.Value : (object)Amount);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Transaction = Transaction;

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    RTabID = Convert.ToInt32(dr["TabID"].ToString());
                }
                dr.Close();

                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
            return RTabID;
        }

        public string Calculate(string TransType, string StockDate, string AccountCode, string SectorID)
        {
            SqlDataReader dr; string TotalAmount = "";
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Get_AccHeadForInventry]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@TransType", TransType);
                cmd.Parameters.AddWithValue("@StockDate", StockDate);
                cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Transaction = Transaction;

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    TotalAmount = dr["TotalAmount"].ToString();
                }
                dr.Close();

                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
            return TotalAmount;
        }

        public DataTable Post(string TransType, string stockdate, string SectorID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_AccHeadForInventry";  //sp_getInventoryDetails
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@StockDate", stockdate);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            try
            {
                cn.Open();
                sda.Fill(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

    }
}