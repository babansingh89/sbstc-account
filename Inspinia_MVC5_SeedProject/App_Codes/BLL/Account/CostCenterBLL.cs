﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class CostCenterBLL: BllBase
    {
        public List<CostCenter> GetVoucherByTypeDate(CostCenter CostCenterObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[account].[sp_VoucherByTypeDate]";
                cmd.Parameters.AddWithValue("@VCH_NO", DBNull.Value);
                cmd.Parameters.AddWithValue("@VCH_TYPE", CostCenterObj.VCH_Type);
                cmd.Parameters.AddWithValue("@UserID", DBNull.Value);
                cmd.Parameters.AddWithValue("@fromdate", CostCenterObj.Fromdate);
                cmd.Parameters.AddWithValue("@toDate", CostCenterObj.ToDate);
                cmd.Parameters.AddWithValue("@SectorID", CostCenterObj.SectorID);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new CostCenter
                {
                    VCH_No = t.Field<int?>("VoucherNumber"),
                    VCH_Date = t.Field<string>("VCH_DATE"),
                    VCH_Ref = t.Field<string>("RefVoucherSlNo"),
                    Narration = t.Field<string>("NARRATION"),
                }).ToList();
                return lst;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<CostCenter> getVoucherByID(CostCenter CostCenterObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[account].[sp_GetVoucherByID]";
                cmd.Parameters.AddWithValue("@VCH_NO", CostCenterObj.VCH_No);
                cmd.Parameters.AddWithValue("@SectorID", DBNull.Value);
                cmd.Parameters.AddWithValue("@UserID", DBNull.Value);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new CostCenter
                {
                    VoucherDetailID = t.Field<int?>("VoucherDetailID"),
                    VCH_No = t.Field<int?>("VoucherNumber"),
                    AccountCode = t.Field<string>("AccountCode"),
                    OldAccountHead = t.Field<string>("OldAccountHead"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    DRCR = t.Field<string>("DRCR"),
                    CR_AMT = t.Field<decimal>("CR_AMT"),
                    DR_AMT = t.Field<decimal>("DR_AMT"),

                }).ToList();
                return lst;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<CostCenter> GetCostCenterList(CostCenter CostCenterObj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "[account].[sp_GetCostCenterList]";
                cmd.Parameters.AddWithValue("@VCH_NO", CostCenterObj.VCH_No);
                cmd.Parameters.AddWithValue("@AccountCode",CostCenterObj.AccountCode);
                cmd.Parameters.AddWithValue("@SectorID", DBNull.Value);
                cmd.Parameters.AddWithValue("@UserID", DBNull.Value);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new CostCenter
                {
                    BillId = t.Field<Int64>("BillId"),
                    CostCenterCode = t.Field<string>("CostCenterCode"),
                    CostCenterDesc = t.Field<string>("CostCenterDesc"),
                    DeptCode = t.Field<string>("DeptCode"),
                    DeptDesc = t.Field<string>("DeptDesc"),
                    AdjustmentAmount = t.Field<decimal>("AdjustmentAmount"),
                    InstrumentNumber = t.Field<string>("InstrumentNumber"),
                    InstrumentDate = t.Field<string>("InstrumentDate"),
                    AdjustmentNumber = t.Field<string>("AdjustmentNumber"),
                    InFavour = t.Field<string>("InFavour"),
                    WardNo= t.Field<int?>("BranchID"),
                    radioVal= t.Field<int?>("centerid"),

                }).ToList();
                return lst;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void InsUpdCostCenterdetails(CostCenter CostCenterObj)
        {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Sp_InsUpdCostCenterdetails]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@BillId", CostCenterObj.BillId.Equals(" ")? DBNull.Value:(object)Convert.ToInt64(CostCenterObj.BillId));
                cmd.Parameters.AddWithValue("@DRCR",!string.IsNullOrEmpty(CostCenterObj.DRCR) ? (object)CostCenterObj.DRCR: DBNull.Value);
                cmd.Parameters.AddWithValue("@VCH_No", CostCenterObj.VCH_No ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@VoucherDetailID", CostCenterObj.VoucherDetailID ?? DBNull.Value as object);
                cmd.Parameters.AddWithValue("@AccountCode", !string.IsNullOrEmpty(CostCenterObj.AccountCode) ? CostCenterObj.AccountCode:(object)DBNull.Value);
                cmd.Parameters.AddWithValue("@CostCenterCode", CostCenterObj.CostCenterCode);
                cmd.Parameters.AddWithValue("@DeptCode", CostCenterObj.DeptCode);
                cmd.Parameters.AddWithValue("@AdjustmentAmount", CostCenterObj.AdjustmentAmount);
                cmd.Parameters.AddWithValue("@WardNo", CostCenterObj.WardNo);
                //cmd.Parameters.AddWithValue("@ScoreDesc", !string.IsNullOrEmpty(assmtBeltSheetInsUp.ToRange) ? assmtBeltSheetInsUp.ToRange : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@radioVal", CostCenterObj.radioVal);
                cmd.Parameters.AddWithValue("@SectorID", CostCenterObj.SectorID);
                cmd.Parameters.AddWithValue("@InsertedBy", CostCenterObj.InsertedBy);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object GetCostCenter()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "[account].[sp_GetCostCenter]";
            //cmd.Parameters.AddWithValue("@TransactionType", "OffDayList");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    CostCenterCode = t.Field<string>("CostCenterCode"),
                    CostCenterDesc = t.Field<string>("CostCenterDesc")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object GetDept()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "[account].[sp_GetDept]";
            //cmd.Parameters.AddWithValue("@TransactionType", "OffDayList");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    DeptCode = t.Field<string>("DeptCode"),
                    DeptDesc = t.Field<string>("DeptDesc")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}