﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class BalanceSheet_BLL:BllBase
    {
            public DataSet Show_Details(string FromDate, string ToDate, string SectorID, string OpeningClosing)
            {
                SqlCommand cmd = new SqlCommand("account.PROC_ACC_BALANCE_SHEET", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_FROM_DATE", FromDate);
                cmd.Parameters.AddWithValue("@p_TO_DATE", ToDate);
                cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            cmd.Parameters.AddWithValue("@OpCl", OpeningClosing);
            //cmd.Parameters.AddWithValue("@p_PL", "Y");          //DEFAULT FOR PROFIT AND LOSS
            SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                try
                {
                    cn.Open();
 cmd.CommandTimeout = 0;
                    da.Fill(ds);
                    return ds;
                }
                catch (SqlException)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }

            public DataSet Show_Group(string AccCode, string FromDate, string ToDate, string SectorID, string OpeningClosing)
            {
                SqlCommand cmd = new SqlCommand("account.PROC_ACC_GROUP", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_ACCCODE", AccCode);
                cmd.Parameters.AddWithValue("@p_FROM_DATE", FromDate);
                cmd.Parameters.AddWithValue("@p_TO_DATE", ToDate);
                cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
                cmd.Parameters.AddWithValue("@OpCl", OpeningClosing);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                try
                {
                    cn.Open();
                    da.Fill(ds);
                    return ds;
                }
                catch (SqlException)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }

        public DataTable Show_Exp_Main_2(string AccountCode, string StartDate, string EndDate, string SectorID, string IS_GL)
        {
            SqlCommand cmd = new SqlCommand("account.Acc_Month_Wise_Dtls", cn);  //Get_SubAccountDescriptionByMonth
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Start_dt", StartDate);
            cmd.Parameters.AddWithValue("@End_dt", EndDate);
            cmd.Parameters.AddWithValue("@PAccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@Sectorids", SectorID);
            cmd.Parameters.AddWithValue("@IS_GL", IS_GL);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;

                da.Fill(dt);
                // return Convert_Show_Details(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
    
}