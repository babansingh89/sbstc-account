﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class AccountVoucherOpening: BllBase
    {
        public object getAccountDetails(string FinYear, string VoucherType, string SectorID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GET_VoucherOpeningDetail";
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@VoucharType", VoucherType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //return convertAcHead(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    AccountCode = t.Field<string>("AccountCode"),
                    OldAccountHead = t.Field<string>("OldAccountHead"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    DRCR = t.Field<string>("DRCR"),
                    OpBalance = t.Field<decimal>("OpBalance"),
                    
                });
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object getAccountDetailsbyCodeName(string AutoText, string SearchType, string VoucherType, string FinYear, string SectorID) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GET_VoucherOpeningDetailBySearch";
            cmd.Parameters.AddWithValue("@SearchValue", AutoText);
            cmd.Parameters.AddWithValue("@SearchType", SearchType);
            cmd.Parameters.AddWithValue("@VoucharType", VoucherType);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //return convertAcHead(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    AccountCode = t.Field<string>("AccountCode"),
                    OldAccountHead = t.Field<string>("OldAccountHead"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    DRCR = t.Field<string>("DRCR"),
                    OpBalance = t.Field<decimal>("OpBalance"),

                });
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void update_Details(string AccountCode, string ParentAccountCode, string DRCR, decimal Amount, string FinYear, string SectorID, long UserID) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.Update_AccountOpeningBalance";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
                cmd.Parameters.AddWithValue("@ParentAccountCode", ParentAccountCode);
                cmd.Parameters.AddWithValue("@DRCR", DRCR);
                cmd.Parameters.AddWithValue("@Amount", Amount);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@SecID", SectorID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Auto_Ledger_Details(string AccountCode, string Desc, string FinYear, string SectorID)
        {
            DataTable dt = new DataTable();
           
            try
            {
                SqlCommand cmd = new SqlCommand();
                cn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.Load_SubAccount_ByAccCode";
                cmd.Parameters.AddWithValue("@AccCode", AccountCode);
                cmd.Parameters.AddWithValue("@Desc", Desc);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
               
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public DataTable Auto_SubLedger_Details(string AccountCode, string Desc, string FinYear, string SectorID)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand();
                cn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.Load_LedgerSubAccount_ByAccCode";
                cmd.Parameters.AddWithValue("@AccCode", AccountCode);
                cmd.Parameters.AddWithValue("@Desc", Desc);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
    }
}