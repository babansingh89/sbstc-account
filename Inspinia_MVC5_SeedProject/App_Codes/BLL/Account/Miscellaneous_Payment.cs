﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Miscellaneous_Payment:BllBase
    {
        public List<Miscellaneous_Payment_MD> Get_PaymentType()
        {
            SqlCommand cmd = new SqlCommand("account.Load_PaymentType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Miscellaneous_Payment_MD> Get_PaymentTypeByID(int RTypeID)
        {
            SqlCommand cmd = new SqlCommand("account.Load_PaymentTypeByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReimbursementTypeID", RTypeID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_EmpNoAuto_Complete(int isnostring, string EmpNo_EmpName, string ReimbursementTypeID, string TransType, string EmpType, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", EmpNo_EmpName == "" ? DBNull.Value : (object)EmpNo_EmpName);
            cmd.Parameters.AddWithValue("@isEmpNo_EmpName", isnostring);
            cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Populate_Adjustment_Details(long UserID, string EmployeeID, string ReimbursementTypeID, string TransType, string EmpType, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", EmployeeID);
            cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Save(string ReimbursementTypeID, string EmployeeID, string BeneficiaryID, string FromDate, string ToDate, string NetAmount, string PaymentType,
            string Remarks, string SectorID, string ReimbursementID, string Emptype, long UserID)
        {
            string RReimbursementID = ""; string MemoNo = ""; string Result = "";
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);//transaction
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", (ReimbursementID == null || ReimbursementID == "") ? "Save" : "Update");
                    cmd.Parameters.AddWithValue("EmployeeID", (EmployeeID == null || EmployeeID == "") ? DBNull.Value : (object)EmployeeID);
                    cmd.Parameters.AddWithValue("@BeneficiaryID", (BeneficiaryID == null || BeneficiaryID == "") ? DBNull.Value : (object)BeneficiaryID);
                    cmd.Parameters.AddWithValue("@FromDate", FromDate == null ? DBNull.Value : (object)FromDate);
                    cmd.Parameters.AddWithValue("@ToDate", ToDate == null ? DBNull.Value : (object)ToDate);
                    cmd.Parameters.AddWithValue("@NetAmount", NetAmount == null ? DBNull.Value : (object)NetAmount);
                    cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID == null ? DBNull.Value : (object)ReimbursementTypeID);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID == null ? DBNull.Value : (object)SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                    cmd.Parameters.AddWithValue("@Emptype", Emptype == null ? DBNull.Value : (object)Emptype);
                    cmd.Parameters.AddWithValue("@PaymentMode", PaymentType == null ? DBNull.Value : (object)PaymentType);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks == null ? DBNull.Value : (object)Remarks);
                    cmd.Parameters.AddWithValue("@ReimbursementfID", ReimbursementID);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        MemoNo = dr["MemoNo"].ToString();
                        RReimbursementID = dr["ReimbursementID"].ToString();
                    }
                    dr.Close();

                    if (RReimbursementID != null)
                    {
                        cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "Flag");
                        cmd.Parameters.AddWithValue("@ReimbursementfID", RReimbursementID);
                        cmd.ExecuteNonQuery();
                    }

                    Result = "success";
                    transaction.Commit();
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                }
            }
            catch (SqlException e)
            {
                Result = "fail";
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            if (Result == "success")
                return MemoNo;
            else
                return Result;
        }


        #region Converter
        List<Miscellaneous_Payment_MD> Convert(DataTable dt)
        {
            List<Miscellaneous_Payment_MD> lst = new List<Miscellaneous_Payment_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Miscellaneous_Payment_MD();
                obj.ReimbursementTypeID = t.Field<int>("ReimbursementTypeID");
                obj.ReimbursementType = t.Field<string>("ReimbursementType");
                obj.TempleteID = t.Field<int?>("TempleteID");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}