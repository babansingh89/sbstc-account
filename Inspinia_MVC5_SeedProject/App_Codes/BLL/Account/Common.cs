﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Common: BllBase
    {
        public List<Common_MD> FilName()
        {
            List<Common_MD> lst = new List<Common_MD>();
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "FileName");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
               
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Common_MD();
                    obj.FileName = t.Field<string>("FileName");
                    obj.ID = t.Field<long?>("ID");
                    return obj;
                }).ToList();
               
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
    }
}