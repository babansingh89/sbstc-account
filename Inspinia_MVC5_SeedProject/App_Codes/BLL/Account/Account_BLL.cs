﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Account_BLL:BllBase
    {
        public DataTable TreeView()
        {
            SqlCommand cmd = new SqlCommand("account.Load_AccountHeader", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
               
                //return Convert(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public List<Account_MD> Load_Account()
        {
            SqlCommand cmd = new SqlCommand("account.Load_AccountHeader", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Account_MD> Convert(DataTable dt)
        {
            List<Account_MD> lst = new List<Account_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Account_MD();
                obj.AccountCode = t.Field<string>("AccountCode");
                obj.AccountDescription = t.Field<string>("AccountDescription");
                obj.ParentAccountCode = t.Field<string>("ParentAccountCode");
                return obj;
            }).ToList();
            return lst;
        }


        #endregion


        public object GetSubledgerAuto(string AccountDescription, string SectorID) {
            SqlCommand cmd = new SqlCommand("account.AccountHeaderAuto", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Where(r => r.Field<string>("GroupLedger") == "S").Select(t => new
                {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).Take(10).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally {
                cn.Close();
            }
        }

        public object GetAccountHdeadAuto(string AccountDescription, string Group)
        {
            SqlCommand cmd = new SqlCommand("account.AccountHeaderAuto", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                da.Fill(dt);

                //foreach (DataRow dr in dt.Rows)
                //{
                //    //result.Add(string.Format("{0}|{1}", dr["AccountCode"].ToString().Trim(), dr["AccountDescription"].ToString().Trim()));
                //    result.Add(string.Format("{0}|{1}", dr["AccountDescription"].ToString().Trim(), dr["AccountCode"].ToString().Trim()));
                //}
                //return result.Take(10).ToList();

                var lst = dt.AsEnumerable().Where(r => r.Field<string>("GroupLedger") == Group).Select(t => new
                {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).Take(10).ToList();
                return lst;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object Load_AccountAutoComplete(string AccountDescription, string AccountORSubLedger)
        {
            SqlCommand cmd = new SqlCommand("account.Search_AccountDescription", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@AccountORSubLedger", (object)AccountORSubLedger ?? DBNull.Value); 
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object subledgerAutoComplete(string IFSC) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Search_IFSC";
            cmd.Parameters.AddWithValue("@IFSCCode", (object)IFSC ?? DBNull.Value);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<string> result = new List<string>();
            try
            {
                cn.Open();
                sda.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    result.Add(string.Format("{0}|{1}", dr["IFSC"].ToString().Trim(), dr["BranchID"].ToString().Trim()));
                }
                return result;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Account_Description> Account_DescriptionAuto(string Desc, string Subledger, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Load_AccountHeader", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Acc_SubLedger", Subledger);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Account_Description> lsthead = new List<Account_Description>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new Account_Description
                {
                    AccountCode = t.Field<string>("AccountCode"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    text = t.Field<string>("AccountDescription"),
                    ParentAccountCode = t.Field<string>("ParentAccountCode"),
                    GroupLedger = t.Field<string>("GroupLedger"),
                   }).ToList();

                var ledger = lst.Select(x => new Account_Description
                {
                    AccountCode = x.AccountCode,
                    AccountDescription = x.AccountDescription,
                    text = x.AccountDescription,
                    ParentAccountCode = x.ParentAccountCode,
                    //Children = getSub(lst,x.AccountCode),
                    nodes = getSub(lst, x.AccountCode)
                }).ToList();



                return ledger;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Account_Description> GetAccountHead(int? SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Load_AccountHeader", cn);
            cmd.Parameters.AddWithValue("@SectorID", (object)SectorID ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@Acc_SubLedger", "A");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Account_Description> lsthead = new List<Account_Description>();
            try
            {
                cn.Open();
                da.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    Account_Description achead = new Account_Description();
                    achead.AccountCode = row.Field<string>("AccountCode");
                    achead.AccountDescription = row.Field<string>("AccountDescription");
                    achead.text= row.Field<string>("AccountDescription");
                    achead.ParentAccountCode = row.Field<string>("ParentAccountCode");
                    achead.GroupLedger = row.Field<string>("GroupLedger");
                    lsthead.Add(achead);

                }
                var mainList = GetMenuTree(lsthead, "0");

                return mainList;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        private List<Account_Description> GetMenuTree(List<Account_Description> list, string parent)
        {
            var xxx = list.Where(x => x.ParentAccountCode == parent);
            return list.Where(x => x.ParentAccountCode==parent ).Select(x => new Account_Description
            {
                AccountCode = x.AccountCode,
                AccountDescription = x.AccountDescription,
                text= x.AccountDescription,
                ParentAccountCode = x.ParentAccountCode,
                //Children = GetMenuTree(list, x.AccountCode),
                nodes= GetMenuTree(list, x.AccountCode)
            }).ToList();
        }

        public List<Account_Description> getSubledger(int? SectorID) {
            SqlCommand cmd = new SqlCommand("account.Load_AccountHeader", cn);
            cmd.Parameters.AddWithValue("@SectorID", (object)SectorID ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@Acc_SubLedger", "S");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            //List<Account_Description> lsthead = new List<Account_Description>();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new Account_Description
                {
                    AccountCode = t.Field<string>("AccountCode"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    text = t.Field<string>("AccountDescription"),
                    ParentAccountCode = t.Field<string>("ParentAccountCode"),
                    GroupLedger = t.Field<string>("GroupLedger"),
                    SubLedger = t.Field<string>("SubLedger")
                }).ToList();

                var ledger = lst.Where(x => x.GroupLedger == "L" && x.SubLedger == "Y").Select(x => new Account_Description
                {
                    AccountCode = x.AccountCode,
                    AccountDescription = x.AccountDescription,
                    text = x.AccountDescription,
                    ParentAccountCode = x.ParentAccountCode,
                    //Children = getSub(lst,x.AccountCode),
                    nodes = getSub(lst, x.AccountCode)
                }).ToList();



                return ledger;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            
        }

        private List<Account_Description> getSub(List<Account_Description> list, string parent)
        {
            var lst = list.Where(x => x.ParentAccountCode == parent).Select(x => new Account_Description
            {
                AccountCode = x.AccountCode,
                AccountDescription = x.AccountDescription,
                text = x.AccountDescription,
                ParentAccountCode = x.ParentAccountCode,
            }).ToList();
            return lst;
            //return list.Where(x => x.ParentAccountCode == parent).Select(x => new Account_Description
            //{
            //    AccountCode = x.AccountCode,
            //    AccountDescription = x.AccountDescription,
            //    text = x.AccountDescription,
            //    ParentAccountCode = x.ParentAccountCode,
            //}).ToList();
        }

        public object getState() {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Load_State";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    StateId = t.Field<int>("StateId"),
                    State = t.Field<string>("State")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public object getDistrict(int Id) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Load_District";
            cmd.Parameters.AddWithValue("@StateID", Id);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    DistID = t.Field<int>("DistID"),
                    District = t.Field<string>("District")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public object getAllBank() {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GET_BankList_BenMaster";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return convertbank(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        object convertbank(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                BankID = t.Field<int>("BankID"),
                BankName = t.Field<string>("BankName")
            }).ToList();
            return lst;
        }

        public object getAllSector() {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GetAllSector";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    SectorID = t.Field<int>("SectorID"),
                    SectorName = t.Field<string>("SectorName")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public object getAllBranch(int bankID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_Branch_BenMaster";
            cmd.Parameters.AddWithValue("@bankID", bankID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Conversion(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        object Conversion(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                BranchID = t.Field<int>("BranchID"),
                Branch = t.Field<string>("Branch"),
            }).ToList();
            return lst;
        }

        public DataSet getAccountHeadById(string AccountCode, string SectorID) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_ChildAccountDescription";
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                //return convertAcHead(dt);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        object convertAcHead(DataTable dt)
        {
            //List<AssmtScoreSheet> lst = new List<AssmtScoreSheet>();
            var lst = dt.AsEnumerable().Select(t => new {
                AccountCode = t.Field<string>("AccountCode"),
                AccountName = t.Field<string>("AccountName"),
                AccountDescription = t.Field<string>("AccountDescription"),
                ParentAccountCode = t.Field<string>("ParentAccountCode"),
                ParentName = t.Field<string>("ParentName"),
                OldAccountHead = t.Field<string>("OldAccountHead"),
                GST = t.Field<string>("GST"),
                PAN = t.Field<string>("PAN"),
                ContactNo = t.Field<string>("ContactNo"),
                PayeeContactPerson = t.Field<string>("PayeeContactPerson"),
                BankID = t.Field<int>("BankID"),
                BankName = t.Field<string>("BankName"),
                BranchID = t.Field<int>("BranchID"),
                Branch = t.Field<string>("Branch"),
                IFSC = t.Field<string>("IFSC"),
                MICR = t.Field<string>("MICR"),
                Address = t.Field<string>("Address"),
                AccountTypeID = t.Field<int>("AccountTypeID"),
                AccountNumber = t.Field<string>("AccountNumber"),
                SubLedger = t.Field<string>("SubLedger"),
                ChildCount = t.Field<int>("ChildCount"),
                GroupLedger = t.Field<string>("GroupLedger"),
                Schedule = t.Field<string>("Schedule"),
                PLDirectIndirect = t.Field<string>("PLDirectIndirect"),
                sectorid = t.Field<int?>("sectorid"),
                IELA = t.Field<string>("IELA"),
                State = t.Field<string>("State"),
                DistrictId = t.Field<int?>("DistrictId"),
                PinNo = t.Field<string>("PinNo"),
                centerid = t.Field<int?>("centerid"),
                BankCash=t.Field<string>("BankCash")
            });
            return lst;
        }

        public object getSubledgerByBranchId(string id) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Get_BankandBranch";
            cmd.Parameters.AddWithValue("@BranchID", id);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //return convertAcHead(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    BranchID = t.Field<int>("BranchID"),
                    Branch = t.Field<string>("Branch"),
                    BankName = t.Field<string>("BankName"),
                    MICR = t.Field<string>("MICR"),
                });
                return lst;
            }
            catch (SqlException)
            {
                throw ;
            }
            finally
            {
                cn.Close();
            }
        }
        
        public string saveAccountHead(string AccountID,string parent, string acDesc, string GLS, string IELA,string IsSubLedger, string AccountType, int branchId, string acno, 
            string ISBank, string achead, int? SectorID, long InsertedBy, string schedule, string PL, int? centerid, string Active, string IFSC, string MICR, string BankName, string BranchName, string AllowNegtive) {
            SqlTransaction Transaction = null; string Msg = "";
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.Insert_AccountApproval";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@AccountID", !string.IsNullOrEmpty(AccountID) ? AccountID : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AccDesc",  !string.IsNullOrEmpty(acDesc) ? acDesc : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ParentAccCode", parent);
                cmd.Parameters.AddWithValue("@GrouporLedger", GLS);
                cmd.Parameters.AddWithValue("@IELA", IELA);
                cmd.Parameters.AddWithValue("@IsSubLedger", !string.IsNullOrEmpty(IsSubLedger) ? IsSubLedger : (object)DBNull.Value );
                cmd.Parameters.AddWithValue("@BranchID", branchId);
                cmd.Parameters.AddWithValue("@AccountNumber",  !string.IsNullOrEmpty(acno) ? acno : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@isBankCash", !string.IsNullOrEmpty(ISBank) ? ISBank:(object)DBNull.Value);
                cmd.Parameters.AddWithValue("@OldAccountHead", achead);
                cmd.Parameters.AddWithValue("@Address",DBNull.Value);
                cmd.Parameters.AddWithValue("@PAN", DBNull.Value);
                cmd.Parameters.AddWithValue("@ContactNo", DBNull.Value);
                cmd.Parameters.AddWithValue("@AccountType", AccountType);
                cmd.Parameters.AddWithValue("@GST", DBNull.Value);
                cmd.Parameters.AddWithValue("@PayeeContactPerson", DBNull.Value);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                cmd.Parameters.AddWithValue("@schedule",  !string.IsNullOrEmpty(schedule) ? schedule : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PL", PL);
                cmd.Parameters.AddWithValue("@StateId", DBNull.Value);
                cmd.Parameters.AddWithValue("@DistrictId", DBNull.Value);
                cmd.Parameters.AddWithValue("@PIN", DBNull.Value);
                cmd.Parameters.AddWithValue("@Active", Active);
                cmd.Parameters.AddWithValue("@IFSC", IFSC == "" ? DBNull.Value : (object)IFSC);
                cmd.Parameters.AddWithValue("@MICR", MICR == "" ? DBNull.Value : (object)MICR);
                cmd.Parameters.AddWithValue("@BankName", BankName == "" ? DBNull.Value : (object)BankName);
                cmd.Parameters.AddWithValue("@BranchName", BranchName == "" ? DBNull.Value : (object)BranchName);
                cmd.Parameters.AddWithValue("@centerid", centerid ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AllowNegtive", AllowNegtive == "" ? DBNull.Value : (object)AllowNegtive);
                cmd.Transaction = Transaction;

                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Msg = dr["Msg"].ToString();
                    }
                }
                dr.Close();

                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
            return Msg;
        }
        public void updateAccountHead(string Id, string Name, string Parent, string ParentId, string acHead, int branchId, string txtacno, string IsSubLedger,string GroupLedger,int? SectorID,long InsertedBy, string schedule, string PL, string Active, string IFSC)
        {
            SqlTransaction Transaction = null;
            try
            {

                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "account.Update_AccountApproval";
                cmd.Connection = cn;


                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Parent", Parent);
                cmd.Parameters.AddWithValue("@ParentId", ParentId);
                cmd.Parameters.AddWithValue("@BranchID", branchId);
                cmd.Parameters.AddWithValue("@AccountNumber", !string.IsNullOrEmpty(txtacno) ? txtacno : (object)DBNull.Value);
                //cmd.Parameters.AddWithValue("@isBankCash", ISBank);
                cmd.Parameters.AddWithValue("@OldAccountHead", !string.IsNullOrEmpty(acHead) ? (object)acHead : DBNull.Value);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                cmd.Parameters.AddWithValue("@IsSubLedger", !string.IsNullOrEmpty(IsSubLedger) ? IsSubLedger : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@GroupLedger",!string.IsNullOrEmpty( GroupLedger)? GroupLedger:(object)DBNull.Value);
                cmd.Parameters.AddWithValue("@schedule", schedule);
                cmd.Parameters.AddWithValue("@PL", PL);
                cmd.Parameters.AddWithValue("@StateId", DBNull.Value);
                cmd.Parameters.AddWithValue("@DistrictId", DBNull.Value);
                cmd.Parameters.AddWithValue("@PIN", DBNull.Value);
                cmd.Parameters.AddWithValue("@Active", Active);
                cmd.Parameters.AddWithValue("@IFSC", IFSC);
                cmd.Parameters.AddWithValue("@centerid", DBNull.Value);
                //comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                //comd.Parameters.AddWithValue("@LastName", obj.LastName);
                //comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                //comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                //comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                //comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                //comd.Parameters.AddWithValue("@UserType", obj.UserType);
                //comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                //comd.Parameters.AddWithValue("@UserName", obj.UserName);
                //comd.Parameters.AddWithValue("@Password", Password);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();

                Transaction.Commit();
            }
            catch (Exception)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string saveSubLedger(string AccountID, string Parent, string Payee, string GST, string Address, string PAN, string GroupLedger, string IELA, 
            string Mob, string PayeeContactPerson, string branch, int AccountType, int? SectorID, long InsertedBy, int state, int district, string PIN, 
            List<Account_Description> EffectedGL, string AccNo, string Active, string IFSC, string MICR, string BankName, string BranchName)
        {
		 string Msg = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
             SqlDataReader dr;
            try
            {
                cmd = new SqlCommand("account.Insert_AccountApproval", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccountID", !string.IsNullOrEmpty(AccountID) ? AccountID : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AccDesc",  !string.IsNullOrEmpty(Payee) ? Payee : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ParentAccCode", Parent);
                cmd.Parameters.AddWithValue("@GrouporLedger", !string.IsNullOrEmpty(GroupLedger) ? GroupLedger : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IELA", !string.IsNullOrEmpty(IELA) ? IELA : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@IsSubLedger", DBNull.Value);
                cmd.Parameters.AddWithValue("@BranchID",  !string.IsNullOrEmpty(branch) ? branch : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AccountNumber", !string.IsNullOrEmpty(AccNo) ? AccNo : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@isBankCash", DBNull.Value);
                cmd.Parameters.AddWithValue("@OldAccountHead", DBNull.Value);
                cmd.Parameters.AddWithValue("@Address",  !string.IsNullOrEmpty(Address) ? Address : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PAN",  !string.IsNullOrEmpty(PAN) ? PAN : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@ContactNo",  !string.IsNullOrEmpty(Mob) ? Mob : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AccountType", AccountType);
                cmd.Parameters.AddWithValue("@GST",  !string.IsNullOrEmpty(GST) ? GST : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@PayeeContactPerson",  !string.IsNullOrEmpty(PayeeContactPerson) ? PayeeContactPerson : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                cmd.Parameters.AddWithValue("@schedule", DBNull.Value);
                cmd.Parameters.AddWithValue("@PL", DBNull.Value);
                cmd.Parameters.AddWithValue("@StateId", state);
                cmd.Parameters.AddWithValue("@DistrictId", district);
                cmd.Parameters.AddWithValue("@PIN",  !string.IsNullOrEmpty(PIN) ? PIN : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Active", Active);
                cmd.Parameters.AddWithValue("@IFSC", IFSC);
                cmd.Parameters.AddWithValue("@MICR", MICR == "" ? DBNull.Value : (object)MICR);
                cmd.Parameters.AddWithValue("@BankName", BankName == "" ? DBNull.Value : (object)BankName);
                cmd.Parameters.AddWithValue("@BranchName", BranchName == "" ? DBNull.Value : (object)BranchName);
                cmd.Parameters.AddWithValue("@centerid", DBNull.Value);
                
				dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Msg = dr["Msg"].ToString();
                    }
                }
                dr.Close();

                if (EffectedGL !=null)
                {
                    if (EffectedGL.Count > 0)
                    {
                        cmd = new SqlCommand("account.Delete_OtherSubLedger", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AccCode", AccountID);
                        cmd.Parameters.AddWithValue("@ParentAccCode", Parent);
                        cmd.ExecuteNonQuery();

                        foreach (var list in EffectedGL)
                        {
                            cmd = new SqlCommand("account.Insert_OtherSubLedger", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@AccCode", list.AccountCode);
                            cmd.Parameters.AddWithValue("@ParentAccCode", list.ParentAccountCode);
                            cmd.Parameters.AddWithValue("@SectorID", SectorID);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
			return Msg;
        }

        public object getAccConfig(int? SectorID) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GetAccConfigBySector";
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                //return convertAcHead(dt);
                var lst = dt.AsEnumerable().Select(t => new {
                    UnitType = t.Field<string>("UnitType"),
                    SectorName=t.Field<string>("SectorName")
                });
                return lst;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        public DataTable Get_AccountRule(string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AccountRule", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Account_MD> Account_Description(string Desc, string MND)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AccountDescMND", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDesc", Desc);
            cmd.Parameters.AddWithValue("@MND", MND);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string DeleteAccountCode(string AccCode, string SectorID)
        {
            string Result = "";
            SqlCommand cmd = new SqlCommand("account.Delete_AccountCode", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccCode", AccCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                    Result = dt.Rows[0]["Result"].ToString();
                    
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return Result;
        }
    }
}