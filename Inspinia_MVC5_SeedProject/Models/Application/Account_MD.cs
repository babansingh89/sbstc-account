﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Account_MD
    {
        public string AccountCode { get; set; }
        public string AccountDescription { get; set; }
        public string ParentAccountCode { get; set; }
    }
}