﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5_SeedProject.Models
{
    public class Services_MD
    {
        public string AccountCode { get; set; }
        public string DRCR { get; set; }
        public string AccountDescription { get; set; }
        public int? WayBillID { get; set; }
        public string WayBillNo { get; set; }
        public string WayBillDate { get; set; }
        public string AccountGroup { get; set; }
        public decimal TicketFare { get; set; }
        public int VoucherTypeID { get; set; }
        public int SectorID { get; set; }
        public int CenterId { get; set; }
        public int InsertedBy { get; set; }
    }
    public class Data<T>
    {
        public T detail { get; set; }
        public Data()
        {

        }

    }

    public class Missleanous_MD
    {
        public string From { get; set; }
        public string To { get; set; }
        public string RepNo { get; set; }
    }

    public class PaperTicket_MD
    {
        public string WayBillID { get; set; }
        public string WayBillNo { get; set; }
        public string WayBillDate { get; set; }
        public string IssueType { get; set; }
        public string ConductorID { get; set; }
        public string Remarks { get; set; }
        public string SectorID { get; set; }
        public string FinYear { get; set; }
        public string UserID { get; set; }
       
        public List<PaperTicketDetail_MD> PDetail { get; set; }
        public PaperTicket_MD()
        {
            PDetail = new List<PaperTicketDetail_MD>();
        }
    }
    public class PaperTicketDetail_MD
    {
        public string TransDt { get; set; }
        public string DenominationId { get; set; }
        public string DenominationRemarks { get; set; }
        public string UPDN { get; set; }
        public string TktSrlFrom  { get; set; }
        public string TktSrlTo { get; set; }
        public string TktSeries { get; set; }
        public string IssueType { get; set; }
        public string SectorId { get; set; }
        public string TransType { get; set; }
    }

    public class LedgerClosingBal_MD
    {
        public string SectorId { get; set; }
        public string CenterId { get; set; }
        public string LCode { get; set; }
        public string MCompID { get; set; }
        public string EffDate { get; set; }
    }
}