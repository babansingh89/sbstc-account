﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class StockIssue_MD
    {
        public long? IssueId { get; set; }
        public string IssueNo { get; set; }
        public string Issuedt { get; set; }
        public int? StockPointIdFrom { get; set; }
        public int? SectionIdFrom { get; set; }
        public int? StockPointIdTo { get; set; }
        public int? SectionIdTo { get; set; }
        public string IssueStatus { get; set; }
        public string Remarks { get; set; }
        public int? SectorId { get; set; }

        public List<StockIssueSub_MD> SubDetail { get; set; }
        public StockIssue_MD()
        {
            SubDetail = new List<StockIssueSub_MD>();
        }
    }

    public class StockIssueSub_MD
    {
        public long? DatIssueId { get; set; }
        public long? IssueId { get; set; }
        public string ItemId { get; set; }
        public int? Uom { get; set; }
        public decimal? qty { get; set; }
        public decimal? ActualQty { get; set; }
        public string ItemStatus { get; set; }
        public string Remarks { get; set; }
        public int? SectorId { get; set; }
        
    }
}