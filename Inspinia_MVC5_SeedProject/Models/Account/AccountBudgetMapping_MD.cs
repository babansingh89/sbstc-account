﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class AccountBudgetMapping_MD
    {
        public int? TabID { get; set; }
        public string BDesc { get; set; }
        public List<AccountBudgetMapping_MD> detail { get; set; }
        public List<AccountBudgetMapping_MD> nodes { get; set; }

        public AccountBudgetMapping_MD()
        {
            detail = new List<AccountBudgetMapping_MD>();
            nodes = new List<AccountBudgetMapping_MD>();
        }
    }
}