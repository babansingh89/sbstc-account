﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class Item_MD
    {

        public int? UnitId { get; set; }
        public string UnitDesc { get; set; }

        public int? ItemCatId { get; set; }
        public string CatDesc { get; set; }

        public int? TaxTypeId { get; set; }
        public string TaxTypeDesc { get; set; }

        public string AccountCode { get; set; }
        public int? AccountTypeID { get; set; }
        public string AccountType { get; set; }
        public string AccountDescription { get; set; }
        public string OldAccountHead { get; set; }
        public string ParentAccountCode { get; set; }
        public string PAccountDescription { get; set; }
        public string HsnCode { get; set; }
        public decimal? OpBalance { get; set; }
        public decimal? OpbalQty { get; set; }
        public string OpbalAsOn { get; set; }
        public decimal? LowStkAlert { get; set; }
        public string PurAccCode { get; set; }
        public string PurAccountDescription { get; set; }
        public string SaleAccCode { get; set; }
        public string SaleAccountDescription { get; set; }
        public string PrefVendorCode { get; set; }
        public string PrefAccountDescription { get; set; }
        public string DocFilePath { get; set; }
        public string ActiveFlag { get; set; }
        public string ItemType { get; set; }
        public string TransType { get; set; }
        public string TransDesc { get; set; }
        public string DefaultValue { get; set; }
        public int? Sectorid { get; set; }
        public List<ItemCatDetail_MD> CatDetail { get; set; }
        public Item_MD()
        {
            CatDetail = new List<ItemCatDetail_MD>();
        }

    }

    public class ItemCatDetail_MD
    {
        public int? CategoryDetailID { get; set; }
        public int? CategoryID { get; set; }
        public string AccountCode { get; set; }
        public string CatDesc { get; set; }
    }
}