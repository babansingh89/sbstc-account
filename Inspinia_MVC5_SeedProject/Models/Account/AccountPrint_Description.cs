﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class AccountPrint_Description
    {
        public AccountPrint_Description()
        {
            Children = new List<AccountPrint_Description>();
            nodes = new List<AccountPrint_Description>();
        }
        public long? AccountCode { get; set; }
        public string AccountDescription { get; set; }
        public string text { get; set; }
        public long? ParentAccountCode { get; set; }
        public List<AccountPrint_Description> Children { get; set; }
        public List<AccountPrint_Description> nodes { get; set; }
        public string GroupLedger { get; set; }
        public string SubLedger { get; set; }
    }
}