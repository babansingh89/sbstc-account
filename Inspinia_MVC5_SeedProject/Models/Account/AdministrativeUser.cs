﻿using wbEcsc.Models.Application;

namespace wbEcsc.Models.Account
{
    public class AdministrativeUser : InternalUser
    {
        public string UserName
        {
            get;
            set;
        }
        
    }
}
