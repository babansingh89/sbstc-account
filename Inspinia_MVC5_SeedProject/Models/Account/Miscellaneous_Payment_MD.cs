﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class Miscellaneous_Payment_MD
    {
        public int ReimbursementTypeID { get; set; }
        public string ReimbursementType { get; set; }
        public int? TempleteID { get; set; }
    }
    public class Miscellaneous_Payment_EmpNoName_MD
    {
        public int EmployeeID { get; set; }
        public string EmpName { get; set; }
        public string Category { get; set; }
        public string Status { get; set; }
    }
}