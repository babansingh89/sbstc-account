﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.Models.AppResource
{
    public class InOut
    {
        public bool IsAuthenticated { get; set; }
        public IInternalUser User { get; set; }
        public string LogoutUrl { get; set; }
        public string LandingPageUrl { get; set; }
        public string LoginUrl { get; set; }
        public string LoginUIUrl { get; set; }
        public string RedirectTo { get; set; }
    }
}