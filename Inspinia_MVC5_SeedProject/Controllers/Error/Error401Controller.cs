﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Error
{
    [SecuredFilter]
    public class Error401Controller : Controller
    {
        // GET: Error401
        public ActionResult Index()
        {
            return View("~/Views/Error/Error401/Index.cshtml");
        }
    }
}