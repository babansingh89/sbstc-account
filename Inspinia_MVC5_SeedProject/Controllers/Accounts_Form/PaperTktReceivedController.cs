﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class PaperTktReceivedController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public PaperTktReceivedController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/PaperTktReceived.cshtml");
        }

        [HttpPost]
        public ActionResult Get_AllData(string TransType, string SectorID, string IssueId)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTktReceive_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@IssueId", IssueId);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Save(string TransType, string Remarks, string RecDate, string IssueID)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.PaperTktReceive_SP", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", TransType);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@IssueDate", RecDate);
                    cmd.Parameters.AddWithValue("@IssueId", IssueID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }
}