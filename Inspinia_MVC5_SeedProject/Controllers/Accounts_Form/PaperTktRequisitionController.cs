﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Content
{
    public class PaperTktRequisitionController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public PaperTktRequisitionController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/PaperTktRequisition.cshtml");
        }

        [HttpPost]
        public ActionResult Save(string RequisitionID, string RequisitionDate, string SectorTo, string Remarks, string SectorID, string FinYear, List<ReqDetail> ReqDetail)
        {
            string ReqID = "", RequisitionNo="";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.PaperTktRequistion_SP", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", (RequisitionID == "" || RequisitionID == null) ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@RequisitionID", RequisitionID == "" ? DBNull.Value : (object)RequisitionID);
                    cmd.Parameters.AddWithValue("@RequisitionDate", RequisitionDate == "" ? DBNull.Value : (object)RequisitionDate);
                    cmd.Parameters.AddWithValue("@SectorTo", SectorTo == "" ? DBNull.Value : (object)SectorTo);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks == "" ? DBNull.Value : (object)Remarks);
                    cmd.Parameters.AddWithValue("@FinYear", FinYear == "" ? DBNull.Value : (object)FinYear);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        ReqID = dr["RequisitionID"].ToString();
                        RequisitionNo = dr["RequisitionNo"].ToString();
                    }
                    dr.Close();

                    if (ReqDetail != null)
                    {
                        cmd = new SqlCommand("account.PaperTktRequistion_SP", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "Delete");
                        cmd.Parameters.AddWithValue("@RequisitionID", ReqID);
                        cmd.ExecuteNonQuery();

                        foreach (var list in ReqDetail)
                        {
                            cmd = new SqlCommand("account.PaperTktRequistion_SP", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert_Detail");
                            cmd.Parameters.AddWithValue("@RequisitionID", ReqID);
                            cmd.Parameters.AddWithValue("@DenominationID", list.DenominationID == "" ? DBNull.Value : (object)list.DenominationID); 
                            cmd.Parameters.AddWithValue("@Qty", list.Qty == "" ? DBNull.Value : (object)list.Qty);
                            cmd.Parameters.AddWithValue("@UpDown", list.UpDown == "" ? DBNull.Value : (object)list.UpDown);
                            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    cr.Data = RequisitionNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", RequisitionNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Get_Requisition(string TransType, string Requisition, string SectorID, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTktRequistion_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@Requisition", Requisition == "" ? DBNull.Value : (object)Requisition);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }


    }

    public class ReqDetail
    {
        public string DenominationID { get; set; }
        public string Denomination { get; set; }
        public string Qty { get; set; }
        public string UpDown { get; set; }
    }
}