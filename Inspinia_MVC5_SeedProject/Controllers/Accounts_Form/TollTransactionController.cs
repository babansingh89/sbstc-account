﻿using System;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using System.Configuration;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using System.Data.SqlClient;
using System.Linq;
using wbEcsc.App_Codes;
using static System.Net.Mime.MediaTypeNames;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class TollTransactionController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        string date = ""; string a = "";
        SessionData sData; string checking = "";
        DataTable dtCloned = new DataTable();
        DataTable dtTransaction = new DataTable();

        public TollTransactionController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            if (!SessionContext.IsAuthenticated)
            {
                return new RedirectResult("/Error/Error401/Index");
            }

            return View("~/Views/Accounts/TollTransaction.cshtml");
        }

        [HttpPost]
        public ActionResult UploadPicture()
        {
            string fname = "", fileWithEXT = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", "ChkPosting");
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                if(dt.Rows.Count >0)
                {
                    int AccPost = Convert.ToInt32(dt.Rows[0]["AccPost"].ToString());
                    int RowCount = Convert.ToInt32(dt.Rows[0]["RowCount"].ToString());

                    if (RowCount == 0 || AccPost == 0)
                    {
                        if (Request.Files.Count > 0)
                        {
                            try
                            {
                                HttpFileCollectionBase files = Request.Files;
                                for (int i = 0; i < files.Count; i++)
                                {
                                    HttpPostedFileBase file = files[i];

                                    // Checking for Internet Explorer  
                                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                    {
                                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                        fname = testfiles[testfiles.Length - 1];
                                    }
                                    else
                                    {
                                        fname = file.FileName;
                                    }

                                    string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                                    fileWithEXT = fname;

                                    //deleting code starts here
                                    string dfname = Path.Combine(Server.MapPath("~/TollTransaction/"));
                                    string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                                    foreach (string f in dfiles)
                                    {
                                        System.IO.File.Delete(f);
                                    }

                                    fname = Path.Combine(Server.MapPath("~/TollTransaction/"), fname);
                                    file.SaveAs(fname);
                                }
                                cr.Data = fileWithEXT.ToString();
                            }
                            catch (Exception ex)
                            {
                                cr.Data = 0;
                            }
                        }
                    }
                    else
                    {
                        cr.Data = 1;
                    }
                }

                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = 0;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Checking(string FileName)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open(); 
            transaction = dbConn.BeginTransaction();
            try
            {
                string pathToFiles = HttpContext.Server.MapPath("~/TollTransaction/" + FileName);

                XSSFWorkbook hssfwb;
                string sheetName = "";
                ISheet sheet = null;

                using (var fs = new FileStream(pathToFiles, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(fs);
                    sheetName = hssfwb.GetSheetAt(0).SheetName;
                    sheet = (XSSFSheet)hssfwb.GetSheet(sheetName);
                }
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sheet.GetRow(0);
                int colCount = headerRow.LastCellNum;

                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(headerRow.GetCell(c).ToString());

                var i = 1; Decimal Total = 0;
                var currentRow = sheet.GetRow(i);

                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    for (var j = 0; j < currentRow.Cells.Count; j++)
                    {
                        var cell = currentRow.GetCell(j);
                        if (cell != null)
                        {
                            switch (cell.CellType)
                            {
                                case CellType.Numeric:
                                    dr[j] = DateUtil.IsCellDateFormatted(cell)
                                        ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture)
                                        : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                    break;
                                case CellType.String:
                                    a = cell.StringCellValue;
                                    if (a != "")
                                        dr[j] = cell.StringCellValue;

                                    break;
                                case CellType.Blank:
                                    dr[j] = string.Empty;
                                    break;

                            }
                        }
                        else
                            break;
                    }
                    string trid = dr["TRANSACTIONID"].ToString();
                    decimal fTotal = Convert.ToDecimal((dr["TRANSAMOUNT"].ToString() == "" || dr["TRANSAMOUNT"].ToString() == null) ? 0 : (object)dr["TRANSAMOUNT"].ToString());
                    if (trid != "")
                        dtExcelTable.Rows.Add(dr);

                    Total = Total + fTotal;
                    i++;
                    currentRow = sheet.GetRow(i);
                }
                dt = dtExcelTable;

                if (dt.Rows.Count > 0)
                {
                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("VEHICLENUMBER", typeof(string));
                    Detail.Columns.Add("TAGID", typeof(string));
                    Detail.Columns.Add("BAR_CODE", typeof(string));
                    Detail.Columns.Add("TRANSACTIONID", typeof(string));
                    Detail.Columns.Add("TOLLREADERTIME", typeof(string));
                    Detail.Columns.Add("TRANSACTIONTIME", typeof(string));
                    Detail.Columns.Add("TOLLPLAZAID", typeof(string));
                    Detail.Columns.Add("TOLLPLAZANAME", typeof(string));
                    Detail.Columns.Add("TRANSAMOUNT", typeof(string));
                    Detail.Columns.Add("ACQ_ID", typeof(string));
                    Detail.Columns.Add("REMARKS", typeof(string));
                    Detail.Columns.Add("Decline", typeof(string));
                    Detail.Columns.Add("FileName", typeof(string));

                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow drr = Detail.NewRow();
                        drr["VEHICLENUMBER"] = row["VEHICLENUMBER"].ToString();
                        drr["TAGID"] = row["TAGID"].ToString();
                        drr["BAR_CODE"] = row["BAR_CODE"].ToString();
                        drr["TRANSACTIONID"] = row["TRANSACTIONID"].ToString();

                        CultureInfo culture = new CultureInfo("en-US");
                        DateTime tempDate = Convert.ToDateTime(row["TOLLREADERTIME"].ToString(), culture);

                        drr["TOLLREADERTIME"] = tempDate.ToString("dd/MM/yyyy");
                        drr["TRANSACTIONTIME"] = row["TRANSACTIONTIME"].ToString();
                        drr["TOLLPLAZAID"] = row["TOLLPLAZAID"].ToString();
                        drr["TOLLPLAZANAME"] = row["TOLLPLAZANAME"].ToString();
                        drr["TRANSAMOUNT"] = row["TRANSAMOUNT"].ToString();
                        drr["ACQ_ID"] = row["ACQ_ID"].ToString();
                        drr["REMARKS"] = row["REMARKS"].ToString();
                        drr["Decline"] = "N";
                        drr["FileName"] = FileName;
                        Detail.Rows.Add(drr);
                    }
                    if (Detail.Rows.Count > 0)
                    {
                        cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", dbConn, transaction);
                        cmd.Parameters.AddWithValue("@TransType", "Dat_Insert");
                        cmd.Parameters.AddWithValue("@TollDetail", Detail);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtexist = new DataTable();

                        SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        da1.Fill(dt1);
                    }

                    transaction.Commit();

                    cr.Data = dt1;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = "upload";
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message + "-" + a;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Save(string fileName, string SectorID)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open(); string Status = "";
            transaction = dbConn.BeginTransaction();
            SqlDataReader dar;
            try
            {
                string pathToFiles = HttpContext.Server.MapPath("~/TollTransaction/" + fileName);

                XSSFWorkbook hssfwb;
                string sheetName = "";
                ISheet sheet = null;

                using (var fs = new FileStream(pathToFiles, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(fs);
                    sheetName = hssfwb.GetSheetAt(0).SheetName;
                    sheet = (XSSFSheet)hssfwb.GetSheet(sheetName);
                }
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sheet.GetRow(0);
                int colCount = headerRow.LastCellNum;

                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(headerRow.GetCell(c).ToString());

                var i = 1; Decimal Total = 0;
                var currentRow = sheet.GetRow(i);

                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    for (var j = 0; j < currentRow.Cells.Count; j++)
                    {
                        var cell = currentRow.GetCell(j);
                        if (cell != null)
                        {
                            switch (cell.CellType)
                            {
                                case CellType.Numeric:
                                    dr[j] = DateUtil.IsCellDateFormatted(cell)
                                        ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture)
                                        : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                    break;
                                case CellType.String:
                                    a = cell.StringCellValue;
                                    if (a != "")
                                        dr[j] = cell.StringCellValue;

                                    break;
                                case CellType.Blank:
                                    dr[j] = string.Empty;
                                    break;

                            }
                        }
                        else
                            break;
                    }
                    string trid = dr["TRANSACTIONID"].ToString();
                    decimal fTotal = Convert.ToDecimal((dr["TRANSAMOUNT"].ToString() == "" || dr["TRANSAMOUNT"].ToString() == null) ? 0 : (object)dr["TRANSAMOUNT"].ToString());
                    if (trid != "")
                    dtExcelTable.Rows.Add(dr);

                    Total = Total + fTotal;
                    i++;
                    currentRow = sheet.GetRow(i);
                }
                dt  = dtExcelTable;

                if (dt.Rows.Count > 0)
                {
                    DataView view = new DataView(dt);
                    DataTable distinctValues = view.ToTable(true, "TRANSACTIONTIME");

                    dtCloned = distinctValues.Clone();
                    dtCloned.Columns[0].DataType = typeof(string);

                    foreach (DataRow row in distinctValues.Rows)
                    {
                        a = row["TRANSACTIONTIME"].ToString();
                        if (a != "")
                        {
                            string sub = a.Substring(0, 10);
                            var c = sub.Split('-');
                            date = c[0] + "/" + c[1] + "/" + c[2];
                            DataRow drr = dtCloned.NewRow();
                            drr[0] = date;
                            dtCloned.Rows.Add(drr);
                        }
                    }

                    DataView newview = new DataView(dtCloned);
                    DataTable dtdistinct = newview.ToTable(true, "TRANSACTIONTIME");

                    string commaSeparatedString = String.Join(",", dtdistinct.AsEnumerable().Select(x => x.Field<string>("TRANSACTIONTIME").ToString()).ToArray());

                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("VEHICLENUMBER", typeof(string));
                    Detail.Columns.Add("TAGID", typeof(string));
                    Detail.Columns.Add("BAR_CODE", typeof(string));
                    Detail.Columns.Add("TRANSACTIONID", typeof(string));
                    Detail.Columns.Add("TOLLREADERTIME", typeof(string));
                    Detail.Columns.Add("TRANSACTIONTIME", typeof(string));
                    Detail.Columns.Add("TOLLPLAZAID", typeof(string));
                    Detail.Columns.Add("TOLLPLAZANAME", typeof(string));
                    Detail.Columns.Add("TRANSAMOUNT", typeof(string));
                    Detail.Columns.Add("ACQ_ID", typeof(string));
                    Detail.Columns.Add("REMARKS", typeof(string));

                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow drr = Detail.NewRow();
                        drr["VEHICLENUMBER"] = row["VEHICLENUMBER"].ToString();
                        drr["TAGID"] = row["TAGID"].ToString();
                        drr["BAR_CODE"] = row["BAR_CODE"].ToString();
                        drr["TRANSACTIONID"] = row["TRANSACTIONID"].ToString();
                        drr["TOLLREADERTIME"] = row["TOLLREADERTIME"].ToString();
                        drr["TRANSACTIONTIME"] = row["TRANSACTIONTIME"].ToString();
                        drr["TOLLPLAZAID"] = row["TOLLPLAZAID"].ToString();
                        drr["TOLLPLAZANAME"] = row["TOLLPLAZANAME"].ToString();
                        drr["TRANSAMOUNT"] = row["TRANSAMOUNT"].ToString();
                        drr["ACQ_ID"] = row["ACQ_ID"].ToString();
                        drr["REMARKS"] = row["REMARKS"].ToString();
                        Detail.Rows.Add(drr);
                    }
                    if (Detail.Rows.Count > 0)
                    {
                        cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", dbConn, transaction);
                        cmd.Parameters.AddWithValue("@TransType", "Dat_Insert");
                        cmd.Parameters.AddWithValue("@TRANSACTIONTIMES", commaSeparatedString);
                        cmd.Parameters.AddWithValue("@TollDetail", Detail);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtexist = new DataTable();

                        SqlDataAdapter da1 = new SqlDataAdapter(cmd);

                        da1.Fill(dt1);
                    }


                    transaction.Commit();

                    cr.Data = dt1;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = "upload";
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message + "-" + a;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult TollDetail(List<TollDetail_MD> TollDetail)
        {
            DataTable dt = new DataTable();
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open(); 
            transaction = dbConn.BeginTransaction();
            try
            {
                if (TollDetail != null)
                {
                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("VehicleNo", typeof(string));
                    Detail.Columns.Add("TabId", typeof(string));
                    Detail.Columns.Add("TransactionID", typeof(string));
                    Detail.Columns.Add("Decline", typeof(string));
                    Detail.Columns.Add("ID", typeof(string));

                    foreach (var list in TollDetail)
                    {
                        DataRow drr = Detail.NewRow();
                        drr["VehicleNo"] = list.VehicleNo.ToString();
                        drr["TabId"] = list.TabId.ToString();
                        drr["TransactionID"] = list.TransactionID.ToString();
                        drr["Decline"] = list.Decline.ToString();
                        drr["ID"] = list.ID.ToString();
                        Detail.Rows.Add(drr);
                    }

                    if (Detail.Rows.Count > 0)
                    {
                        cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", dbConn, transaction);
                        cmd.Parameters.AddWithValue("@TransType", "Dat_NewInsert");
                        cmd.Parameters.AddWithValue("@TollFDetail", Detail);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult DeleteFile(string FileName, string ID)
        {
            DataTable dt = new DataTable();
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            try
            {
                cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", dbConn, transaction);
                cmd.Parameters.AddWithValue("@TransType", "DeleteFile");
                cmd.Parameters.AddWithValue("@FileName", FileName);
                cmd.Parameters.AddWithValue("@MAXID", ID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                transaction.Commit();
                cr.Data = "success";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string Decline, string ID)
        {
            decimal Total = 0; string Position = ""; string SumFieldName = "";
            try {
                if(Decline == "")
                {
                    string pathToFiles = HttpContext.Server.MapPath("~/TollTransaction/Sample.xlsx");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + "Sample.xlsx");
                    Response.WriteFile(pathToFiles);
                    Response.End();
                }
                else
                {
                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
                    cmd.Parameters.AddWithValue("@TransType", "Show");
                    cmd.Parameters.AddWithValue("@MAXID", ID);
                    cmd.Parameters.AddWithValue("@Status", Decline);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            string fileName = "";
                            if (Decline == "L")
                                fileName = "All.xls";
                            else if(Decline == "Y")
                                fileName = "Desputed.xls";
                            else if (Decline == "N")
                                fileName = "UnDesputed.xls";

                            string saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult Load(string TransType, string SectorID, string TransDate, string Type)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorId", (SectorID == null || SectorID == "null" || SectorID =="") ? DBNull.Value : (object)SectorID);
            cmd.Parameters.AddWithValue("@Status", Type);
            cmd.Parameters.AddWithValue("@TRANSACTIONTIMES", TransDate);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Post(string TransType, string SectorID, string TransDate, string FileName, string ID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@TRANSACTIONTIMES", TransDate);
            cmd.Parameters.AddWithValue("@FileName", FileName);
            cmd.Parameters.AddWithValue("@MAXID", ID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ViewRecord(string TransType, string ID, string Decline)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@MAXID", ID);
            cmd.Parameters.AddWithValue("@Status", Decline);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Decline(string TransType, string TabID, string VehicleNo, string chkVeh, string Decline)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@TabID", TabID);
            cmd.Parameters.AddWithValue("@VEHICLENUMBER", VehicleNo);
            cmd.Parameters.AddWithValue("@Status", Decline);
            cmd.Parameters.AddWithValue("@chkVeh", chkVeh);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MissingDate(string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_FastagMissingDate", con);
            cmd.Parameters.AddWithValue("@finyear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveMissingDate(string TransType, string Sattledate, string Remarks)
        {
            string Result = "";
            SqlDataReader dr;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@TransactionTime", Sattledate);
            cmd.Parameters.AddWithValue("@REMARKS", Remarks);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Result = dr["Result"].ToString();
            }
            dr.Close();

            try
            {
                cr.Data = Result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", Result);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeclinedTrans(string TransType, string FromDate, string ToDate, string VehicleNo, string TollID, string WayBillID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@fdate", FromDate == "" ? DBNull.Value : (object)FromDate);
            cmd.Parameters.AddWithValue("@tdate", ToDate == "" ? DBNull.Value : (object)ToDate);
            cmd.Parameters.AddWithValue("@VEHICLENUMBER", VehicleNo == "" ? DBNull.Value : (object)VehicleNo);
            cmd.Parameters.AddWithValue("@TabID", TollID == "" ? DBNull.Value : (object)TollID);
            cmd.Parameters.AddWithValue("@MAXID", WayBillID == "" ? DBNull.Value : (object)WayBillID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DisputedExcel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Proc_Gen_SectorwiseFasAmt", con);
                    cmd.Parameters.AddWithValue("@TransType", master[0]["TransType"].ToString());
                    cmd.Parameters.AddWithValue("@fdate", master[0]["FromDate"].ToString() == "" ? DBNull.Value : (object)master[0]["FromDate"].ToString());
                    cmd.Parameters.AddWithValue("@tdate", master[0]["ToDate"].ToString() == "" ? DBNull.Value : (object)master[0]["ToDate"].ToString());
                    cmd.Parameters.AddWithValue("@VEHICLENUMBER", master[0]["VehicleNo"].ToString() == "" ? DBNull.Value : (object)master[0]["VehicleNo"].ToString());
                    cmd.Parameters.AddWithValue("@TabID", master[0]["TollID"].ToString() == "" ? DBNull.Value : (object)master[0]["TollID"].ToString());
                    cmd.Parameters.AddWithValue("@MAXID", master[0]["WayBillID"].ToString() == "" ? DBNull.Value : (object)master[0]["WayBillID"].ToString());
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);

                    decimal Total = 0; string Position = ""; string SumFieldName = "";
                    string fileName = master[0]["FileName"].ToString().Trim() + "_" + sData.UserID + ".xls";// + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls";
                    //string fileName = master[0]["FileName"].ToString() + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls";
                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        string saveAsFileName = "";
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }

        public class TollDetail_MD
        {
            public long? TabId { get; set; }
            public string VehicleNo { get; set; }
            public string TransactionID { get; set; }
            public string Decline { get; set; }
            public string ID { get; set; }
        }
    }
}