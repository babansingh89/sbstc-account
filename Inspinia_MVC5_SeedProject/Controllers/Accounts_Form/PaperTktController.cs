﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class PaperTktController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        // GET: PaperTkt
        public PaperTktController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            
                return View("~/Views/Accounts/PaperTkt.cshtml");

        }

        [HttpPost]
        public ActionResult Get_Denomination(string TransType, string Denomination)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTkt_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@Denomination", Denomination == "" ? DBNull.Value : (object)Denomination);
            cmd.Parameters.AddWithValue("@SectorId", sData.CurrSector);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_AllData(string TransType, string TicketID, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTkt_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            //cmd.Parameters.AddWithValue("@Denomination", Denomination == "" ? DBNull.Value : (object)Denomination);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@TicketID", TicketID == "" ? DBNull.Value : (object)TicketID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Final_Save(string TicketID, string DenominationID, string Series, string TicketFrom, string TicketTo, 
            string Remarks, string TransType, string TransDate, string FinYear, string SectorID, string UPDN)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTkt_SP", con);
            cmd.Parameters.AddWithValue("@TransType", "Insert");
            cmd.Parameters.AddWithValue("@TicketID", TicketID == "" ? DBNull.Value : (object)TicketID);
            cmd.Parameters.AddWithValue("@DenominationID", DenominationID == "" ? DBNull.Value : (object)DenominationID);
            cmd.Parameters.AddWithValue("@Series", Series == "" ? DBNull.Value : (object)Series);
            cmd.Parameters.AddWithValue("@TicketFrom", TicketFrom == "" ? DBNull.Value : (object)TicketFrom);
            cmd.Parameters.AddWithValue("@TicketTo", TicketTo == "" ? DBNull.Value : (object)TicketTo);
            cmd.Parameters.AddWithValue("@Remarks", Remarks == "" ? DBNull.Value : (object)Remarks);
            cmd.Parameters.AddWithValue("@TranType", TransType == "" ? DBNull.Value : (object)TransType);
            cmd.Parameters.AddWithValue("@TransDate", TransDate == "" ? DBNull.Value : (object)TransDate);
            cmd.Parameters.AddWithValue("@UPDN", UPDN == "" ? DBNull.Value : (object)UPDN);
            cmd.Parameters.AddWithValue("@FinYear", FinYear == "" ? DBNull.Value : (object)FinYear);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }






    }//ALL END
}