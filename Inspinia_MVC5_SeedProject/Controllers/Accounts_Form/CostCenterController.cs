﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Accounts_Form
{
   // [SecuredFilter]
    public class CostCenterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        CostCenterBLL csBll = new CostCenterBLL();
        SessionData sData;
        // GET: CostCenter
        public CostCenterController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/CostCenter.cshtml");
        }

        [HttpPost]
        public JsonResult GetVoucherByTypeDate(CostCenter CostCenterObj) {
            try
            {
                CostCenterObj.SectorID = sData.CurrSector;
                List<CostCenter> lst = csBll.GetVoucherByTypeDate(CostCenterObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getVoucherByID(CostCenter CostCenterObj) {
            try
            {
                CostCenterObj.SectorID = sData.CurrSector;
                List<CostCenter> lst = csBll.getVoucherByID(CostCenterObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetCostCenter()
        {
            try
            {
                var x = csBll.GetCostCenter();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Getdept()
        {
            try
            {
                var x = csBll.GetDept();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetCostCenterList(CostCenter CostCenterObj) {
            try
            {
                CostCenterObj.SectorID = sData.CurrSector;
                List<CostCenter> lst = csBll.GetCostCenterList(CostCenterObj);

                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult InsUpdCostCenterdetails(CostCenter CostCenterObj)
        {
            try
            {
                CostCenterObj.SectorID = sData.CurrSector;
                CostCenterObj.InsertedBy = sData.UserID;

                csBll.InsUpdCostCenterdetails(CostCenterObj);

                cr.Data = true;
                if (CostCenterObj.BillId != 0)
                    cr.Message = "Cost Center Updated Successfully !!";
                else
                    cr.Message = "Cost Center Inserted Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

    }
}