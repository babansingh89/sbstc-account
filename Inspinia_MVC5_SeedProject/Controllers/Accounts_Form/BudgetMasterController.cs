﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BudgetMasterController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;


        public BudgetMasterController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            if (!SessionContext.IsAuthenticated)
            {
                //Response.Redirect("/Error/Error401/Index", true);
                return new RedirectResult("/Error/Error401/Index");
            }

            ViewBag.FinancialLimit = sData.FinancialLimit;
            return View("~/Views/Accounts/BudgetMaster.cshtml");
        }

        [HttpPost]
        public ActionResult Get_Particulars(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                var lst = dt.AsEnumerable().Select(t => new
                {
                    BId = t.Field<long?>("BId"),
                    BudgetID = t.Field<int?>("BudgetID"),
                    BudgetDescription = t.Field<string>("BudgetDescription"),
                    BudgetCode = t.Field<decimal?>("BudgetCode"),
                    BudgetAmount = t.Field<decimal?>("BudgetAmount"),
                }).ToList();


                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_ConParticulars(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                var lst = dt.AsEnumerable().Select(t => new
                {
                    BudgetID = t.Field<int>("BudgetID"),
                    BudgetDescription = t.Field<string>("BudgetDescription"),
                    BudgetCode = t.Field<string>("BudgetCode"),
                    BudgetAmount = t.Field<decimal>("BudgetAmount"),
                    ConcurranceAmount = t.Field<decimal>("ConcurranceAmount"),
                    BalanceAmount = t.Field<decimal>("BalanceAmount"),
                }).ToList();


                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_BudgetDetails(string TrasactionType, string BudgetID, string SectorID, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_HeadNameAuto(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                var lst = dt.AsEnumerable().Select(t => new
                {
                    BudgetID = t.Field<int?>("BudgetID"),
                    BudgetDescription = t.Field<string>("BudgetDescription"),
                }).ToList();


                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Bind_Sector()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", "Sector");
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertUpdate(BudgetMaster_MD Master)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string Result = "";
            try
            {
                try
                {
                    DataTable Detail = new DataTable();
                    Detail.Columns.Add("BId", typeof(int));
                    Detail.Columns.Add("BAmt", typeof(decimal));
                    Detail.Columns.Add("EffDate", typeof(string));
                    Detail.Columns.Add("Remarks", typeof(string));
                    int k = 0;
                    if (Master.Budgetdetail != null)
                    {
                        foreach (var MD in Master.Budgetdetail)
                        {
                            DataRow drr = Detail.NewRow();
                            drr["BId"] = MD.BudgetID;
                            drr["BAmt"] = MD.BudgetAmount;
                            drr["EffDate"] = MD.EffectiveDate;
                            drr["Remarks"] = MD.Remarks;
                            Detail.Rows.Add(drr);
                        }
                    }
                    BudgetMaster_MD obj = new BudgetMaster_MD();
                    SqlConnection con = new SqlConnection(conString);
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", Master.BudgetID == null ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@BudgetID", Master.BudgetID);
                    cmd.Parameters.AddWithValue("@BudgetCode", Master.BudgetCode);
                    cmd.Parameters.AddWithValue("@BudgetAmount", Master.BudgetAmount);
                    cmd.Parameters.AddWithValue("@SectorID", Master.SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    cmd.Parameters.AddWithValue("@BudgetDetail", Detail);
                    cmd.Parameters.AddWithValue("@SFinYear", Master.FinYear);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Result = dr["Result"].ToString();
                        }
                    }
                    dr.Close();
                    transaction.Commit();
                    cr.Data = Result;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", Result);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult InsertUpdate_Concurrance(string BudgetID, string SectorID, string ConSlNo, string bugtAmt, string effDate, string refNo, string remarks, string FinYear)
        {
            string Code = "";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            DataTable dt = new DataTable();
            SqlDataReader dr;
            string Result = "";
            try
            {
                try
                {
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "FCLimit");
                    cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
                    cmd.Parameters.AddWithValue("@SFinYear", FinYear);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@ConcurranceSlNo", ConSlNo);
                    cmd.Parameters.AddWithValue("@ConcurranceAmount", bugtAmt);
                    cmd.Parameters.AddWithValue("@ConcurranceRefNo", refNo);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Code = dr["Code"].ToString();
                    }
                    dr.Close();

                    if (Code == "0")
                    {
                        //SqlConnection con = new SqlConnection(conString);
                        cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "ConInsert");
                        cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
                        cmd.Parameters.AddWithValue("@SectorID", SectorID);
                        cmd.Parameters.AddWithValue("@ConcurranceSlNo", ConSlNo);
                        cmd.Parameters.AddWithValue("@ConcurranceAmount", bugtAmt);
                        cmd.Parameters.AddWithValue("@ConcurranceDate", effDate);
                        cmd.Parameters.AddWithValue("@ConcurranceRefNo", refNo);
                        cmd.Parameters.AddWithValue("@ConcurranceRemarks", remarks);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        cmd.Parameters.AddWithValue("@SFinYear", FinYear);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);

                        //con.Open();
                        da.Fill(dt);
                       
                    }

                    transaction.Commit();
                    cr.Data = dt;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = Code;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Transfer_Concurrance(string BudgetID, string TransBudgetID, string SectorID, string CurrSectorID, string conSlNo,  string refNo, string FinYear)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string Result = "";
            try
            {
                try
                {
                    SqlConnection con = new SqlConnection(conString);
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "ConTransfer");
                    cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
                    cmd.Parameters.AddWithValue("@TransBudgetID", TransBudgetID);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@CurrSectorID", CurrSectorID);
                    cmd.Parameters.AddWithValue("@ConcurranceSlNo", conSlNo);
                    cmd.Parameters.AddWithValue("@ConcurranceRefNo", refNo);
                    cmd.Parameters.AddWithValue("@SFinYear", FinYear);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    da.Fill(dt);

                    transaction.Commit();
                    cr.Data = dt;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", Result);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Get_Dashboard(string TrasactionType, string BudgetTypeID, string CategoryID, string SubCategoryID, string SectorID, string Desc, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetTypeID);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_MemoFC(string TrasactionType, string BudgetType, string CategoryID, string BudgetID, string SectorID, string Desc, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BudgetType", BudgetType);
            cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
            cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@SFinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertUpdateHead(string BudgetType, string CategoryID, string SubCategoryID, string HeadName, string EffDate, string HeadAmount, 
            string Remarks, string TabID, string SectorID, string FinYear)
        {
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            string Result = ""; int RTabID=0;
            try
            {
                try
                {
                    SqlConnection con = new SqlConnection(conString);
                    cmd = new SqlCommand("account.BudgetMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", (TabID == null || TabID =="") ? "InsertHead" : "UpdateHead");
                    cmd.Parameters.AddWithValue("@BudgetID", TabID);
                    cmd.Parameters.AddWithValue("@BudgetType", BudgetType);
                    cmd.Parameters.AddWithValue("@BudgetCat", CategoryID);
                    cmd.Parameters.AddWithValue("@BudgetSubID", SubCategoryID);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    cmd.Parameters.AddWithValue("@Desc", HeadName);
                    cmd.Parameters.AddWithValue("@ConcurranceDate", EffDate);
                    cmd.Parameters.AddWithValue("@BudgetAmount", HeadAmount);
                    cmd.Parameters.AddWithValue("@ConcurranceRemarks", Remarks);
                    cmd.Parameters.AddWithValue("@SFinYear", FinYear);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            RTabID = Convert.ToInt32(dr["MaxBudgetID"].ToString());
                        }
                    }

                    dr.Close();
                    transaction.Commit();
                    cr.Data = RTabID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0}", Result);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult CopylastYeardata(string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_CopyBudgetFromLastYear", con);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = "success";
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateActualAmt(string TrasactionType, string BId, string BudgetID, string ActAmt)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BudgetMaster", con);
            cmd.Parameters.AddWithValue("@TransactionType", TrasactionType);
            cmd.Parameters.AddWithValue("@BIds", BId);
            cmd.Parameters.AddWithValue("@BudgetID", BudgetID);
            cmd.Parameters.AddWithValue("@ConcurranceAmount", ActAmt);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = "success";
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FinYear(string SectorID)
        {
            try
            {
                if (SectorID != "")
                {
                    sData.CurrSector = Convert.ToInt32(SectorID);
                    var lst = new EmployeeMaster().Get_SectorWiseFinYearBudget(Convert.ToInt32(SectorID));
                    cr.Data = lst;
                }
                else
                {
                    sData.CurrSector = null;
                    cr.Data = "";
                }
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Get_BudgetTypeReport", con);
                    cmd.Parameters.AddWithValue("@SectorID", master[0]["SectorID"].ToString());
                    cmd.Parameters.AddWithValue("@BudgetType", master[0]["BudgetType"].ToString());
                    cmd.Parameters.AddWithValue("@SFinYear", master[0]["SFinYear"].ToString());
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);

                    string fileName = master[0]["BTypeName"].ToString() + ".xls";
                    if (dt.Rows.Count > 0)
                    {
                        GridView gv = new GridView();
                        gv.DataSource = dt;
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename= '" + fileName);
                        Response.ContentType = "application/ms-excel";
                        Response.Charset = "";
                        StringWriter sw = new StringWriter();
                        HtmlTextWriter htw = new HtmlTextWriter(sw);
                        gv.RenderControl(htw);
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}