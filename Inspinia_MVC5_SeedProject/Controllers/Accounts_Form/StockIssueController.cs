﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.IO;
using wbEcsc.App_Start;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class StockIssueController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public StockIssueController()
        {
            if (SessionContext.IsAuthenticated)
            {
                sData = SessionContext.SessionData;
            }
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/StockIssue.cshtml");
        }

        [HttpPost]
        public JsonResult InsertUpdate(StockIssue_MD StockIssue, List<StockIssueSub_MD> Detail)
        {
            string IssueNo = "", IssueID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.StockIssue", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", (StockIssue.IssueId == 0 || StockIssue.IssueId == null) ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@IssueId", (StockIssue.IssueId == 0 || StockIssue.IssueId == null ) ? 0 : (object)StockIssue.IssueId);
                    cmd.Parameters.AddWithValue("@Issuedt", StockIssue.Issuedt == "" ? DBNull.Value : (object)StockIssue.Issuedt);
                    cmd.Parameters.AddWithValue("@StockPointIdFrom", StockIssue.StockPointIdFrom == 0 ? DBNull.Value : (object)StockIssue.StockPointIdFrom);
                    cmd.Parameters.AddWithValue("@StockPointIdTo", StockIssue.StockPointIdTo == 0 ? DBNull.Value : (object)StockIssue.StockPointIdTo);
                    cmd.Parameters.AddWithValue("@SectionIdFrom", StockIssue.SectionIdFrom == 0 ? DBNull.Value : (object)StockIssue.SectionIdFrom);
                    cmd.Parameters.AddWithValue("@SectionIdTo", StockIssue.SectionIdTo == 0 ? DBNull.Value : (object)StockIssue.SectionIdTo);
                    cmd.Parameters.AddWithValue("@Remarks", StockIssue.Remarks == "" ? DBNull.Value : (object)StockIssue.Remarks);
                    cmd.Parameters.AddWithValue("@SectorId", StockIssue.SectorId == 0 ? DBNull.Value : (object)StockIssue.SectorId);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        IssueID = dr["IssueID"].ToString();
                        IssueNo = dr["IssueNo"].ToString();
                    }
                    dr.Close();

                    if (IssueNo != "" || IssueNo != null)
                    {
                        if (Detail != null)
                        {
                            foreach (var list in Detail)
                            {
                                cmd = new SqlCommand("account.StockIssue", dbConn, transaction);
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransType", "Insert_detail");
                                cmd.Parameters.AddWithValue("@IssueID", IssueID);
                                cmd.Parameters.AddWithValue("@ItemId", (list.ItemId == "" || list.ItemId == null || list.ItemId == "null") ? DBNull.Value : (object)list.ItemId);
                                cmd.Parameters.AddWithValue("@Uom", list.Uom == 0 ? DBNull.Value : (object)list.Uom);
                                cmd.Parameters.AddWithValue("@qty", list.qty == 0 ? DBNull.Value : (object)list.qty);
                                cmd.Parameters.AddWithValue("@Remarks", list.Remarks == "" ? DBNull.Value : (object)list.Remarks);
                                cmd.Parameters.AddWithValue("@SectorId", list.SectorId == 0 ? DBNull.Value : (object)list.SectorId);
                                cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }


                    transaction.Commit();
                    cr.Data = IssueNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", IssueNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }
}