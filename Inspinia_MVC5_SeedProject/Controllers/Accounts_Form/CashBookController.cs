﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using Newtonsoft.Json.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using NPOI.HSSF.UserModel;
using System.Configuration;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class CashBookController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public CashBookController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/CashBook.cshtml");
        }

        [HttpPost]
        public ActionResult Get_Ledger_Details(string Desc, string FinYear, string SectorID)
        {
            try
            {
                DataTable lst = new GeneralLedger_Bll().Get_CashBookLedger_Details(Desc, FinYear, SectorID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }

            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Show_GeneralLedger(string AccountCode, string Desc, string FinYear, string SectorID, string FromDate, string ToDate, string LegerType, string unPaidVch, string ReportType)
        {
            try
            {
                DataTable lst = new GeneralLedger_Bll().Show_GeneralLedger(AccountCode, Desc, FinYear, SectorID, FromDate, ToDate, LegerType, unPaidVch, "V", ReportType);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    DataTable dt = new GeneralLedger_Bll().Show_GeneralLedger(master[0]["AccountCode"].ToString(), master[0]["Desc"].ToString(), master[0]["FinYear"].ToString(),
                        master[0]["SectorID"].ToString(), master[0]["FromDate"].ToString(),
                        master[0]["ToDate"].ToString(), master[0]["LegerType"].ToString(), master[0]["unPaidVch"].ToString(), "E", "C");

                    decimal Total = 0; string Position = ""; string SumFieldName = "";
                    string fileName = master[0]["FileName"].ToString() + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls";
                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            string saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}