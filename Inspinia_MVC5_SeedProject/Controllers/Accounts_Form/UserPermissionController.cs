﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class UserPermissionController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public UserPermissionController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/UserPermission.cshtml");
        }

        [HttpPost]
        public ActionResult Load(string TransactionType, string Desc, string UserID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_UserPermission", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@Desc", Desc == "" ? DBNull.Value : (object)Desc);
            cmd.Parameters.AddWithValue("@UserID", UserID == "" ? DBNull.Value : (object)UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUser(string TransactionType, string UserID, string firstName, string lastName, string Email, string Mobile, string Active, string DesignationID, string UserName, List<Sector> param)
        {
            string Return_UserID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Get_UserPermission", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
                    cmd.Parameters.AddWithValue("@UserID", UserID == "" ? DBNull.Value : (object)UserID);
                    cmd.Parameters.AddWithValue("@firstName", firstName == "" ? DBNull.Value : (object)firstName);
                    cmd.Parameters.AddWithValue("@lastName", lastName == "" ? DBNull.Value : (object)lastName);
                    cmd.Parameters.AddWithValue("@Email", Email == "" ? DBNull.Value : (object)Email);
                    cmd.Parameters.AddWithValue("@Mobile", Mobile == "" ? DBNull.Value : (object)Mobile);
                    cmd.Parameters.AddWithValue("@Active", Active == "" ? DBNull.Value : (object)Active);
                    cmd.Parameters.AddWithValue("@DesignationID", DesignationID == "" ? DBNull.Value : (object)DesignationID);
                    cmd.Parameters.AddWithValue("@UserName", UserName == "" ? DBNull.Value : (object)UserName);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Return_UserID = dr["UserID"].ToString();
                    }
                    dr.Close();

                    if(Return_UserID !="")
                    {
                        if (param != null)
                        {
                            int i = 0;

                            cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TableName", "DAT_User_Sector");
                            cmd.Parameters.AddWithValue("@ColumnName", "UserID");
                            cmd.Parameters.AddWithValue("@PId", Return_UserID);
                            cmd.ExecuteNonQuery();

                            foreach (var list in param)
                            {
                                i = i + 1;
                                cmd = new SqlCommand("account.Get_UserPermission", dbConn, transaction);
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "Insert_Sector");
                                cmd.Parameters.AddWithValue("@SectorID", list.SectorID);
                                cmd.Parameters.AddWithValue("@UserID", Return_UserID);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                   
                    

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = "fail";
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveMenu(string TransactionType, string MenuUserID, string MenuID, string isRemove, string isInsert, string isEdit, string isDelete)
        {
            string Return_UserID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Get_UserPermission", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
                    cmd.Parameters.AddWithValue("@UserID", MenuUserID == "" ? DBNull.Value : (object)MenuUserID);
                    cmd.Parameters.AddWithValue("@MenuID", MenuID == "" ? DBNull.Value : (object)MenuID);
                    cmd.Parameters.AddWithValue("@Active", isRemove == "" ? DBNull.Value : (object)isRemove);
                    cmd.Parameters.AddWithValue("@isInsert", isInsert == "" ? DBNull.Value : (object)isInsert);
                    cmd.Parameters.AddWithValue("@isEdit", isEdit == "" ? DBNull.Value : (object)isEdit);
                    cmd.Parameters.AddWithValue("@isDelete", isDelete == "" ? DBNull.Value : (object)isDelete);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = "fail";
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        public class Sector
        {
            public string SectorID { get; set; }
        }
    }
}