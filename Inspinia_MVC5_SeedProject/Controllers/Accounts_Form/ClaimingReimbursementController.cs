﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class ClaimingReimbursementController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public ClaimingReimbursementController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/ClaimingReimbursement.cshtml");
        }

        [HttpPost]
        public ActionResult Load(string TransType, string ClaimMonth, string MemoNo, string SectorID, string ClaimRemimID, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Claim_Reimbursement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@ClaimforMonth", (ClaimMonth == "" ? DBNull.Value : (object)ClaimMonth));
            cmd.Parameters.AddWithValue("@SMemoNo", (MemoNo == "" ? DBNull.Value : (object)MemoNo));
            cmd.Parameters.AddWithValue("@ClaimReimbursementID", ClaimRemimID == "" ? DBNull.Value : (object)ClaimRemimID);
            cmd.Parameters.AddWithValue("@p_SECTORID", SectorID);
            cmd.Parameters.AddWithValue("@FFinYear", FinYear);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult EditDetail(string TransType, string TabID, string MemoNo, string ClaimMonth, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Claim_Reimbursement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@TabID", (TabID == "" ? DBNull.Value : (object)TabID));
            cmd.Parameters.AddWithValue("@SMemoNo", (MemoNo == "" ? DBNull.Value : (object)MemoNo));
			 cmd.Parameters.AddWithValue("@ClaimforMonth", (ClaimMonth == "" ? DBNull.Value : (object)ClaimMonth));
            cmd.Parameters.AddWithValue("@p_SECTORID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult VoucherDetails(string TransType, string SubDetailID, string ClaimID, string MonYear, string MemoNo, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Claim_Reimbursement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@ClaimforMonth", (MonYear == "" ? DBNull.Value : (object)MonYear));
            cmd.Parameters.AddWithValue("@SubDetailsID", SubDetailID);
			  cmd.Parameters.AddWithValue("@ClaimsID", ClaimID);
            cmd.Parameters.AddWithValue("@p_SECTORID", SectorID);
            cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Update(string TransType, string TabID, string VoucherNumber, string SubDetailID, string MemoNo, string DetailID, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Claim_Reimbursement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
            cmd.Parameters.AddWithValue("@TabID", TabID);
            cmd.Parameters.AddWithValue("@VoucherNumber", VoucherNumber);
            cmd.Parameters.AddWithValue("@SubDetailsID", SubDetailID);
			  cmd.Parameters.AddWithValue("@ClaimReimbursementID", DetailID);
            cmd.Parameters.AddWithValue("@p_SECTORID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(string ClaimMonth, string ClaimforMonth, string SanctionAmt, string OpeningBal, string ReimbursementBal, string ExpIncurred, string ExpIncurredOutSale,
            string EffKm, string EffPerKm, string CostPerKm, string TotalIncome, string TotalExpense, string SectorID, string TabID, string MemoNo,
            string Status, List<Masters> Param)
        {
            string ID = "", RMemoNo ="";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    // cmd.Parameters.AddWithValue("@TransType", (MemoNo == "" || MemoNo == "0" || MemoNo == null) ? "Insert" : "Update");
					 cmd.Parameters.AddWithValue("@TransType", "Insert");  //MemoNo == "" ? "Insert" : "Update"
                    cmd.Parameters.AddWithValue("@TabID", TabID == "" ? DBNull.Value : (object)TabID);
                    cmd.Parameters.AddWithValue("@SMemoNo", MemoNo == "" ? DBNull.Value : (object)MemoNo);
                    cmd.Parameters.AddWithValue("@MonYear", ClaimMonth == "" ? DBNull.Value : (object)ClaimMonth);
                    cmd.Parameters.AddWithValue("@ClaimforMonth", ClaimforMonth == "" ? DBNull.Value : (object)ClaimforMonth);
                    cmd.Parameters.AddWithValue("@SanctionAmt", SanctionAmt == "" ? DBNull.Value : (object)SanctionAmt);
                    cmd.Parameters.AddWithValue("@OpeningBal", OpeningBal == "" ? DBNull.Value : (object)OpeningBal);
                    cmd.Parameters.AddWithValue("@ReimbursementBal", ReimbursementBal == "" ? DBNull.Value : (object)ReimbursementBal);
                    cmd.Parameters.AddWithValue("@ExpenditureIncurred", ExpIncurred == "" ? DBNull.Value : (object)ExpIncurred);
                    cmd.Parameters.AddWithValue("@ExpIncurredOutSale", ExpIncurredOutSale == "" ? DBNull.Value : (object)ExpIncurredOutSale);
                    cmd.Parameters.AddWithValue("@EffectiveKM", EffKm == "" ? DBNull.Value : (object)EffKm);
                    cmd.Parameters.AddWithValue("@EarningPerKM", EffPerKm == "" ? DBNull.Value : (object)EffPerKm);
                    cmd.Parameters.AddWithValue("@CostPerKM", CostPerKm == "" ? DBNull.Value : (object)CostPerKm);
                    cmd.Parameters.AddWithValue("@TotalExpense", TotalExpense == "" ? DBNull.Value : (object)TotalExpense);
                    cmd.Parameters.AddWithValue("@TotalIncome", TotalIncome == "" ? DBNull.Value : (object)TotalIncome);
                    cmd.Parameters.AddWithValue("@p_SECTORID", SectorID == "" ? DBNull.Value : (object)SectorID);
                    cmd.Parameters.AddWithValue("@Status", Status == "" ? DBNull.Value : (object)Status);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID );
					SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
					 cmd.CommandTimeout = 0;
                    da.Fill(ds);

                    //dr = cmd.ExecuteReader();
                    //while (dr.Read())
                    //{
                        //ID = dr["ID"].ToString();
                       // RMemoNo = dr["MemoNo"].ToString();
                    //}
                    //dr.Close();
					//if (ID != "0")
                    //{
                    //if (Param != null)
                    //{
                        //cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                        //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@TransType", "Delete");
                        //cmd.Parameters.AddWithValue("@ClaimReimbursementID", ID);
                        //cmd.ExecuteNonQuery();

                        //foreach (var list in Param)
                        //{
                           // cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            //cmd.Parameters.AddWithValue("@TransType", "Detail");
                            //cmd.Parameters.AddWithValue("@ClaimReimbursementID", ID);
                            //cmd.Parameters.AddWithValue("@SubDetailsID", list.SubdetailID);
                            //cmd.Parameters.AddWithValue("@Amt", list.Amount);
                            //cmd.Parameters.AddWithValue("@Remarks", list.Remarks == "" ? DBNull.Value : (object)list.Remarks);
                            //cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                            //cmd.ExecuteNonQuery();
                        //}
                    //}
					//}

                    transaction.Commit();
                    cr.Data =  ds; //RMemoNo + "#" + ID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", RMemoNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBudget(string BudgetID, string SectorID, string FinYear, string Amount, string SubdetailID, string ClaimRemimID, string MemoNo, string MonYear)
        {
            string ID = "", RMemoDate = "", FCNo="", RBudgetID="";
            string STabID = "", SMemoNo = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", "InsertBudget");
                    cmd.Parameters.AddWithValue("@TabID", BudgetID == "" ? DBNull.Value : (object)BudgetID);
                    cmd.Parameters.AddWithValue("@FinYr", FinYear == "" ? DBNull.Value : (object)FinYear);
                    cmd.Parameters.AddWithValue("@Amt", Amount == "" ? DBNull.Value : (object)Amount);
                    cmd.Parameters.AddWithValue("@SubDetailsID", SubdetailID == "" ? DBNull.Value : (object)SubdetailID);
                    cmd.Parameters.AddWithValue("@ClaimReimbursementID", ClaimRemimID == "" ? DBNull.Value : (object)ClaimRemimID);
                    cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                    cmd.Parameters.AddWithValue("@p_SECTORID", SectorID == "" ? DBNull.Value : (object)SectorID);
                    cmd.Parameters.AddWithValue("@ClaimforMonth", MonYear);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        FCNo = dr["FCNo"].ToString();
                        RBudgetID = dr["BudgetID"].ToString();
                        RMemoDate = dr["RMemoDate"].ToString();
                    }
                    dr.Close();

                    if (RBudgetID != "" || RBudgetID != null)
                    {
                        cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "Insert");
                        cmd.Parameters.AddWithValue("@MemoNo", MemoNo);
                        cmd.Parameters.AddWithValue("@MemoDate", RMemoDate);
                        cmd.Parameters.AddWithValue("@Amount", Amount);
                        cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                        cmd.Parameters.AddWithValue("@SectorId", SectorID);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            STabID = dr["TabID"].ToString();
                            SMemoNo = dr["MemoNo"].ToString();
                        }
                        dr.Close();

                        cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "UpdateMemo");
                        cmd.Parameters.AddWithValue("@SMemoNo", SMemoNo);
                        cmd.Parameters.AddWithValue("@SubDetailsID", SubdetailID == "" ? DBNull.Value : (object)SubdetailID);
                        cmd.Parameters.AddWithValue("@ClaimReimbursementID", ClaimRemimID == "" ? DBNull.Value : (object)ClaimRemimID);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "VoucherDetails");
                        cmd.Parameters.AddWithValue("@ClaimforMonth", (MonYear == "" ? DBNull.Value : (object)MonYear));
                        cmd.Parameters.AddWithValue("@SubDetailsID", SubdetailID);
                        cmd.Parameters.AddWithValue("@p_SECTORID", SectorID);
                        cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        int i = 0;
                        if (dt.Rows.Count > 0)
                        {
                            for (int k = 0; k < dt.Rows.Count; k++)
                            {
                                i = i + 1;
                                cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransType", "Insert_FC");
                                cmd.Parameters.AddWithValue("@MemoNo", SMemoNo);
                                cmd.Parameters.AddWithValue("@SLNo", i);
                                cmd.Parameters.AddWithValue("@FcNo", (FCNo == "" || FCNo == null || FCNo == "null") ? DBNull.Value : (object)FCNo);
                                cmd.Parameters.AddWithValue("@BHeadID", RBudgetID == "" ? DBNull.Value : (object)RBudgetID);
                                cmd.Parameters.AddWithValue("@Amount", dt.Rows[k]["VoucherAmount"]);
                                cmd.Parameters.AddWithValue("@RefType", "V");
                                cmd.Parameters.AddWithValue("@VchNo", dt.Rows[k]["VoucherNumber"] == null ? DBNull.Value : (object)dt.Rows[k]["VoucherNumber"]);
                                cmd.Parameters.AddWithValue("@MemoDate", dt.Rows[k]["VchDate"] == null ? DBNull.Value : (object)dt.Rows[k]["VchDate"]);
                                cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                                cmd.ExecuteNonQuery();
                            }
                            cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert_Cheque");
                            cmd.Parameters.AddWithValue("@MemoNo", SMemoNo);
                            cmd.Parameters.AddWithValue("@MemoDate", DBNull.Value);
                            cmd.ExecuteNonQuery();
                        }
                    }


                    transaction.Commit();
                    cr.Data = FCNo + "#"+ SMemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", FCNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
		
		        [HttpPost]
        public JsonResult UpdateFlag(string MemoNo)
        {
            string  RMemoNo = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Claim_Reimbursement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", "Update");
                    cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        RMemoNo = dr["MemoNo"].ToString();
                    }
                    dr.Close();

                    transaction.Commit();
                    cr.Data = RMemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", RMemoNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        public class Masters
        {
            public int? SubdetailID { get; set; }
            public decimal? Amount { get; set; }
            public string Remarks { get; set; }
        }
    }
}