﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class PaperTktReportController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public PaperTktReportController()
        {
            sData = SessionContext.SessionData;
        }

        // GET: PaperTktReport
        public ActionResult Index()
        {
            return View("~/Views/Accounts/PaperTktReport.cshtml");
        }
    }
}