﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class PaperTktAccountPrintController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        AccountPrint_BLL acbll = new AccountPrint_BLL();

        SessionData sData;
        public PaperTktAccountPrintController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/PaperTktAccountPrint.cshtml");
        
    }

        [HttpPost]
        public JsonResult GetAccountHead()
        {
            try
            {
                int? SectorID = sData.CurrSector;
                var root = acbll.GetAccountHead(SectorID);
                cr.Data = root;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetSubledgerAuto(string AccountDescription, string SectorID)
        {
            try
            {
                var lst = acbll.GetSubledgerAuto(AccountDescription, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult AccountHeadAutoComplete(string AccountDescription, string Group)
        {
            try
            {
                var lst = acbll.GetAccountHdeadAuto(AccountDescription, Group);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_AccountAutoComplete(string AccountDescription, string AccountORSubLedger)
        {
            try
            {
                DataTable dt = acbll.Load_AccountAutoComplete(AccountDescription, AccountORSubLedger);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SubledgerAutoComplete(string IFSC)
        {
            try
            {
                var lst = acbll.subledgerAutoComplete(IFSC);
                cr.Data = lst;
                cr.Message = string.Format("{0}", lst);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getSubledgerFillByBranchID(string id)
        {
            try
            {
                var x = acbll.getSubledgerByBranchId(id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetState()
        {
            try
            {
                var x = acbll.getState();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetDistrict(int Id)
        {
            try
            {
                var x = acbll.getDistrict(Id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllBank()
        {
            try
            {
                var x = acbll.getAllBank();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllSector()
        {
            try
            {
                var lst = acbll.getAllSector();
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllBranch(int bankID)
        {

            try
            {
                var x = acbll.getAllBranch(bankID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAccuntHeadById(string id, string SectorID)
        {
            try
            {
                var x = acbll.getAccountHeadById(id, SectorID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

    [HttpPost]
        public JsonResult SaveAccountHead(string AccountCode, string ParentAccountCode, string AccountDescription, string GLS, string IELA, 
           string Schedule, string PL, string Active, string MenuOrder, string SectorID, string FinYear, List<Detail> Detail)
        {
            try
            {
                string RIssueID = "", RAccountCode = "";
                SqlConnection dbConn = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand();
                System.Data.SqlClient.SqlTransaction transaction;
                dbConn.Open();
                transaction = dbConn.BeginTransaction();
                SqlDataReader dr;

                try
                {
                    cmd = new SqlCommand("account.PaperTkt_AccountPrint_SP", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", AccountCode == "" ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
                    cmd.Parameters.AddWithValue("@Desc", AccountDescription == "" ? DBNull.Value : (object)AccountDescription);
                    cmd.Parameters.AddWithValue("@ParentAccCode", ParentAccountCode == "" ? DBNull.Value : (object)ParentAccountCode);
                    cmd.Parameters.AddWithValue("@GrouporLedger", GLS);
                    cmd.Parameters.AddWithValue("@IELA", IELA);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                    cmd.Parameters.AddWithValue("@Schedule", Schedule == "" ? DBNull.Value : (object)Schedule);
                    cmd.Parameters.AddWithValue("@MenuOrder", MenuOrder == "" ? DBNull.Value : (object)MenuOrder);
                    cmd.Parameters.AddWithValue("@PL", PL);
                    cmd.Parameters.AddWithValue("@Active", Active);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        RAccountCode = dr["AccountCode"].ToString();
                    }
                    dr.Close();

                    if (Detail != null)
                    {
                        cmd = new SqlCommand("account.PaperTkt_AccountPrint_SP", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "DeleteDetail");
                        cmd.Parameters.AddWithValue("@AccountCode", RAccountCode);
                        cmd.ExecuteNonQuery();

                        foreach (var list in Detail)
                        {
                            cmd = new SqlCommand("account.PaperTkt_AccountPrint_SP", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert_Detail");
                            cmd.Parameters.AddWithValue("@AccountCode", RAccountCode);
                            cmd.Parameters.AddWithValue("@ParentAccCode", list.accCode == "" ? DBNull.Value : (object)list.accCode);
                            cmd.Parameters.AddWithValue("@OpBalance", list.OpBalance == "" ? DBNull.Value : (object)list.OpBalance);
                            cmd.Parameters.AddWithValue("@OpDrCr", list.OpDrCr == "" ? DBNull.Value : (object)list.OpDrCr);
                            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();

                    cr.Data = "success";
                    cr.Message = "Account Ledger Inserted Successfully !!";
                    cr.Status = ResponseStatus.SUCCESS;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = "fail";
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }


                finally
                {
                    dbConn.Close();
                }
            }
            catch (Exception ex)
            {
                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult UpdateAccountHead(string Id, string Name, string Parent, string ParentId, string acHead, int branchId, string txtacno, string IsSubLedger, string GroupLedger, bool sec, string schedule, string PL, string Active, string IFSC)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                SectorID = sec ? SectorID : 0;
                acbll.updateAccountHead(Id, Name, Parent, ParentId, acHead, branchId, txtacno, IsSubLedger, GroupLedger, SectorID, InsertedBy, schedule, PL, Active, IFSC);
                cr.Data = true;
                cr.Message = "SubLedger Updated Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveSubledger(string AccountID, string Parent, string Payee, string GST, string Address, string PAN, string GroupLedger, string IELA, string Mob,
            string PayeeContactPerson, string branch, int AccountType, int state, int district, string PIN, List<Account_Description> EffectedGL,
            string AccNo, string Active, string IFSC, string MICR, string BankName, string BranchName)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;

                string Msg = acbll.saveSubLedger(AccountID, Parent, Payee, GST, Address, PAN, GroupLedger, IELA, Mob, PayeeContactPerson, branch, AccountType,
                                SectorID, InsertedBy, state, district, PIN, EffectedGL, AccNo, Active, IFSC, MICR, BankName, BranchName);
                cr.Data = Msg;

                if (Msg != "2" && Msg != "3" && Msg != "")
                    cr.Message = "Account Ledger Inserted Successfully !!";
                if (Msg == "2")
                    cr.Message = "Account Ledger Updated Successfully !!";
                if (Msg == "3")
                    cr.Message = "Sorry ! You are not Authorised !!";
                if (Msg == "")
                    cr.Message = "Sorry ! There is Some Problem !!";
                if (Msg == "4")
                    cr.Message = "Sorry ! This Vendor/Payee Bank Account No. for this IFSC is already available !!";

                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }


        [HttpPost]
        public JsonResult GetAccountHeader()
        {
            try
            {
                DataTable dt = acbll.TreeView();

                var headers = dt.AsEnumerable().Where(r => (string)r["ParentAccountCode"] == "0").Select(r => (string)r["AccountCode"]);

                JObject acc = new JObject();

                foreach (string header in headers)
                {
                    acc.Add(header, (JToken)getNestedChildren(header, ref dt));
                }


                cr.Data = acc;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;

            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getAccConfig()
        {
            try
            {
                int? SectorID = sData.CurrSector;
                var lst = acbll.getAccConfig(SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        private IDictionary<string, JToken> getNestedChildren(string header, ref DataTable dt)
        {
            JObject acc = new JObject();

            var enumData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == header).Select(r => new
            {
                AccountCode = r.Field<string>("AccountCode"),
                AccountDescription = r.Field<string>("AccountDescription"),
                ParentAccountCode = r.Field<string>("ParentAccountCode")
            }).ToList();

            string strArr = JsonConvert.SerializeObject(enumData).ToString();
            if (acc[header] == null)
            {
                acc.Add(header, JArray.Parse(strArr));
            }
            foreach (JObject child in acc[header])
            {

                var childData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == child["AccountCode"].ToString()).Select(r => new
                {
                    AccountCode = r.Field<string>("AccountCode"),
                    AccountDescription = r.Field<string>("AccountDescription"),
                    ParentAccountCode = r.Field<string>("ParentAccountCode")

                }).ToList();

                if (childData.Count() > 0)
                {
                    child["AccountCode"] = (JObject)getNestedChildren(child["AccountCode"].ToString(), ref dt);

                }

            }
            // }

            return acc;
        }

        [HttpPost]
        public JsonResult AccountRule(string SectorID)
        {
            try
            {
                DataTable dt = acbll.Get_AccountRule(SectorID);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Account_Description(string Desc, string MND)
        {
            try
            {
                DataTable dt = acbll.Account_Description(Desc, MND);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Account_DescriptionAuto(string Desc, string Subledger, string SectorID)
        {
            try
            {
                var root = acbll.Account_DescriptionAuto(Desc, Subledger, SectorID);
                cr.Data = root;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult DeleteAccountCode(string AccCode, string SectorID)
        {
            try
            {
                string result = acbll.DeleteAccountCode(AccCode, SectorID);
                cr.Data = result;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CheckInOpeningBalance(string AccountCode, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTkt_AccountPrint_SP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "CheckInOpeningBalance ");
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult FinYear(string SectorID)
        {
            try
            {
                if (SectorID != "")
                {
                    sData.CurrSector = Convert.ToInt32(SectorID);
                    var lst = new EmployeeMaster().Get_SectorWiseFinYear(Convert.ToInt32(SectorID));
                    cr.Data = lst;
                }
                else
                {
                    sData.CurrSector = null;
                    cr.Data = "";
                }
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

    }

    public class Detail
    {
        public string accCode { get; set; }
        public string OpBalance { get; set; }
        public string OpDrCr { get; set; }
    }
}