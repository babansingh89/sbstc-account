﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers
{
    public class WayBillMismatchReportController : Controller
    {
        // GET: WayBillMismatchReport
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        string date = ""; string a = "";
        SessionData sData; string checking = "";

        public WayBillMismatchReportController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            if (!SessionContext.IsAuthenticated)
            {
                return new RedirectResult("/Error/Error401/Index");
            }

            return View("~/Views/Accounts/WayBillMismatchReport.cshtml");
        }
    }
}