﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.App_Codes.BLL;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;

namespace wbEcsc.Controllers.Accounts
{
    //[SecuredFilter]
    public class AccountMasterController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        Account_BLL acbll= new Account_BLL();

        SessionData sData;
        public AccountMasterController()
        {
            sData = SessionContext.SessionData;
        }
     
        // GET: AccountMaster
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Index.cshtml");
        }

        public ActionResult Subledger()
        {
            return View("~/Views/Accounts/Subledger.cshtml");
        }

        [HttpPost]
        public JsonResult GetAccountHead() {
            try
            {
                int? SectorID = sData.CurrSector;
                var root = acbll.GetAccountHead(SectorID);
                cr.Data = root;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetSubledger() {
            try
            {
                int? SectorID = sData.CurrSector;
                var lst = acbll.getSubledger(SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetSubledgerAuto(string AccountDescription, string SectorID)
        {
            try
            {
                var lst = acbll.GetSubledgerAuto(AccountDescription, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status= ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult AccountHeadAutoComplete(string AccountDescription,string Group)
        {
            try
            {
                var lst = acbll.GetAccountHdeadAuto(AccountDescription, Group);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_AccountAutoComplete(string AccountDescription, string AccountORSubLedger)
        {
            try
            {
                var lst = acbll.Load_AccountAutoComplete(AccountDescription, AccountORSubLedger);
                cr.Data = lst;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SubledgerAutoComplete(string IFSC) {
            try
            {
                var lst = acbll.subledgerAutoComplete(IFSC);
                cr.Data = lst;
                cr.Message = string.Format("{0}", lst);
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getSubledgerFillByBranchID(string id) {
            try
            {
                var x = acbll.getSubledgerByBranchId(id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetState() {
            try
            {
                var x = acbll.getState();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetDistrict(int Id) {
            try
            {
                var x = acbll.getDistrict(Id);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult GetAllBank()
        {
            try
            {
                var x = acbll.getAllBank();
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllSector() {
            try
            {
                var lst = acbll.getAllSector();
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data="";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllBranch(int bankID)
        {

            try
            {
                var x = acbll.getAllBranch(bankID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 5);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAccuntHeadById(string id, string SectorID)
        {
            try
            {
                var x = acbll.getAccountHeadById(id, SectorID);
                cr.Data = x;
                cr.Message = string.Format("{0} Municipalities Found", 1);
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveAccountHead(string AccountID,string parent , string acDesc ,string GLS ,string IELA ,string IsSubLedger, string AccountType, int branchId ,string acno ,
            string ISBank ,string achead,int? centerid, string schedule,string PL, string Active, string IFSC, string MICR, string BankName, string BranchName, string AllowNegtive)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;

                string Msg =  acbll.saveAccountHead(AccountID,parent, acDesc, GLS, IELA, IsSubLedger, AccountType, branchId, acno, ISBank, achead, SectorID, InsertedBy, 
                    schedule,  PL, centerid,  Active, IFSC, MICR, BankName, BranchName, AllowNegtive);

                cr.Data = Msg;

                if (Msg != "2" && Msg != "3" && Msg != "")
                    cr.Message = "Account Ledger Inserted Successfully !!";
                if (Msg == "2")
                    cr.Message = "Account Ledger Updated Successfully !!";
                if (Msg == "3")
                    cr.Message = "Sorry ! You are not Authorised !!";
                if (Msg == "")
                    cr.Message = "Sorry ! There is Some Problem !!";
                if (Msg == "4")
                    cr.Message = "Sorry ! This Vendor/Payee Bank Account No. for this IFSC is already available !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult UpdateAccountHead(string Id, string Name, string Parent, string ParentId, string acHead, int branchId, string txtacno, string IsSubLedger, string GroupLedger, bool sec, string schedule, string PL, string Active, string IFSC)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                SectorID = sec ? SectorID : 0;
                acbll.updateAccountHead(Id, Name, Parent, ParentId, acHead, branchId, txtacno, IsSubLedger, GroupLedger, SectorID, InsertedBy, schedule, PL, Active, IFSC);
                cr.Data = true;
                cr.Message = "SubLedger Updated Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveSubledger(string AccountID,string Parent,string Payee,string GST,string Address, string PAN,string GroupLedger,string IELA, string Mob,
            string PayeeContactPerson,string branch,int AccountType,int state,int district,string PIN, List<Account_Description> EffectedGL, 
            string AccNo, string Active, string IFSC, string MICR, string BankName, string BranchName)
        {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;

                string Msg =  acbll.saveSubLedger(AccountID, Parent,Payee,GST, Address, PAN,GroupLedger, IELA, Mob,PayeeContactPerson,branch,AccountType,
                                SectorID, InsertedBy,state,district,PIN, EffectedGL, AccNo, Active, IFSC, MICR, BankName, BranchName);
                cr.Data = Msg;

                if (Msg != "2" && Msg != "3" && Msg != "")
                    cr.Message = "Account Ledger Inserted Successfully !!";
                if (Msg == "2")
                    cr.Message = "Account Ledger Updated Successfully !!";
                if (Msg == "3")
                    cr.Message = "Sorry ! You are not Authorised !!";
                if (Msg == "")
                    cr.Message = "Sorry ! There is Some Problem !!";
                if (Msg == "4")
                    cr.Message = "Sorry ! This Vendor/Payee Bank Account No. for this IFSC is already available !!";

                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        
        [HttpPost]
        public JsonResult GetAccountHeader()
        {
            try
            {
                DataTable dt = acbll.TreeView();

                var headers = dt.AsEnumerable().Where(r=> (string)r["ParentAccountCode"] == "0").Select(r=> (string)r["AccountCode"]);

                JObject acc = new JObject();

                foreach (string header in headers) {
                    acc.Add(header,(JToken) getNestedChildren(header, ref dt));
                }
                

                cr.Data = acc;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult getAccConfig() {
            try
            {
                int? SectorID = sData.CurrSector;
                var lst = acbll.getAccConfig(SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        private IDictionary<string, JToken> getNestedChildren(string header, ref DataTable dt) {
            JObject acc = new JObject();

            var enumData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == header).Select(r => new 
            {
                AccountCode = r.Field<string>("AccountCode"),
                AccountDescription = r.Field<string>("AccountDescription"),
                ParentAccountCode = r.Field<string>("ParentAccountCode")
            }).ToList();

           string strArr = JsonConvert.SerializeObject(enumData).ToString();
            if (acc[header] == null)
            {
                acc.Add(header, JArray.Parse(strArr));
            }
                foreach (JObject child in  acc[header]) {

                    var childData = dt.AsEnumerable().Where(r => r.Field<string>("ParentAccountCode") == child["AccountCode"].ToString()).Select(r => new
                    {
                        AccountCode = r.Field<string>("AccountCode"),
                        AccountDescription = r.Field<string>("AccountDescription"),
                        ParentAccountCode = r.Field<string>("ParentAccountCode")

                    }).ToList();

                    if (childData.Count() > 0)
                    {
                        child["AccountCode"] =(JObject) getNestedChildren(child["AccountCode"].ToString(), ref dt);

                    }

                }
           // }

            return acc;
        }


        [HttpPost]
        public JsonResult test(dynamic obj)
        {
            //IEnumerable<dynamic> obj
            return Json(obj);
        }

        [HttpPost]
        public JsonResult test1(string obj)
        {
            //IEnumerable<dynamic> obj
            return Json(obj);
        }



        [HttpPost]
        public JsonResult AccountRule(string SectorID)
        {
            try
            {
                DataTable dt = acbll.Get_AccountRule(SectorID);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Account_Description(string Desc, string MND)
        {
            try
            {
                List<Account_MD> dt = acbll.Account_Description(Desc, MND);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Account_DescriptionAuto(string Desc, string Subledger, string SectorID)
        {
            try
            {
                var root = acbll.Account_DescriptionAuto(Desc, Subledger, SectorID);
                cr.Data = root;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult DeleteAccountCode(string AccCode, string SectorID)
        {
            try
            {
                string result = acbll.DeleteAccountCode(AccCode, SectorID);
                cr.Data = result;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CheckInOpeningBalance(string AccountCode, string ParentAccountCode, string SectorID, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.CheckInOpeningBalance", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@ParentAccountCode", ParentAccountCode);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult FinYear(string SectorID)
        {
            try
            {
                if (SectorID != "")
                {
                    sData.CurrSector = Convert.ToInt32(SectorID);
                    var lst = new EmployeeMaster().Get_SectorWiseFinYear(Convert.ToInt32(SectorID));
                    cr.Data = lst;
                }
                else
                {
                    sData.CurrSector = null;
                    cr.Data = "";
                }
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JsonResult DetailbyIFSC(string IFSC)
        {
            int IFSCCOUNT = 0;
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                string userAuthenticationURI = "https://api.techm.co.in/api/v1/ifsc/" + IFSC;
                if (!string.IsNullOrEmpty(userAuthenticationURI))
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(userAuthenticationURI);
                    request.Method = "GET";
                    request.ContentType = "application/json";
                    WebResponse response = request.GetResponse();
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var ApiStatus = reader.ReadToEnd();

                        var keys = ApiStatus;
                        if (keys != null)
                        {
                            JObject obj = JObject.Parse(keys);
                            var Status = obj.SelectToken("status");
                            if (Status.ToString() == "success")
                            {
                                var master = (JObject)obj.SelectToken("data");
                                string BANK = master["BANK"].ToString();
                                string Branch = master["BRANCH"].ToString();
                                string IFSCCode = master["IFSC"].ToString();
                                string MICR = master["MICRCODE"].ToString();
                                string ADDRESS = master["ADDRESS"].ToString();
                                string STATE = master["STATE"].ToString();
                                string DISTRICT = master["DISTRICT"].ToString();
                                string CITY = master["CITY"].ToString();

                                if (IFSCCode != null || IFSCCode != "")
                                {
                                    cmd = new SqlCommand("account.Get_BankandBranch", dbConn, transaction);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@TransactionType", "IFSC_Check");
                                    cmd.Parameters.AddWithValue("@IFSCCode", IFSCCode);
                                    dr = cmd.ExecuteReader();
                                    if (dr.HasRows)
                                    {
                                        while (dr.Read())
                                        {
                                            IFSCCOUNT = Convert.ToInt32(dr["IFSCCOUNT"].ToString());
                                        }
                                    }
                                    dr.Close();
                                }

                                if (IFSCCOUNT > 0)
                                {
                                    cr.Data = master;
                                    cr.Message = "success";
                                }
                                else
                                {
                                    cmd = new SqlCommand("account.Get_BankandBranch", dbConn, transaction);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_IFSC");
                                    cmd.Parameters.AddWithValue("@IFSCCode", IFSCCode);
                                    cmd.Parameters.AddWithValue("@MICRCode", MICR);
                                    cmd.Parameters.AddWithValue("@BankName", BANK);
                                    cmd.Parameters.AddWithValue("@BranchName", Branch);
                                    cmd.Parameters.AddWithValue("@State", STATE);
                                    cmd.Parameters.AddWithValue("@City", CITY);
                                    cmd.Parameters.AddWithValue("@District", DISTRICT);
                                    cmd.Parameters.AddWithValue("@Address", ADDRESS);
                                    cmd.ExecuteNonQuery();

                                    cr.Data = master;
                                    cr.Message = "success";
                                }
                            }
                            else
                            {
                                cr.Data = "fail";
                                cr.Message = "Sorry ! Invalid IFSC Code.";
                            }

                        }

                    }
                }

                transaction.Commit();
                cr.Status = ResponseStatus.SUCCESS;

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }
    }
}