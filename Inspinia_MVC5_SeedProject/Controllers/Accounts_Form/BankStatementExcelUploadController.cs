﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BankStatementExcelUploadController : Controller
    {
        // GET: BankStatementExcelUpload
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        string date = ""; string a = "";
        SessionData sData; string checking = "";
        DataTable dtCloned = new DataTable();
        DataTable dtTransaction = new DataTable();

        public BankStatementExcelUploadController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            if (!SessionContext.IsAuthenticated)
            {
                return new RedirectResult("/Error/Error401/Index");
            }

            return View("~/Views/Accounts/BankStatementExcelUpload.cshtml");
        }

        [HttpPost]
        public ActionResult Upload()
        {
            string fname = "", fileWithEXT = "";
            DataTable dt = new DataTable();
            try
            {
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            // Checking for Internet Explorer  
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                fname = file.FileName;
                            }

                            string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                            fileWithEXT = fname;

                            //deleting code starts here
                            string dfname = Path.Combine(Server.MapPath("~/BankStatement/"));
                            string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                            foreach (string f in dfiles)
                            {
                                System.IO.File.Delete(f);
                            }

                            fname = Path.Combine(Server.MapPath("~/BankStatement/"), fname);
                            file.SaveAs(fname);
                        }
                        cr.Data = fileWithEXT.ToString();
                    }
                    catch (Exception ex)
                    {
                        cr.Data = 0;
                    }
                }

                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = 0;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Checking(string FileName, string BankCode, string SectorID, string DRCR)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            try
            {
                string pathToFiles = HttpContext.Server.MapPath("~/BankStatement/" + FileName);

                XSSFWorkbook hssfwb;
                //HSSFWorkbook hssfwb;
                string sheetName = "";
                ISheet sheet = null;

                using (var fs = new FileStream(pathToFiles, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(fs);
                   // hssfwb = new HSSFWorkbook();

                    sheetName = hssfwb.GetSheetAt(0).SheetName;
                    sheet = (XSSFSheet)hssfwb.GetSheet(sheetName);
                }
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sheet.GetRow(0);
                int colCount = headerRow.LastCellNum;

                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(headerRow.GetCell(c).ToString().Trim());

                var i = 1;
                var currentRow = sheet.GetRow(i);

                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    for (var j = 0; j < currentRow.Cells.Count; j++)
                    {
                        var cell = currentRow.GetCell(j);
                        if (cell != null)
                        {
                            switch (cell.CellType)
                            {
                                case CellType.Numeric:
                                    dr[j] = DateUtil.IsCellDateFormatted(cell)
                                        ? cell.DateCellValue.ToString(CultureInfo.InvariantCulture)
                                        : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                    break;
                                case CellType.String:
                                    a = cell.StringCellValue;
                                    if (a != "")
                                        dr[j] = cell.StringCellValue;

                                    break;
                                case CellType.Blank:
                                    dr[j] = string.Empty;
                                    break;

                            }
                        }
                        else
                            break;
                    }
                    i++;
                    currentRow = sheet.GetRow(i);
                    dtExcelTable.Rows.Add(dr);
                }
                dt = dtExcelTable;

                if (dt.Rows.Count > 0)
                {
                    //DataTable Detail = new DataTable();
                    //Detail.Columns.Add("Txn Date", typeof(string));
                    //Detail.Columns.Add("Description", typeof(string));
                    //if(DRCR=="D") Detail.Columns.Add("Debit", typeof(decimal)); else Detail.Columns.Add("Credit", typeof(decimal));

                    //foreach (DataRow row in dt.Rows)
                    //{
                    //    double amt = 0;
                    //    if (DRCR == "D")
                    //        amt = Convert.ToDouble(row["Debit"].ToString().Trim() == "" || row["Debit"].ToString().Trim() == null ? "0" : row["Debit"].ToString().Trim());
                    //    else
                    //        amt = Convert.ToDouble(row["Credit"].ToString().Trim() == "" || row["Credit"].ToString().Trim() == null ? "0" : row["Credit"].ToString().Trim());

                    //    if (amt > 0)
                    //    {
                    //        DataRow drr = Detail.NewRow();

                    //        drr["Txn Date"] = row["Txn Date"].ToString().Trim();
                    //        drr["Description"] = row["Description"].ToString().Trim();
                    //        if (DRCR == "D") drr["Debit"] = row["Debit"].ToString().Trim(); else drr["Credit"] = row["Credit"].ToString().Trim();
                    //        Detail.Rows.Add(drr);
                    //    }
                    //}
                   
                    for(int j=0; j < dt.Rows.Count; j++)
                    {
                        cmd = new SqlCommand("account.BankStatement_ExcelUpload", dbConn, transaction);
                        cmd.Parameters.AddWithValue("@TransType", "Upload");
                        cmd.Parameters.AddWithValue("@SectorID", SectorID);
                        cmd.Parameters.AddWithValue("@TxtDate", dt.Rows[j][0].ToString().Trim() );
                        cmd.Parameters.AddWithValue("@ValueDate", dt.Rows[j][1].ToString().Trim());
                        cmd.Parameters.AddWithValue("@Desc", dt.Rows[j][2].ToString().Trim());
                        cmd.Parameters.AddWithValue("@RefNo", dt.Rows[j][3].ToString().Trim());
                        cmd.Parameters.AddWithValue("@BranchCode", dt.Rows[j][4].ToString().Trim());
                        cmd.Parameters.AddWithValue("@Debit", (dt.Rows[j][5].ToString().Trim() == "" || dt.Rows[j][5].ToString().Trim() == null) ? 0 : (object)Convert.ToDecimal(dt.Rows[j][5].ToString().Trim()));
                        cmd.Parameters.AddWithValue("@Credit", (dt.Rows[j][6].ToString().Trim() == "" || dt.Rows[j][6].ToString().Trim() == null) ? 0 : (object)Convert.ToDecimal(dt.Rows[j][6].ToString().Trim()));
                        cmd.Parameters.AddWithValue("@Amount", (dt.Rows[j][7].ToString().Trim() == "" || dt.Rows[j][7].ToString().Trim() == null) ? 0 : (object)Convert.ToDecimal(dt.Rows[j][7].ToString().Trim()));
                        cmd.Parameters.AddWithValue("@BankCode", BankCode);
                        cmd.Parameters.AddWithValue("@DRCR", DRCR);
                        cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    cmd = new SqlCommand("account.BankStatement_ExcelUpload", dbConn, transaction);
                    cmd.Parameters.AddWithValue("@TransType", "Load");
                    cmd.Parameters.AddWithValue("@BankCode", BankCode);
                    cmd.Parameters.AddWithValue("@DRCR", DRCR);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt2 = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt2);

                    transaction.Commit();

                    cr.Data = dt2;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = "upload";
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load(string TransType, string SectorID, string Desc, string AccountCode)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BankStatement_ExcelUpload", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc == "" ? DBNull.Value : (object)Desc);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string TransType, string BankCode, string SectorID, string DRCR, string RType, string FromDate, string ToDate)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BankStatement_ExcelUpload", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            //cmd.Parameters.AddWithValue("@PostingDate", PostingDate);
            cmd.Parameters.AddWithValue("@BankCode", BankCode);
            cmd.Parameters.AddWithValue("@DRCR", DRCR);
            cmd.Parameters.AddWithValue("@RType", RType);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(string TxnDate, string Amount, string Description, string SectorID, string BankCode, string LedgerCode, string SubLedgerCode,
            string StatementID, string PostingDate, string VoucherNumber)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BankStatement_ExcelUpload", con);
            cmd.Parameters.AddWithValue("@TransType", "Update");
            cmd.Parameters.AddWithValue("@LedgerCode", LedgerCode == "" ? DBNull.Value : (object)LedgerCode);
            cmd.Parameters.AddWithValue("@SubLedgerCode", (SubLedgerCode == "" || SubLedgerCode == null) ? DBNull.Value : (object)SubLedgerCode);
            cmd.Parameters.AddWithValue("@PostingDate", PostingDate == "" ? DBNull.Value : (object)PostingDate);
            cmd.Parameters.AddWithValue("@StatementID", StatementID);
            cmd.Parameters.AddWithValue("@VCHNo", VoucherNumber == "" ? DBNull.Value : (object)VoucherNumber);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "Account Posting done Successfully.");
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult VchList(string TransType, string SectorID, string FromDate, string ToDate, string BankCode)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BankStatement_ExcelUpload", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@BankCode", BankCode);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Excel()
        {
            try
            {

                    string pathToFiles = HttpContext.Server.MapPath("~/BankStatement/Sample.xlsx");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + "Sample.xlsx");
                    Response.WriteFile(pathToFiles);
                    Response.End();
                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }


        }

    }
}