﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class RazorController : Controller
    {
        // GET: Razor
        public ActionResult Index()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;

            // Generate random receipt number for order
            Random randomObj = new Random();
            string transactionId = randomObj.Next(10000000, 100000000).ToString();
            Razorpay.Api.RazorpayClient client = new Razorpay.Api.RazorpayClient("rzp_test_BOx2CiEBj25wLw", "NfIkwsduP7CqH7CcHZhYD2JV");
            Dictionary<string, object> options = new Dictionary<string, object>();
            options.Add("amount", 10100);  // Amount will in paise
            options.Add("receipt", transactionId);
            options.Add("currency", "INR");
           // options.Add("payment_capture", "0"); // 1 - automatic  , 0 - manual
                                                 //options.Add("notes", "-- You can put any notes here --");
            Razorpay.Api.Order orderResponse = client.Order.Create(options);
            string orderId = orderResponse["id"].ToString();

            return View();
        }
    }
}