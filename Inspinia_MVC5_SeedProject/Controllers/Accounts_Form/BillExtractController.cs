﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BillExtractController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public BillExtractController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/BillExtract.cshtml");
        }

        [HttpPost]
        public JsonResult Auto_FC(string TransType, string TabID,  string Desc, string MemoDate, string SectorID, string BudgetID, string FinYear, string MainFinYear, string Flag)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@TabId", TabID);
            cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@Section", BudgetID);
            cmd.Parameters.AddWithValue("@CurFinYear", FinYear);
            cmd.Parameters.AddWithValue("@MainFinYear", MainFinYear);
            cmd.Parameters.AddWithValue("@Flag", Flag);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Bind_Section()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", "Section");
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load(string SectorID, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", "Select");
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@MainFinYear", FinYear);
            cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Insert(string MemoNo, string MemoDate, string MemoSrlNo, string PartyCode,   string SectionID, string FCNo, string RefType, string RefNo, string SRNo, string InvoiceNo,
                            string InvoiceDate, string GrossAmount, string Remarks, string SectorID, string RefDate, string SrvDate, string PayeeName, string DocFilePath, List<FCMasters> param)
        {
            string RMemoNo = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", MemoNo == "" ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                    cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
                    cmd.Parameters.AddWithValue("@SMemoSrlNo", MemoSrlNo);
                    cmd.Parameters.AddWithValue("@Section", SectionID);
                    //cmd.Parameters.AddWithValue("@FcNo", FCNo);
                    //cmd.Parameters.AddWithValue("@RefType", RefType);
                    //cmd.Parameters.AddWithValue("@RefNo", RefNo);
                    //cmd.Parameters.AddWithValue("@SrNo", SRNo);
                    cmd.Parameters.AddWithValue("@SubCode", PartyCode);
                    //cmd.Parameters.AddWithValue("@TaxInvNo", InvoiceNo);
                    //cmd.Parameters.AddWithValue("@TaxInvDt", InvoiceDate);
                    cmd.Parameters.AddWithValue("@GrossAmt", (GrossAmount == "" ? 0 : (object)GrossAmount));
                    cmd.Parameters.AddWithValue("@BillStatus", 1);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@DocFilePath", DocFilePath == "null" ? DBNull.Value : (object)DocFilePath);
                    //cmd.Parameters.AddWithValue("@RefDate", RefDate == "" ? DBNull.Value : (object)RefDate);
                    //cmd.Parameters.AddWithValue("@SrDate", SrvDate == "" ? DBNull.Value : (object)SrvDate);
                    cmd.Parameters.AddWithValue("@PayeeName", PayeeName == "" ? DBNull.Value : (object)PayeeName);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        RMemoNo = dr["MemoNo"].ToString();
                    }
                    dr.Close();

                    if (param != null)
                    {
                        int i = 0;

                        foreach (var list in param)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransactionType", "Insert_FCDetail");
                            cmd.Parameters.AddWithValue("@SMemoNo", RMemoNo);
                            cmd.Parameters.AddWithValue("@NewJobId", 1);  //default
                            cmd.Parameters.AddWithValue("@Section", i);
                            cmd.Parameters.AddWithValue("@FcNo", (list.FCNo == "" || list.FCNo == null || list.FCNo == "null") ? DBNull.Value : (object)list.FCNo);
                            cmd.Parameters.AddWithValue("@BHeadID", list.BudgetID == "" ? DBNull.Value : (object)list.BudgetID);
                            cmd.Parameters.AddWithValue("@JobId", list.FcDetailID == "" ? DBNull.Value : (object)list.FcDetailID);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.Amount);
                            cmd.Parameters.AddWithValue("@RefType", list.RefType == "" ? DBNull.Value : (object)list.RefType);
                            cmd.Parameters.AddWithValue("@RefNo", list.RefNo == "" ? DBNull.Value : (object)list.RefNo);
                            cmd.Parameters.AddWithValue("@RefDate", list.RefDate == "" ? DBNull.Value : (object)list.RefDate);
                            cmd.Parameters.AddWithValue("@SrNo", list.SrNo == "" ? DBNull.Value : (object)list.SrNo);
                            cmd.Parameters.AddWithValue("@SrDate", list.SrDate == "" ? DBNull.Value : (object)list.SrDate);
                            cmd.Parameters.AddWithValue("@TaxInvNo", list.InvNo == "" ? DBNull.Value : (object)list.InvNo);
                            cmd.Parameters.AddWithValue("@TaxInvDt", list.InvDate =="" ? DBNull.Value : (object)list.InvDate);
                            cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    cr.Data = RMemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", RMemoNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadPicture()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string[] a = fname.Split('.');
                        string MemoNo = a[0];
                        string fileWithoutEXT = MemoNo; // Path.GetFileNameWithoutExtension(fname);
                        string[] fs = MemoNo.Split('/');
                        string[] fsname = fname.Split('/');
                        string fileWithEXT = fsname[4]; //fname
                       

                        //deleting code starts here
                        string dfname = Path.Combine(Server.MapPath("~/Upload_BillExtractFile/"));
                        string[] dfiles = System.IO.Directory.GetFiles(dfname, fs[3] + ".*");  //fileWithoutEXT
                        foreach (string f in dfiles)
                        {
                            System.IO.File.Delete(f);
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Upload_BillExtractFile/"), fileWithEXT); //fname
                        file.SaveAs(fname);


                        using (SqlConnection con = new SqlConnection())
                        {
                            con.ConnectionString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = con;
                                cmd.CommandText = "UPDATE account.Mst_Acc_Bill_Extract SET DocFilePath='/Upload_BillExtractFile/" + fileWithEXT + "' WHERE MemoNo ='" + MemoNo + "'";
                                //cmd.CommandText = "UPDATE account.Mst_Acc_Bill_Extract SET DocFilePath='/Upload_BillExtractFile/" + fileWithEXT + "' WHERE MemoNo ='" + fileWithoutEXT + "'";
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    // Returns message that successfully uploaded  

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details:" + ex.Message);
                }
            }
            else
            {
                return Json("Data Saved But No File Selected");
            }
        }


        [HttpPost]
        public ActionResult Insert_BillStatus(string TabID, string JobID, string SectorID)
        {
            string STabID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_BillStatus");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@JobId", JobID);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                    }
                    dr.Close();


                    transaction.Commit();
                    cr.Data = STabID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Insert_VerifyBill(string TabID,  string DocType, string isDocVerified, string BVID, string Remarks, string BasicAmount, string ISPOAmtExceed)
        {
            string STabID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_VerifyBill");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@RefType", DocType);
                    cmd.Parameters.AddWithValue("@JobId", isDocVerified);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@GrossAmt", BasicAmount);
                    cmd.Parameters.AddWithValue("@ISPOAmtExceed", ISPOAmtExceed);
                    cmd.Parameters.AddWithValue("@BHeadID", BVID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                    }
                    dr.Close();


                    transaction.Commit();
                    cr.Data = STabID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Check_VerifyBill(string TabId,  string DocType, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);
            cmd.Parameters.AddWithValue("@RefType", DocType);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Insert_ConfirmBudgetHead(string TabID, string JobID, string SectorID, string BankCash, string VoucherType, List<FCMasters> param, string TotalAmt)
        {
            string STabID = "", MemoNo="";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    if (param != null)
                    {
                        int i = 0;

                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "Update_Gross");
                        cmd.Parameters.AddWithValue("@TabId", TabID);
                        cmd.Parameters.AddWithValue("@GrossAmt", TotalAmt);

                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            MemoNo = dr["MemoNo"].ToString();
                        }
                        dr.Close();

                        if (MemoNo != null || MemoNo != "")
                        {
                            foreach (var list in param)
                            {
                                i = i + 1;
                                cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "Insert_FCDetail");
                                cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                                cmd.Parameters.AddWithValue("@NewJobId", 2);  //default
                                cmd.Parameters.AddWithValue("@Section", i);
                                cmd.Parameters.AddWithValue("@BHeadID", list.BudgetID);
                                cmd.Parameters.AddWithValue("@FcNo", list.FCNo);
                                cmd.Parameters.AddWithValue("@GrossAmt", list.Amount);
                                cmd.Parameters.AddWithValue("@TaxInvNo", list.TaxinvoiceNo);
                                cmd.Parameters.AddWithValue("@JobId", list.FcDetailID);
                                cmd.Parameters.AddWithValue("@ISPOAmtExceed", BankCash);
                                cmd.Parameters.AddWithValue("@VoucherType", VoucherType);
                                cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                                cmd.ExecuteNonQuery();
                            }
                        }
                    }


                    transaction.Commit();
                    cr.Data = MemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", MemoNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Insert_Forward(string TabID, string NewJobID, string JobID, string ActionID, string SectorID, string Remarks, string ISObjection, List<Masters> param, string strUserID)
        {
            string STabID = ""; 
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_Forward");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@JobId",  JobID);
                    cmd.Parameters.AddWithValue("@NewJobId", NewJobID);
                    cmd.Parameters.AddWithValue("@BillStatus", ActionID);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@ISPOAmtExceed", ISObjection);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                    }
                    dr.Close();

                            if (param != null)
                    {
                        //foreach (var list in param)
                        //{
                            cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@TransactionType", "Insert_ForwardUser");
                            cmd.Parameters.AddWithValue("@TabId", STabID);
                            cmd.Parameters.AddWithValue("@JobId", JobID);
                            cmd.Parameters.AddWithValue("@SectorId", SectorID);
                            cmd.Parameters.AddWithValue("@Remarks", Remarks=="" ? DBNull.Value : (object)Remarks);
                            cmd.Parameters.AddWithValue("@ISPOAmtExceed", ISObjection);
                            cmd.Parameters.AddWithValue("@StrUserID", strUserID);

                            cmd.ExecuteNonQuery();
                        //}
                    }


                    transaction.Commit();
                    cr.Data = STabID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Insert_BankChequeNo(string TabID, string BankCash, string ChequeNo, string ChequeDate, string AccountCode, string InstType)
        {
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand(); 
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_BankChequeNo");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@RefType", BankCash);
                    cmd.Parameters.AddWithValue("@SrNo", ChequeNo == "" ? DBNull.Value : (object)ChequeNo);
                    cmd.Parameters.AddWithValue("@MemoDate", ChequeDate == "" ? DBNull.Value : (object)ChequeDate);
                    cmd.Parameters.AddWithValue("@SubCode", AccountCode);
                    cmd.Parameters.AddWithValue("@VoucherType", InstType);
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VoucherEntry(string TabID, string JobID, string ActionID, string Remarks, string PostingDate, string SectorID, string VoucherType, List<Masters> param)
        {
            string STabID = "",  VoucherNo ="0", MemoNo ="", BankCash ="",  BillStatusDate="", GrossAmt ="", InFavourof= "", SubLedger ="", isBankCash="", RSubCode="", SubCode ="",
            InstNo = "", InstDate = "", InstType = "";
            int VCHNo = 0, VchSrl =0, CountVoucherType=0;
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "VoucherEntry");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@JobId", JobID);
                    cmd.Parameters.AddWithValue("@BillStatus", ActionID);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@VoucherType", VoucherType);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                        MemoNo = dr["MemoNo"].ToString();
                        BankCash = dr["BankCash"].ToString();
                        BillStatusDate= dr["BillStatusDate"].ToString();
                        GrossAmt = dr["GrossAmt"].ToString();
                        InFavourof = dr["InFavourof"].ToString();
                        SubCode = dr["SubCode"].ToString();
                        CountVoucherType = Convert.ToInt32(dr["CountVoucherType"].ToString());
                        InstNo = dr["InstNo"].ToString();
                        InstDate = dr["InstDate"].ToString();
                        InstType = dr["InstType"].ToString();
                    }
                    dr.Close();

                    if (CountVoucherType > 0)
                    {
                        if (STabID != "")
                        {
                            if (param != null)
                            {
                                foreach (var list in param)
                                {
                                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                                    cmd.Parameters.AddWithValue("@TransactionType", "Insert_ForwardUser");
                                    cmd.Parameters.AddWithValue("@TabId", STabID);
                                    cmd.Parameters.AddWithValue("@JobId", JobID);
                                    cmd.Parameters.AddWithValue("@InsertedBy", list.UserID);

                                    cmd.ExecuteNonQuery();
                                }
                            }


                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ VOUCHER SAVE
                            string TR_Type = "";

                            cmd = new SqlCommand("account.Get_Acc_VchTypeByID", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherType == "J" ? 6 : (BankCash == "B" ? 2 : 4));

                            dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                TR_Type = dr["Abbv"].ToString();
                            }
                            dr.Close();

                            cmd = new SqlCommand("account.Insert_Acc_VoucherMaster", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@AutoTrans", "E");
                            cmd.Parameters.AddWithValue("@VCH_TYPE", VoucherType == "J" ? 6 : (BankCash == "B" ? 2 : 4));
                            cmd.Parameters.AddWithValue("@VOUCHER_DATE", VoucherType == "J" ? PostingDate : BillStatusDate);  //BillStatusDate
                            cmd.Parameters.AddWithValue("@VCH_AMOUNT", GrossAmt);
                            cmd.Parameters.AddWithValue("@NARRATION", Remarks);
                            cmd.Parameters.AddWithValue("@STATUS", "D");
                            cmd.Parameters.AddWithValue("@NP_SP", "SP");
                            cmd.Parameters.AddWithValue("@TR_TYPE", TR_Type);
                            cmd.Parameters.AddWithValue("@SectorID", SectorID);
                            cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                            cmd.Parameters.Add("@Vch_NoResult", SqlDbType.Int);
                            cmd.Parameters.Add("@VoucherNoResult", SqlDbType.VarChar, 25);
                            cmd.Parameters["@Vch_NoResult"].Direction = ParameterDirection.Output;
                            cmd.Parameters["@VoucherNoResult"].Direction = ParameterDirection.Output;

                            cmd.ExecuteNonQuery();

                            VCHNo = Convert.ToInt32(cmd.Parameters["@Vch_NoResult"].Value.ToString());
                            VoucherNo = cmd.Parameters["@VoucherNoResult"].Value.ToString();

                            if (VCHNo > 0)
                            {
                                cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "ForVoucher");
                                cmd.Parameters.AddWithValue("@TabID", TabID);
                                cmd.Parameters.AddWithValue("@VoucherType", VoucherType);

                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                cmd.CommandTimeout = 0;
                                da.Fill(dt);

                                //updateing remarks
                                cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "VoucherRemarks");
                                cmd.Parameters.AddWithValue("@TabID", TabID);
                                cmd.Parameters.AddWithValue("@Section", VCHNo);
                                cmd.Parameters.AddWithValue("@Narration", Remarks);
                                cmd.Parameters.AddWithValue("@Remarks", dt.Rows[0]["Remarks"].ToString());
                                cmd.ExecuteNonQuery();

                                if (dt.Rows.Count > 0)
                                {
                                    int i = 0;
                                    for (int j = 0; j < dt.Rows.Count; j++)
                                    {
                                        i = i + 1;
                                        cmd = new SqlCommand("account.Insert_Acc_VoucherDetail", dbConn, transaction);

                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.AddWithValue("@MaxVoucherNumber", VCHNo);
                                        cmd.Parameters.AddWithValue("@SerialNumber", i);
                                        cmd.Parameters.AddWithValue("@Accountcode", dt.Rows[j]["AccountCode"]);
                                        cmd.Parameters.AddWithValue("@DRCR", dt.Rows[j]["DrCr"]);
                                        cmd.Parameters.AddWithValue("@Amount", dt.Rows[j]["Amount"]);
                                        cmd.Parameters.AddWithValue("@sectorID", SectorID);
                                        cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);

                                        SqlDataAdapter daa = new SqlDataAdapter(cmd);
                                        DataTable dts = new DataTable();
                                        daa.Fill(dts);



                                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.AddWithValue("@TransactionType", "CheckSubLedger");
                                        cmd.Parameters.AddWithValue("@SubCode", dt.Rows[j]["AccountCode"]);
                                        cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                                        cmd.Parameters.AddWithValue("@VoucherNo", VCHNo);
                                        cmd.CommandTimeout = 0;
                                        dr = cmd.ExecuteReader();
                                        while (dr.Read())
                                        {
                                            SubLedger = dr["SubLedger"].ToString();
                                            isBankCash = dr["BankCash"].ToString();
                                            RSubCode = dr["RSubCode"].ToString();
                                        }
                                        dr.Close();

                                        if (SubLedger == "Y" || isBankCash == "B")
                                        {
                                            if (dts.Rows.Count > 0)
                                            {
                                                VchSrl = Convert.ToInt32(dts.Rows[0]["VCHSRL"].ToString());

                                                cmd = new SqlCommand("account.Updt_AccDatBills", dbConn, transaction);
                                                cmd.CommandType = CommandType.StoredProcedure;

                                                cmd.Parameters.AddWithValue("@InFavourof", SubLedger == "Y" ? DBNull.Value : (object)InFavourof);
                                                cmd.Parameters.AddWithValue("@InstrumentType", SubLedger == "Y" ? DBNull.Value : (object)InstType);
                                                cmd.Parameters.AddWithValue("@InstrumentNo", SubLedger == "Y" ? DBNull.Value : (object)InstNo);
                                                cmd.Parameters.AddWithValue("@InstrumentDate", InstDate);
                                                cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);
                                                cmd.Parameters.AddWithValue("@sectorID", SectorID);
                                                cmd.Parameters.AddWithValue("@Accountcode", dt.Rows[j]["AccountCode"]);
                                                cmd.Parameters.AddWithValue("@SubCode", isBankCash == "B" ? DBNull.Value : ((RSubCode == "" || RSubCode == null) ? SubCode : (object)RSubCode));
                                                cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                                cmd.Parameters.AddWithValue("@MaxVoucherNumber", VCHNo);
                                                cmd.Parameters.AddWithValue("@AdjustmentAmount", dt.Rows[j]["Amount"]);
                                                cmd.Parameters.AddWithValue("@DRCR", dt.Rows[j]["DrCr"]);
                                                cmd.Parameters.AddWithValue("@VoucherSerial", VchSrl);
                                                cmd.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }


                                cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "VoucherApproved");
                                cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                                cmd.Parameters.AddWithValue("@JobId", ActionID);
                                cmd.Parameters.AddWithValue("@Remarks", dt.Rows[0]["Remarks"].ToString());
                                cmd.Parameters.AddWithValue("@TabID", VCHNo);
                                cmd.Parameters.AddWithValue("@RefType", VoucherType);
                                cmd.ExecuteNonQuery();

                            }
                        }
                    }
                    else
                    {
                        //updateing remarks
                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "VoucherRemarks");
                        cmd.Parameters.AddWithValue("@TabID", TabID);
                        cmd.Parameters.AddWithValue("@Narration", Remarks);
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    cr.Data = VCHNo + "#" + VoucherNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VoucherEntry1(string TabID, string JobID, string ActionID, string Remarks, string PostingDate, string VchNo, string SectorID, string VoucherType, List<Masters> param)
        {
            string STabID = "", VoucherNo = "", MemoNo = "", BankCash = "", BillStatusDate = "", GrossAmt = "", InFavourof = "", SubLedger = "", SubCode = "", RSubCode = "";
            int VCHNo = 0, VchSrl = 0, VoucherCount=0;
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@TransactionType", "VoucherEntry");
                    cmd.Parameters.AddWithValue("@TabId", TabID);
                    cmd.Parameters.AddWithValue("@JobId", JobID);
                    cmd.Parameters.AddWithValue("@BillStatus", ActionID);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    cmd.Parameters.AddWithValue("@VoucherNo", VchNo);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                        MemoNo = dr["MemoNo"].ToString();
                        BankCash = dr["BankCash"].ToString();
                        BillStatusDate = dr["BillStatusDate"].ToString();
                        GrossAmt = dr["GrossAmt"].ToString();
                        InFavourof = dr["InFavourof"].ToString();
                        SubCode = dr["SubCode"].ToString();
                        VoucherCount = Convert.ToInt32(dr["VoucherCount"].ToString());
                    }
                    dr.Close();

                    if (VoucherCount > 0)
                    {
                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ VOUCHER SAVE
                        string TR_Type = "";

                        cmd = new SqlCommand("account.Get_Acc_VchTypeByID", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherType == "J" ? 6 : (BankCash == "B" ? 2 : 4));

                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            TR_Type = dr["Abbv"].ToString();
                        }
                        dr.Close();


                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "ForRevVoucher");
                        cmd.Parameters.AddWithValue("@VoucherNo", VchNo);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dt);


                        cmd = new SqlCommand("account.Insert_Acc_VoucherMaster", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AutoTrans", "E");
                        cmd.Parameters.AddWithValue("@VCH_TYPE", VoucherType == "J" ? 6 : (BankCash == "B" ? 2 : 4));
                        cmd.Parameters.AddWithValue("@VOUCHER_DATE", VoucherType == "J" ? PostingDate : BillStatusDate);  //BillStatusDate
                        cmd.Parameters.AddWithValue("@VCH_AMOUNT", dt.Rows[0]["Amount"].ToString());
                        cmd.Parameters.AddWithValue("@NARRATION", Remarks + " Reverse.");
                        cmd.Parameters.AddWithValue("@STATUS", "D");
                        cmd.Parameters.AddWithValue("@NP_SP", "SP");
                        cmd.Parameters.AddWithValue("@TR_TYPE", TR_Type);
                        cmd.Parameters.AddWithValue("@SectorID", SectorID);
                        cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                        cmd.Parameters.Add("@Vch_NoResult", SqlDbType.Int);
                        cmd.Parameters.Add("@VoucherNoResult", SqlDbType.VarChar, 25);
                        cmd.Parameters["@Vch_NoResult"].Direction = ParameterDirection.Output;
                        cmd.Parameters["@VoucherNoResult"].Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        VCHNo = Convert.ToInt32(cmd.Parameters["@Vch_NoResult"].Value.ToString());
                        VoucherNo = cmd.Parameters["@VoucherNoResult"].Value.ToString();

                        if (VCHNo > 0)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                int i = 0;
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    i = i + 1;
                                    cmd = new SqlCommand("account.Insert_Acc_VoucherDetail", dbConn, transaction);

                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@MaxVoucherNumber", VCHNo);
                                    cmd.Parameters.AddWithValue("@SerialNumber", i);
                                    cmd.Parameters.AddWithValue("@Accountcode", dt.Rows[j]["AccountCode"]);
                                    cmd.Parameters.AddWithValue("@DRCR", dt.Rows[j]["DrCr"]);
                                    cmd.Parameters.AddWithValue("@Amount", dt.Rows[j]["Amount"]);
                                    cmd.Parameters.AddWithValue("@sectorID", SectorID);
                                    cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);

                                    SqlDataAdapter daa = new SqlDataAdapter(cmd);
                                    DataTable dts = new DataTable();
                                    daa.Fill(dts);



                                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@TransactionType", "CheckSubLedger");
                                    cmd.Parameters.AddWithValue("@SubCode", dt.Rows[j]["AccountCode"]);
                                    cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                                    cmd.CommandTimeout = 0;
                                    dr = cmd.ExecuteReader();
                                    while (dr.Read())
                                    {
                                        SubLedger = dr["SubLedger"].ToString();
                                        RSubCode = dr["RSubCode"].ToString();
                                    }
                                    dr.Close();

                                    if (SubLedger == "Y")
                                        if (dts.Rows.Count > 0)
                                        {
                                            VchSrl = Convert.ToInt32(dts.Rows[0]["VCHSRL"].ToString());

                                            cmd = new SqlCommand("account.Updt_AccDatBills", dbConn, transaction);
                                            cmd.CommandType = CommandType.StoredProcedure;

                                            cmd.Parameters.AddWithValue("@InFavourof", InFavourof);
                                            cmd.Parameters.AddWithValue("@InstrumentType", "O");
                                            cmd.Parameters.AddWithValue("@InstrumentNo", "");
                                            cmd.Parameters.AddWithValue("@InstrumentDate", VoucherType == "J" ? PostingDate : BillStatusDate);  //BillStatusDate
                                            cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);
                                            cmd.Parameters.AddWithValue("@sectorID", SectorID);
                                            cmd.Parameters.AddWithValue("@Accountcode", dt.Rows[j]["AccountCode"]);
                                            cmd.Parameters.AddWithValue("@SubCode", (RSubCode == "" || RSubCode == null) ? SubCode : (object)RSubCode);
                                            cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                            cmd.Parameters.AddWithValue("@MaxVoucherNumber", VCHNo);
                                            cmd.Parameters.AddWithValue("@AdjustmentAmount", dt.Rows[j]["Amount"]);
                                            cmd.Parameters.AddWithValue("@DRCR", dt.Rows[j]["DrCr"]);
                                            cmd.Parameters.AddWithValue("@VoucherSerial", VchSrl);
                                            cmd.ExecuteNonQuery();
                                        }
                                }
                            }


                            cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransactionType", "VoucherApproved");
                            cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
                            cmd.Parameters.AddWithValue("@JobId", ActionID);
                            cmd.Parameters.AddWithValue("@Remarks", Remarks + " Reverse");
                            cmd.Parameters.AddWithValue("@TabID", VCHNo);
                            cmd.Parameters.AddWithValue("@RefType", "R");
                            cmd.ExecuteNonQuery();

                        }
                    }

                    transaction.Commit();
                    cr.Data = VoucherNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Insert_DRCRHead(string TabID, string MemoNo, string JournalRemarks, string PaymentRemarks,  List<BillSubVch_MD> param, List<BillSubVch_MD> param1)
        {
            string STabID = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    if (param != null)
                    {
                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "Delete_DRCRHead");
                        cmd.Parameters.AddWithValue("@TabID", TabID);
                        cmd.Parameters.AddWithValue("@VoucherType", param[0].VoucherType);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in param)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransactionType", "Insert_DRCRHead");
                            cmd.Parameters.AddWithValue("@TabID", TabID);
                            cmd.Parameters.AddWithValue("@Section", list.Ord);
                            cmd.Parameters.AddWithValue("@RefType", list.Drcr);
                            cmd.Parameters.AddWithValue("@SubCode", list.AccountCode);
                            cmd.Parameters.AddWithValue("@NSubCode", (list.SubCode == null || list.SubCode == "null") ? DBNull.Value: (object)list.SubCode);
                            cmd.Parameters.AddWithValue("@VoucherType", list.VoucherType);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.Amount);
                            cmd.Parameters.AddWithValue("@Remarks", JournalRemarks);

                            cmd.ExecuteNonQuery();
                        }
                    }
                    if (param1 != null)
                    {
                        cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransactionType", "Delete_DRCRHead");
                        cmd.Parameters.AddWithValue("@TabID", TabID);
                        cmd.Parameters.AddWithValue("@VoucherType", param1[0].VoucherType);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in param1)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransactionType", "Insert_DRCRHead");
                            cmd.Parameters.AddWithValue("@TabID", TabID);
                            cmd.Parameters.AddWithValue("@Section", list.Ord);
                            cmd.Parameters.AddWithValue("@RefType", list.Drcr);
                            cmd.Parameters.AddWithValue("@SubCode", list.AccountCode);
                            cmd.Parameters.AddWithValue("@NSubCode", (list.SubCode == null || list.SubCode == "null") ? DBNull.Value : (object)list.SubCode);
                            cmd.Parameters.AddWithValue("@VoucherType", list.VoucherType);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.Amount);
                            cmd.Parameters.AddWithValue("@Remarks", PaymentRemarks);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    cr.Data = STabID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", TabID);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Insert_PANGST(string TransType, string TabID, string DeductionType, string SectionID, string TDSPerct, string TDSTaxAmount, string CGSTPerct, string SGSTPerct,
                                        string IGSTPerct, string CGSTAmount, string SGSTAmount, string IGSTAmount)
        {
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.BillExtract", dbConn, transaction);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", TransType);
                    cmd.Parameters.AddWithValue("@TabID", TabID);
                    cmd.Parameters.AddWithValue("@RefType", DeductionType);
                    cmd.Parameters.AddWithValue("@Section", SectionID == "" ? DBNull.Value : (object)SectionID);
                    cmd.Parameters.AddWithValue("@TDSPert", TDSPerct == "" ? DBNull.Value : (object)TDSPerct);
                    cmd.Parameters.AddWithValue("@GrossAmt", TDSTaxAmount == "" ? DBNull.Value : (object)TDSTaxAmount);
                    cmd.Parameters.AddWithValue("@CGSTPert", CGSTPerct == "" ? DBNull.Value : (object)CGSTPerct);
                    cmd.Parameters.AddWithValue("@SGSTPert", SGSTPerct == "" ? DBNull.Value : (object)SGSTPerct);
                    cmd.Parameters.AddWithValue("@IGSTPert", IGSTPerct == "" ? DBNull.Value : (object)IGSTPerct);
                    cmd.Parameters.AddWithValue("@CGSTAmount", CGSTAmount == "" ? DBNull.Value : (object)CGSTAmount);
                    cmd.Parameters.AddWithValue("@SGSTAmount", SGSTAmount == "" ? DBNull.Value : (object)SGSTAmount);
                    cmd.Parameters.AddWithValue("@IGSTAmount", IGSTAmount == "" ? DBNull.Value : (object)IGSTAmount);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "Success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = "fail";
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AppliedDetail(string TabId, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);
           
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeApproveStatus(string TabID, string Remarks)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", "VoucherRemarks");
            cmd.Parameters.AddWithValue("@TabID", TabID);
            cmd.Parameters.AddWithValue("@Narration", Remarks);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ForwardUser(string TabId, string TransType, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AppliedDetails(string TabId, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult VoucherDetail(string TabId, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Account_Description(string TabId, string Desc, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@TabId", TabId);
            cmd.Parameters.AddWithValue("@Desc", Desc);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BankCash(string BankCash, string Desc, string TransType, string SectorID, string MemoDate)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@RefType", BankCash);
            cmd.Parameters.AddWithValue("@Desc", Desc);
			 cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GST(string JobID, string TabId, string TransType, string ISPOAmtExceed)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@JobId", JobID);
            cmd.Parameters.AddWithValue("@TabId", TabId);
            cmd.Parameters.AddWithValue("@ISPOAmtExceed", ISPOAmtExceed);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Status()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", "Status");

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult JobWork(string JobID, string TabId, string TransType, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@JobId", JobID);
            cmd.Parameters.AddWithValue("@TabId", TabId);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FCDetail(string MemoNo, string TabID, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@SMemoNo", MemoNo);
            cmd.Parameters.AddWithValue("@TabId", TabID);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string FromDate, string ToDate, string SectorID, string Desc, string PartyName, string TransType, string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@RefDate", FromDate);
            cmd.Parameters.AddWithValue("@SrDate", ToDate);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
			 cmd.Parameters.AddWithValue("@PayeeName", PartyName);
            cmd.Parameters.AddWithValue("@MainFinYear", FinYear);
            cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
		
		[HttpPost]
        public ActionResult DeleteMemo(string MemoNo, string MemoNoSlNo)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Delete_BE", con);
            cmd.Parameters.AddWithValue("@MemoNo", MemoNo);
            cmd.Parameters.AddWithValue("@MemoSrlNo", MemoNoSlNo);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = "success";
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", "success");
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PendingJob(string UserID, string SectorID, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", "success");
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult VoucherSubledger(string SubCode, string Desc, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.BillExtract", con);
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@SubCode" , SubCode);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", "success");
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }

   public class Masters
    {
        public int  UserID { get; set; }
    }
    public class FCMasters
    {
        public string FCNo { get; set; }
        public decimal? Amount { get; set; }
        public string RefNo { get; set; }
        public string RefDate { get; set; }
        public string SrNo { get; set; }
        public string SrDate { get; set; }
        public string InvNo { get; set; }
        public string InvDate { get; set; }
        public string RefType { get; set; }
        public string BudgetID { get; set; }
        public string TaxinvoiceNo { get; set; }
        public string FcDetailID { get; set; }
    }
}