﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using wbEcsc.App_Codes.BLL;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class TDSPayableController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public TDSPayableController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/TDSPayable.cshtml");
        }

        [HttpPost]
        public JsonResult TDSDetail(string FromDate, string ToDate, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Acc_Tds", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Fdate", FromDate);
            cmd.Parameters.AddWithValue("@Tdate", ToDate);
            cmd.Parameters.AddWithValue("@SectorIds", SectorID);
            cmd.Parameters.AddWithValue("@OutputType", "V");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    string SectorID = master[0]["SectorID"].ToString();

                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Proc_Acc_Tds", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Fdate", master[0]["FromDate"].ToString());
                    cmd.Parameters.AddWithValue("@Tdate", master[0]["ToDate"].ToString());
                    cmd.Parameters.AddWithValue("@SectorIds", master[0]["SectorID"].ToString());
                    cmd.Parameters.AddWithValue("@OutputType", "E");
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    da.Fill(dt);

                    GridView gv = new GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename= TDS-Payable-Report" + ".xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}