﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class Update_Voucher_FCController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();

        SessionData sData;
        public Update_Voucher_FCController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Update_Voucher_FC.cshtml");
        }
        [HttpPost]
        public JsonResult Load(string VchTypeID, string FromDate, string ToDate, string TransType, string MemoNo, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@VchType", VchTypeID);
            cmd.Parameters.AddWithValue("@Fdate", FromDate);
            cmd.Parameters.AddWithValue("@Tdate", ToDate);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@MemoNo", MemoNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(string MemoNo, string MemoDate, string BudgetID, string FCNo, string SectorID, string Remarks, string Amount, List<Masters> param, string FinYear)
        {
            string STabID = "",  SMemoNo = "";
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", "Insert");
                    cmd.Parameters.AddWithValue("@MemoNo", MemoNo);
                    cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
                    cmd.Parameters.AddWithValue("@Amount", Amount);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@SFinYear", FinYear);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        STabID = dr["TabID"].ToString();
                        SMemoNo = dr["MemoNo"].ToString();
                    }
                    dr.Close();

                    if (param != null)
                    {
                        int i = 0;
                        foreach (var list in param)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert_FC");
                            cmd.Parameters.AddWithValue("@MemoNo", SMemoNo);
                            cmd.Parameters.AddWithValue("@SLNo", i);
                            cmd.Parameters.AddWithValue("@FcNo", (FCNo == "" || FCNo == null || FCNo == "null") ? DBNull.Value : (object)FCNo);
                            cmd.Parameters.AddWithValue("@BHeadID", BudgetID == "" ? DBNull.Value : (object)BudgetID);
                            //cmd.Parameters.AddWithValue("@JobId", FcDetailID == "" ? DBNull.Value : (object)FcDetailID);
                            cmd.Parameters.AddWithValue("@Amount", list.Amount);
                            cmd.Parameters.AddWithValue("@RefType", list.RefType == null ? DBNull.Value : (object)list.RefType);
                            cmd.Parameters.AddWithValue("@VchNo", list.VoucherNo == null ? DBNull.Value : (object)list.VoucherNo);
                            cmd.Parameters.AddWithValue("@MemoDate", list.VchDate == "" ? DBNull.Value : (object)list.VchDate);
                            cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", "Insert_Cheque");
                    cmd.Parameters.AddWithValue("@MemoNo", SMemoNo);
                    cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    cr.Data = SMemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", SMemoNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FCDetail(string MemoNo, string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@MemoNo", MemoNo);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Auto_FC(string TransType, string TabID, string Desc, string MemoDate, string SectorID, string BudgetID, string FinYear, string MainFinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Updt_Voucher_FC", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@TabId", TabID);
            cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@Section", BudgetID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        public class Masters
        {
            public int? VoucherNo { get; set; }
            public string VchDate { get; set; }
            public string Amount { get; set; }
            public string RefType { get; set; }
        }
    }
}