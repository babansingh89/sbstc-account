﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.ViewModels.Accounts;

namespace wbEcsc.Controllers.Accounts_Form
{
    public class Report_VoucherEntryController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;

        SessionData sData;
        public Report_VoucherEntryController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Report_VoucherEntry.cshtml");
        }

        public ActionResult Cancel_Voucher(string VoucherNo, string RefVoucherNo, string Reason, string VchType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Cancel_Voucher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", VchType);
            cmd.Parameters.AddWithValue("@VoucherNo", VoucherNo == "" ? DBNull.Value : (object)VoucherNo);
            cmd.Parameters.AddWithValue("@RefVoucherNo", RefVoucherNo == "" ? DBNull.Value : (object)RefVoucherNo);
            cmd.Parameters.AddWithValue("@Reason", Reason == "" ? DBNull.Value : (object)Reason);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                int result = Convert.ToInt32(dt.Rows[0]["Result"].ToString());

                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load_VoucherNo(string VoucherNo, string FromDate, string ToDate, string SectorID)
        {
            try
            {
                List<VoucherMaster_VM> lstVoucherDetails = new VoucherMaster_BLL().Load_VoucherNo_BT(VoucherNo, FromDate, ToDate, SectorID, sData.UserID);
                cr.Data = lstVoucherDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstVoucherDetails.Count);
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}