﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class PaperTktIssueController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public PaperTktIssueController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/PaperTktIssue.cshtml");
        }

        [HttpPost]
        public ActionResult IssueType(string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTktIssue_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_IssueTo(string TransType, string Desc, string IssueTo, string IssueType, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTktIssue_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@IssueType", IssueType);
            cmd.Parameters.AddWithValue("@IssueTo", IssueTo);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_IssueDetail(string TransType, string IssueID,  string SectorID, string FinYear, string CON)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PaperTktIssue_SP", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@IssueID", IssueID);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@CON", CON);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_Ticket(string TransType, string TransDt, string DenominationId, string TktSeries, string IssueType, 
            string SectorID, string IssuedToSectorIdEmpId, string TktRequisitionWaybillId, string TktSeriesFrom, string TktSeriesTo, string UPDN)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Paper_Tkt_Issue", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@TransDt", TransDt);
            cmd.Parameters.AddWithValue("@DenominationId", DenominationId);
            cmd.Parameters.AddWithValue("@TktSeries", TktSeries);
            cmd.Parameters.AddWithValue("@IssueType", IssueType);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);
            cmd.Parameters.AddWithValue("@IssuedToSectorIdEmpId", IssuedToSectorIdEmpId);
            cmd.Parameters.AddWithValue("@TktRequisitionWaybillId", TktRequisitionWaybillId);
            cmd.Parameters.AddWithValue("@TktFromNo", TktSeriesFrom);
            cmd.Parameters.AddWithValue("@TktToNo", TktSeriesTo);
            cmd.Parameters.AddWithValue("@Updn", UPDN);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(string IssueID, string IssueDate, string Remarks, string IssueType, string IssueTo, string ReferanceNo,  string SectorID, string FinYear, List<IssueDetail> IssueDetail)
        {

            string RIssueID = "", IssueNo = "";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.PaperTktIssue_SP", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", (IssueID == "" || IssueID == null) ? "Insert" : "Update");
                    cmd.Parameters.AddWithValue("@IssueID", IssueID == "" ? DBNull.Value : (object)IssueID);
                    cmd.Parameters.AddWithValue("@IssueDate", IssueDate == "" ? DBNull.Value : (object)IssueDate);
                    cmd.Parameters.AddWithValue("@IssueType", IssueType == "" ? DBNull.Value : (object)IssueType);
                    cmd.Parameters.AddWithValue("@IssueTo", IssueTo == "" ? DBNull.Value : (object)IssueTo);
                    cmd.Parameters.AddWithValue("@ReferanceNo", ReferanceNo == "" ? DBNull.Value : (object)ReferanceNo);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks == "" ? DBNull.Value : (object)Remarks);
                    cmd.Parameters.AddWithValue("@FinYear", FinYear == "" ? DBNull.Value : (object)FinYear);
                    cmd.Parameters.AddWithValue("@SectorId", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        RIssueID = dr["IssueID"].ToString();
                        IssueNo = dr["IssueNo"].ToString();
                    }
                    dr.Close();

                    if (IssueDetail != null)
                    {
                        cmd = new SqlCommand("account.PaperTktIssue_SP", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "Delete");
                        cmd.Parameters.AddWithValue("@IssueID", RIssueID);
                        cmd.ExecuteNonQuery();

                        foreach (var list in IssueDetail)
                        {
                            cmd = new SqlCommand("account.PaperTktIssue_SP", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert_Detail");
                            cmd.Parameters.AddWithValue("@IssueID", RIssueID);
                            cmd.Parameters.AddWithValue("@DenominationID", list.DenominationID == "" ? DBNull.Value : (object)list.DenominationID);
                            cmd.Parameters.AddWithValue("@UPDN", list.UPDN == "" ? DBNull.Value : (object)list.UPDN);
                            cmd.Parameters.AddWithValue("@TicketSeries", list.TicketSeries == "" ? DBNull.Value : (object)list.TicketSeries);
                            cmd.Parameters.AddWithValue("@TicketFrom", list.TicketFrom == "" ? DBNull.Value : (object)list.TicketFrom);
                            cmd.Parameters.AddWithValue("@TicketTo", list.TicketTo == "" ? DBNull.Value : (object)list.TicketTo);
                            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    cr.Data = IssueNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", IssueNo);
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }

    public class IssueDetail
    {
        public string DenominationID { get; set; }
        public string TicketSeries { get; set; }
        public string TicketFrom { get; set; }
        public string TicketTo { get; set; }
        public string UPDN { get; set; }
        
    }
}