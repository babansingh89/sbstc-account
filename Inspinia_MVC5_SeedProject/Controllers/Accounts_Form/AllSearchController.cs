﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class AllSearchController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        public ActionResult Index()
        {
            return View("~/Views/Accounts/AllSearch.cshtml");
        }

        public ActionResult Load_ItemDetail(string From, string To, string AmountFrom, string AmountTo, string AccountCode, string DRCR, string VchType, string AdjType,
                                            string ChequeNo, string Narration, string SectorID, string IncludeUnpaidTransac)
        {
            List<AllSearch_MD> lst = new List<AllSearch_MD>();
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_AllSearch", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@FromDate", From == "" ? DBNull.Value : (object)From);
            cmd.Parameters.AddWithValue("@ToDate", To == "" ? DBNull.Value : (object)To);
            cmd.Parameters.AddWithValue("@AmountFrom", AmountFrom == "" ? 0 : (object)AmountFrom);
            cmd.Parameters.AddWithValue("@AmountTo", AmountTo == "" ? 0 : (object)AmountTo);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            cmd.Parameters.AddWithValue("@DRCR", DRCR == "" ? DBNull.Value : (object)DRCR);
            cmd.Parameters.AddWithValue("@VCH_TYPE", VchType == "" ? DBNull.Value : (object)VchType);
            cmd.Parameters.AddWithValue("@Adj_TYPE", AdjType == "" ? DBNull.Value : (object)AdjType);
            cmd.Parameters.AddWithValue("@ChequeNo", ChequeNo == "" ? DBNull.Value : (object)ChequeNo);
            cmd.Parameters.AddWithValue("@Narration", Narration == "" ? DBNull.Value : (object)Narration);
            cmd.Parameters.AddWithValue("@SectorID", SectorID == "" ? DBNull.Value : (object)SectorID);
            cmd.Parameters.AddWithValue("@IncludeUnpaidTransac", IncludeUnpaidTransac == "" ? DBNull.Value : (object)IncludeUnpaidTransac);
            cmd.Parameters.AddWithValue("@OutputType", "V");

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                lst = CommonMethod.ConvertToList<AllSearch_MD>(dt);

                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Load_AllSearch", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FromDate", master[0]["From"].ToString() == "" ? DBNull.Value : (object)master[0]["From"].ToString());
                    cmd.Parameters.AddWithValue("@ToDate", master[0]["To"].ToString() == "" ? DBNull.Value : (object)master[0]["To"].ToString());
                    cmd.Parameters.AddWithValue("@AmountFrom", master[0]["AmountFrom"].ToString() == "" ? "0" : master[0]["AmountFrom"].ToString());
                    cmd.Parameters.AddWithValue("@AmountTo", master[0]["AmountTo"].ToString() == "" ? "0" : master[0]["AmountTo"].ToString());
                    cmd.Parameters.AddWithValue("@AccountCode", master[0]["AccountCode"].ToString() == "" ? DBNull.Value : (object)master[0]["AccountCode"].ToString());
                    cmd.Parameters.AddWithValue("@DRCR", master[0]["DRCR"].ToString() == "" ? DBNull.Value : (object)master[0]["DRCR"].ToString());
                    cmd.Parameters.AddWithValue("@VCH_TYPE", master[0]["VchType"].ToString() == "" ? DBNull.Value : (object)master[0]["VchType"].ToString());
                    cmd.Parameters.AddWithValue("@Adj_TYPE", master[0]["AdjType"].ToString() == "" ? DBNull.Value : (object)master[0]["AdjType"].ToString());
                    cmd.Parameters.AddWithValue("@ChequeNo", master[0]["ChequeNo"].ToString() == "" ? DBNull.Value : (object)master[0]["ChequeNo"].ToString());
                    cmd.Parameters.AddWithValue("@Narration", master[0]["Narration"].ToString() == "" ? DBNull.Value : (object)master[0]["Narration"].ToString());
                    cmd.Parameters.AddWithValue("@SectorID", master[0]["SectorID"].ToString() == "" ? DBNull.Value : (object)master[0]["SectorID"].ToString());
                    cmd.Parameters.AddWithValue("@IncludeUnpaidTransac", master[0]["IncludeUnpaidTransac"].ToString() == "" ? DBNull.Value : (object)master[0]["IncludeUnpaidTransac"].ToString());
                    cmd.Parameters.AddWithValue("@OutputType", "E");
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    da.Fill(dt);

                    GridView gv = new GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename= Voucher-Payable-Report" + ".xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #region
        public static class CommonMethod
        {
            public static List<T> ConvertToList<T>(DataTable dt)
            {
                var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
                var properties = typeof(T).GetProperties();
                return dt.AsEnumerable().Select(row => {
                    var objT = Activator.CreateInstance<T>();
                    foreach (var pro in properties)
                    {
                        if (columnNames.Contains(pro.Name.ToLower()))
                        {
                            try
                            {
                                pro.SetValue(objT, row[pro.Name]);
                            }
                            catch (Exception ex) { }
                        }
                    }
                    return objT;
                }).ToList();
            }
        }
        #endregion
    }
}