﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using System.IO;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class Advance_AdjustmentController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        int? TempleteID;
        public Advance_AdjustmentController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Adjestment_Adjustment.cshtml");
        }

        public ActionResult CallingForm(int ID)
        {
            try
            {
                List<Miscellaneous_Payment_MD> lst = new Miscellaneous_Payment().Get_PaymentTypeByID(ID);
                foreach (var temid in lst)
                {
                    TempleteID = temid.TempleteID.Equals(null) ? 0 : temid.TempleteID;
                    Session["ReimbursementTypeID"] = temid.TempleteID;
                }
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return PartialView(string.Format("~/Views/Accounts/Miscellaneous_Payment/Payment_CallingForms/Templete_{0}.cshtml", TempleteID));
        }

        [HttpPost]
        public JsonResult Search_EmpNo_EmpName_AutoComplete(string EmpNo_EmpName, string ReimbursementTypeID, string TransType, string EmpType, string SectorID)
        {
            try
            {
                int intorstring;
                int intValue;
                bool myValue = int.TryParse(EmpNo_EmpName, out intValue);
                if (myValue)
                    intorstring = 1;
                else
                    intorstring = 0;

                DataTable dt = new Miscellaneous_Payment().Get_EmpNoAuto_Complete(intorstring, EmpNo_EmpName, ReimbursementTypeID, TransType, EmpType, SectorID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Populate_Adjustment_Details(string EmployeeID, string ReimbursementTypeID, string TransType, string EmpType, string SectorID)
        {
            try
            {
                DataTable dt = new Miscellaneous_Payment().Populate_Adjustment_Details(sData.UserID, EmployeeID, ReimbursementTypeID, TransType, EmpType, SectorID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult AccountDescription_AutoComplete(string Desc, string TransType)
        {
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", Desc);
        

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load(string ReimbursementTypeID, string EmpType, string EmpName, string TransType, string SectorID)
        {
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", EmpName);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Search_AutoComplete(string SearchID, string Desc, string TransType, string ReimbursementTypeID, string EmpType, string SectorID)
        {
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@isEmpNo_EmpName", SearchID);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", Desc);
            cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(string ReimbursementTypeID, string EmployeeID, string BeneficiaryID, string FromDate, string NetAmount, string PaymentType,
            string Remarks, string SectorID, string ReimbursementID, string Emptype)
        {
            string result = "";
            try
            {
                result = new Miscellaneous_Payment().Save(ReimbursementTypeID, EmployeeID, BeneficiaryID, FromDate, FromDate, NetAmount, PaymentType, Remarks, SectorID, ReimbursementID, Emptype, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Save_AdjustmentAmount(string ReimbursementTypeID, string EmployeeID, string BeneficiaryID, string ReimbursementAdvanceID, string FromDate, string ToDate,
            string NetAmount, string ReturnAmount, string PaymentType, string Remarks, string SectorID, string ReimbursementID, string Emptype, List<Deduction> Deduction, string AdjDate)
        {
            int RReimbursementID = 0; string  MemoNo = "" ;
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);//transaction
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", (ReimbursementID == null || ReimbursementID == "") ? "Save" : "Update");
                    cmd.Parameters.AddWithValue("@EmployeeID", (EmployeeID == null || EmployeeID == "") ? DBNull.Value : (object)EmployeeID);
                    cmd.Parameters.AddWithValue("@BeneficiaryID", (BeneficiaryID == null || BeneficiaryID == "") ? DBNull.Value : (object)BeneficiaryID);
                    cmd.Parameters.AddWithValue("@NetAmount", NetAmount == null ? DBNull.Value : (object)NetAmount);
                    cmd.Parameters.AddWithValue("@ReturnAmount", ReturnAmount == null ? DBNull.Value : (object)ReturnAmount);
                    cmd.Parameters.AddWithValue("@ReimbursementTypeID", ReimbursementTypeID == null ? DBNull.Value : (object)ReimbursementTypeID);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID == null ? DBNull.Value : (object)SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    cmd.Parameters.AddWithValue("@Emptype", Emptype == null ? DBNull.Value : (object)Emptype);
                    cmd.Parameters.AddWithValue("@PaymentMode", (PaymentType == null || PaymentType == "") ? DBNull.Value : (object)PaymentType);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks == null ? DBNull.Value : (object)Remarks);
                    cmd.Parameters.AddWithValue("@ReimbursementfID", ReimbursementID == null ? DBNull.Value : (object)ReimbursementID);
                    cmd.Parameters.AddWithValue("@ReimbursementAdvanceID", ReimbursementAdvanceID == null ? DBNull.Value : (object)ReimbursementAdvanceID);
                    cmd.Parameters.AddWithValue("@AdjDate", AdjDate == null ? DBNull.Value : (object)AdjDate);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        RReimbursementID = Convert.ToInt32(dr["ReimbursementID"].ToString());
                    }
                    dr.Close();

                    if (RReimbursementID > 0)
                    {
                        if (Deduction != null)
                        {
                            cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransactionType", "Delete_AdjDetail");
                            cmd.Parameters.AddWithValue("@ReimbursementfID", RReimbursementID);
                            cmd.ExecuteNonQuery();

                            foreach (var list in Deduction)
                            {
                                cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@TransactionType", "AdjDetail");
                                cmd.Parameters.AddWithValue("@ReimbursementfID", RReimbursementID);
                                cmd.Parameters.AddWithValue("@AccCode", list.AccCode);
                                cmd.Parameters.AddWithValue("@NetAmount", list.Amount);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    cmd = new SqlCommand("account.Insert_ReimbursementBill", dbConn, transaction);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "Flag");
                    cmd.Parameters.AddWithValue("@ReimbursementfID", RReimbursementID);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Dashboard(string TransType, string Desc, string FromDate, string ToDate, string SectorID)
        {
            string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@EmpNo_EmpName", Desc == "" ? DBNull.Value : (object)Desc);
            cmd.Parameters.AddWithValue("@FromDate", FromDate == "" ? DBNull.Value : (object)FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate == "" ? DBNull.Value : (object)ToDate);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Advance_Adjustment", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransType", master[0]["TransType"].ToString());
                    cmd.Parameters.AddWithValue("@ReimbursementTypeID", master[0]["ReimbursementTypeID"].ToString());
                    cmd.Parameters.AddWithValue("@EmpType", master[0]["EmpType"].ToString());
                    cmd.Parameters.AddWithValue("@EmpNo_EmpName", master[0]["EmpName"].ToString());
                    cmd.Parameters.AddWithValue("@SectorID", master[0]["SectorID"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);
                    decimal Total = 0; string Position = ""; string SumFieldName = "";
                    string fileName = master[0]["FileName"].ToString() + ".xls";
                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            string saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }


    }

    public class Deduction
    {
        public string AccCode { get; set; }
        public string Amount { get; set; }
    }

}