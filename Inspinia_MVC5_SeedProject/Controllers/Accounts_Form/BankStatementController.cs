﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BankStatementController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public BankStatementController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/BankStatement.cshtml");
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JsonResult BindVendor(string TransType, string AccountCode, string Desc)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                //string userAuthenticationURI = "https://api.techm.co.in/api/v1/ifsc/" + "UTBI0SLK047";
                //if (!string.IsNullOrEmpty(userAuthenticationURI))
                //{
                //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(userAuthenticationURI);
                //    request.Method = "GET";
                //    request.ContentType = "application/json";
                //    WebResponse response = request.GetResponse();
                //    using (var reader = new StreamReader(response.GetResponseStream()))
                //    {
                //        var ApiStatus = reader.ReadToEnd();

                //        var keys = ApiStatus;
                //        if (keys != null)
                //        {
                //            JObject obj = JObject.Parse(keys);
                //            var master = (JObject)obj.SelectToken("data");
                //            string BANK = master["BANK"].ToString();
                //        }

                //    }
                //}

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CloseMemo(string TransType, string StatementID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@StatementID", StatementID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Auto_Bank(string TransactionType, string Desc, string MND, string SetorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_AccountDescByGroupLedger", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@AccountDesc", Desc);
            cmd.Parameters.AddWithValue("@MND", MND);
            cmd.Parameters.AddWithValue("@SectorID", SetorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Insert(string BankAccountCode, string Signature, string MemoNo, string MemoDate, string ChequeNo, string ChequeDate, string Remarks, List<BankStatement_MD> EffectedGL, string SectorID)
        {
            int StatementID = 0; string RMemoNo = "";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            try
            {
                try {
                    cmd = new SqlCommand("account.Get_BankStatement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "Insert");
                    cmd.Parameters.AddWithValue("@BankAccCode", BankAccountCode);
                    cmd.Parameters.AddWithValue("@Signature", Signature);
                    cmd.Parameters.AddWithValue("@MemoNo", MemoNo == "" ? DBNull.Value : (object)MemoNo);
                    cmd.Parameters.AddWithValue("@MemoDate", MemoDate);
                    cmd.Parameters.AddWithValue("@ChequeNo", ChequeNo);
                    cmd.Parameters.AddWithValue("@ChequeDate", ChequeDate);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //RMemoNo = dr["MemoNo"].ToString();
                            StatementID = Convert.ToInt32(dr["StatementID"].ToString());
                        }
                    }
                    dr.Close();

                    if (StatementID >0)
                    {
                        if (EffectedGL != null)
                        {
                            if (EffectedGL.Count > 0)
                            {
                                int i = 0;
                                foreach (var list in EffectedGL)
                                {
                                    i = i + 1;
                                    cmd = new SqlCommand("account.Get_BankStatement", dbConn, transaction);
                                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@TransactionType", "InsertDetail");
                                    cmd.Parameters.AddWithValue("@StatementID", StatementID);
                                    cmd.Parameters.AddWithValue("@SerialNo", i);
                                    cmd.Parameters.AddWithValue("@AccountCode", list.AccountCode);
                                    cmd.Parameters.AddWithValue("@VendorName", list.VendorName);
                                    cmd.Parameters.AddWithValue("@VoucherNo", list.VoucherNo);
                                    cmd.Parameters.AddWithValue("@VoucherDate", list.VoucherDate);
                                    cmd.Parameters.AddWithValue("@Amount", list.Amount);
                                    cmd.Parameters.AddWithValue("@IFSC", list.IFSC);
                                    cmd.Parameters.AddWithValue("@BankName", list.BankName);
                                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    transaction.Commit();
                    cr.Data = StatementID;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Data Found", "success");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }
    }

    public class Api
    {
        public string BANK { get; set; }
    }
}