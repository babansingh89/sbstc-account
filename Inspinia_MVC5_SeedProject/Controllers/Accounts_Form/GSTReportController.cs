﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class GSTReportController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public GSTReportController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/GSTReport.cshtml");
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.GST_Reports", con);
                    cmd.Parameters.AddWithValue("@TransactionType", master[0]["TransType"].ToString());
                    cmd.Parameters.AddWithValue("@SectorId", master[0]["SectorID"].ToString() == "" ? DBNull.Value : (object)master[0]["SectorID"].ToString());
                    cmd.Parameters.AddWithValue("@FromDate", master[0]["FromDate"].ToString() == "" ? DBNull.Value : (object)master[0]["FromDate"].ToString());
                    cmd.Parameters.AddWithValue("@ToDate", master[0]["ToDate"].ToString() == "" ? DBNull.Value : (object)master[0]["ToDate"].ToString());

                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);

                    decimal Total = 0; string Position = ""; string SumFieldName = "";
                    string fileName = master[0]["FileName"].ToString().Trim() + "_" + sData.UserID + ".xls";// + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls";
                    //string fileName = master[0]["FileName"].ToString() + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls";
                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        string saveAsFileName = "";
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                        // using (var exportData = new MemoryStream())
                        // {
                        ////string path = @"E:\BABANEXCEL" + "//" + fileName;
                        //string path = Path.Combine(Server.MapPath("~/GSTReport/"), fileName);
                        ////deleting code starts here
                        //string dfname = Path.Combine(Server.MapPath("~/GSTReport/"));
                        //string[] dfiles = System.IO.Directory.GetFiles(dfname, fileName + ".*");
                        //foreach (string f in dfiles)
                        //{
                        //    System.IO.File.Delete(f);
                        //}

                        // Declare one MemoryStream variable for write file in stream  
                        //workbook.Write(exportData);
                            //Write to file using file stream  
                            //FileStream file = new FileStream(path, FileMode.CreateNew, FileAccess.Write);
                            //exportData.WriteTo(file);
                            //file.Close();
                            //exportData.Close();
                            //System.Diagnostics.Process.Start(path);
                            //ProcessStartInfo startInfo = new ProcessStartInfo();
                            //startInfo.FileName = "EXCEL.EXE";
                            //startInfo.Arguments = path;
                            //Process.Start(startInfo);
                        //}
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }
    }
}