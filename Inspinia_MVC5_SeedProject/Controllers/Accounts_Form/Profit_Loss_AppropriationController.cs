﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class Profit_Loss_AppropriationController : Controller
    {
        // GET: Profit_Loss_Appropriation
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Profit_Loss_AppropriationController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            ViewBag.AccFinYear = sData.CurFinYear;
            return View("~/Views/Accounts/Profit_Loss_Appropriation.cshtml");
        }

        [HttpPost]
        public ActionResult Show_Details(string FromDate, string ToDate, string SectorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Acc_PlAppro", con);
            cmd.Parameters.AddWithValue("@p_FROM_DATE", FromDate);
            cmd.Parameters.AddWithValue("@p_TO_DATE", ToDate);
            cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            cmd.Parameters.AddWithValue("@p_PL", "Y");
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }


    }
}