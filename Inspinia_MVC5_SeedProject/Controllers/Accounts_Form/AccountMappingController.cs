﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Accounts
{
    //[SecuredFilter]
    public class AccountMappingController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SessionData sData;

        ClientJsonResult cr = new ClientJsonResult();
        AccountMappingBLL acMap = new AccountMappingBLL();
        public AccountMappingController()
        {
            sData = SessionContext.SessionData;
        }
        // GET: AccountMapping
        public ActionResult AccountMapping()
        {
            return View("~/Views/Accounts/AccountMapping.cshtml");
        }

        [HttpPost]
        public ActionResult GetAccountMapType() {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;
                var lst = acMap.getAccountMapType();
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetComponentByTypeId(int MapTypeId) {
            try
            {
                var lst = acMap.getComponentByTypeId(MapTypeId);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = "";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAuto(string AccountDescription, string SectorID)
            {
            try
            {
                var lst = acMap.GetAuto(AccountDescription, SectorID);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult AccountMapUpdate(List<AccountMapvalue> AccountMapvalues) {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;
                acMap.InsertAccountMap(AccountMapvalues, SectorID, InsertedBy);

                cr.Data = "";
                cr.Message = "Details are Inserted Successfully !!";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult DeleteMapping(string detailID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.DELETE_FromAnyTable", con);
            cmd.Parameters.AddWithValue("@TableName", "DAT_BudgetAccountMapping");
            cmd.Parameters.AddWithValue("@ColumnName", "DetailID");
            cmd.Parameters.AddWithValue("@PId", detailID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = "success";
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

    }
}